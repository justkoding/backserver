/**
 * 
 */
package authorize;

import be.objectify.deadbolt.core.DeadboltAnalyzer;
import be.objectify.deadbolt.core.models.Permission;
import be.objectify.deadbolt.core.models.Subject;
import be.objectify.deadbolt.java.AbstractDynamicResourceHandler;
import be.objectify.deadbolt.java.DeadboltHandler;
import be.objectify.deadbolt.java.DynamicResourceHandler;
import play.Logger;
import play.mvc.Http;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author admin
 *
 */
public class ICPDynamicResourceHandler implements DynamicResourceHandler {

	private static final Map<String, DynamicResourceHandler> HANDLERS = new HashMap<String, DynamicResourceHandler>();

	static {
		HANDLERS.put("user-management-delete", new AbstractDynamicResourceHandler() {
			public boolean isAllowed(String name, String meta,
					DeadboltHandler deadboltHandler, Http.Context context) {
				// check permission exist first
				boolean allowed = DeadboltAnalyzer.checkPatternEquality(deadboltHandler.getSubject(context), meta);
				if(allowed)
					return allowed;
				
				Http.Request request = context.request();
				String value = getLastSlashValue(request.uri());
				
				Logger.info("param:" + value);
				return false;
			}
		});
	}

	public boolean isAllowed(String name, String meta,
			DeadboltHandler deadboltHandler, Http.Context context) {
		DynamicResourceHandler handler = HANDLERS.get(name);
		boolean result = false;
		if (handler == null) {
			Logger.error("No handler available for " + name);
		} else {
			result = handler.isAllowed(name, meta, deadboltHandler, context);
		}
		return result;
	}

	// only used for Pattern.CUSTOM style
	public boolean checkPermission(final String permissionValue,
			final DeadboltHandler deadboltHandler, final Http.Context ctx) {
		Subject subject = deadboltHandler.getSubject(ctx);

		boolean permissionOk = false;

		if (subject != null) {
			List<? extends Permission> permissions = subject.getPermissions();
			for (Iterator<? extends Permission> iterator = permissions
					.iterator(); !permissionOk && iterator.hasNext();) {
				Permission permission = iterator.next();
				permissionOk = permission.getValue().contains(permissionValue);
			}
		}

		return permissionOk;

	}
	
	protected static String getLastSlashValue(String url) {
		int idx = url.lastIndexOf("/");
		return idx >= 0 ? url.substring(idx + 1) : url;
	}
}
