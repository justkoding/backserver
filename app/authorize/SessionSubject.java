package authorize;

import be.objectify.deadbolt.core.models.Permission;
import be.objectify.deadbolt.core.models.Role;
import be.objectify.deadbolt.core.models.Subject;
import models.User;

import java.util.List;

/**
 * Created by Aaron.Wen on 2016/4/21.
 */
public class SessionSubject implements Subject {
    private User user;

    public SessionSubject(User user) {
        this.user = user;
    }

    @Override
    public List<? extends Role> getRoles() {
        return null;
    }

    @Override
    public List<? extends Permission> getPermissions() {
        return null;
    }

    @Override
    public String getIdentifier() {
        return getId().toString();
    }

    public Long getId() {
        return user.getId();
    }

    public String getName() {
        return user.getName();
    }
}
