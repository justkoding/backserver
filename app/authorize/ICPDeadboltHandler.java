/**
 * 
 */
package authorize;

import be.objectify.deadbolt.core.models.Subject;
import be.objectify.deadbolt.java.AbstractDeadboltHandler;
import be.objectify.deadbolt.java.DynamicResourceHandler;
import common.Constants;
import play.libs.F.Function0;
import play.libs.F.Promise;
import play.mvc.Http;
import play.mvc.Http.Context;
import play.mvc.SimpleResult;
import services.SubjectService;

/**
 * @author liang.song
 *
 */
public class ICPDeadboltHandler extends AbstractDeadboltHandler {

	@Override
	public Subject getSubject(Context context) {
		final String identifer = context.session().get(Constants.USER_IDENTIFIER);

		return SubjectService.getSubject(identifer);
	}

	@Override
	public Promise<SimpleResult> onAuthFailure(Context context, String content) {
		// if no subject, unauthorized
		Subject subject = getSubject(context);
		if(subject == null) {

			return Promise.promise(new Function0<SimpleResult>() {
				@Override
				public SimpleResult apply() {
					return unauthorized("must login to call the api.");
				}
			});
		}
		
		// else forbidden
		final String content2 = content;
		return Promise.promise(new Function0<SimpleResult>() {
			@Override
			public SimpleResult apply() {
				return forbidden("do not have enough priviledge for content: " + content2);
			}
		});
	}
	
	public DynamicResourceHandler getDynamicResourceHandler(Http.Context context)
    {
        return new ICPDynamicResourceHandler();
    }

	@Override
	public Promise<SimpleResult> beforeAuthCheck(Context arg0) {

		return Promise.pure(null);
	}
}
