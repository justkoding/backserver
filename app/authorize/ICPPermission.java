package authorize;

import be.objectify.deadbolt.core.models.Permission;

public class ICPPermission implements Permission{

	private String value;
	
	public ICPPermission(String value) {
		this.value = value;
	}
	
	@Override
	public String getValue() {
		return value;
	}
	
}
