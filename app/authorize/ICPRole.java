package authorize;

import be.objectify.deadbolt.core.models.Role;

/**
 * Created by Aaron.Wen on 2015/11/30.
 */
public class ICPRole implements Role{
    private String name;

    public ICPRole(String name){
        this.name = name;
    }
    @Override
    public String getName() {
        return this.name;
    }
}
