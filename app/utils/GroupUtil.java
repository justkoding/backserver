package utils;

import java.util.*;

public class GroupUtil {
	public static final <T extends Comparable<T> ,D> Map<T ,List<D>> group(List<D> colls ,Index<T> index){
        if(colls == null || colls.isEmpty()) {
            return null ;
        }
        if(index == null) {
            return null ;
        }
        Iterator<D> iter = colls.iterator() ;
        Map<T ,List<D>> map = new HashMap<T, List<D>>() ;
        while(iter.hasNext()) {
            D d = iter.next() ;
            T t = index.getIndex(d) ;
            if(map.containsKey(t)) {
                map.get(t).add(d) ;
            } else {
                List<D> list = new ArrayList<D>() ;
                list.add(d) ;
                map.put(t, list) ;
            }
        }
        return map ;
    } 
	
    public interface Index<T> {
        T getIndex(Object obj) ;
    }
}
