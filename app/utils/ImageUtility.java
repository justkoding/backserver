/**
 * 
 */
package utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.dcm4che3.image.PaletteColorModel;
import org.dcm4che3.imageio.plugins.dcm.DicomImageReadParam;
import play.Logger;
import play.Play;

import javax.imageio.*;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.*;
import java.util.Iterator;

/**
 * @author liang.song
 *
 */
public class ImageUtility {
    public final static String defaultDoctorGroupLocation = Play.application().configuration()
            .getString("default.group.path");
    public final static String defaultAvatarLocation= Play.application().configuration()
            .getString("default.avatar.path");
	public final static String avatarStoragePath = Play.application().configuration()
			.getString("avatar.storage.path");
	public final static String snapshotStoragePath = Play.application().configuration()
			.getString("snapshot.storage.path");
	
	public static String getImageFormatName(Object o) {
		try {
			ImageInputStream iis = ImageIO.createImageInputStream(o);
			Iterator<ImageReader> iter = ImageIO.getImageReaders(iis);
			if (!iter.hasNext()) {
				return null;
			}
			ImageReader reader = (ImageReader) iter.next();
			iis.close();

			return reader.getFormatName();
		} catch (IOException e) {
		}
		return null;
	}
	
	public static BufferedImage fillTransparentPixels(BufferedImage image,
			Color fillColor) {
		int w = image.getWidth();
		int h = image.getHeight();
		BufferedImage image2 = new BufferedImage(w, h,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D g = image2.createGraphics();
		g.setColor(fillColor);
		g.fillRect(0, 0, w, h);
		g.drawRenderedImage(image, null);
		g.dispose();
		return image2;
	}
	
	public static void dcm2png(File src, File dest) throws IOException {
		new DcmToPng().convert(src, dest);
	}
	
	protected static void convertImage(File inputfile, String new_file, int new_width,
			int new_height) throws Exception{
			BufferedImage src = ImageIO.read(inputfile);

			if (src.getColorModel().getTransparency() != Transparency.OPAQUE) {
				src = ImageUtility.fillTransparentPixels(src,
						Color.WHITE);
			}
			
			String suffix = StringUtils.substringAfterLast(new_file , ".");
			
			BufferedImage bufImg = new BufferedImage(new_width, new_height,
					BufferedImage.TYPE_INT_RGB);

			Graphics g = bufImg.getGraphics();
			g.drawImage(src, 0, 0, new_width, new_height, null);
			g.dispose();
			ImageIO.write(bufImg, suffix, new File(new_file));
	}

    public static class DoctorGroupIconHandle{
        BufferedImage groupbg;
        Graphics2D g2d;
        Integer bgwidth;
        Integer bgheight;
        Integer total;
        Integer idx;
        Integer gap;
        int x = 0, y = 0, width = 0, height = 0, maxrows = 3, maxcolumns = 3;

        public DoctorGroupIconHandle(BufferedImage groupbg, Graphics2D g2d, Integer
                total) throws Exception{
            this.groupbg = groupbg;
            this.g2d = g2d;
            this.bgwidth = groupbg.getWidth();
            this.bgheight = groupbg.getHeight();
            this.total = total;
            init();
        }

        void init() throws Exception{
            this.idx = 0;
            this.gap = 5;
            BufferedImage avatar = ImageIO.read(Play.application().getFile(defaultAvatarLocation));
            if (total > 9){
                total = 9;
            }
            int rows = (int)Math.ceil(Float.intBitsToFloat(total)/Float.intBitsToFloat(maxrows));

            int columns = total > maxcolumns ? maxcolumns : total;
            Logger.debug("total  {} <==> rows {}", total, rows);

            if (rows > columns){
                height =  (bgheight - (rows - 1)*gap)/rows;
                width = avatar.getWidth() * height / avatar.getHeight();
            } else {
                width = (bgwidth - (columns - 1)*gap)/columns;
                height = avatar.getHeight() * width / avatar.getWidth();
            }

            y = (bgwidth - width * rows - (rows - 1) * gap ) / 2;
            x = (bgheight - height * columns - (columns - 1) * gap)/2;
        }

        public void appendAvatar(final String avatarPath){
            try {
                BufferedImage avatar = ImageIO.read(Play.application().getFile(defaultAvatarLocation));

                if (!StringUtils.isBlank(avatarPath)){
                    File avatarFile = new File(ImageUtility.avatarStoragePath + avatarPath);
                    if (avatarFile.exists()){
                        avatar = ImageIO.read(avatarFile);
                    }
                }
                final int point_x = (idx%maxcolumns)*(width + gap) + x;
                final int point_y = (idx/maxrows)*(height + gap) + y;
                g2d.drawImage(avatar, point_x, point_y, width, height, null);

                if (idx == total){
                    g2d.dispose();
                } else {
                    idx++;
                }
            } catch (IOException e) {
                Logger.error("append avatar to DoctorGroupIcon failed!", e);
            }
        }

        public String output(){
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                ImageIO.write(groupbg, "png", baos);
                byte[] imageBytes = baos.toByteArray();
                String imgstring = new String(Base64.encodeBase64(imageBytes));
                System.out.println("<img src='data:img/jpg;base64," + imgstring + "'/>");
                baos.close();
                return imgstring;
            } catch (IOException e) {
                Logger.error("output DoctorGroupIcon failed!", e);
                return null;
            }
        }
    }

    public static DoctorGroupIconHandle getDoctorGroupIconHandle(int total){
        try {
            BufferedImage groupbg = ImageIO.read(Play.application().getFile(defaultDoctorGroupLocation));
            Graphics2D g = groupbg.createGraphics();
            DoctorGroupIconHandle dgbg = new DoctorGroupIconHandle(groupbg, g, total);
            return dgbg;
        } catch (Exception e) {
            Logger.error("get DoctorGroupIconHandle failed!", e);
            return null;
        }
    }

    public static String convertAvatar(File inputfile,String prefix,String contentType){
		try{
            BufferedImage defaultAvatar = ImageIO.read(Play.application().getFile(defaultAvatarLocation));
		    final int MaxWidth = defaultAvatar.getWidth();
		    final int MaxHeight = defaultAvatar.getHeight();
			File directory = new File(avatarStoragePath);
			if (!directory.exists()){
				directory.mkdirs();
			}
			String path = getRelativePathByType(prefix, contentType);
			String dest_file = avatarStoragePath + path;
			convertImage(inputfile , dest_file  , MaxWidth , MaxHeight);
			return path;
		} catch (Exception e) {
			Logger.error("Error! Convert image : " , e);
			return "";
		}
	}
	
	public static boolean removeAvatar(String path){
		String dest_file = avatarStoragePath + path;
		File old_file = new File(dest_file);
		if (old_file.exists()){
			return old_file.delete();
		}
		return true;
	}
	
	public static String uploadSnapshot(File inputfile,String prefix,String contentType){
		try{
			File directory = new File(snapshotStoragePath);
			if (!directory.exists()){
				directory.mkdirs();
			}
			String path = getRelativePathByType(prefix, contentType);
			String dest_file = snapshotStoragePath + path;
			OutputStream out = new FileOutputStream(dest_file);
			java.nio.file.Files.copy(inputfile.toPath(), out);
			out.close();
			return path;
		} catch (Exception e) {
			Logger.error("Error! Convert image : " , e);
			return "";
		}
	}
	
	private static String getRelativePathByType(String prefix,String contentType){
		String[] suffixs = {"png","jpg","jpeg","gif","pdf"};
		String path = prefix + "." + suffixs[0];//default
		for (String suffix : suffixs){
			if (StringUtils.endsWithIgnoreCase(contentType, suffix)){
				path = prefix + "." + suffix; 
			}
		}
		return path;
	}
}

class DcmToPng {

    private final ImageReader imageReader =
            ImageIO.getImageReadersByFormatName("DICOM").next();
    
    private ImageWriter imageWriter = 
    		ImageIO.getImageWritersByFormatName("PNG").next();
    
    public static void execute(File src, File dest) throws IOException {
    	new DcmToPng().convert(src, dest);
    }
    
	public void convert(File src, File dest) throws IOException {
		ImageInputStream iis = ImageIO.createImageInputStream(src);
		try {
			BufferedImage bi = readImage(iis);
			bi = convert(bi);
			dest.delete();
			ImageOutputStream ios = ImageIO.createImageOutputStream(dest);
			try {
				writeImage(ios, bi);
			} finally {
				try {
					ios.close();
				} catch (IOException ignore) {
				}
			}
		} finally {
			try {
				iis.close();
			} catch (IOException ignore) {
			}
		}
	}
	
    private BufferedImage readImage(ImageInputStream iis) throws IOException {
        imageReader.setInput(iis);
        return imageReader.read(0, readParam());
    }
    
    private ImageReadParam readParam() {
        DicomImageReadParam param =
                (DicomImageReadParam) imageReader.getDefaultReadParam();
//        param.setWindowCenter(windowCenter);
//        param.setWindowWidth(windowWidth);
//        param.setAutoWindowing(autoWindowing);
//        param.setWindowIndex(windowIndex);
//        param.setVOILUTIndex(voiLUTIndex);
//        param.setPreferWindow(preferWindow);
//        param.setPresentationState(prState);
//        param.setOverlayActivationMask(overlayActivationMask);
//        param.setOverlayGrayscaleValue(overlayGrayscaleValue);
        return param;
    }
    
    private BufferedImage convert(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        if (cm instanceof PaletteColorModel)
            return ((PaletteColorModel) cm).convertToIntDiscrete(bi.getData());
        return bi;
    }
    
    private void writeImage(ImageOutputStream ios, BufferedImage bi)
            throws IOException {
        imageWriter.setOutput(ios);
        imageWriter.write(new IIOImage(bi, null, null));
    }

}

