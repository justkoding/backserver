/**
 * 
 */
package utils;

/**
 * @author liang.song
 *
 */
public class PaginationUtil {
	public static final Integer defaultPage = 1;
	public static final Integer defaultPageSize = 20;

	static public int getDefaultPage(int page) {
		if(page <= defaultPage)
			page = defaultPage;
		
		return page;
	}
	
	static public  int getDefaultPageSize(int pageSize) {
		if (pageSize < 0)
			return 0;
		if (pageSize == 0)
			return defaultPageSize;

		return pageSize;
	}
}
