package utils;

import com.mysql.jdbc.Driver;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import play.Logger;
import play.db.jpa.JPA;
import play.libs.Yaml;

import java.io.File;
import java.sql.*;
import java.util.*;
import java.util.Map.Entry;

public class DBUtil {
	private static final Config appConfig = ConfigFactory.parseFile(new File("conf/application.conf"))
			.resolve();
	
	public static Map<String, String> createSettings(final String url, final String dbname){
		Config appConfig = ConfigFactory.parseFile(new File("conf/application.conf"))
				.resolve();
		
		Map<String, String> settings = new HashMap<String, String>();

		settings.put("db.default.url",url+dbname+"?characterEncoding=UTF-8&sessionVariables=storage_engine=InnoDB");
		
		settings.put("db.default.user",
				appConfig.getString("db.default.user"));
		settings.put("db.default.pass",
				appConfig.getString("db.default.pass"));
		settings.put("db.default.jndiName", "DefaultDS"); // make connection
															// available to
															// dbunit
															// through JNDI
		settings.put("db.default.driver", "com.mysql.jdbc.Driver");
		settings.put("jpa.default", "defaultPersistenceUnit");
		settings.put("enableBackend", "false");
		
		return settings;
	}
	
	
	public static void initDB(final String url,final String dbname) throws Exception {
		Properties props = loadProperties();

		// We need to register driver every time this way because Play DBPlugin
		// deregisters all drivers in onStop()
		// and Class.forName("org.postgresql.Driver") registers the driver only
		// the first time (when the class is loaded)
		Driver driver = new Driver();
		DriverManager.registerDriver(driver);

		createDb(props,url,dbname);

		// We remove the driver so that we don't collide with Play's own driver
		// management
		DriverManager.deregisterDriver(driver);
	}
	
	private static Properties loadProperties(){
		Properties props = new Properties();
		
		props.setProperty("user", appConfig.getString("db.default.user"));
		props.setProperty("password", appConfig.getString("db.default.pass"));
		return props;
	}
	
	
	private static void createDb(final Properties props,final String url,final String dbname) throws SQLException {

		try (Connection templateConnection = DriverManager.getConnection(url,
				props)) {
			ResultSet dbExistsResult = templateConnection.prepareStatement(
					"SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '"
							+ dbname + "'").executeQuery();
			if (dbExistsResult.next())
				cleanDb(props,url,dbname);

			templateConnection
					.prepareStatement(
							"create database "
									+ dbname
									+ " DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;")
					.execute();
		}
	}
	
	private static void cleanDb(final Properties props,final String url,final String dbname) throws SQLException {
		try (Connection connection = DriverManager.getConnection(url, props)) {
			// This statement removes all the data in from database
			connection.prepareStatement("drop database " + dbname)
					.execute();
		}
	}
	
	public static void truncateTable(final String url, final String dbname) {
		Properties props = loadProperties();
		try (Connection templateConnection = DriverManager.getConnection(url, props)) {
			ResultSet tablenameSet = templateConnection.prepareStatement("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '" 
					+ dbname+"' ").executeQuery();
			
			if(tablenameSet!=null){
				while(tablenameSet.next()){
					Statement s = templateConnection.createStatement();
					s.addBatch("USE "+ dbname);
					s.addBatch("SET FOREIGN_KEY_CHECKS=0");
					s.addBatch("TRUNCATE TABLE "+tablenameSet.getString(1));
					s.addBatch("SET FOREIGN_KEY_CHECKS = 1");
					s.executeBatch();
					s.close();
				}
			}
		} catch (Exception e) {
			Logger.error("init database data error!{}", e.getMessage());
		}
		
	}

    public static void recreateDB(final String url, final String dbname) {

    }
	
	public static void insertData(final String ymlfilename) {

		JPA.withTransaction(new play.libs.F.Callback0() {
			@SuppressWarnings("unchecked")
			@Override
			public void invoke() throws Throwable {
				Map<String, List<Object>> yamlMap;

				yamlMap = (Map<String, List<Object>>) Yaml
						.load(ymlfilename);

				Set<Entry<String, List<Object>>> set = yamlMap.entrySet();
				// Get an iterator
				Iterator<Entry<String, List<Object>>> i = set.iterator();
				// Display elements
				while (i.hasNext()) {
					Entry<String, List<Object>> me = i.next();
					writeDataToDb(me.getKey(), me.getValue());
				}

			}
		});
	}

	private static void writeDataToDb(String modelName,
			List<Object> objectList) throws ClassNotFoundException {

		if (0 == (Long) JPA.em()
				.createQuery("select count(e) from " + modelName + "  e")
				.getSingleResult()) {

			for (Object obj : objectList) {
				Class<?> cls = Class.forName("models." + modelName);

				JPA.em().persist(cls.cast(obj));
			}
		}
	}
}
