package utils;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GenericUtil {
	public static final String WHOLE_TIME = "yyyy-MM-dd HH:mm:ss";
	public static final String FULL_DATE = "yyyy-MM-dd";
	public static final String FULL_TIME = "HH:mm:ss";
	public static final String WHOLE_TIME_NO_SECOND = "yyyy-MM-dd HH:mm";
	public static final String FULL_TIME_NO_SECOND = "HH:mm";
	public static final String SHORT_DATE_NO_DASH = "yyMM";
	public static final String WHOLE_TIME_UTC = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static final String CONCISE_TIME = "yyyyMMddHHmm";

	public static boolean isBlank(String str){
		if (str == null){
			return true;
		}
		if (str.trim().length() == 0){
			return true;
		}
		return false;
	}
	
	public static String getDateDesc(String format , Date date){
		DateFormat df = new SimpleDateFormat(format);
		if (date == null){
			return null;
		}
		return df.format(date);
	}
	
	public static String removeExtension(String fileName) {
		int pos = fileName.lastIndexOf(".");
		if (pos > 0) {
			fileName = fileName.substring(0, pos);
		}
		
		return fileName;
	}
	
	public static Long getFileSize(String file) {
		return new File(file).length();
	}

}
