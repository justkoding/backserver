package dao;

import models.DepositDetail;
import models.User;
import play.db.jpa.JPA;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Aaron.Wen on 2016/4/21.
 */
public class DepositDetailDAO extends AbstractDAO<DepositDetail> {
    public List<DepositDetail> findDetails(User user) {
        EntityManager em = JPA.em();
        Query query = em
                .createQuery(
                        "select d from DepositDetail d where d.user.id = :USERID order by d.createTime desc ")
                .setParameter("USERID", user.getId());


        return query.getResultList();
    }
}
