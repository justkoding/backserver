/**
 *
 */
package dao;

import models.User;
import play.db.jpa.JPA;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * @author admin
 *
 */
public class UserDAO extends AbstractDAO<User> {

	public User findUser(String name, String password) {
		EntityManager em = JPA.em();
		Query query = em
				.createQuery(
						"select u from User u where UPPER(name)=:NAME and password=:PASSWORD")
				.setParameter("NAME", name.trim().toUpperCase())
				.setParameter("PASSWORD", password);

		@SuppressWarnings("unchecked")
		List<User> users = query.getResultList();
		if (users.size() > 0) {
			return users.get(0);
		} else
			return null;
	}
}
