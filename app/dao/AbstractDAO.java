package dao;

import play.db.jpa.JPA;

import javax.persistence.EntityManager;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class AbstractDAO<T> {

	private Class<T> entityClass;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		Type genType = getClass().getGenericSuperclass();
		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
		entityClass = (Class<T>) params[0];
	}
	
	public T findById(long id) {
		return JPA.em().find(entityClass, id);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		EntityManager em = JPA.em();
		return em.createQuery("select e from " + entityClass.getName() + " e").getResultList();
	}

	public Long count() {
		EntityManager em = JPA.em();
		return ((Number)em.createQuery("select count(e) from " + entityClass.getName() + " e").getSingleResult()).longValue();
	}
	
	public int deleteById(long id) {
		EntityManager em = JPA.em();
        return em.createQuery("delete from " + entityClass.getName() + " where id = :ID ").setParameter("ID", id).executeUpdate();
	}

	public void save(T value) {
		EntityManager em = JPA.em();
		em.persist(value);
	}

	public void save(Collection<T> values) {
		if (null != values){
			Iterator<T> iterator = values.iterator();
			while (iterator.hasNext()){
				save(iterator.next());
			}
		}
	}
	
	public T merge(T value) {
		EntityManager em = JPA.em();
		return em.merge(value);
	}

	public void delete(T value) {
		EntityManager em = JPA.em();
		em.remove(value);
	}
	
	public <E> void delete(Collection<E> values){
		EntityManager em = JPA.em();
		for(E value : values){
			em.remove(value);
		}
	}
}
