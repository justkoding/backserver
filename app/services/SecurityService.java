/**
 * 
 */
package services;

import dao.UserDAO;
import global.Environment;
import models.User;
import utils.SecurityUtils;

/**
 * @author liang.song
 *
 */
public class SecurityService {
	
	public static User authenticate(final String name, String passwd) {
		String passwdHash = SecurityUtils.getSHASecurePassword(passwd);
		return new UserDAO().findUser(name, passwdHash);
	}

	public static String getDefaultPassword() {
		return SecurityUtils.getSHASecurePassword(Environment.defaultUserPassword);
	}
}
