package services;

import be.objectify.deadbolt.core.models.Subject;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Aaron.Wen on 2016/4/21.
 */
public class SubjectService {
    private static Map<String, Subject> activators
            = new ConcurrentHashMap<String, Subject>();


    public static Subject getSubject(final String identifier) {
        return activators.get(identifier);
    }

    public static void addSubject(Subject subject) {
        if (!alreadyExist(subject.getIdentifier())) {
            activators.put(subject.getIdentifier(), subject);
        }
    }

    public static void removeSubject(final String identifier) {
        activators.remove(identifier);
    }

    private static boolean alreadyExist(final String identifier) {
        return activators.containsKey(identifier);
    }
}
