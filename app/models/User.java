package models;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "t_user_info")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "password")
	private String password;

	@Column(name = "create_time")
	@Temporal(TemporalType.DATE)
	private Date createTime;

	@OneToMany(mappedBy = "user", targetEntity = DepositDetail.class)
	private Set<DepositDetail> details = new HashSet<DepositDetail>();

	public User() {
	}

	public User(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Transient
	public Double getBalance() {
		double balance = 0d;
		for (DepositDetail detail :
				details) {
			balance += detail.getAmount();
		}
		return balance;
	}

	public Set<DepositDetail> getDetails() {
		return details;
	}

	public void setDetails(Set<DepositDetail> details) {
		this.details = details;
	}
}
