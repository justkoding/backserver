package models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Aaron.Wen on 2016/4/21.
 */
@Entity
@Table(name = "t_deposit_detail")
public class DepositDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name = "amount")
    private Double amount = 0d;

    @Column(name="create_time")
    @Temporal(TemporalType.DATE)
    private Date createTime;

    public DepositDetail() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
