package controllers;

import dao.DepositDetailDAO;
import dao.UserDAO;
import models.DepositDetail;
import models.User;
import org.joda.time.DateTime;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Result;
import services.SecurityService;
import views.html.dashboard;
import views.html.login;

import java.util.List;

import static play.data.Form.form;

public class Application extends BaseController {

    // -- Authentication
    
    public static class Login {
        public String username;
        public String password;
        
        public String validate() {
            if(SecurityService.authenticate(username, password) == null) {
                return "Invalid user or password";
            }
            return null;
        }
        
    }

    /**
     * Login page.
     */
    public static Result login() {
        return ok(
            login.render(form(Login.class))
        );
    }
    
    /**
     * Handle login form submission.
     */
    @Transactional
    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        if(loginForm.hasErrors()) {
            return badRequest(login.render(loginForm));
        } else {
            User user = SecurityService.authenticate(loginForm.get().username, loginForm.get().password);
            List<DepositDetail> details = new DepositDetailDAO().findDetails(user);
            addSession(user);
            return ok(views.html.dashboard.render(user, details, form(Deposit.class)));
        }
    }

    public static class Deposit{
        public Double amount;
    }

    @Transactional
    public static Result deposit() {
        Form<Deposit> depositForm = form(Deposit.class).bindFromRequest();
        User user = new UserDAO().findById(Long.parseLong(getSubject().getIdentifier()));
        List<DepositDetail> details = new DepositDetailDAO().findDetails(user);

        if(depositForm.hasErrors()) {
            return badRequest(dashboard.render(user, details, form(Deposit.class)));
        } else {
            final Double amount = depositForm.get().amount;
            deposit(user, amount);
            details = new DepositDetailDAO().findDetails(user);
            return ok(views.html.dashboard.render(user, details, form(Deposit.class)));
        }
    }

    private static void deposit(User user, double amount){
        DepositDetail record = new DepositDetail();
        record.setUser(user);
        record.setAmount(amount);
        record.setCreateTime(DateTime.now().toDate());
        new DepositDetailDAO().save(record);
    }

    /**
     * Logout and clean the session.
     */
    public static Result logout() {
        session().clear();
        flash("success", "You've been logged out");
        return redirect(
            routes.Application.login()
        );
    }

}
