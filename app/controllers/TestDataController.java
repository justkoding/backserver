/**
 * 
 */
package controllers;

import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import utils.DBUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Shujuan.Wang
 *
 */
public class TestDataController extends Controller {
	private static final Map<String,String> YMLMAP = new HashMap<String,String>(){
		private static final long serialVersionUID = -3550554303239615903L;
	{
		put("initial","initial-data.yml");
		put("test","unit-test-data.yml");
	}};
	
	@Transactional
	public static Result insertTestData(final String type) {
		try{
			if (YMLMAP.containsKey(type)){
				initDB();
				DBUtil.insertData(YMLMAP.get(type));
			}
			return ok(views.html.testdata.render(type + " data insert successfully"));
		}catch(Exception e){
			Logger.error("insert application data failed! ",e);
			return ok(views.html.testdata.render(type + " data insert failed"));
		}
	}
	
	private static void initDB() throws Exception{
		final String fullurl = Play.application().configuration()
				.getString("db.default.url");
		if (!StringUtils.isBlank(fullurl)){
			String address = StringUtils.substring(fullurl,0,
					StringUtils.lastIndexOf(fullurl, "/")) +"/";
			String dbname = StringUtils.substring(fullurl,
					StringUtils.lastIndexOf(fullurl, "/") + 1,StringUtils.indexOf(fullurl,"?"));
//			DBUtil.initDB(address, dbname);
			DBUtil.truncateTable(address, dbname);
		}
	}
}

