package controllers;

import authorize.SessionSubject;
import be.objectify.deadbolt.core.models.Subject;
import common.Constants;
import models.User;
import play.mvc.Controller;
import services.SubjectService;

public class BaseController extends Controller {

	protected static Subject getSubject(){
		return SubjectService.getSubject(session().get(Constants.USER_IDENTIFIER));
	}

	protected static void addSession(User user){
		session().put(Constants.USER_IDENTIFIER, user.getId().toString());
		SubjectService.addSubject(new SessionSubject(user));
	}

	protected static void removeSession(User user) {
		SubjectService.removeSubject(user.getId().toString());
		session().remove(Constants.USER_IDENTIFIER);
	}
}
