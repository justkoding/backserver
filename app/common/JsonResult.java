/**
 * 
 */
package common;

/**
 * @author admin
 *
 */
public class JsonResult {
	
	public static final String OK	= "OK";
	public static final String FAILED	= "FAILED";
	public static final String LOGIN_NO_USER_OR_PASSWORD 	= "NO_USER_OR_PASSWORD";
	public static final String WEB_SOCKET_NOT_CONNECTED 	= "WEB_SOCKET_NOT_CONNECTED";
	public static final String MDT_NOT_STARTED = "MDT_NOT_STARTED";
	public static final String JSON_PARSE_FAILED = "JSON_PARSE_FAILED";
	public static final String RECORD_NOT_EXIST = "RECORD_NOT_EXIST";
	public static final String RECORD_ALREADY_EXIST = "RECORD_ALREADY_EXIST";
	public static final String DUPLICATE_RECORD = "DUPLICATE_RECORD";
	public static final String FORBIDDEN_FOR_SYSTEM_RECORD = "FORBIDDEN_FOR_SYSTEM_RECORD";
	public static final String USER_ALREADY_EXISTS = "USER_ALREADY_EXISTS";
	public static final String ERROR_DATA = "ERROR_DATA";
	public static final String ERROR_STATUS = "ERROR_STATUS";
	public static final String INAVAILABLE_ATTENDEE = "INAVAILABLE_ATTENDEE";
	public static final String MUST_BE_HOST = "MUST_BE_HOST";
	public static final String ALREADY_BE_HOST = "ALREADY_BE_HOST";
	public static final String MDT_ALREADY_ONGOING = "MDT_ALREADY_ONGOING";
	public static final String ALREADY_REMOVED = "ALREADY_REMOVED";
	public static final String NEED_JOIN = "NEED_JOIN";
	public static final String NO_ACCOUNT = "NO_ACCOUNT";
	public static final String NO_CONTENT = "NO_CONTENT";
	public static final String NO_DOCUMENT = "NO_DOCUMENT";
	public static final String RECORD_NOT_FOUND = "RECORD_NOT_FOUND";
	public static final String BAD_REQUEST = "BAD_REQUEST";
	public static final String REQUEST_ACCEPTED = "REQUEST_ACCEPTED";
	public static final String REQUEST_DENIED = "REQUEST_DENIED";
	public static final String IN_DISCUSSING = "IN_DISCUSSING";
	public static final String IN_QUEUING = "IN_QUEUING";
	public static final String IN_MEETING = "IN_MEETING";
	public static final String STATUS_FINISHED = "STATUS_FINISHED";
	public static final String NO_DOCTOR = "NO_DOCTOR";
	public static final String INCORRECT_IDNUM = "INCORRECT_IDNUM";
	public static final String IN_PROCESSING = "IN_PROCESSING";

	protected String result;

	public JsonResult() {
		result = OK;
	}
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
	public static JsonResult success() {
		return error(OK);
	}
	
	public static JsonResult error(String errorMsg) {
		JsonResult result = new JsonResult();
		result.setResult(errorMsg);
		return result;
	}
}
