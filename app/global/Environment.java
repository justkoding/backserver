/**
 * 
 */
package global;

import akka.actor.*;
import akka.pattern.AskableActorSelection;
import akka.util.Timeout;
import play.Logger;
import play.Play;
import play.libs.Akka;
import scala.concurrent.Await;
import scala.concurrent.Future;

import java.util.concurrent.TimeUnit;

/**
 * @author admin
 *
 */
public class Environment {

	private static ActorSystem actorSystem;
	
	// Global configuration preload
	public static String defaultUserPassword;


	public static ActorSystem akkaSystem() {
		return actorSystem;
	}
	
	public static void init() {
		defaultUserPassword = Play.application().configuration().getString("defaultUserPassword");
	}

	public static void createAkkaSystem() {
		//actorSystem = ActorSystem.create("master", ConfigFactory.load().getConfig("master"));
		actorSystem = Akka.system();
	}
	
	public static ActorRef getActorwithPath(String path) {
		ActorSystem actorSystem = Environment.akkaSystem();
    	ActorSelection myActor =
                actorSystem.actorSelection(path);
    	
    	AskableActorSelection asker = new AskableActorSelection(myActor);
    	Timeout timeout = new Timeout(5,
    	        TimeUnit.SECONDS);
    	Future<Object> future = asker.ask(new Identify(1), timeout);
    	ActorIdentity identity;
		try {
			identity = (ActorIdentity) Await.result(future, timeout.duration());
			
			ActorRef reference = identity.getRef();
	    	if(reference == null)
	    		Logger.warn("Can not get the actor with path: " + path);
	    	
	    	return reference;
		} catch (Exception e) {
			Logger.error("Failed to get actor: " + e.getMessage());
			return null;
		}
	}
}
