import akka.actor.ActorRef;
import akka.actor.Props;
import global.Environment;
import play.*;
import play.libs.Akka;
import scala.concurrent.duration.Duration;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Global extends GlobalSettings {

	public void onStart(Application app) {

		Environment.init();
		
		boolean enableBackend = Play.application().configuration().getBoolean("enableBackend");
		if(enableBackend) {
			Environment.createAkkaSystem();
			startBackendServices();
		}
		
		try {
			addLibraryPath(Play.application().path().getAbsolutePath() + "/lib");
		} catch (Exception e) {
			Logger.info("Failed to add 'lib' to JAVA.LIBRARY.PATH");
		}

	}

	public void startBackendServices() {

		List<Configuration> configs = Play.application().configuration()
				.getConfigList("backends");
		if (configs == null)
			return;

		for (Configuration config : configs) {

			try {
				Class<?> clazz = Class.forName(config.getString("class"));
				Logger.info("start backend service: " + clazz.getSimpleName());
				ActorRef actor = Environment.akkaSystem().actorOf(
						Props.create(clazz), clazz.getSimpleName());
				Integer interval = config.getInt("interval");
				if(interval != null) {
					Environment.akkaSystem().scheduler().schedule(Duration.create(5, TimeUnit.SECONDS),
			        		Duration.create(interval, TimeUnit.SECONDS),
			        		actor, "",  Akka.system().dispatcher(),
			                null);
					Logger.info("interval: " + interval + "s, Path: " + actor.path());
				} else
					Logger.info("Path: " + actor.path());

			} catch (ClassNotFoundException e) {
				Logger.error("Can not find class: " + e.getMessage() + ", will skip this service.");
			}
		}
	}

	/**
	 * Adds the specified path to the java library path
	 *
	 * @param pathToAdd
	 *            the path to add
	 * @throws Exception
	 */
	public static void addLibraryPath(String pathToAdd) throws Exception {
		final Field usrPathsField = ClassLoader.class
				.getDeclaredField("usr_paths");
		usrPathsField.setAccessible(true);

		// get array of paths
		final String[] paths = (String[]) usrPathsField.get(null);

		// check if the path to add is already present
		for (String path : paths) {
			if (path.equals(pathToAdd)) {
				return;
			}
		}

		// add the new path
		final String[] newPaths = Arrays.copyOf(paths, paths.length + 1);
		newPaths[newPaths.length - 1] = pathToAdd;
		usrPathsField.set(null, newPaths);
	}

}