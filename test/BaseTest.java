import org.junit.After;
import org.junit.Before;
import play.test.FakeApplication;
import utils.DBUtil;

import java.util.Map;

import static play.test.Helpers.fakeApplication;

/**
 * 
 */

/**
 * @author Liang.Song
 *
 */
public abstract class BaseTest {

	protected final String tokenString = "085a6fba94c34acf8073280a9a54848f";//accountid 1 zhang san
	protected final static String TEST_DB_NAME = "nontest";
	protected final static String URL = "jdbc:mysql://localhost/";

	protected static FakeApplication app = null;
	
	protected static void createFakeApp() throws Exception {
		DBUtil.initDB(URL, TEST_DB_NAME);
		
		Map<String, String> settings = DBUtil.createSettings(URL, TEST_DB_NAME);
		
		app = fakeApplication(settings);
	}
	
	@Before
	public void beforeTest() throws Exception{
		DBUtil.truncateTable(URL, TEST_DB_NAME);
		DBUtil.insertData("unit-test-data.yml");
	}
	
	@After
	public void afterTest() {
		
	}
}


