import org.junit.AfterClass;
import org.junit.BeforeClass;
import play.test.Helpers;

/**
 * 
 */

public abstract class BaseDBTest extends BaseTest {

	protected static int dbAppCount = 0;
	

	@BeforeClass
	public static void beforeClassTest() throws Exception{		
		if (dbAppCount == 0) {
			try {
				createFakeApp();
				
				Helpers.start(app);
				
				System.out.println("=========================================BeforeTest");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		
		dbAppCount++;		
	}	
	
	@AfterClass
	public static void afterClassTest() {
		dbAppCount--;		
		if(dbAppCount == 0) {
			Helpers.stop(app);
			
			System.out.println("=========================================AfterTest");
		}
	} 
	
	
	
}


