describe('patientRepository.spec', function () {
    var patientRepository, commonRepository, dataService, apiUrls, $q, $scope;

    // test data.
    var patients ={
        "data": [
            {
                "id": 1,
                "ptnum": "xyz1234",
                "name": "病人Test1",
                "sex": 1,
                "bed": 3,
                "hospital": "2",
                "extendInfo": null,
                "age": 47,
                "status": 2,
                "mobile": "13664856581",
                "birthday": "1968-12-03T16:00:00.000Z",
                "create_time": "2015-07-28T05:14:21.000Z",
                "dept": {
                    "id": 1,
                    "name": "Surgical"
                },
                "creator": {
                    "id": 1,
                    "name": "Zhang"
                }
            },
            {
                "id": 2,
                "ptnum": "1122",
                "name": "病人Test2",
                "sex": 1,
                "bed": 3,
                "hospital": "1122",
                "extendInfo": null,
                "age": 19,
                "status": null,
                "mobile": null,
                "birthday": "1996-05-15T16:00:00.000Z",
                "create_time": "2015-08-06T05:34:15.000Z",
                "dept": {
                    "id": 3,
                    "name": "Paediatrics"
                },
                "creator": {
                    "id": 1,
                    "name": "Zhang"
                }
            },
            {
                "id": 3,
                "ptnum": "11234",
                "name": "病人Test3",
                "sex": 1,
                "bed": 3,
                "hospital": "877888",
                "extendInfo": null,
                "age": 0,
                "status": null,
                "mobile": null,
                "birthday": "2015-08-03T16:00:00.000Z",
                "create_time": "2015-08-07T01:40:34.000Z",
                "dept": {
                    "id": 4,
                    "name": "ENT"
                },
                "creator": {
                    "id": 1,
                    "name": "Zhang"
                }
            }
        ],
        "count": 3
     },

    newPatient = {
        "id": 101,
        "ptnum": "11234",
        "name": "病人Test4",
        "sex": 1,
        "bed": 3,
        "hospital": "877888",
        "extendInfo": null,
        "age": 0,
        "status": null,
        "mobile": null,
        "birthday": "2015-08-03T16:00:00.000Z",
        "create_time": "2015-08-07T01:40:34.000Z",
        "dept": {
            "id": 4,
            "name": "ENT"
        },
        "creator": {
            "id": 1,
            "name": "Zhang"
        }
    },

    updatedPatient = {
        "id": 2,
        "ptnum": "2233",
        "name": "病人Test2_update",
        "sex": 1,
        "bed": 3,
        "hospital": "2233",
        "extendInfo": null,
        "age": 19,
        "status": null,
        "mobile": null,
        "birthday": "1996-04-18T16:00:00.000Z",
        "create_time": "2015-08-06T05:34:15.000Z",
        "dept": {
        "id": 3,
            "name": "Paediatrics"
        },
            "creator": {
            "id": 1,
                "name": "Zhang"
        }
    };

    beforeEach(function () {
        module('toaster');
        module('pascalprecht.translate');
        module('mdtConstants');
        module('mdtRepositories');
        module('mdtServices');

        inject(function ($injector) {
            patientRepository = $injector.get('patientRepository');
            commonRepository = $injector.get('commonRepository');
            dataService = $injector.get('dataService');
            apiUrls = $injector.get('apiUrls');
            $q = $injector.get('$q');
            $scope = $injector.get('$rootScope').$new();
        });
    });

    // [CASE 1]: list.
    it('[patientRepository.list]: should return 3 patients in list', function (done) {
        spyOn(commonRepository, ['getData']).and.returnValue($q.when(patients));
        // mock data.
        var options = {
            filter: '', // account name or name
            dept: -1,
            size: 5,
            page: 1
        };
        patientRepository.list(options).then(function (result) {
            expect(commonRepository.getData).toHaveBeenCalledWith('/patients', options);
            expect(result.count).toBe(3);

            done();
        });

        $scope.$digest();
    });

    // [CASE 2]:add.
    it('[patientRepository.add]: should find the new patient that was added just now', function (done) {
        function mockAdd() {
            patients.data.push(newPatient);
            return $q.when(patients);
        }

        spyOn(commonRepository, ['postData']).and.returnValue(mockAdd());

        patientRepository.add(newPatient).then(function (result) {
            expect(commonRepository.postData).toHaveBeenCalledWith('/patients', newPatient);
            expect(result.data[3]).not.toBeNull();

            done();
        });

        $scope.$digest();
    });

    // [CASE 3]: detail.
    it('[patientRepository.detail]: should return the first patient detail information', function (done) {
        function mockDetail() {
            return $q.when(patients.data[0]);
        }

        spyOn(commonRepository, ['getData']).and.returnValue(mockDetail());

        patientRepository.detail(1).then(function (result) {
            expect(commonRepository.getData).toHaveBeenCalledWith('/patients/1');
            expect(result).not.toBeNull();

            done();
        });

        $scope.$digest();
    });

    // [CASE 4]:deletePatient.
    it('[patientRepository.deletePatient]: should delete  a special patient', function (done) {
        function mockDeletePatient() {
            patients.data.splice(0, 1);
            patients.count = 1;

            return $q.when(patients);
        }

        spyOn(commonRepository, ['deleteData']).and.returnValue(mockDeletePatient());

        patientRepository.remove(1).then(function (result) {
            expect(commonRepository.deleteData).toHaveBeenCalledWith('/patients/1', {});
            expect(result.count).toBe(1);

            done();
        });

        $scope.$digest();
    });

    // [CASE 5]:update.
    it('[patientRepository.update]: should be updated a special patient', function (done) {
        function mockUpdate() {
            patients.data[0] = updatedPatient;
            return $q.when(patients);
        }

        spyOn(commonRepository, ['putData']).and.returnValue(mockUpdate());

        patientRepository.update(updatedPatient).then(function (result) {
            expect(commonRepository.putData).toHaveBeenCalledWith('/patients/' + updatedPatient.id, updatedPatient);
            expect(result.data[0].name).toBe('病人Test2_update');

            done();
        });

        $scope.$digest();
    });


    // [CASE 6]:checkPatient.
    it('[patientRepository.checkPatient]: should be checked a special patient', function (done) {
        function mockCheckPatient() {
            return $q.when({ "result": "OK" });
        }
        var data = { patientid: '1' };

        spyOn(commonRepository, ['getData']).and.returnValue(mockCheckPatient());

        patientRepository.checkPatient(data).then(function (result) {
            expect(commonRepository.getData).toHaveBeenCalledWith('/patients/checkexistence/' + data.patientid);
            expect(result).not.toBeNull();

            done();
        });

        $scope.$digest();
    });


    // [CASE 7]:mdts. should get all mdt list for a special patient
    //it('[patientRepository.mdts]: should get all mdt list for a special patient', function (done) {
    //    function mockMdts() {
    //        return $q.when({ "result": "OK" });
    //    }
    //
    //    spyOn(commonRepository, ['getData']).and.returnValue(mockMdts());
    //
    //    patientRepository.mdts(2).then(function (result) {
    //
    //
    //        done();
    //    });
    //
    //    $scope.$digest();
    //});




});
