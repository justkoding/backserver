﻿describe('userRepository.spec', function () {
    var userRepository, commonRepository, dataService, apiUrls, $q, $scope;

    // test data.
    var users = {
        "data": [{
            "id": 1,
            "accountName": "Zhang",
            "name": "Zhang San",
            "userType": { "id": 1, "name": "其他" },
            "dept": { "id": 1, "name": "Surgical" },
            "accountStatus": 1,
            "roles": [{ "id": 1, "name": "role-admin" }],
            "create_time": "2015-03-16 00:00:00"
        },
                {
                    "id": 3,
                    "accountName": "Wang",
                    "name": "Wang wu",
                    "userType": { "id": 3, "name": "副主任医师" },
                    "dept": { "id": 2, "name": "Medicine" },
                    "accountStatus": 1,
                    "roles": [{ "id": 2, "name": "role-doctor" }],
                    "create_time": "2015-03-16 00:00:00"
                }],
        "count": 2
    },
        newUser = {
            "id": 100,
            "accountName": "Zhang_100",
            "name": "Zhang San_100",
            "userType": { "id": 1, "name": "其他" },
            "dept": { "id": 1, "name": "Surgical" },
            "accountStatus": 1,
            "roles": [{ "id": 1, "name": "role-admin" }],
            "create_time": "2015-03-16 00:00:00"
        },
        updatedUser = {
            "id": 1,
            "accountName": "Zhang_updated",
            "name": "Zhang San",
            "userType": { "id": 1, "name": "其他" },
            "dept": { "id": 1, "name": "Surgical" },
            "accountStatus": 1,
            "roles": [{ "id": 1, "name": "role-admin" }],
            "create_time": "2015-03-16 00:00:00"
        },
        deptList = [
            { "id": 1, "name": "Surgical" },
            { "id": 2, "name": "Medicine" },
            { "id": 3, "name": "Paediatrics" },
            { "id": 4, "name": "ENT" },
            { "id": 5, "name": "Tumours" }
        ],
        permissionList = [
            {
                "name": "role-management",
                "data": [
                    { "id": 6, "name": "role-management-add", "groupname": "role-management" },
                    { "id": 7, "name": "role-management-modify", "groupname": "role-management" },
                    { "id": 8, "name": "role-management-delete", "groupname": "role-management" },
                    { "id": 9, "name": "role-management-list", "groupname": "role-management" }]
            },
            {
                "name": "user-management",
                "data": [
                    { "id": 1, "name": "user-management-list", "groupname": "user-management" },
                    { "id": 2, "name": "user-management-add", "groupname": "user-management" },
                    { "id": 3, "name": "user-management-delete", "groupname": "user-management" },
                    { "id": 4, "name": "user-management-modify", "groupname": "user-management" },
                    { "id": 5, "name": "user-management-set-assistant", "groupname": "user-management" }]
            }
        ];

    beforeEach(function () {
        module('toaster');
        module('pascalprecht.translate');
        module('mdtConstants');
        module('mdtRepositories');
        module('mdtServices');

        inject(function ($injector) {
            userRepository = $injector.get('userRepository');
            commonRepository = $injector.get('commonRepository');
            dataService = $injector.get('dataService');
            apiUrls = $injector.get('apiUrls');
            $q = $injector.get('$q');
            $scope = $injector.get('$rootScope').$new();
        });
    });

    // [CASE 1]: list.
    it('[userRepository.list]: should return 2 users in list', function (done) {
        spyOn(commonRepository, ['getData']).and.returnValue($q.when(users));
        // mock data.
        var option = {
            size: 10,
            page: 2
        };
        userRepository.list(option).then(function (result) {
            expect(commonRepository.getData).toHaveBeenCalledWith('/users', option);
            expect(result.count).toBe(2);

            done();
        });

        $scope.$digest();
    });

    // [CASE 2]:add.
    it('[userRepository.add]: should find the new user that was added just now', function (done) {
        function mockAdd() {
            users.data.push(newUser);
            return $q.when(users);
        }

        spyOn(commonRepository, ['postData']).and.returnValue(mockAdd());

        userRepository.add(newUser).then(function (result) {
            expect(commonRepository.postData).toHaveBeenCalledWith('/users', newUser);
            expect(result.data[2]).not.toBeNull();

            done();
        });

        $scope.$digest();
    });

    // [CASE 3]: detail.
    it('[userRepository.detail]: should return the frist user detail information', function (done) {
        function mockDetail() {
            return $q.when(users.data[0]);
        }

        spyOn(commonRepository, ['getData']).and.returnValue(mockDetail());

        userRepository.detail(1).then(function (result) {
            expect(commonRepository.getData).toHaveBeenCalledWith('/users/1');
            expect(result).not.toBeNull();

            done();
        });

        $scope.$digest();
    });

    // [CASE 4]:deleteUser.
    it('[userRepository.deleteUser]: should delete special user', function (done) {
        function mockDeleteUser() {
            users.data.splice(0, 1);
            users.count = 1;

            return $q.when(users);
        }

        spyOn(commonRepository, ['deleteData']).and.returnValue(mockDeleteUser());

        userRepository.remove(1).then(function (result) {
            expect(commonRepository.deleteData).toHaveBeenCalledWith('/users/1', {});
            expect(result.count).toBe(1);

            done();
        });

        $scope.$digest();
    });

    // [CASE 5]:update.
    it('[userRepository.update]: should be updated special user', function (done) {
        function mockUpdate() {
            users.data[0] = updatedUser;
            return $q.when(users);
        }

        spyOn(commonRepository, ['putData']).and.returnValue(mockUpdate());

        userRepository.update(updatedUser).then(function (result) {
            expect(commonRepository.putData).toHaveBeenCalledWith('/users/' + updatedUser.id, updatedUser);
            expect(result.data[0].accountName).toBe('Zhang_updated');

            done();
        });

        $scope.$digest();
    });

    // [CASE 6]:getDeptlist.
    it('[userRepository.getDeptlist]: should return department list', function (done) {
        function mockGetDeptlist() {
            return $q.when(deptList);
        }

        spyOn(commonRepository, ['getData']).and.returnValue(mockGetDeptlist());

        userRepository.getDeptlist().then(function (result) {
            expect(commonRepository.getData).toHaveBeenCalledWith('/depts');
            expect(result.length).toBe(5);

            done();
        });

        $scope.$digest();
    });

    // [CASE 7]:getPermissionList.
    it('[userRepository.getPermissionList]: should return permission list', function (done) {
        function mockGetPermissionList() {
            return $q.when(permissionList);
        }

        spyOn(commonRepository, ['getData']).and.returnValue(mockGetPermissionList());

        userRepository.getPermissionList().then(function (result) {
            expect(commonRepository.getData).toHaveBeenCalledWith('/permissions');
            expect(result.length).toBe(2);

            done();
        });

        $scope.$digest();
    });

    // [CASE 8]:enableAccount.
    it('[userRepository.enableAccount]: should enable special account', function (done) {
        function mockEnableAccount() {
            return $q.when({ "result": "OK" });
        }

        spyOn(commonRepository, ['putData']).and.returnValue(mockEnableAccount());

        userRepository.enableAccount(1).then(function (result) {
            expect(commonRepository.putData).toHaveBeenCalledWith('/accounts/1/enable');
            expect(result.result).toBe('OK');

            done();
        });

        $scope.$digest();
    });

    // [CASE 9]:disableAccount.
    it('[userRepository.disableAccount]: should disable special account', function (done) {
        function mockDisableAccount() {
            return $q.when({ "result": "OK" });
        }

        spyOn(commonRepository, ['putData']).and.returnValue(mockDisableAccount());

        userRepository.disableAccount(1).then(function (result) {
            expect(commonRepository.putData).toHaveBeenCalledWith('/accounts/1/disable');
            expect(result.result).toBe('OK');

            done();
        });

        $scope.$digest();
    });

    // [CASE 10]:checkAccount.
    it('[userRepository.checkAccount]: should check whether special account is existing', function (done) {
        function mockCheckAccount() {
            return $q.when({ "result": "OK" });
        }
        var option = { accountName: 'test' };

        spyOn(commonRepository, ['getData']).and.returnValue(mockCheckAccount());
        userRepository.checkAccount(option).then(function (result) {
            expect(commonRepository.getData).toHaveBeenCalledWith('/accounts/checkexistence', option);
            expect(result.result).toBe('OK');

            done();
        });

        $scope.$digest();
    });
});