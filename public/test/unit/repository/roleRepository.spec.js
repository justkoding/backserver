﻿describe('userRepository.spec', function () {
    var roleRepository, commonRepository, dataService, apiUrls, $q, $scope;

    // test data.
    var roles = [
        { "id": 1, "name": "role-admin", "desc": "role-for-admin-the-system", "level": 0 },
        { "id": 2, "name": "role-doctor", "desc": "role-for-doctor", "level": 0 },
        { "id": 3, "name": "role-guest", "desc": "role-for-guest", "level": 1 },
        { "id": 4, "name": "role-test", "desc": "role-for-test", "level": 1 }
    ],
    rolesValues = [
        { "id": 1, "name": "role-admin", "desc": "role-for-admin-the-system" },
        { "id": 2, "name": "role-doctor", "desc": "role-for-doctor" },
        { "id": 3, "name": "role-guest", "desc": "role-for-guest" },
        { "id": 4, "name": "role-test", "desc": "role-for-test" }
    ],
        newRole = { "id": 5, "name": "role-admin-new", "desc": "role-for-admin-the-system", "level": 0 },
        updatedRole = { "id": 1, "name": "role-admin-updated", "desc": "role-for-admin-the-system", "level": 0 };

    beforeEach(function () {
        module('toaster');
        module('pascalprecht.translate');
        module('mdtConstants');
        module('mdtRepositories');
        module('mdtServices');

        inject(function ($injector) {
            roleRepository = $injector.get('roleRepository');
            commonRepository = $injector.get('commonRepository');
            dataService = $injector.get('dataService');
            apiUrls = $injector.get('apiUrls');
            $q = $injector.get('$q');
            $scope = $injector.get('$rootScope').$new();
        });
    });

    // [CASE 1]: list.
    it('[roleRepository.list]: should return 4 role in list', function (done) {
        spyOn(commonRepository, ['getData']).and.returnValue($q.when(roles));

        roleRepository.list().then(function (result) {
            expect(commonRepository.getData).toHaveBeenCalledWith('/roles');
            expect(result.length).toBe(4);

            done();
        });

        $scope.$digest();
    });

    // [CASE 2]: listValues.
    it('[roleRepository.listValues]: should return 4 role in list', function (done) {
        spyOn(commonRepository, ['getData']).and.returnValue($q.when(rolesValues));

        roleRepository.listValues().then(function (result) {
            expect(commonRepository.getData).toHaveBeenCalledWith('/roles/values');
            expect(result.length).toBe(4);

            done();
        });

        $scope.$digest();
    });

    // [CASE 3]: add.
    it('[roleRepository.add]: should add a new role', function (done) {
        function mockAddRole() {
            roles.push(newRole)
            return $q.when(roles);
        }
        spyOn(commonRepository, ['postData']).and.returnValue($q.when(mockAddRole()));

        roleRepository.add(newRole).then(function (result) {
            expect(commonRepository.postData).toHaveBeenCalledWith('/roles', newRole);
            expect(result.length).toBe(5);

            done();
        });

        $scope.$digest();
    });

    // [CASE 4]: detail.
    it('[roleRepository.detail]: should return role detail info by id', function (done) {
        function mockDetail() {
            return $q.when(roles[0]);
        }
        spyOn(commonRepository, ['getData']).and.returnValue($q.when(mockDetail()));

        roleRepository.detail(1).then(function (result) {
            expect(commonRepository.getData).toHaveBeenCalledWith('/roles/1');
            expect(result.id).toBe(1);

            done();
        });

        $scope.$digest();
    });

    // [CASE 5]: deleteRole.
    it('[roleRepository.deleteRole]: should return role detail info by id', function (done) {
        function mockDeleteRole() {
            roles.splice(0, 1);
            return $q.when(roles);
        }
        spyOn(commonRepository, ['deleteData']).and.returnValue($q.when(mockDeleteRole()));

        roleRepository.remove(1).then(function (result) {
            expect(commonRepository.deleteData).toHaveBeenCalledWith('/roles/1', {});
            expect(result.length).toBe(4);

            done();
        });

        $scope.$digest();
    });

    // [CASE 5]: update.
    it('[roleRepository.update]: should return role detail info by id', function (done) {
        function mockDeleteRole() {
            roles[0] = updatedRole;
            return $q.when(roles);
        }
        spyOn(commonRepository, ['putData']).and.returnValue($q.when(mockDeleteRole()));

        roleRepository.update(updatedRole).then(function (result) {
            expect(commonRepository.putData).toHaveBeenCalledWith('/roles/1', updatedRole);
            expect(result[0].name).toBe('role-admin-updated');

            done();
        });

        $scope.$digest();
    });
});