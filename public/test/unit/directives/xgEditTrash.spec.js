﻿describe('xgEditTrash test', function () {
    var element, scope, $compile;

    beforeEach(function () {
        module('mdtDrectives');
        module('mdtTemplates');
        module('mdtServices');
        module('mybootstrap.modal');

        inject(function ($injector) {
            scope = $injector.get('$rootScope').$new();
            $compile = $injector.get('$compile');
        });
    });

    it('should generate xgEditTrash element', function () {
        scope.edit = function () { };
        scope.trash = function () { };

        // create an instance of the directive.
        element = angular.element('<xg-edit-trash></xg-edit-trash>');

        // compile the directive
        $compile(element)(scope);

        // update the html
        scope.$digest();

        // get the isolate scope for the directive
        var isoScope = element.isolateScope();

        // verify.
        expect(isoScope.edit).not.toBeNull();
        expect(element.html()).toContain('<div class="xg-edit" ng-click="edit()">');
        expect(element.html()).toContain('<div class="xg-trash" ng-click="trash()">');
    });
});