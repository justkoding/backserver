﻿describe('xgFilter test', function () {
    var element, scope, $compile;

    beforeEach(function () {
        module('mdtDrectives');
        module('mdtTemplates');
        module('mdtServices');
        module('mybootstrap.modal');

        inject(function ($injector) {
            scope = $injector.get('$rootScope').$new();
            $compile = $injector.get('$compile');
        });
    });

    it('should generate xgFilter element', function () {
        scope.title = 'xg-filter';
        scope.sItem = {
            value: 'item#1'
        };
        scope.dpData = [
            { id: 1, name: 'item#1' },
            { id: 2, name: 'item#2' }
        ];
        scope.onToggle = jasmine.createSpy('onToggle');
        scope.onSelected = jasmine.createSpy('onSelected');

        // create an instance of the directive.
        element = angular.element('<div xg-filter on-selected="onSelected" xg-toggle="onToggle" xg-title="{{title}}" xg-s-text="sItem" dp-data="dpData"></div>');

        // compile the directive
        $compile(element)(scope);

        // update the html
        scope.$digest();

        // get the isolate scope for the directive
        var isoScope = element.isolateScope();

        // verify.
        expect(typeof(isoScope.toggled)).toBe('function');
        expect(typeof(isoScope.selected)).toBe('function');
               
        // verify dbData is two-way bound.
        isoScope.dpData.push({ id: 3, name: 'item#3' });
        expect(scope.dpData.length).toBe(3);

        // verify title is one-way bound.
        isoScope.title = 'xg-filter-2';
        expect(scope.title).toBe('xg-filter');

        // verify html
        var html = element.html();
        expect(html).toContain('<label class="ng-binding">xg-filter</label>');
        expect(html).toContain(' <span class="dropdown xg-filter" dropdown="" on-toggle="toggled(open)">');
        expect(html).toContain('<ul class="dropdown-menu">');
        expect(html).toContain('<a href="" class="ng-binding">item#1</a>');
        expect(html).toContain('<a href="" class="ng-binding">item#2</a>');
    });
});