﻿describe('constants.apiUrls', function () {
    var apiUrls;

    beforeEach(function () {
        module('mdtConstants');

        inject(function ($injector) {
            apiUrls = $injector.get('apiUrls');
        });
    });

    it('verify those contants', function () {
        expect(apiUrls.signIn).toBe('/token');
        expect(apiUrls.accounts).toBe('/accounts');
        expect(apiUrls.checkAccount).toBe('/accounts/checkexistence');
        expect(apiUrls.changepwd).toBe('/accounts/password');
        expect(apiUrls.users).toBe('/users');
        expect(apiUrls.depts).toBe('/depts');
        expect(apiUrls.usertypes).toBe('/usertypes');
        expect(apiUrls.permissions).toBe('/permissions');
        expect(apiUrls.roles).toBe('/roles');
        expect(apiUrls.rolesValues).toBe('/roles/values');
        expect(apiUrls.healthrecords).toBe('/healthrecords');
        expect(apiUrls.patients).toBe('/patients');
        expect(apiUrls.mdts).toBe('/mdts');
        expect(apiUrls.icp).toBe('/icp');
        expect(apiUrls.tempAttendees).toBe('/tempattendees');
        expect(apiUrls.anonymous).toBe('/anonymous');
    });
});