﻿describe('strings.apiUrls', function () {
    var strings;

    beforeEach(function () {
        module('mdtConstants');

        inject(function ($injector) {
            strings = $injector.get('strings');
        });
    });

    it('pageSizes array should equal [5,10,20,30,50]', function () {
        expect(strings.pageSizes).toBeDefined();
        expect(strings.pageSizes.length).toBe(5);
        expect(strings.pageSizes[0]).toBe(5);
        expect(strings.pageSizes[1]).toBe(10);
        expect(strings.pageSizes[2]).toBe(20);
        expect(strings.pageSizes[3]).toBe(30);
        expect(strings.pageSizes[4]).toBe(50);
    });

    it('mdtStatusOptions should have 3 items', function () {
        expect(strings.mdtStatusOptions).toBeDefined();
        expect(strings.mdtStatusOptions.length).toBe(3);

        expect(strings.mdtStatusOptions[0].text).toBe('MDT_LIST_STATUS_START_OR_JOIN');
        expect(strings.mdtStatusOptions[0].value).toBe(0);

        expect(strings.mdtStatusOptions[1].text).toBe('MDT_LIST_STATUS_FINISHED');
        expect(strings.mdtStatusOptions[1].value).toBe(1);

        expect(strings.mdtStatusOptions[2].text).toBe('MDT_LIST_STATUS_REVOKE');
        expect(strings.mdtStatusOptions[2].value).toBe(2);        
    });

    it('mdtTimestampOptions should have 9 items', function () {
        expect(strings.mdtTimestampOptions).toBeDefined();
        expect(strings.mdtTimestampOptions.length).toBe(9);

        expect(strings.mdtTimestampOptions[0].text).toBe('TEXT_ALL');
        expect(strings.mdtTimestampOptions[0].value).toBe(0);

        expect(strings.mdtTimestampOptions[1].text).toBe('TEXT_LAST_ONE_WEEK');
        expect(strings.mdtTimestampOptions[1].value).toBe(1);

        expect(strings.mdtTimestampOptions[2].text).toBe('TEXT_LAST_TWO_WEEK');
        expect(strings.mdtTimestampOptions[2].value).toBe(2);

        expect(strings.mdtTimestampOptions[3].text).toBe('TEXT_LAST_ONE_MONTH');
        expect(strings.mdtTimestampOptions[3].value).toBe(3);

        expect(strings.mdtTimestampOptions[4].text).toBe('TEXT_LAST_TWO_MONTHS');
        expect(strings.mdtTimestampOptions[4].value).toBe(4);

        expect(strings.mdtTimestampOptions[5].text).toBe('TEXT_NEXT_ONE_WEEK');
        expect(strings.mdtTimestampOptions[5].value).toBe(5);

        expect(strings.mdtTimestampOptions[6].text).toBe('TEXT_NEXT_TWO_WEEK');
        expect(strings.mdtTimestampOptions[6].value).toBe(6);

        expect(strings.mdtTimestampOptions[7].text).toBe('TEXT_NEXT_ONE_MONTH');
        expect(strings.mdtTimestampOptions[7].value).toBe(7);

        expect(strings.mdtTimestampOptions[8].text).toBe('TEXT_NEXT_TWO_MONTHS');
        expect(strings.mdtTimestampOptions[8].value).toBe(8);
    });
});