﻿describe('constants.routes', function () {
    var routes;

    beforeEach(function () {
        module('mdtConstants');

        inject(function ($injector) {
            routes = $injector.get('routes');
        });
    });

    function getRouteByState(state) {
        var route;

        for (var i = 0; i < routes.length; i++) {
            if (routes[i].state === state) {
                route = routes[i];
                break;
            }
        }

        return route;
    }
    
    it('verify login route', function () {
        var route = getRouteByState('login');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('login');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('LoginCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/login/login.html');
        expect(route.config.url).toBe('/login?returnUrl');
        expect(route.config.params).toBeDefined();
        expect(route.config.params.dest).toBeNull();
    });

    it('verify anonymous route', function () {
        var route = getRouteByState('anonymous');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('anonymous');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('AnonymousCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/anonymous/anonymous.html');
        expect(route.config.url).toBe('/anonymous?token');
    });

    it('verify page route', function () {
        var route = getRouteByState('page');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('PageController');
        expect(route.config.templateUrl).toBe('assets/src/pages/page/page.html');
        expect(route.config.url).toBe('/page');
        expect(route.config.params).toBeDefined();
        expect(route.config.params.dest).toBeNull();
        expect(route.config.resolve).toBeDefined();
    });

    it('verify page.forbidden route', function () {
        var route = getRouteByState('page.forbidden');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.forbidden');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('ForbiddenCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/forbidden/forbidden.html');
        expect(route.config.url).toBe('/forbidden');
    });

    it('verify page.dashboard route', function () {
        var route = getRouteByState('page.dashboard');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.dashboard');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('DashboardCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/dashboard/dashboard.html');
        expect(route.config.url).toBe('/dashboard');       
        expect(route.config.resolve).toBeDefined();
    });

    it('verify page.meeting route', function () {
        var route = getRouteByState('page.meeting');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.meeting');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('ListMdtCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/meeting/list/list.html');
        expect(route.config.url).toBe('/mdt?search');
    });

    it('verify page.meeting.detail route', function () {
        var route = getRouteByState('page.meeting.detail');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.meeting.detail');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('DetailMdtCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/meeting/detail/detail.html');
        expect(route.config.url).toBe('/detail/:id');
        expect(route.config.params).toBeDefined();
        expect(route.config.params.id).toBeNull();       
    });

    it('verify page.meeting.edit route', function () {
        var route = getRouteByState('page.meeting.edit');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.meeting.edit');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('AddOrEditMdtCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/meeting/addOrEdit/addOrEdit.html');
        expect(route.config.url).toBe('/edit/:id');
        expect(route.config.params).toBeDefined();
        expect(route.config.params.id).toBeNull();
    });

    it('verify page.meeting.add route', function () {
        var route = getRouteByState('page.meeting.add');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.meeting.add');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('AddMdtCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/meeting/add/add.html');
        expect(route.config.url).toBe('/add');
    });

    it('verify page.report route', function () {
        var route = getRouteByState('page.report');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.report');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('ReportsCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/reports/reports.html');
        expect(route.config.url).toBe('/report?search');
    });

    it('verify page.docx route', function () {
        var route = getRouteByState('page.docx');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.docx');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('DocxCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/docx/docx.html');
        expect(route.config.url).toBe('/docx?search');
    });

    it('verify page.patient route', function () {
        var route = getRouteByState('page.patient');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.patient');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('ListPatientsCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/patients/list/list.html');
        expect(route.config.url).toBe('/patient?search');
    });

    it('verify page.patient.add route', function () {
        var route = getRouteByState('page.patient.add');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.patient.add');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('AddorEditPatientCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/patients/addOrEdit/addOrEdit.html');
        expect(route.config.url).toBe('/add');
    });

    it('verify page.patient.edit route', function () {
        var route = getRouteByState('page.patient.edit');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.patient.edit');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('AddorEditPatientCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/patients/addOrEidt/addOrEidt.html');
        expect(route.config.url).toBe('/edit/:id');
        expect(route.config.params).toBeDefined();
        expect(route.config.params.id).toBeNull();
    });

    it('verify page.patient.detail route', function () {
        var route = getRouteByState('page.patient.detail');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.patient.detail');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('DetailPatientCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/patients/detail/detail.html');
        expect(route.config.url).toBe('/detail/:id');
        expect(route.config.params).toBeDefined();
        expect(route.config.params.id).toBeNull();
    });

    it('verify page.pwd route', function () {
        var route = getRouteByState('page.pwd');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.pwd');
        expect(route.config).toBeDefined();

        // verify config.      
        expect(route.config.templateUrl).toBe('assets/src/pages/password/reset.html');
        expect(route.config.url).toBe('/pwd');
    });

    it('verify page.user route', function () {
        var route = getRouteByState('page.user');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.user');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('ListUserCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/users/list/list.html');
        expect(route.config.url).toBe('/user?search');
        expect(route.config.resolve).toBeDefined();        
    });

    it('verify page.user.add route', function () {
        var route = getRouteByState('page.user.add');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.user.add');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('AddOrEidtUserCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/users/addOrEidt/addOrEidt.html');
        expect(route.config.url).toBe('/add');
    });

    it('verify page.user.edit route', function () {
        var route = getRouteByState('page.user.edit');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.user.edit');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('AddOrEidtUserCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/users/addOrEidt/addOrEidt.html');
        expect(route.config.url).toBe('/edit/:id');
        expect(route.config.params).toBeDefined();
        expect(route.config.params.id).toBeNull();
    });

    it('verify page.user.detail route', function () {
        var route = getRouteByState('page.user.detail');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.user.detail');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('DetailUserCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/users/detail/detail.html');
        expect(route.config.url).toBe('/detail/:id');
        expect(route.config.params).toBeDefined();
        expect(route.config.params.id).toBeNull();
    });

    it('verify page.role route', function () {
        var route = getRouteByState('page.role');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.role');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('ListRoleCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/roles/list/list.html');
        expect(route.config.url).toBe('/role');
    });

    it('verify page.role.add route', function () {
        var route = getRouteByState('page.role.add');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.role.add');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('AddOrEidtRoleCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/roles/addOrEidt/addOrEidt.html');
        expect(route.config.url).toBe('/add');
    });

    it('verify page.role.edit route', function () {
        var route = getRouteByState('page.role.edit');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.role.edit');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('AddOrEidtRoleCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/roles/addOrEidt/addOrEidt.html');
        expect(route.config.url).toBe('/edit/:id');
        expect(route.config.params).toBeDefined();
        expect(route.config.params.id).toBeNull();
    });

    it('verify page.role.detail route', function () {
        var route = getRouteByState('page.role.detail');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.role.detail');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('DetailRoleCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/roles/detail/detail.html');
        expect(route.config.url).toBe('/detail/:id');
        expect(route.config.params).toBeDefined();
        expect(route.config.params.id).toBeNull();
    });

    it('verify page.log route', function () {
        var route = getRouteByState('page.log');

        // verify
        expect(route).toBeDefined();
        expect(route.state).toBe('page.log');
        expect(route.config).toBeDefined();

        // verify config.
        expect(route.config.controller).toBe('LogsCtrl');
        expect(route.config.templateUrl).toBe('assets/src/pages/logs/logs.html');
        expect(route.config.url).toBe('/log?search');
    });
});