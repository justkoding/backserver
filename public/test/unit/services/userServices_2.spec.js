describe('user.service', function () {
    var users = { "data": [{ "id": 1, "accountName": "Zhang", "name": "Zhang San", "userType": { "id": 1, "name": "其他" }, "dept": { "id": 1, "name": "Surgical" }, "accountStatus": 1, "roles": [{ "id": 1, "name": "role-admin" }], "create_time": "2015-03-16 00:00:00" }, { "id": 3, "accountName": "Wang", "name": "Wang wu", "userType": { "id": 3, "name": "副主任医师" }, "dept": { "id": 2, "name": "Medicine" }, "accountStatus": 1, "roles": [{ "id": 2, "name": "role-doctor" }], "create_time": "2015-03-16 00:00:00" }], "count": 2 };

    var userServices, repoService, $q, $scope;

    beforeEach(function () {
        module('toaster');
        module('mdtConstants');
        module('mdtRepositories');
        module('pascalprecht.translate');
        module('mdtServices');

        inject(function ($injector) {
            userServices = $injector.get('userServices');
            repoService = $injector.get('repoService');
            $q = $injector.get('$q');
            $scope = $injector.get('$rootScope').$new();
        });
    });

    // [Test]: list
    it('[userServices.list]:should return 2 users', function (done) {
        spyOn(repoService, ['withFunction']).and.returnValue({
            exec: function () {
                return $q.when(users);
            }
        });

        userServices.list().then(function (result) {
            // verify 'withFunction' was called.
            expect(repoService.withFunction).toHaveBeenCalledWith('userRepository', 'list');

            // verify the response is correct.
            expect(result.count).toBe(2);

            done();
        });

        $scope.$digest();
    });

    // [Test]: add
    it('[userServices.add]:should return 3 users after adding a new one', function (done) {
        spyOn(repoService, ['withFunction']).and.returnValue({
            exec: function () {
                var newOne = angular.copy(users.data[0]);
                newOne['id'] = 100;
                users.count = 3;
                users.data.push(newOne);

                return $q.when(users);
            }
        });

        userServices.add().then(function (result) {
            // verify 'withFunction' was called.
            expect(repoService.withFunction).toHaveBeenCalledWith('userRepository', 'add');

            // verify the response is correct.
            expect(result.count).toBe(3);

            done();
        });

        $scope.$digest();
    });
});
