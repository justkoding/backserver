describe('user.service', function () {
    var users = { "data": [{ "id": 1, "accountName": "Zhang", "name": "Zhang San", "userType": { "id": 1, "name": "其他" }, "dept": { "id": 1, "name": "Surgical" }, "accountStatus": 1, "roles": [{ "id": 1, "name": "role-admin" }], "create_time": "2015-03-16 00:00:00" }, { "id": 3, "accountName": "Wang", "name": "Wang wu", "userType": { "id": 3, "name": "副主任医师" }, "dept": { "id": 2, "name": "Medicine" }, "accountStatus": 1, "roles": [{ "id": 2, "name": "role-doctor" }], "create_time": "2015-03-16 00:00:00" }], "count": 2 };

    var userServices, repoServiceMock;

    repoServiceMock = {
        withFunction: function () {
            return {
                exec: function () {
                    return users;
                }
            };
        },
        repositories: {
            user: ''
        }
    };

    // mock entry reproService.
    beforeEach(module('mdtServices', function ($provide) {
        $provide.factory('repoService', function () {
            return repoServiceMock;
        });
    }));

    beforeEach(function () {
        module('toaster');
        module('mdtConstants');
        module('mdtRepositories');
        module('pascalprecht.translate');

        inject(function ($injector) {
            userServices = $injector.get('userServices');
        });
    });

    it('should return 2 users in list api', function () {
        var result = userServices.list();

        expect(result.count).toBe(2);
    });
});
