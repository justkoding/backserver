(function () {
    'use strict';

    var app = angular.module('app');

    app.config(['$stateProvider',
        '$urlRouterProvider',
        '$httpProvider',
        '$locationProvider',
        '$translateProvider',
        '$compileProvider',
        '$provide',      
        'valdrProvider',
        'constraints',
        'routes',
        function ($stateProvider,
            $urlRouterProvider,
            $httpProvider,
            $locationProvider,
            $translateProvider,
            $compileProvider,
            $provide,          
            valdrProvider,
            constraints,
            routes) {

            // add interceptors service for auth feature.
            //$httpProvider.interceptors.push('httpInterceptor');

            // add 'icp' protocol in whitelist.
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|http?|ftp|mailto|icp):/);

            // route
            configRoute();

            // locale.
            configLocale();

            // validate: 
            // register custom validator in valdr provider. 
            // the validator was defined in myUrl.js file under serivces/validator folder.
            valdrProvider.addValidator('myUrl');
            valdrProvider.addValidator('mobile');
            valdrProvider.addValidator('idnum');
            valdrProvider.addValidator('tel');
            valdrProvider.addValidator('mail');
            valdrProvider.addValidator('select');
            valdrProvider.addConstraints(constraints);

            /************ private methods **********/
            function configRoute() {
                routes.forEach(function (r) {
                    $stateProvider.state(r.state, r.config);
                });

                $urlRouterProvider.otherwise(function ($injector, $location) {
                    var state = $injector.get('$state');
                    
                    // if the state in url is empty like 'http://localhost:9000'
                    // then redirect to login page.
                    if (!$location.url()) {
                        state.go('login');
                    } else {
                        state.go('error.404');
                    }
                    return $location.path();
                });
            }

            function configLocale() {
                // locale.
                $translateProvider.useSanitizeValueStrategy('escaped');

                $translateProvider.useStaticFilesLoader({
                    prefix: '/assets/src/resources/locale.',
                    suffix: '.json'
                });

                // config translate adaptor and try to find preferred language by yourself.
                var lang = $translateProvider
                    .registerAvailableLanguageKeys(['en', 'cn'], {
                        'en_*': 'en',
                        'zh_*': 'cn',
                        'zh': 'cn'
                    })
                    .determinePreferredLanguage();
            };

            /**************************************/
        }
    ]);

    app.run(['$translate',
        'authApi',
        'amMoment',
        '$rootScope',
        '$window',
        '$locale',
        '$state',
        '$location',
        'modalService',
        'locale',
        'accessServices',
        'routerEventServices',
        function ($translate,
            authApi,
            amMoment,
            $rootScope,
            $window,
            $locale,
            $state,
            $location,
            modalService,
            locale,
            accessServices,
            routerEventServices) {

            // add global method to $rootScope
            $rootScope.changeLanguage = function (langKey) {
                $translate.use(langKey);
                if (langKey == 'cn') {
                    amMoment.changeLocale('zh-cn');
                } else {
                    amMoment.changeLocale(langKey);
                }

                angular.copy(locale[langKey], $locale);

                if (langKey === 'cn') {
                    // zh-cn
                    moment.locale('zh-cn');
                } else {
                    moment.locale(langKey);
                }
            };

            initApp();

            /*******private methods**********/
            function initApp() {
                var lang = $translate.preferredLanguage();
                angular.copy(locale[lang], $locale);
                
                // set locale key to moment provider.
                amMoment.changeLocale(lang);

                if (lang === 'cn') {
                    // zh-cn
                    moment.locale('zh-cn');
                } else {
                    moment.locale(lang);
                }                

                // authentication
                authApi.init();

                // regester router events
                routerEventServices.addEvents();
            }
            /**************************************/
        }]);
})();
