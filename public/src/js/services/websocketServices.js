(function () {
    'use strict';

    angular.module('mdtServices').factory('websocketServices', ['$rootScope',
        'configModel',
        'commonService',
        'strings',
        function ($rootScope,
            configModel,
            commonService,
            strings) {
            // websocket instance.
            var ws, opt = {};

            /**
             * Create websocket channel.
             * - token: user token, to identity the user.
             * - option: json object for callbacks
             *     - onopen : fire when websocket is connected
             *     - onmessage : fire when received data
             *     - onclose  : fire when websocket is closed
             */
            function createChannel(token, option) {
                // if websocket instance is created or token is empty.
                // return directly. no need to create again.
                if (ws || !token) {
                    return;
                }             

                // configModel.baseUrl + url + commonService.getQueryParams(data)
                if ('WebSocket' in window) {
                    // Let us open a web socket
                    var url = configModel.wsBaseUrl + '/ws/login' + commonService.getQueryParams({
                        token: token,
                        device: 'browser' // from web app.
                    });
                    ws = new WebSocket(url);

                    // update opt variable.
                    opt.token = token;
                    opt.option = option;

                    // adding 'open' event
                    ws.onopen = function () {                       
                        option && option.onopen && option.onopen();
                    };

                    // adding 'message' event
                    ws.onmessage = function (ev) {
                        var result = ev.data ? JSON.parse(ev.data) : {};
                        var topic;

                        // check the websocket type
                        // - notification
                        // - doctor social
                        // - discussion
                        switch (result.kind) {
                            case strings.websocket.dataTypes.notification:
                                topic = strings.websocket.topic.notification;
                                break;
                            case strings.websocket.dataTypes.doctorSocial:
                                topic = strings.websocket.topic.doctorSocial;
                                break;
                            case strings.websocket.dataTypes.discussion:
                                topic = strings.websocket.topic.discussion;
                                break;
                            default:
                                // statements_def
                                break;
                        }                       
                        if (topic) {
                            $rootScope.$broadcast(topic, result.data);
                        }                     
                        option && option.onmessage && option.onmessage(result);

                        return ws;
                    };

                    // adding 'close' event
                    ws.onclose = function () {                      
                        option && option.onclose && option.onclose();
                    };

                } else {
                    // the browser doesn't support websocket
                    // nothing to do here
                    console.log('the browser does not support websocket');
                }

                return ws;
            }

            /**
            * Transmits data using the connection
            */
            function send(data) {
                if (!ws && opt.token) {
                    createChannel(opt.token, opt.option);
                }

                if (ws && data) {
                    ws.send(data);
                }
            }

            /**
            * Terminate any existing connection.
            */
            function close() {
                if (ws) {
                    ws.close();
                    ws = null;
                }
            }

            return {
                createChannel: createChannel,
                send: send,
                close: close
            };
        }
    ])
}());
