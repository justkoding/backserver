(function () {
    'use strict';

    angular.module('mdtServices').factory('snapshotsServices', [
        '$q',
        '$rootScope',
        '$location',
        '$translate',
        'repoService',
        'commonService',
        'entityServices',
        function ($q,
            $rootScope,
            $location,
            $translate,
            repoService,
            commonService,
            entityServices) {
            /**
            * Get all patients by query strings.
            */
            function list(params) {
                var options = {
                    mdtId: -1,  // -1 => all
                    healthrecordId: 0  // 0 => all
                };
                var defer = $q.defer();

                params = angular.extend(options, params);
                repoService.withFunction(repoService.repositories.snapshots, 'list')
                    .exec({
                        data: params,
                        skipBroadcast: true
                        //topic: 'comments.list'
                    }).then(function (result) {
                        console.log(result);
                        if (result.data && result.data.length != 0) {
                            result.data.map(function (item) {
                                item.thumbnail = commonService.formatSnapshot(item.id);
                                return item;
                            });
                        }
                        defer.resolve(result);
                        $rootScope.$broadcast('snapshot.list.done', result);
                    }, function (err) {
                        $rootScope.$broadcast('snapshot.list.failed');
                        defer.reject(err);
                    });

                return defer.promise;
            };

            return {
                list: list
            }
        }
    ])
}());
