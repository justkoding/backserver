(function() {
    'use-strict';

    angular.module('mdtServices').factory('idnum', function () {
        return {
            name: 'idnum',
            validate: function (value, arguments) {
                var isIDCard1=/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/;
                var isIDCard2=/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/;

                // allow empty.
                return value == null || value == "" || isIDCard1.test(value) || isIDCard2.test(value);
            }
        };
    });
}());
