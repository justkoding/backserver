(function() {
    'use-strict';

    angular.module('mdtServices').factory('myUrl', function () {
        return {
            name: 'myUrl', 
            validate: function (value, arguments) {
                //var reg = /^(http)?:\/\//;
                //return reg.test(value); //value.indexOf(arguments.prefix) != -1;

                return value == arguments.prefix;
            }
        };
    });
}());
