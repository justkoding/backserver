(function() {
    'use-strict';

    angular.module('mdtServices').factory('mail', function () {
        return {
            name: 'mail',
            validate: function (value, arguments) {
                var reg = /^[\w.-]+@[A-Za-z0-9]+\.[a-z]{2,6}$/;

                // allow empty.
                return value == null || value == "" || reg.test(value);
            }
        };
    });
}());
