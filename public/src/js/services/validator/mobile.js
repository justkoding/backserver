(function() {
    'use-strict';

    angular.module('mdtServices').factory('mobile', function () {
        return {
            name: 'mobile',
            validate: function (value, arguments) {
                var regCN = /^1\d{10}$/;
                var regEN = /^\d{3}-?\d{3}-?\d{4}$/;

                // allow empty.
                return value == null || value == "" || regCN.test(value) || regEN.test(value);
            }
        };
    });
}());
