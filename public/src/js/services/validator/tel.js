(function() {
    'use-strict';

    angular.module('mdtServices').factory('tel', function () {
        return {
            name: 'tel',
            validate: function (value, arguments) {
                var reg = /^(0\d{2,3}-)?\d{7,8}(-\d{3,4})?$/;
                var regEN = /^\d{3}-?\d{3}-?\d{4}$/;

                // allow empty.
                return value == null || value == "" || reg.test(value) || regEN.test(value);
            }
        };
    });
}());
