(function() {
    'use-strict';

    angular.module('mdtServices').factory('select', function () {
        return {
            name: 'select',
            validate: function (value, arguments) {
                // not allow empty or -1
                return (value != null && value != '' && value.id != -1 && value.id != '-1');
            }
        };
    });
}());
