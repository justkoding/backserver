(function () {
    'use strict';

    angular.module('mdtServices').factory('favoritesServices', [
        '$q',
        '$rootScope',
        '$location',
        '$translate',
        'repoService',
        'commonService',
        'entityServices',
        function ($q,
            $rootScope,
            $location,
            $translate,
            repoService,
            commonService,
            entityServices) {
            /**
            * Get all favorites by query strings.
            */
            function list(params) {
                var options = {
                    count: 20
                };

                params = angular.extend(options, params);
                return repoService.withFunction(repoService.repositories.favorite, 'list')
                    .exec({
                        data: params,
                        topic: 'favorite.list'
                    });
            };

            /**
            * Add a new favorite.
            */
            function add(patientId, favorite) {
                return repoService.withFunction(repoService.repositories.favorite, 'add')
                    .exec({
                        options: patientId,
                        data: favorite,
                        topic: 'favorite.add'
                    });
            }

            /**
            * Get favorite information by favorite id.
            */
            function detail(id) {
                return repoService.withFunction(repoService.repositories.favorite, 'detail')
                    .exec({
                        data: id,
                        topic: 'favorite.detail'
                    });
            }


            /**
            * Delete favorite by id
            */
            function del(id) {
                return repoService.withFunction(repoService.repositories.favorite, 'remove')
                    .exec({
                        data: id,
                        topic: 'favorite.remove'
                    });
            }

            /**
            * Update favorite information
            */
            function update(favorite) {
                return repoService.withFunction(repoService.repositories.favorite, 'update')
                    .exec({
                        data: favorite,
                        topic: 'favorite.update'
                    });
            }

            return {
                list: list,
                add: add,
                detail: detail,
                remove: del,
                update: update
            }
        }
    ])
}());
