(function () {
    'use strict';

    angular.module('mdtServices').factory('loginServices', ['$rootScope',
        '$cookieStore',
        '$location',
        '$state',
        'repoService',
        'authApi',
        function ($rootScope,
            $cookieStore,
            $location,
            $state,
            repoService,
            authApi) {
            function login(data, fn) {
                return repoService.withFunction(repoService.repositories.login, 'login')
                    .exec({
                        data: data,
                        skipBroadcast: true
                    }).then(function (data) {
                        var token = data.token;

                        if (token) {
                            authApi.init(token);
                            $cookieStore.put('user', data);
                            $cookieStore.put('token', token);
                            $rootScope.$broadcast('user.login.done', data);
                        } 
                        return data;
                    }, function (err) {
                        $rootScope.$broadcast('user.login.failed', err);
                        return err;
                    });
            };

            function logout(fn) {
                return repoService.withFunction(repoService.repositories.login, 'logout')
                   .exec({
                       data: {},
                       topic: 'user.logout'
                   });
            }

            return {
                login: login,
                logout: logout
            };
        }
    ])
}());
