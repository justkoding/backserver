(function () {
    'use strict';

    angular.module('mdtServices').factory('healthrecordServices', [
        '$q',
        '$rootScope',
        '$location',
        'repoService',
        'commonService',
        'Upload',
        'apiUrls',
        function ($q,
            $rootScope,
            $location,
            repoService,
            commonService,
            Upload,
            apiUrls) {

            /**
            * Get all healthrecords by query strings.
            */
            function list(historyId, skipBroadcast) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.healthrecord, 'list')
                    .exec({
                        data: historyId,
                        //topic: 'healthrecord.list'
                        skipBroadcast: true
                    }).then(function (data) {
                        if (data && data.length != 0) {
                            data = makeThumbnailUri(data);
                        }

                        if (!skipBroadcast) {
                            $rootScope.$broadcast('healthrecord.list.done', data);
                        }

                        defer.resolve(data);
                    }, function (err) {
                        if (!skipBroadcast) {
                            $rootScope.$broadcast('healthrecord.list.failed', err);
                        }

                        defer.reject(err);
                    });

                return defer.promise;
            }

            /**
            * Get all healthrecords by patient id.
            */
            function download(id) {
                return repoService.withFunction(repoService.repositories.healthrecord, 'download')
                    .exec({
                        data: id,
                        topic: 'healthrecord.download'
                    });
            };

            /**
            * Delete healthrecord by id.
            */
            function del(id) {
                return repoService.withFunction(repoService.repositories.healthrecord, 'del')
                    .exec({
                        data: id,
                        topic: 'healthrecord.delete'
                    });
            };

            /**
            * Get thumbnail by patient id.
            */
            function thumbnail(id) {
                return repoService.withFunction(repoService.repositories.healthrecord, 'thumbnail')
                    .exec({
                        data: id,
                        topic: 'healthrecord.thumbnail'
                    });
            };

            /**
             * Get password by patient id.
             */
            function password(id) {
                return repoService.withFunction(repoService.repositories.healthrecord, 'password')
                    .exec({
                        data: id,
                        topic: 'healthrecord.password'
                    });
            };

            /**
            * Upload healthrecord to server.
            */
            function upload(historyId, file, opt) {
                var defer = $q.defer();
                    //fields = {
                    //    count: file.count,
                    //    current: file.current,
                    //    desc: file.desc,
                    //    classification: file.classification
                    //};

                Upload.upload({
                    url: apiUrls.histories + '/' + historyId + '/healthrecords',
                    fields: opt,
                    file: file,
                    ignoreLoadingBar: true
                }).progress(function (evt) {
                    var value = parseInt(evt.loaded / evt.total);
                    defer.notify({
                        file: file,
                        value: value
                    });
                }).success(function (data, status, headers, config) {
                    defer.resolve({
                        file: file,
                        results: data
                    });
                }).error(function (err) {
                    defer.reject({
                        file: file,
                        error: err
                    });
                });

                return defer.promise;
            }

            /**
             * Update healthrecord by healthrecordid.
             */
            function update(options) {
                return repoService.withFunction(repoService.repositories.healthrecord, 'update')
                    .exec({
                        data: options,
                        skipBroadcast: true
                    }).then(function (resp) {
                        if (resp.result == 'FAILED') {
                            $rootScope.$broadcast('healthrecords.update.failed', { message: 'FAILED' });

                        } else {
                            $rootScope.$broadcast('healthrecords.update.done', resp);
                        }
                    }, function (err) {
                        $rootScope.$broadcast('healthrecords.update.failed', err);
                    });
            }

            /**
            * Set thumbnail uri for each healthrecord
            */
            function makeThumbnailUri(healthrecords) {
                if (healthrecords && healthrecords.length != 0) {
                    healthrecords.map(function (item) {
                        // status: 0-> not ready, 1-> ready.
                        if (item.status === 1) {
                            item.thumbnail = apiUrls.healthrecords + '/' + item.id + '/thumbnail';
                        } else { // for dicom file
                            // take default placeholder.
                            item.thumbnail = '/assets/src/img/icons/dicomPlaceholder.png';
                        }

                        return item;
                    });
                }

                return healthrecords;
            }

            /**
             * Get all classifications of patient healthrecords 
             */
            function classifications() {
                return repoService.withFunction(repoService.repositories.healthrecord, 'classifications')
                    .exec({
                        topic: 'healthrecord.classifications'
                    });
            }

            /**
             * get all healthrecords under specific patient history and then
             * group by classification.
             */
            function combine(historyId, skipBroadcast) {
                var defer = $q.defer();

                list(historyId, true).then(function (healthrecords) {
                    var tempData = commonService.session.get('classifications');
                    if (tempData) {
                        var data = combineHealthrecords(healthrecords, tempData);

                        if (!skipBroadcast) {
                            $rootScope.$broadcast('healthrecord.combine.done', data);
                        }

                        defer.resolve(data);
                    } else {
                        classifications().then(function (classifications) {
                            if (classifications && classifications.length != 0) {
                                commonService.session.set('classifications', classifications);
                            }
                            var data = combineHealthrecords(healthrecords, classifications);

                            if (data && data.length != 0) {
                                data.map(function (item) {
                                    // convert utc date to local.
                                    item.createTime = commonService.date.utcToLocal(item.createTime);

                                    return item;
                                });
                            }
                            if (!skipBroadcast) {
                                $rootScope.$broadcast('healthrecord.combine.done', data);
                            }

                            defer.resolve(data);
                        }, function () {
                            if (!skipBroadcast) {
                                $rootScope.$broadcast('healthrecord.combine.failed', error);
                            }

                            defer.reject(error);
                        });
                    }
                }, function (error) {
                    if (!skipBroadcast) {
                        $rootScope.$broadcast('healthrecord.combine.failed', error);
                    }

                    defer.reject(error);
                });

                return defer.promise;
            }

            /*---------private------------*/
            function combineHealthrecords(healthrecords, classifications) {
                if (!healthrecords) {
                    return classifications;
                }

                if (!classifications) {
                    return [];
                }

                var data = [];
                classifications.forEach(function (v, i) {
                    var item = {
                        classification: v,
                        count: 0,
                        healthrecords: []
                    };
                    healthrecords.forEach(function (h, j) {
                        if (v.id == h.classification.id) {
                            item.healthrecords.push(h);
                            item.count += 1;
                        }
                    });

                    if (item.healthrecords.length != 0) {
                        makeThumbnailUri(item.healthrecords);

                        item.healthrecords.map(function (h) {
                            // convert utc to local date.
                            h.createTime = commonService.date.utcToLocal(h.createTime);

                            return h;
                        });
                    }

                    data.push(item);
                });

                return data;
            }
            /*-----------end--------------*/

            return {
                list: list,
                download: download,
                thumbnail: thumbnail,
                password: password,
                upload: upload,
                del: del,
                makeThumbnailUri: makeThumbnailUri,
                update: update,
                classifications: classifications,
                combine: combine
            }
        }
    ])
}());
