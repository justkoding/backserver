(function () {
    'use strict';

    angular.module('mdtServices').factory('hospitalsServices', [
        '$q',
        '$rootScope',
        '$location',
        '$translate',
        'repoService',
        'commonService',
        'entityServices',
        function ($q,
            $rootScope,
            $location,
            $translate,
            repoService,
            commonService,
            entityServices) {
            /**
            * Get all patients by query strings.
            */
            function list() {
                return repoService.withFunction(repoService.repositories.hospitals, 'list')
                     .exec({
                         data: {},
                         topic: 'hospitals.list'
                     });
            };

            return {
                list: list
            }
        }
    ])
}());
