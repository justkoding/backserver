(function () {
    'use strict';

    angular.module('mdtServices').factory('routerEventServices', ['$rootScope',
        '$location',
        '$state',
        'loadingServices',
        'strings',
        'accessServices',
        function ($rootScope,
            $location,
            $state,
            loadingServices,
            strings,
            accessServices) {

            function addEvents() {
                // router start
                $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                    loadingServices.show();
                })

                // router success
                $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                    loadingServices.hide();
                })

                // router state not found
                $rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromParams) {
                    loadingServices.hide();
                    $state.go('error.404');
                })

                // router error
                $rootScope.$on("$stateChangeError", function (event, current, currentParams, previous, previusParam, rejection) {

                    loadingServices.hide();

                    var path = $location.path();
                    var paths = path.split('/');
                    if (paths.length > 3) {
                        current.params.id = paths[paths.length - 1];
                    }

                    if (rejection == strings.ajaxStatus.UNAUTHORIZED) {
                        $state.go('error.401');
                    } else if (rejection == strings.ajaxStatus.FORBIDDEN) {
                        $state.go('error.403');
                    } else if (rejection == strings.ajaxStatus.NOTFOUND) {
                        $state.go('error.404');
                    } else {
                        $state.go('login', {
                            dest: current,
                            returnUrl: path
                        });
                    }
                });
            }

            return {
                addEvents: addEvents
            };
        }
    ])
}());
