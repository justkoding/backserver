(function () {
    'use strict';

    angular.module('mdtServices').factory('patientServices', [
        '$q',
        '$rootScope',
        '$location',
        '$translate',
        'strings',
        'repoService',
        'commonService',
        'historiesServices',
        'entityServices',
        'mdtServices',
        'favoritesServices',
        function ($q,
            $rootScope,
            $location,
            $translate,
            strings,
            repoService,
            commonService,
            historiesServices,
            entityServices,
            mdtServices,
            favoritesServices) {
            /**
            * Get all patients by query strings.
            */
            function list(params) {
                var options = {
                    filter: '', // account name or name
                    dept: -1,
                    size: 5,
                    page: 1
                };
                var defer = $q.defer();

                params = angular.extend(options, params);

                repoService.withFunction(repoService.repositories.patient, 'list')
                    .exec({
                        data: params,
                        skipBroadcast: true
                        //topic: 'patient.list'
                    }).then(function (result) {
                        if (result && result.data) {
                            // get all favorites list.
                            favoritesServices.list({
                                count: 0 // 0-> all items
                            }).then(function (resp) {
                                if (resp && resp.data) {
                                    resp.data.forEach(function (f) {
                                        result.data.forEach(function (p) {
                                            if (p.id === f.patient.id) {
                                                p.isFavorite = true;
                                            }
                                        });
                                    });

                                    $rootScope.$broadcast('patient.list.done', result);
                                    defer.resolve(result);
                                }

                                $rootScope.$broadcast('patient.list.done', result);
                                defer.resolve(result);
                            }, function (err) {
                                defer.reject(err);
                            });
                        } else {
                            $rootScope.$broadcast('patient.list.done', result);
                            defer.resolve(result);
                        }
                    }, function (err) {
                        $rootScope.$broadcast('patient.list.failed', err);
                        defer.reject(err);
                    });

                return defer.promise;
            };

            /**
             * Get all pending patient by query strings.
             */
            function listPending(params) {
                var options = {
                    filter: '', // account name or name
                    dept: -1,
                    status: 1,
                    size: 10,
                    page: 1
                };
                var defer = $q.defer();
                delete params.status;
                params = angular.extend(options, params);

                repoService.withFunction(repoService.repositories.patient, 'list')
                    .exec({
                        data: params,
                        skipBroadcast: true
                        //topic: 'patient.list'
                    }).then(function (result) {
                        $rootScope.$broadcast('patient.pending.done', result);
                        defer.resolve(result);
                    }, function (err) {
                        $rootScope.$broadcast('patient.pending.failed', err);
                        defer.reject(err);
                    });

                return defer.promise;
            };
            /**
             * Get all discussion patient by query strings.
             */
            function listDiscussion(params) {
                var options = {
                    size: 10,
                    page: 1
                };
                var defer = $q.defer();
                delete params.status;
                params = angular.extend(options, params);

                repoService.withFunction(repoService.repositories.patient, 'discussion')
                    .exec({
                        data: params,
                        skipBroadcast: true
                        //topic: 'patient.discussion'
                    }).then(function (result) {
                        $rootScope.$broadcast('patient.discussion.done', result);
                        defer.resolve(result);
                    }, function (err) {
                        $rootScope.$broadcast('patient.discussion.failed', err);
                        defer.reject(err);
                    });

                return defer.promise;
            };
            /**
             * Get all meeting patient by query strings.
             */
            function listMeeting(params) {
                var options = {
                    size: 10,
                    page: 1
                };
                var defer = $q.defer();
                delete params.status;
                params = angular.extend(options, params);

                repoService.withFunction(repoService.repositories.patient, 'meeting')
                    .exec({
                        data: params,
                        skipBroadcast: true
                        //topic: 'patient.meeting'
                    }).then(function (result) {
                        $rootScope.$broadcast('patient.meeting.done', result);
                        defer.resolve(result);
                    }, function (err) {
                        $rootScope.$broadcast('patient.meeting.failed', err);
                        defer.reject(err);
                    });

                return defer.promise;
            };
            /**
            * Add a new patient.
            */
            function add(patient) {
                return repoService.withFunction(repoService.repositories.patient, 'add')
                    .exec({
                        data: patient,
                        skipBroadcast: true
                    }).then(function (resp) {
                        if (resp.result == 'FAILED') {
                            $rootScope.$broadcast('patient.add.failed', { message: 'FAILED' });

                        } else {
                            $rootScope.$broadcast('patient.add.done', resp);
                        }
                    }, function (err) {
                        $rootScope.$broadcast('patient.add.failed', err);
                    });
            }

            /**
            * Get patient information by patient id.
            */
            function detail(id) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.patient, 'detail')
                    .exec({
                        data: id,
                        skipBroadcast: true
                    }).then(function (result) {

                        if (result && result.data) {
                            // get all favorites list.
                            favoritesServices.list({
                                count: 0 // 0-> all items
                            }).then(function (resp) {
                                if (resp && resp.data) {
                                    resp.data.forEach(function (f) {
                                        if (result.data.id === f.patient.id) {
                                            result.data.isFavorite = true;
                                        }
                                    });

                                    // format for rending.
                                    resp.data = formatPatient(resp.data);
                                }

                                $rootScope.$broadcast('patient.detail.done', result);
                                defer.resolve(result);
                            }, function (err) {
                                defer.reject(err);
                            });
                        } else {
                            $rootScope.$broadcast('patient.detail.done', result);
                            defer.resolve(result);
                        }
                    }, function (err) {
                        $rootScope.$broadcast('patient.detail.failed', err);
                        defer.reject(err);
                    });

                return defer.promise;
            }

            /**
            * Delete patient by patient id
            */
            function deletePatient(id) {
                return repoService.withFunction(repoService.repositories.patient, 'remove')
                    .exec({
                        data: id,
                        topic: 'patient.remove'
                    });
            }

            /**
            * Update patient information
            */
            function update(patient) {
                return repoService.withFunction(repoService.repositories.patient, 'update')
                    .exec({
                        data: patient,
                        skipBroadcast: true
                    }).then(function (resp) {
                        if (resp.result == 'FAILED') {
                            $rootScope.$broadcast('patient.update.failed', { message: 'FAILED' });

                        } else {
                            $rootScope.$broadcast('patient.update.done', resp);
                        }
                    }, function (err) {
                        $rootScope.$broadcast('patient.update.failed', err);
                    });
            }

            /**
            * Check if the special patient is existing
            */
            function checkPatient(idnum) {
                return repoService.withFunction(repoService.repositories.patient, 'checkPatient')
                    .exec({
                        data: {
                            'idnum': idnum
                        },
                        skipBroadcast: true
                    }).then(function (resp) {
                        if (resp.result == 'RECORD_NOT_EXIST') {
                            $rootScope.$broadcast('patient.exist.failed', { message: 'RECORD_NOT_EXIST' });
                        } else {
                            $rootScope.$broadcast('patient.exist.done', resp);
                        }
                    }, function (err) {
                        $rootScope.$broadcast('patient.exist.failed', err);
                    });
            };

            /**
            * Get all mdt list for special patient
            */
            function mdts(patientId) {
                return repoService.withFunction(repoService.repositories.patient, 'mdts')
                    .exec({
                        data: patientId,
                        topic: 'patient.mdts'
                    });
            }

            /**
            * Get all healthrecords for each mdt under special patient
            */
            function patientMdtHealthrecords(patientId) {
                var defer = $q.defer();
                var data = [];
                var iNow = 0;

                if (patientId != null) {
                    // get all mdts under special patient.
                    mdts(patientId).then(function (mdts) {
                        if (mdts && mdts.length != 0) {
                            mdts.forEach(function (m) {
                                // get mdt basic info
                                mdtServices.detail(m).then(function (basicInfo) {
                                    // get healthrecord for each patient each mdt.
                                    mdtServices.healthrecordsList({
                                        mdtid: m,
                                        patientId: patientId
                                    }).then(function (hrs) {
                                        data.push({
                                            mdt: basicInfo,
                                            records: hrs
                                        });

                                        if (++iNow == mdts.length) {
                                            defer.resolve(data);
                                        }
                                    }, function () {
                                        data.push({
                                            mdt: basicInfo,
                                            records: []
                                        });
                                        if (++iNow == mdts.length) {
                                            defer.resolve(data);
                                        }
                                    });
                                }, function (err) {
                                    defer.reject(err);
                                });
                            });
                        } else {
                            defer.resolve(data);
                        }
                    });
                }

                return defer.promise;
            }

            /**
             * format patient data for rending.
             */
            function formatPatient(patient) {
                if (patient) {
                    // conver utc date to local time.
                    if (patient.birthday) {
                        patient.birthday = commonService.date.utcToLocal(patient.birthday);
                    }

                    if (patient.createTime) {
                        patient.createTime = commonService.date.utcToLocal(patient.createTime);
                    }

                    // format creator
                    if (patient.creator) {
                        // creator avatar
                        patient.creator.avatar = commonService.formatAvatar(patient.creator.avatar, patient.creator.userId, true);

                        // format gender.
                        patient.creator.gender = commonService.translateGender(patient.creator.sex);
                    }
                }
                return patient;
            }

            /**
            * Get all patients which can be accessing by special doctor.
            * @param options[object] query string.
            * - filter: search by patient name
            * - doctor: doctor id(user id)
            * - size: records number each page.
            * - page: the current page index.
            */
            function relation(params, skipBroadcast) {
                var options = {
                    filter: '', // patient name
                    //doctor: '', the current login doctor by default.
                    size: -1, // no pagination
                    page: 1
                };
                var defer = $q.defer();

                params = angular.extend(options, params);

                repoService.withFunction(repoService.repositories.patient, 'relation')
                    .exec({
                        data: params,
                        skipBroadcast: true
                        //topic: 'patient.relation'
                    }).then(function (result) {
                        var arr = [];
                        if (result && result.data.map) {
                            result.data.map(function (v) {
                                // creator avatar
                                v.avatar = commonService.formatAvatar(null, null, true);

                                // format gender.
                                v.gender = commonService.translateGender(v.sex);

                                return v;
                            });

                            arr = result.data;
                        }

                        if (!skipBroadcast) {
                            $rootScope.$broadcast('patient.relation.done', arr);
                        }
                        defer.resolve(arr);
                    }, function (err) {
                        if (!skipBroadcast) {
                            $rootScope.$broadcast('patient.relation.failed', err);
                        }
                        defer.reject(err);
                    });

                return defer.promise;
            };


            /**
            * Get all patients which can be accessing by special doctor.          
            */
            function summary(patientId, skipBroadcast) {              
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.patient, 'summary')
                    .exec({
                        data: patientId,
                        skipBroadcast: true                      
                    }).then(function (data) {                        
                        // format data for rendering.
                        if (data) {
                            // format patient
                            if (data.patient) {
                                // create time
                                if (data.patient.createTime) {
                                    data.patient.createTime = commonService.date.utcToLocal(data.patient.createTime);
                                }

                                // format gender.
                                data.patient.gender = commonService.translateGender(data.patient.sex);
                            }

                            // format diagnosis histories.
                            if (data.histories && data.histories.length != 0) {
                                data.histories.map(function (v) {
                                    // create time
                                    if (v.createTime) {
                                        v.createTime = commonService.date.utcToLocal(v.createTime);
                                    }

                                    // end time
                                    if (v.endTime) {
                                        v.endTime = commonService.date.utcToLocal(v.endTime);
                                    }

                                    return v;
                                });
                            }
                        }

                        if (!skipBroadcast) {
                            $rootScope.$broadcast('patient.summary.done', data);
                        }
                        defer.resolve(data);
                    }, function (err) {
                        if (!skipBroadcast) {
                            $rootScope.$broadcast('patient.summary.failed', err);
                        }
                        defer.reject(err);
                    });

                return defer.promise;
            };

            return {
                list: list,
                listPending: listPending,
                listDiscussion: listDiscussion,
                listMeeting: listMeeting,
                add: add,
                detail: detail,
                remove: deletePatient,
                update: update,
                checkPatient: checkPatient,
                patientMdtHealthrecords: patientMdtHealthrecords,
                relation: relation,
                summary: summary
            }
        }
    ])
}());
