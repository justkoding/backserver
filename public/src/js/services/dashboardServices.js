(function () {
    'use strict';

    angular.module('mdtServices').factory('dashboardServices', [
        '$q',
        '$rootScope',
        '$location',
        '$translate',
        'repoService',
        'commonService',
        'entityServices',
        function ($q,
            $rootScope,
            $location,
            $translate,
            repoService,
            commonService,
            entityServices) {
            /**
            * Get all announcements by query strings.
            */
            function list() {
                var defer = $q.defer();
                repoService.withFunction(repoService.repositories.dashboard, 'list')
                    .exec({
                        skipBroadcast: true
                    }).then(function (resp) {
                        var mdts = resp? resp : [];

                        $rootScope.$broadcast('dashboard.list.done', mdts);
                    }, function (err) {
                        $rootScope.$broadcast('dashboard.list.failed', err);
                        defer.reject(err);
                    });

                return defer.promise;
            }

            return {
                list: list
            }
        }
    ])
}());
