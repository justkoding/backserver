(function () {
    'use strict';

    angular.module('mdtServices').factory('httpInterceptor', [
        '$q',
        '$window',
        '$location',
        '$cookieStore',
        '$injector',
        'commonService',
        function ($q, $window, $location, $cookieStore, $injector, commonService) {

            return {
                'request': function (req) {
                    var user = commonService.getCurrentUser();
                    var token = user ? user.token : null;
                    var isAnonymous = false;
                    if(req.url.indexOf('anonymous.html') >= 0){
                    	isAnonymous = true;
                    }
                    
                    if(!isAnonymous){
	                    if (token == null && !req.headers.ignore) {
	                        $cookieStore.remove('token');
	                        $location.path('/login');
	                    } else {
	                        req.headers['X-AUTH-TOKEN'] = token;
	                    }                   
                    }
                    
                    return req;
                },

                'requestError': function (rejection) {
                    // do something on error
                    if (rejection.status === 401) {
                        $cookieStore.remove('token');
                        $location.path('/login');
                    }

                    return $q.reject(rejection);
                },

                // optional method
                'response': function (resp) {
                    return resp;
                },

                // optional method
                'responseError': function (rejection) {
                    // do something on error
                    if (rejection.status === 401) {
                        $cookieStore.remove('token');
                        $location.path('/login');
                    }
                    return $q.reject(rejection);
                }
            };
        }
    ]);
}());
