(function() {
    'use strict';

    angular.module('mdtServices').factory('authApi', ['$http', '$cookieStore', function ($http, $cookieStore) {
        return {
            isAuthenticated: function() {
                return $http.defaults.headers.common['X-AUTH-TOKEN'] != null;
            },
            init: function(token) {
                $http.defaults.headers.common['X-AUTH-TOKEN'] = token || $cookieStore.get('token');
            }
        };
    }])
}());
