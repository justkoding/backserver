(function () {
    'use strict';

    angular.module('mdtServices').factory('passwordServices', ['$rootScope',
        '$q',
        'repoService',
        function ($rootScope,
            $q,
            repoService) {
            function changePwd(data) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.password, 'changePwd')
                    .exec({
                        data: data,
                        skipBroadcast: true
                    }).then(function (data) {
                        if (data && data.result === 'NO_USER_OR_PASSWORD') {
                            $rootScope.$broadcast('password.change.failed', data);

                            defer.reject(data);
                        } else {
                            $rootScope.$broadcast('password.change.done', data);
                            defer.resolve(data);
                        }
                    }, function (err) {
                        $rootScope.$broadcast('password.change.failed', data);
                        defer.reject([]);
                    });

                return defer.promise;
            };

            function resetPwd(id) {
                return repoService.withFunction(repoService.repositories.password, 'resetPwd')
                    .exec({
                        data: id,
                        topic: 'password.reset'
                    });
            };

            return {
                changePwd: changePwd,
                resetPwd: resetPwd
            };
        }
    ])
}());
