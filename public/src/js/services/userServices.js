(function () {
    'use strict';

    angular.module('mdtServices').factory('userServices', [
        '$rootScope',
        '$location',
        '$translate',
        '$q',
        'repoService',
        'commonService',
        'entityServices',
        function ($rootScope,
            $location,
            $translate,
            $q,
            repoService,
            commonService,
            entityServices) {
            /**
            * Get all users by query strings.
            */
            function list(params, skipBroadcast) {
                var options = {
                    filter: '', // account name or name
                    dept: -1,
                    //type: -1, // usertype
                    size: 10,
                    page: 1
                };
                var defer = $q.defer();

                params = angular.extend(options, params);

                repoService.withFunction(repoService.repositories.user, 'list')
                    .exec({
                        data: options,
                        skipBroadcast: true
                        //topic: 'user.list'
                    }).then(function (resp) {
                        if (resp && resp.data) {
                            resp.data.map(function (v) {
                                // convert utc to local time
                                if (v.create_time) {
                                    v.create_time = commonService.date.utcToLocal(v.create_time);
                                }

                                // format gender.
                                v.gender = commonService.translateGender(v.gender);

                                return v;
                            });
                        }
                        defer.resolve(resp);
                        if (!skipBroadcast) {
                            $rootScope.$broadcast('user.list.done', resp);
                        }                       
                    }, function (error) {
                        defer.reject(error);
                        if (!skipBroadcast) {
                            $rootScope.$broadcast('user.list.failed', error);
                        }                        
                    });

                return defer.promise;
            };

            /**
            * Add a new user.
            */
            function add(user) {
                return repoService.withFunction(repoService.repositories.user, 'add')
                    .exec({
                        data: user,
                        topic: 'user.add'
                    });
            }

            /**
            * Get user information by user id.
            */
            function detail(id) {
                return repoService.withFunction(repoService.repositories.user, 'detail')
                    .exec({
                        data: id,
                        skipBroadcast: true
                        //topic: 'user.detail'
                    }).then(function (resp) {
                        if (resp.result == 'RECORD_NOT_EXIST') {
                            $rootScope.$broadcast('user.detail.failed', { message: 'RECORD_NOT_EXIST' });
                        } else {
                            $rootScope.$broadcast('user.detail.done', resp);
                        }
                    }, function (err) {
                        $rootScope.$broadcast('user.detail.failed', err);
                    });
            }

            /**
            * Delete user by user id
            */
            function deleteUser(id) {
                return repoService.withFunction(repoService.repositories.user, 'remove')
                    .exec({
                        data: id,
                        topic: 'user.remove'
                    });
            }

            /**
            * Update user information
            */
            function update(user) {
                return repoService.withFunction(repoService.repositories.user, 'update')
                    .exec({
                        data: user,
                        topic: 'user.update'
                    });
            }


            /**
            * Get all dept.
            */
            function getDeptlist() {
                var defer = $q.defer();
                repoService.withFunction(repoService.repositories.user, 'getDeptlist')
                    .exec({
                        data: {},
                        skipBroadcast: true
                    }).then(function (data) {

                        commonService.resource.getValues('ALL').then(function (sources) {

                            data.unshift({"id":"-1","name":sources['ALL']});
                            $rootScope.$broadcast('dept.list.done', data);
                            defer.resolve(data);
                        }, function (err) {
                            $rootScope.$broadcast('dept.list.failed', err);
                            defer.reject(err);
                        });

                    }, function (err) {
                        $rootScope.$broadcast('dept.list.failed', err);
                        defer.reject(err);
                    });

                return defer.promise;
            };

            /**
            * Get all dept.
            */
            function getUserTitleList(skipBroadcast) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.user, 'getUserTitleList')
                    .exec({
                        data: {},
                        skipBroadcast: true
                    }).then(function (data) {
                        var arr = [];
                        $translate(['TEXT_TITLE_LIST_ALL']).then(function (texts) {
                            arr.push(new entityServices.DpListItem(texts['TEXT_TITLE_LIST_ALL'], -1));

                            data.forEach(function (d) {
                                arr.push(new entityServices.DpListItem(d.name, d.id));
                            });

                            if (!skipBroadcast) {
                                $rootScope.$broadcast('title.list.done', arr);
                            }
                            defer.resolve(arr);
                        }, function (error) {
                            if (!skipBroadcast) {
                                $rootScope.$broadcast('title.list.failed', error);
                            }
                            defer.reject(error);
                        });
                    }, function (error) {
                        if (!skipBroadcast) {
                            $rootScope.$broadcast('title.list.failed', error);
                        }
                        defer.reject(error);
                    });

                return defer.promise;
            };

            /**
            * Get all DeptValues.
            */
            function getDeptValues() {
                return getData('getDeptlist', 'dept.list');
            };

            /**
            * Get all userType.
            */
            function getUserTypelist() {
                return getData('getUserTypeList', 'usertype.list');
            };

            /**
            * Get all getPermissionList.
            */
            function getPermissionList() {
                return getData('getPermissionList', 'permission.list');
            };

            /**
            * Enable special acount.
            */
            function enableAccount(id) {
                return getData('enableAccount', 'account.enable', id);
            };

            /**
            * Disable special acount.
            */
            function disableAccount(id) {
                return getData('disableAccount', 'account.disable', id);
            };

            /**
            * Check if the special account is existing
            */
            function checkAccount(accountName) {
                return getData('checkAccount', 'account.exist', {
                    accountName: accountName
                });
            };

            /**
            * Get depts list and available user types.
            */
            function getDeptsAndUserTypesTitles() {
                var defer = $q.defer();
                var iNow = 0;
                var result = {
                    depts: [],
                    userTypes: []
                };

                getDeptlist().then(function (data) {
                    result.depts = data;
                    if (++iNow === 3) {
                        defer.resolve(result);
                    }
                }, function () {
                    if (++iNow === 3) {
                        defer.reject(result);
                    }
                });

                getUserTypelist().then(function (data) {
                    result.userTypes = data;
                    if (++iNow === 3) {
                        defer.resolve(result);
                    }
                }, function () {
                    if (++iNow === 3) {
                        defer.reject(result);
                    }
                });

                getUserTitleList().then(function (data) {
                    result.userTitles = data;
                    if (++iNow === 3) {
                        defer.resolve(result);
                    }
                }, function () {
                    if (++iNow === 3) {
                        defer.reject(result);
                    }
                });


                return defer.promise;
            }

            /**
            * Join roles by symbol ','
            */
            function joinRoles(roles) {
                var str = '',
                    names = [];

                if (roles && roles.length != 0) {
                    names = roles.map(function (role) {
                        return role.name;
                    });

                    str = commonService.joinBy(names);
                }

                return str;
            }

            /*---------private method----------------*/
            function getData(funcName, topic, data) {
                return repoService.withFunction(repoService.repositories.user, funcName)
                   .exec({
                       data: data,
                       topic: topic
                   });
            }
            /*---------end----------------*/

            return {
                list: list,
                add: add,
                detail: detail,
                remove: deleteUser,
                update: update,
                enable: enableAccount,
                disable: disableAccount,
                getDeptlist: getDeptlist,
                getUserTypelist: getUserTypelist,
                getUserTitleList: getUserTitleList,
                getPermissionList: getPermissionList,
                getDeptValues: getDeptValues,
                joinRoles: joinRoles,
                checkAccount: checkAccount,
                getDeptsAndUserTypesTitles: getDeptsAndUserTypesTitles
            }
        }
    ])
}());
