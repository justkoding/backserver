(function () {
    'use strict';

    angular.module('mdtServices').factory('historiesServices', [
        '$q',
        '$rootScope',
        '$location',
        '$translate',
        'repoService',
        'commonService',
        'messageService',
        'strings',
        'entityServices',
        function ($q,
            $rootScope,
            $location,
            $translate,
            repoService,
            commonService,
            messageService,
            strings,
            entityServices) {

            /**
            * Add a new history.
            */
            function add(patientId, history) {
                return repoService.withFunction(repoService.repositories.histories, 'add')
                    .exec({
                        data: patientId,
                        options: history,
                        topic: 'history.add'
                    });
            }

            /**
            * Get all diagonistic histories for specifical patient.
            */
            function list(patientId) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.histories, 'list')
                    .exec({
                        data: patientId,
                        skipBroadcast: true
                        //topic: 'history.list'
                    }).then(function (resp) {
                        if (!resp) {
                            resp = {};
                        }

                        if (resp.result && resp.result.toLowerCase() === strings.jsonResults.FAILED.text.toLowerCase()) {
                            $rootScope.$broadcast('history.list.failed');
                            defer.reject(resp);
                        } else {
                            if (resp && typeof (resp.map) === 'function') {
                                // convert utc date to local date
                                resp.map(function (item) {
                                    if (item.createTime) {
                                        item.createTime = commonService.date.utcToLocal(item.createTime);
                                    }

                                    if (item.endTime) {
                                        item.endTime = commonService.date.utcToLocal(item.endTime);
                                        item.text = commonService.date.format(item.createTime) + '-' + commonService.date.format(item.endTime);
                                    } else {
                                        commonService.resource.getValues('TEXT_TO_NOW').then(function (results) {
                                            item.text = commonService.date.format(item.createTime) + '-' + results['TEXT_TO_NOW'];
                                        });
                                    }

                                    return item;
                                });
                            }
                            defer.resolve(resp);
                            $rootScope.$broadcast('history.list.done', resp);
                        }
                    }, function (error) {
                        $rootScope.$broadcast('history.list.failed');
                        defer.reject(error);
                    });

                return defer.promise;
            };

            /**
            * Get history detail information.
            */
            function detail(historyId) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.histories, 'detail')
                    .exec({
                        data: historyId,
                        skipBroadcast: true
                        //topic: 'history.list'
                    }).then(function (resp) {
                        if (!resp) {
                            resp = {};
                        }

                        if (resp.result && resp.result.toLowerCase() === strings.jsonResults.FAILED.text.toLowerCase()) {
                            $rootScope.$broadcast('history.detail.failed');
                            defer.reject(resp);
                        } else {
                            // conver utc date to local time.
                            resp.createTime = commonService.date.utcToLocal(resp.createTime);
                            resp.endTime = commonService.date.utcToLocal(resp.endTime);
                            resp.assignTime = commonService.date.utcToLocal(resp.assignTime);

                            // format creator
                            if (resp.creator) {
                                // creator avatar
                                resp.creator.avatar = commonService.formatAvatar(resp.creator.avatar, resp.creator.userId);

                                // format gender.
                                resp.creator.gender = commonService.translateGender(resp.creator.sex);
                            }

                            // format doctor
                            if (resp.doctor) {
                                // doctor avatar
                                resp.doctor.avatar = commonService.formatAvatar(resp.doctor.avatar, resp.doctor.userId);

                                // format gender.
                                resp.doctor.gender = commonService.translateGender(resp.doctor.sex);
                            }

                            // format patient
                            if (resp.patient) {
                                // conver utc date to local time.
                                resp.patient.birthday = commonService.date.utcToLocal(resp.patient.birthday);
                                resp.patient.createTime = commonService.date.utcToLocal(resp.patient.createTime);

                                // format patient creator avatar.
                                if (resp.patient.creator) {
                                    resp.patient.creator.avatar = commonService.formatAvatar(resp.patient.creator.avatar, resp.patient.creator.userId, true);
                                }

                                // format gender.
                                resp.patient.gender = commonService.translateGender(resp.patient.sex);
                            }

                            defer.resolve(resp);
                            $rootScope.$broadcast('history.detail.done', resp);
                        }
                    }, function (error) {
                        $rootScope.$broadcast('history.detail.failed');
                        defer.reject(error);
                    });

                return defer.promise;
            };

            /**
            * Update history
            */
            function update(history) {
                return repoService.withFunction(repoService.repositories.histories, 'update')
                   .exec({
                       data: history,
                       topic: 'history.update'
                   });
            };

            /**
            * Make a decision by doctor.
            */
            function decision(historyId, decision) {
                return repoService.withFunction(repoService.repositories.histories, 'decision')
                  .exec({
                      data: historyId,
                      options: decision,
                      topic: 'history.decision'
                  });
            };

            /**
            * assign patient to doctor
            * @param historyId[string] the history id of patient
            * @param doctorId[string] the account id of doctor
            */
            function assign(historyId, doctorId) {
                return repoService.withFunction(repoService.repositories.histories, 'assign')
                  .exec({
                      data: historyId,
                      options: doctorId,
                      topic: 'patient.assign'
                  });
            };

            /**
            * requet offline discussion           
            */
            function requestOffline(historyId, data, skipBroadcast) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.histories, 'requestOffline')
                  .exec({
                      data: data,
                      options: historyId,
                      skipBroadcast: true
                      //topic: 'request.offline'
                  }).then(function (resp) {
                      commonService.resource.getValues(['OK',
                          'PATIENT_ERROR_STATUS',
                          'IN_DISCUSSING',
                          'IN_MEETING_QUEUE',
                          'IN_MEETING',
                          'FAILED',
                          'UNKNOWN_FAILED']).then(function (sources) {
                              switch (resp.result) {
                                  case strings.jsonResults.OK.value: {
                                      resp.errorText = sources['OK'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('request.offline.done', resp);
                                      }
                                      defer.resolve(resp);
                                      break;
                                  }
                                  case strings.jsonResults.PATIENT_ERROR_STATUS.value: { // PATIENT_ERROR_STATUS
                                      resp.errorText = sources['PATIENT_ERROR_STATUS'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('request.offline.failed', resp);
                                      }
                                      defer.reject(resp);
                                      break;
                                  }
                                  case strings.jsonResults.IN_DISCUSSING.value: { //IN_DISCUSSING
                                      resp.errorText = sources['IN_DISCUSSING'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('request.offline.failed', resp);
                                      }
                                      defer.reject(resp);
                                      break;
                                  }
                                  case strings.jsonResults.IN_MEETING_QUEUE.value: { //IN_MEETING_QUEUE
                                      resp.errorText = sources['IN_MEETING_QUEUE'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('request.offline.failed', resp);
                                      }
                                      defer.reject(resp);
                                      break;
                                  }
                                  case strings.jsonResults.IN_MEETING.value: { // IN_MEETING
                                      resp.errorText = sources['IN_MEETING'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('request.offline.failed', resp);
                                      }
                                      defer.reject(resp);
                                      break;
                                  }
                                  default: { // FAILED
                                      resp.errorText = sources['FAILED'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('request.offline.failed', resp);
                                      }
                                      defer.reject(resp);
                                      break;
                                  }
                              }
                          });

                  }, function (error) {
                      commonService.resource.getValues('UNKNOWN_FAILED').then(function (sources) {
                          error.errorText = sources['UNKNOWN_FAILED'];
                          $rootScope.$broadcast('request.offline.failed', error);
                          defer.reject(error);
                      });
                  });

                return defer.promise;
            };

            /**
            * request a online meeting
            * @param historyId[string] the history id of patient           
            */
            function requestMeeting(historyId, data, skipBroadcast) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.histories, 'requestMeeting')
                  .exec({
                      data: data,
                      options: historyId,
                      skipBroadcast: true
                      //topic: 'request.meeting'
                  }).then(function (resp) {
                      commonService.resource.getValues(['OK',
                          'PATIENT_ERROR_STATUS',
                          'FAILED',
                          'UNKNOWN_FAILED']).then(function (sources) {
                              switch (resp.result) {
                                  case strings.jsonResults.OK.value: {
                                      resp.errorText = sources['OK'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('request.meeting.done', resp);
                                      }
                                      defer.resolve(resp);
                                      break;
                                  }
                                  case strings.jsonResults.PATIENT_ERROR_STATUS.value: { // PATIENT_ERROR_STATUS
                                      resp.errorText = sources['PATIENT_ERROR_STATUS'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('request.meeting.failed', resp);
                                      }
                                      defer.reject(resp);
                                      break;
                                  }
                                  default: { // FAILED
                                      resp.errorText = sources['FAILED'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('request.meeting.failed', resp);
                                      }
                                      defer.reject(resp);
                                      break;
                                  }
                              }
                          });
                  }, function (error) {
                      commonService.resource.getValues('UNKNOWN_FAILED').then(function (sources) {
                          error.errorText = sources['UNKNOWN_FAILED'];
                          $rootScope.$broadcast('request.meeting.failed', error);
                          defer.reject(error);
                      });
                  });

                return defer.promise;
            };

            /**
            * request a online meeting
            * @param historyId[string] the history id of patient           
            */
            function makeDecision(historyId, data, skipBroadcast) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.histories, 'makeDecision')
                  .exec({
                      data: data,
                      options: historyId,
                      skipBroadcast: true
                      //topic: 'make.decision'
                  }).then(function (resp) {
                      commonService.resource.getValues(['OK',
                          'PATIENT_ERROR_STATUS',
                          'FAILED',
                          'NO_DOCTOR',
                          'UNKNOWN_FAILED']).then(function (sources) {
                              switch (resp.result) {
                                  case strings.jsonResults.OK.value: {
                                      resp.errorText = sources['OK'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('make.decision.done', resp);
                                      }
                                      defer.resolve(resp);
                                      break;
                                  }
                                  case strings.jsonResults.PATIENT_ERROR_STATUS.value: { // PATIENT_ERROR_STATUS
                                      resp.errorText = sources['PATIENT_ERROR_STATUS'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('make.decision.failed', resp);
                                      }
                                      defer.reject(resp);
                                      break;
                                  }
                                  case strings.jsonResults.NO_DOCTOR.value: { // NO_DOCTOR
                                      resp.errorText = sources['NO_DOCTOR'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('make.decision.failed', resp);
                                      }
                                      defer.reject(resp);
                                      break;
                                  }
                                  default: { // FAILED
                                      resp.errorText = sources['FAILED'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('make.decision.failed', resp);
                                      }
                                      defer.reject(resp);
                                      break;
                                  }
                              }
                          });
                  }, function (error) {
                      commonService.resource.getValues('UNKNOWN_FAILED').then(function (sources) {
                          error.errorText = sources['UNKNOWN_FAILED'];
                          $rootScope.$broadcast('make.decision.failed', error);
                          defer.reject(error);
                      });
                  });

                return defer.promise;
            };

            /**
            * transform patient to another doctor
            * @param historyId[string] the history id of patient           
            */
            function transform(historyId, data, skipBroadcast) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.histories, 'transform')
                  .exec({
                      data: data,
                      options: historyId,
                      skipBroadcast: true
                      //topic: 'make.decision'
                  }).then(function (resp) {
                      commonService.resource.getValues(['OK',
                          'ERROR_STATUS',
                          'FAILED',
                          'IN_PROCESSING',
                          'TEXT_MESSAGE_INFO_TITLE',
                          'TEXT_MESSAGE_TRANSFORM_SUCCESS']).then(function (sources) {
                              switch (resp.result) {
                                  case strings.jsonResults.OK.value: {
                                      resp.errorText = sources['OK'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('make.transform.done', resp);
                                      }
                                      // show success message.
                                      messageService.success(sources['TEXT_MESSAGE_INFO_TITLE'], sources['TEXT_MESSAGE_TRANSFORM_SUCCESS']);

                                      defer.resolve(resp);
                                      break;
                                  }
                                  case strings.jsonResults.ERROR_STATUS.value: {
                                      resp.errorText = sources['ERROR_STATUS'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('make.transform.failed', resp);
                                      }
                                      defer.reject(resp);
                                      break;
                                  }
                                  case strings.jsonResults.IN_DISCUSSING.value: {
                                      resp.errorText = sources['IN_DISCUSSING'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('make.transform.failed', resp);
                                      }
                                      defer.reject(resp);
                                      break;
                                  }
                                  default: { // FAILED
                                      resp.errorText = sources['FAILED'];
                                      if (!skipBroadcast) {
                                          $rootScope.$broadcast('make.transform.failed', resp);
                                      }
                                      defer.reject(resp);
                                      break;
                                  }
                              }
                          });
                  }, function (error) {
                      commonService.resource.getValues('UNKNOWN_FAILED').then(function (sources) {
                          error.errorText = sources['UNKNOWN_FAILED'];
                          if (!skipBroadcast) {
                              $rootScope.$broadcast('make.transform.failed', error);
                          }
                          defer.reject(error);
                      });
                  });

                return defer.promise;
            };

            /**
             * Get history diagnosis summary.
             */
            function summary(id, skipBroadcast) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.histories, 'summary')
                 .exec({
                     data: id,
                     skipBroadcast: true
                 }).then(function (data) {
                     if (data && data.map) {
                         data.map(function (v) {
                             if (v.start) {
                                 v.start = commonService.date.utcToLocal(v.start);
                             }
                             if (v.end) {
                                 v.end = commonService.date.utcToLocal(v.end);
                             }

                             return v;
                         });
                     }

                     if (!skipBroadcast) {
                         $rootScope.$broadcast('history.summary.done', data);
                     }
                     defer.resolve(data);
                 }, function (error) {
                     if (!skipBroadcast) {
                         $rootScope.$broadcast('history.summary.failed', error);
                     }
                     defer.reject(error);
                 });

                return defer.promise;
            }

            /**
             * get all mdts(offline and online) for specifical patient.
             */
            function mdtsList(historyId, skipBroadcast) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.histories, 'mdtsList')
                   .exec({
                       data: historyId,
                       skipBroadcast: true
                   }).then(function (resp) {
                       if (!resp) {
                           resp = [];
                       }

                       resp.map(function (v) {
                           v.mdt = formatMdt(v.mdt);
                           return v;
                       });

                       if (!skipBroadcast) {
                           $rootScope.$broadcast('history.mdtslist.done', resp);
                       }
                       defer.resolve(resp);
                   }, function (error) {
                       if (!skipBroadcast) {
                           $rootScope.$broadcast('history.mdtslist.failed', error);
                       }
                       defer.reject(error);
                   });

                return defer.promise;
            }

            /**
             * Convert original data format to expected format for rendering.
             */
            function formatMdt(mdt) {
                if (!mdt) {
                    return;
                }

                mdt.createTime = commonService.date.utcToLocal(mdt.createTime);
                mdt.end = commonService.date.utcToLocal(mdt.end);
                mdt.start = commonService.date.utcToLocal(mdt.start);

                // attendees.
                if (mdt && mdt.attendees && mdt.attendees.length != 0) {
                    mdt.attendees.map(function (attendee) {
                        // format avatar.
                        attendee.avatar = commonService.formatAvatar(attendee.avatar, attendee.userId);

                        // format gender.
                        mdt.creator.gender = commonService.translateGender(mdt.creator.sex);

                        return attendee;
                    });
                }

                // creator
                if (mdt.creator) {
                    // creator avatar
                    mdt.creator.avatar = commonService.formatAvatar(mdt.creator.avatar, mdt.creator.userId);

                    // format gender.
                    mdt.creator.gender = commonService.translateGender(mdt.creator.sex);
                }

                // grouped attendees by dept.
                mdt.groupedAttendees = groupedAttendees(mdt.attendees);

                return mdt;
            }
            /*-----private------*/
            function groupedAttendees(attendees) {
                var groups = [];

                if (attendees && attendees.length != 0) {
                    var obj = {};
                    attendees.forEach(function (v) {
                        var temp = obj[v.dept.name]; //obj[v.dept.name];

                        if (!temp) {
                            temp = {
                                dept: v.dept,
                                attendees: [v]
                            };
                        } else {
                            temp.attendees.push(v);
                        }

                        obj[v.dept.name] = temp;
                    });

                    for (var o in obj) {
                        groups.push(obj[o]);
                    }
                }

                return groups;
            }

            /*------end------*/

            return {
                add: add,
                list: list,
                update: update,
                detail: detail,
                decision: decision,
                assign: assign,
                requestOffline: requestOffline,
                requestMeeting: requestMeeting,
                makeDecision: makeDecision,
                transform: transform,
                // mdt
                mdtsList: mdtsList,
                formatMdt: formatMdt,
                summary: summary
            }
        }
    ])
}());
