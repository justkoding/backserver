(function () {
    'use strict';

    angular.module('mdtServices').factory('mdtServices', [
        '$rootScope',
        '$location',
        '$translate',
        '$q',
        '$cookieStore',
        'repoService',
        'commonService',
        'entityServices',
        'healthrecordServices',
        'clientServices',
        'favoritesServices',
        function ($rootScope,
            $location,
            $translate,
            $q,
            $cookieStore,
            repoService,
            commonService,
            entityServices,
            healthrecordServices,
            clientServices,
            favoritesServices) {
            /*--------mdt basic---------*/
            /**
            * Get all mdts by query strings.
            */
            function list(params) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.mdt, 'list')
                    .exec({
                        data: params,
                        //topic: 'mdt.list'
                        skipBroadcast: true
                    }).then(function (results) {
                        var data = results.data;

                        // assign new data.
                        results.data = formatMdtData(data);

                        $rootScope.$broadcast('mdt.list.done', results);

                        defer.resolve(results);
                    }, function (err) {
                        $rootScope.$broadcast('mdt.list.failed', err);
                        defer.reject(err);
                    });

                return defer.promise;
            };

            /**
            * Add a new mdt.
            */
            function add(mdt) {
                return repoService.withFunction(repoService.repositories.mdt, 'add')
                    .exec({
                        data: formatMdtDataForPost(mdt),
                        topic: 'mdt.add'
                    });
            }

            /**
            * Get mdt information by mdt id.
            */
            function detail(id) {
                var defer = $q.defer();
                repoService.withFunction(repoService.repositories.mdt, 'detail')
                    .exec({
                        data: id,
                        skipBroadcast: true
                        //topic: 'mdt.detail'
                    }).then(function (resp) {
                        if (resp.result == 'RECORD_NOT_EXIST') {
                            $rootScope.$broadcast('mdt.detail.failed', { message: 'RECORD_NOT_EXIST' });
                        } else {
                            // format data so that rendering directly in UI.
                            var newData = formatMdtData([resp]);
                            resp = newData[0];

                            $rootScope.$broadcast('mdt.detail.done', resp);

                            defer.resolve(resp);
                        }
                    }, function (err) {
                        $rootScope.$broadcast('mdt.detail.failed', err);
                        defer.reject(err);
                    });

                return defer.promise;
            }

            /**
            * Delete mdt by mdt id
            */
            function deleteMdt(id) {
                return repoService.withFunction(repoService.repositories.mdt, 'remove')
                    .exec({
                        data: id,
                        topic: 'mdt.remove'
                    });
            }

            /**
            * Update mdt information
            */
            function update(mdt) {
                return repoService.withFunction(repoService.repositories.mdt, 'update')
                    .exec({
                        data: formatMdtDataForPost(mdt),
                        topic: 'mdt.update'
                    });
            }
            
            /**
             * Get mdt infomation for doctor home dashboard.
             */
            function doctorDashboardMdt(){
            	return repoService.withFunction(repoService.repositories.mdt, 'doctorDashboardMdt')
                .exec({
                    topic: 'mdt.doctordashboard'
                });
            }
            
            /**
            * Build status dropdown datasource.
            */
            function BuildStatusDpData() {
                var results = [];
                var defer = $q.defer();

                $translate(['MDT_LIST_ALL',
                    'MDT_LIST_STATUS_START_OR_JOIN',
                    'MDT_LIST_STATUS_COMPLETED',
                    'MDT_LIST_STATUS_REVOKE']).then(function (texts) {
                        results.push({ id: 0, name: texts['MDT_LIST_ALL'] });
                        results.push({ id: 1, name: texts['MDT_LIST_STATUS_START_OR_JOIN'] });
                        results.push({ id: 2, name: texts['MDT_LIST_STATUS_COMPLETED'] });
                        results.push({ id: 3, name: texts['MDT_LIST_STATUS_REVOKE'] });

                        defer.resolve(results);
                    }, function (e) {
                        defer.reject(e);
                    });

                return defer.promise;
            }

            /**
            * Build timestamp dropdown datasource.
            * flag: 0-> all, 1-> past, 2-> future
            */
            function BuildTimestampDpData(flag) {
                var results = [];
                var defer = $q.defer();

                $translate([
                    'MDT_LIST_SELECTION_DATE',
                    'TEXT_LAST_ONE_WEEK',
                    'TEXT_LAST_TWO_WEEK',
                    'TEXT_LAST_ONE_MONTH',
                    'TEXT_LAST_TWO_MONTHS',
                    'TEXT_NEXT_ONE_WEEK',
                    'TEXT_NEXT_TWO_WEEK',
                    'TEXT_NEXT_ONE_MONTH',
                    'TEXT_NEXT_TWO_MONTHS'
                ]).then(function (texts) {
                    // 1-> past
                    if (flag === 1) {
                        results.push({ id: 0, name: texts['MDT_LIST_SELECTION_DATE'] });
                        results.push({ id: 1, name: texts['TEXT_LAST_ONE_WEEK'] });
                        results.push({ id: 2, name: texts['TEXT_LAST_TWO_WEEK'] });
                        results.push({ id: 3, name: texts['TEXT_LAST_ONE_MONTH'] });
                        results.push({ id: 4, name: texts['TEXT_LAST_TWO_MONTHS'] });
                    } else if (flag === 2) {
                        // 2-> future
                        results.push({ id: 0, name: texts['MDT_LIST_SELECTION_DATE'] });
                        results.push({ id: 5, name: texts['TEXT_NEXT_ONE_WEEK'] });
                        results.push({ id: 6, name: texts['TEXT_NEXT_TWO_WEEK'] });
                        results.push({ id: 7, name: texts['TEXT_NEXT_ONE_MONTH'] });
                        results.push({ id: 8, name: texts['TEXT_NEXT_TWO_MONTHS'] });
                    } else {
                        //0-> all
                        results.push({ id: 0, name: texts['MDT_LIST_SELECTION_DATE'] });
                        results.push({ id: 1, name: texts['TEXT_LAST_ONE_WEEK'] });
                        results.push({ id: 2, name: texts['TEXT_LAST_TWO_WEEK'] });
                        results.push({ id: 3, name: texts['TEXT_LAST_ONE_MONTH'] });
                        results.push({ id: 4, name: texts['TEXT_LAST_TWO_MONTHS'] });
                        results.push({ id: 5, name: texts['TEXT_NEXT_ONE_WEEK'] });
                        results.push({ id: 6, name: texts['TEXT_NEXT_TWO_WEEK'] });
                        results.push({ id: 7, name: texts['TEXT_NEXT_ONE_MONTH'] });
                        results.push({ id: 8, name: texts['TEXT_NEXT_TWO_MONTHS'] });
                    }

                    defer.resolve(results);
                }, function (e) {
                    defer.reject(e);
                });

                return defer.promise;
            }

            function calcDateDuration(flag) {
                // flag: 0->all               
                var offset = 0;
                var dayUnit = true;

                switch (flag) {
                    case 1: { // 1-> last one week
                        offset = -7;
                        break;
                    }
                    case 2: { // 2-> last two weeks
                        offset = -14;
                        break;
                    }
                    case 3: { // last one month
                        offset = -1;
                        dayUnit = false;
                        break;
                    }
                    case 4: { // last two months
                        offset = -2;
                        dayUnit = false;
                        break;
                    }
                    case 5: { // next one week
                        offset = 7;
                        break;
                    }
                    case 6: { // next two weeks
                        offset = 14;
                        break;
                    }
                    case 7: { // next one month
                        offset = 1;
                        dayUnit = false;
                        break;
                    }
                    case 8: { // next two months
                        offset = 2;
                        dayUnit = false;
                        break;
                    }
                    default: { // 0->all
                        offset = 0;
                        break;
                    }
                }

                function format(date) {
                    return moment(date).format('YYYY-MM-DD');
                }

                function updateDate(offset, isDay) {
                    var today = new Date();
                    var date = {
                        datepre: '',
                        datepost: ''
                    };

                    if (offset != 0) {
                        // always set current date.
                        date.datepre = format(today);

                        if (isDay) {
                            today.setDate(today.getDate() + offset);
                        } else {
                            today.setMonth(today.getMonth() + offset);
                        }

                        // update it with new date.
                        date.datepost = format(today);

                        // make sure datepost is greater than datepre.
                        if (offset < 0) {
                            var temp = date.datepost;
                            var curDate = new Date();
                            date.datepost = format(new Date(curDate.getTime() - 24 * 60 * 60 * 1000));
                            date.datepre = temp;
                        }
                    }

                    return date;
                }

                // invoke
                return updateDate(offset, dayUnit);
            }
            /*--------end---------*/


            /*--------mdt patient material---------*/
            /**
            * Get all patients from special mdt.
            */
            function patientsList(options) {
                var defer = $q.defer();
                repoService.withFunction(repoService.repositories.mdt, 'patientsList')
                    .exec({
                        data: options,
                        skipBroadcast: true
                        //topic: 'mdt.patients.list'
                    }).then(function (resp) {
                        if (resp.data) {
                            resp.data.map(function (item) {
                                item.patient.gender = commonService.translateGender(item.patient.sex);
                                return item;
                            });

                            // get all favorites list.
                            favoritesServices.list({
                                count: 0 // 0-> all items
                            }).then(function (result) {
                                if (result && result.data) {
                                    result.data.forEach(function (f) {
                                        resp.data.forEach(function (p) {
                                            if (p.patient.id === f.patient.id) {
                                                p.patient.isFavorite = true;
                                            }
                                        });
                                    });
                                }

                                $rootScope.$broadcast('patient.list.done', resp);
                                defer.resolve(resp);
                            }, function (err) {
                                defer.reject(err);
                            });
                        }
                        $rootScope.$broadcast('mdt.patients.list.done', resp);
                        defer.resolve(resp);
                    }, function (err) {
                        $rootScope.$broadcast('mdt.patients.list.failed', err);
                        defer.reject(err);
                    });

                return defer.promise;
            };

            /**
            * Get all healthrecords from special mdt for special patient.
            */
            function healthrecordsList(options) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.mdt, 'healthrecordsList')
                    .exec({
                        data: options,
                        skipBroadcast: true
                    }).then(function (resp) {
                        var records = resp.data && resp.data.records ? resp.data.records : [];

                        // format thumbnail uri.
                        records = healthrecordServices.makeThumbnailUri(records);

                        $rootScope.$broadcast('mdt.healthrecords.list.done', records);
                        defer.resolve(records);
                    }, function (err) {
                        $rootScope.$broadcast('mdt.healthrecords.list.failed', err);

                        defer.reject(err);
                    });

                return defer.promise;
            };

            /**
            * Get special patient information from special mdt.
            */
            function patient(options) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.mdt, 'patient')
                    .exec({
                        data: options,
                        skipBroadcast: true
                    }).then(function (resp) {
                        if (resp.result == 'RECORD_NOT_EXIST') {
                            $rootScope.$broadcast('mdt.patient.failed', { message: 'RECORD_NOT_EXIST' });
                        } else {
                            var data = resp.data;

                            $rootScope.$broadcast('mdt.patient.done', resp);
                            defer.resolve(resp);
                        }
                    }, function (err) {
                        $rootScope.$broadcast('mdt.patient.failed', err);
                        defer.reject(err);
                    });

                return defer.promise;
            };

            /**
             * Delete special patient information from special mdt.
             */
            function deletePatient(options) {
                return repoService.withFunction(repoService.repositories.mdt, 'deletePatient')
                    .exec({
                        data: options,
                        topic: 'mdt.patient.delete'
                    });
            };

            /**
            * Update special patient information from special mdt.
            */
            function addOrUpdatePatient(data, options) {
                return repoService.withFunction(repoService.repositories.mdt, 'addOrUpdatePatient')
                    .exec({
                        data: data,
                        options: options,
                        topic: 'mdt.patient.addOrUpdate'
                    });
            };

            /**
            * Build doctorship dropdown datasource.
            */
            function BuildDoctorShipDpData() {
                var results = [];
                var defer = $q.defer();

                $translate(['MDT_DOCTORSHIP_OPTION_ALL',
                    'MDT_DOCTORSHIP_OPTION_MINE',
                    'MDT_DOCTORSHIP_OPTION_OTHERS']).then(function (texts) {
                        results.push({ id: 0, name: texts['MDT_DOCTORSHIP_OPTION_ALL'] });
                        results.push({ id: 1, name: texts['MDT_DOCTORSHIP_OPTION_MINE'] });

                        defer.resolve(results);
                    }, function (e) {
                        defer.reject(e);
                    });

                return defer.promise;
            }
            /*--------end---------*/


            /*--------mdt healthrecord---------*/
            /**
            * Added healthrecords relationship to special mdt.
            */
            function addHealthrecords(data, options) {
                return repoService.withFunction(repoService.repositories.mdt, 'addHealthrecords')
                    .exec({
                        data: data,
                        options: options,
                        topic: 'mdt.healthrecords.add'
                    });
            }

            /**
            * Delete special healthrecord from special mdt.
            */
            function deleteHealthrecord(options) {
                return repoService.withFunction(repoService.repositories.mdt, 'deleteHealthrecord')
                    .exec({
                        data: options,
                        topic: 'mdt.healthrecord.delete'
                    });
            };

            /*--------end---------*/

            /*--------attendees---------*/
            /**
             * Add attendees
             */
            function addAttendees(data, options){
                return repoService.withFunction(repoService.repositories.mdt, 'addAttendees')
                    .exec({
                        data: data,
                        options: options,
                        skipBroadcast: options.skipBroadcast,
                        topic: 'attendees.add'
                    });
            };

            /**
            * get attendees
            */
            function getAttendees(options) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.mdt, 'getAttendees')
                    .exec({
                        data: options.id,
                        skipBroadcast: true
                        //topic: 'attendees.list'
                    }).then(function (resp) {
                        var attendees = [];

                        if (resp && resp.attendees && resp.attendees.length != 0) {
                            resp.attendees.map(function (v) {
                                // creator avatar
                                v.avatar = commonService.formatAvatar(v.avatar, v.userId);

                                // format gender.
                                v.gender = commonService.translateGender(v.sex);

                                return v;
                            });

                            attendees = resp.attendees;
                        }

                        if (!(options.skipBroadcast)) {
                            $rootScope.$broadcast('attendees.list.done', attendees);
                        }
                        defer.resolve(attendees);

                    }, function (error) {
                        if (!(options.skipBroadcast)) {
                            $rootScope.$broadcast('attendees.list.failed', error);
                        }
                        defer.reject(error);
                    });

                return defer.promise;
            };

            /*--------end---------*/

            /*--------temprary attendees---------*/
            /**
            * Add temprary attendees
            */
            function addTempattendees(data) {
                return repoService.withFunction(repoService.repositories.mdt, 'addTempattendees')
                    .exec({
                        data: data,
                        topic: 'tempAttendees.add'
                    });
            }

            /**
            * Delete special temprary attendence.
            */
            function deleteTempattendence(id) {
                return repoService.withFunction(repoService.repositories.mdt, 'deleteTempattendence')
                    .exec({
                        data: id,
                        topic: 'tempAttendees.delete'
                    });
            };

            /**
             * Update special temprary attendence.
             */
            function updateTempAttendee(data, options) {
                return repoService.withFunction(repoService.repositories.mdt, 'updateTempAttendee')
                    .exec({
                        data: data,
                        options: options,
                        topic: 'tempAttendees.update'
                    });
            };

            /**
             * Get temprary attendence's information by token.
             */
            function getTempattendees(options) {

                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.mdt, 'getTempattendees')
                    .exec({
                        data: options,
                        skipBroadcast: true
                    }).then(function (resp) {
                        if (resp.result == 'OK') {
                            var data = resp.data;

                            // format data so that rendering directly in UI.
                            var newData = formatMdtData([data]);
                            resp.data = newData[0];

                            $rootScope.$broadcast('mdt.anonymous.access.done', resp);
                            defer.resolve(resp);
                        } else {
                            $rootScope.$broadcast('mdt.anonymous.access.failed', { message: 'FAILED' });
                            defer.reject(resp);
                        }
                    }, function (err) {
                        $rootScope.$broadcast('mdt.anonymous.access.failed', err);
                        defer.reject(err);
                    });

                return defer.promise;

            };

            function mailTempattendees(mdtId) {
                return repoService.withFunction(repoService.repositories.mdt, 'mailTempattendees')
                    .exec({
                        data: mdtId,
                        topic: 'tempAttendees.mail'
                    });
            };
            /*--------end---------*/

            /*--------patient---------*/
            /**
             * Add patients
             */
            function addPatients(data, options){
                return repoService.withFunction(repoService.repositories.mdt, 'addPatients')
                    .exec({
                        data: data,
                        options: options,
                        topic: 'patients.add'
                    });
            };

            /**
             * Delete patients
             */
            function deletePatients(data, options) {
                return repoService.withFunction(repoService.repositories.mdt, 'deletePatients')
                    .exec({
                        data: data,
                        options: options,
                        topic: 'patients.delete'
                    });
            };

            /*--------end---------*/
            /*--------private---------*/
            function formatMdtData(data) {
                var getStatusTranslate = function (id) {

                    // default: pending
                    var text = 'MDT_LIST_STATUS_START_OR_JOIN';

                    if (id == 1) {
                        text = 'MDT_LIST_STATUS_START_OR_JOIN';
                    } else if (id == 2) {// completed
                        text = 'MDT_LIST_STATUS_COMPLETED';
                    } else if (id == 3) {// canceled
                        text = 'MDT_LIST_STATUS_REVOKE';
                    }

                    return text;
                };

                var getTypeTranslate = function (id) {
                    var checked = false;
                    if (id === 2) {
                        checked = true;
                    }
                    return checked;
                };

                if (data && typeof (data.map) == 'function') {
                    data = data.map(function (m) {
                        // conver utc date to local time.
                        if (m.createTime) {
                            m.createTime = commonService.date.utcToLocal(m.createTime);
                        }
                        var startStr = m.start;
                        if (m.start) {
                            m.start = new Date(m.start);
                        }

                        // end: start + duration
                        m.end = new Date(startStr);
                        m.end.setSeconds(m.duration * 60);

                        // format creator
                        if (m.creator) {
                            // creator avatar
                            m.creator.avatar = commonService.formatAvatar(m.creator.avatar, m.creator.userId);

                            // format gender.
                            m.creator.gender = commonService.translateGender(m.creator.sex);
                        }

                        //NORMAL MEETING or RESERVED MEETING
                        m.checked = getTypeTranslate(m.type);

                        // attendees
                        if (m.attendees && m.attendees.length != 0) {
                            m.attendees.map(function (n) {
                                // format avatar.                               
                                n.avatar = commonService.formatAvatar(n.avatar, n.userId);

                                // format gender.
                                n.gender = commonService.translateGender(n.sex);

                                return n;
                            });
                        }

                        // histories
                        if (m.histories && m.histories.length != 0) {
                            m.histories.map(function (h) {
                                // assigntime
                                if (h.assignTime) {
                                    h.assignTime = commonService.date.utcToLocal(h.assignTime);
                                }
                                // create time
                                if (h.createTime) {
                                    h.createTime = commonService.date.utcToLocal(h.createTime);
                                }
                                // end time
                                if (h.endTime) {
                                    h.endTime = commonService.date.utcToLocal(h.endTime);
                                }

                                // request time
                                if (h.requestTime) {
                                    h.requestTime = commonService.date.utcToLocal(h.requestTime);
                                }

                                // assigner
                                if (h.assigner) {
                                    // format avatar.                               
                                    h.assigner.avatar = commonService.formatAvatar(h.assigner.avatar, h.assigner.userId);

                                    // format gender.
                                    h.assigner.gender = commonService.translateGender(h.assigner.sex);
                                }

                                // creator
                                if (h.creator) {
                                    // format avatar.                               
                                    h.creator.avatar = commonService.formatAvatar(h.creator.avatar, h.creator.userId);

                                    // format gender.
                                    h.creator.gender = commonService.translateGender(h.creator.sex);
                                }

                                // doctor
                                if (h.doctor) {
                                    // format avatar.                               
                                    h.doctor.avatar = commonService.formatAvatar(h.doctor.avatar, h.doctor.userId);

                                    // format gender.
                                    h.doctor.gender = commonService.translateGender(h.doctor.sex);
                                }

                                // patient
                                if (h.patient) {
                                    // birthday time
                                    if (h.patient.birthday) {
                                        h.patient.birthday = commonService.date.utcToLocal(h.patient.birthday);
                                    }

                                    // createTime
                                    if (h.patient.createTime) {
                                        h.patient.createTime = commonService.date.utcToLocal(h.patient.createTime);
                                    }

                                    // format gender.
                                    h.patient.gender = commonService.translateGender(h.patient.sex);

                                    // format avatar.                               
                                    h.patient.avatar = commonService.formatAvatar(null, null, true);

                                    // patient creator
                                    if (h.patient.creator) {
                                        // format avatar.                               
                                        h.patient.creator.avatar = commonService.formatAvatar(h.patient.creator.avatar, h.patient.creator.userId, true);

                                        // format gender.
                                        h.patient.creator.gender = commonService.translateGender(h.patient.creator.sex);
                                    }
                                }

                                return h;
                            });
                        }

                        return m;
                    });
                }

                return data;
            }

            function combineDate(type, meetingStartDate, tfrom, tto, discussionDate, discussionTo) {
                var dateStr = '',
                    dateEnd = '',
                    result = {
                        start: '',
                        end: '',
                        duration: 0
                    };

                dateEnd = moment(discussionDate).format('YYYY-MM-DD') + ' ' + discussionTo;
                result.end = new Date(dateEnd).toISOString();

                if (type == 2) {
                    dateStr = moment(meetingStartDate).format('YYYY-MM-DD') + ' ' + tfrom,

                    // create a new date and convert to UTC string.
                    result.start = new Date(dateStr).toISOString();

                    // calc duration between tfrom and tto.
                    if (tfrom != tto) {
                        var tfroms = tfrom.split(':');
                        var ttos = tto.split(':');

                        var h = new Number(ttos[0]) - new Number(tfroms[0]);
                        var m = new Number(ttos[1]) - new Number(tfroms[1]);

                        result.duration = h * 60 + m;
                    }
                }

                return result;
            }

            function formatMdtDataForPost(mdt) {
                var obj = {};
                
                if (mdt) {
                    if (mdt.id) {
                        obj.id = mdt.id;
                    }

                    // meeting title
                    obj.title = mdt.title;

                    // meeting location
                    obj.location = mdt.location;

                    // meeting type
                    // 1-> offline
                    // 2-> video/audio meeting
                    obj.type = 2;

                    // meeting remark
                    obj.remark = mdt.remark;

                    // meeting start date 
                    obj.start = new Date(moment(mdt.dpStartDate).format('YYYY/MM/DD') + ' ' + mdt.dpStartTime.value);
                    
                    // meeting end date 
                    obj.end = new Date(moment(mdt.dpStartDate).format('YYYY/MM/DD') + ' ' + mdt.dpEndTime.value);
                    
                    // meeting duration, minute as unit 
                    obj.duration = Math.ceil((commonService.getTimespan(obj.end, obj.start)) / 60);
                    
                    // meeting attendees
                    var attendees = [];
                    if (mdt.attendees && mdt.attendees.length != 0) {
                        mdt.attendees.forEach(function (v) {
                            attendees.push({
                                id: v.id
                            });
                        });
                    }
                    obj.attendees = attendees;

                    // meeting temp attendees
                    var tempAttendees = [];
                    if (mdt.tempAttendees && mdt.tempAttendees.length != 0) {
                        mdt.tempAttendees.forEach(function (v) {
                            tempAttendees.push({
                                id: v.id
                            });
                        });
                    }
                    obj.tempAttendees = tempAttendees;

                    // meeting queueids
                    var histories = [];
                    if (mdt.pendingPatients && mdt.pendingPatients.length != 0) {
                        mdt.pendingPatients.forEach(function (v) {
                            histories.push({
                                id: v.id //history id.
                            });
                        });
                    }
                    obj.histories = histories;
                }
                
                return obj;
            }
            /*--------end---------*/

            return {
                // mdt
                list: list,
                add: add,
                detail: detail,
                remove: deleteMdt,
                update: update,
                doctorDashboardMdt: doctorDashboardMdt,
                BuildStatusDpData: BuildStatusDpData,
                BuildTimestampDpData: BuildTimestampDpData,
                calcDateDuration: calcDateDuration,
                combineDate: combineDate,

                // mdt patient
                patientsList: patientsList,
                healthrecordsList: healthrecordsList,
                patient: patient,
                deletePatient: deletePatient,
                addOrUpdatePatient: addOrUpdatePatient,
                BuildDoctorShipDpData: BuildDoctorShipDpData,

                // healthrecords
                addHealthrecords: addHealthrecords,
                deleteHealthrecord: deleteHealthrecord,

                //attendence
                addAttendees: addAttendees,
                getAttendees: getAttendees,

                // temprary attendence
                addTempattendees: addTempattendees,
                deleteTempattendence: deleteTempattendence,
                updateTempAttendee: updateTempAttendee,
                getTempattendees: getTempattendees,
                mailTempattendees: mailTempattendees,

                //patient
                addPatients: addPatients,
                deletePatients: deletePatients,
            }
        }
    ])
}());
