(function() {
    'use strict';

    angular.module('mdtServices').factory('userProfileServices', ['$rootScope',
        '$q',
        'repoService',
        function ($rootScope,
            $q,
            repoService) {

            function hasRole(role) {
                var deferred = $q.defer();

                return deferred.promise;
            }

            function hasAnyRole() {
                var deferred = $q.defer();

                return deferred.promise;
            }

            function isAnonymous() {
                var deferred = $q.defer();

                return deferred.promise;
            }

            function isAuthenticated() {
                var deferred = $q.defer();

                return deferred.promise;
            }

            return {
                hasRole: hasRole,
                hasAnyRole: hasAnyRole,
                isAnonymous: isAnonymous,
                isAuthenticated: isAuthenticated
            };
        }
    ])
}());
