(function () {
    'use strict';

    angular.module('mdtServices').factory('rolesServices', [
        '$q', '$rootScope', '$location', 'repoService', 'commonService',
        function ($q, $rootScope, $location, repoService, commonService) {
            /**
            * Get all users by query strings.
            */
            function list(options) {
                return repoService.withFunction(repoService.repositories.role, 'list')
                    .exec({
                        data: options,
                        topic: 'role.list'
                    });
            };

            /**
            * 
            */
            function listValues() {
                return repoService.withFunction(repoService.repositories.role, 'listValues')
                    .exec({
                        data: {},
                        topic: 'role.listValues'
                    });
            }

            /**
            * Add a new role.
            */
            function add(role) {
                return repoService.withFunction(repoService.repositories.role, 'add')
                   .exec({
                       data: role,
                       topic: 'role.add'
                   });
            }

            /**
            * Get role information by role id.
            */
            function detail(id) {
                return repoService.withFunction(repoService.repositories.role, 'detail')
                    .exec({
                        data: id,
                        skipBroadcast: true
                        //topic: 'role.detail'
                    }).then(function (resp) {
                        if (resp.result == 'RECORD_NOT_EXIST') {
                            $rootScope.$broadcast('role.detail.failed', { message: 'RECORD_NOT_EXIST' });
                        } else {
                            $rootScope.$broadcast('role.detail.done', resp);
                        }
                    }, function (err) {
                        $rootScope.$broadcast('role.detail.failed', err);
                    });
            }

            /**
            * Delete role by role id
            */
            function deleteRole(id) {
                var defer = $q.defer();
                 repoService.withFunction(repoService.repositories.role, 'remove')
                    .exec({
                        data: id,
                        skipBroadcast: true
                        //topic: 'role.remove'
                    }).then(function (data) {
                        if (data.result == 'FAILED') {
                            defer.reject(data);
                            $rootScope.$broadcast('role.remove.failed', data);
                        } else {
                            defer.resolve(data);
                            $rootScope.$broadcast('role.remove.done', data);
                        }                       
                    }, function (err) {
                        defer.reject(err);
                        $rootScope.$broadcast('role.remove.failed', err);
                    });

                 return defer.promise;
            }

            /**
            * Update role information
            */
            function update(role) {
                return repoService.withFunction(repoService.repositories.role, 'update')
                   .exec({
                       data: role,
                       topic: 'role.update'
                   });
            }

            /**
            * get detail permission list by permission ids array.
            */
            function permissions(ids) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.systemMisc, 'permissionList')
                     .exec({
                         data: {},
                         skipBroadcast: true
                     }).then(function (data) {
                         if (data && data.length != 0) {
                             // set all checked value to false as default value.
                             data.forEach(function (gp) {
                                 gp.data.forEach(function (p) {
                                     p.checked = false;
                                 });
                             });

                             if (ids && ids.length != 0) {
                                 ids.forEach(function (id) {
                                     var index = -1;
                                     data.forEach(function (gp) {
                                         for (var i = 0; i < gp.data.length; i++) {
                                             if (gp.data[i].id === id) {
                                                 index = i;
                                                 break;
                                             }
                                         }

                                         if (index != -1) {
                                             gp.data[index].checked = true;
                                             index = -1;
                                         }
                                     });
                                 });
                             }
                         }

                         defer.resolve(data);

                         $rootScope.$broadcast('permission.filter.list.done', data);

                     }, function (err) {
                         defer.reject(err);
                         $rootScope.$broadcast('permission.filter.list.failed', err);
                     });

                return defer.promise;
            }

            function getCheckedPermissionIds(permissions) {
                var results = [];

                if (permissions && permissions.length != 0) {
                    permissions.forEach(function (gp) {
                        if (gp.data && gp.data.length != 0) {
                            gp.data.forEach(function (p) {
                                if (p.checked) {
                                    results.push(p.id);
                                }
                            });
                        }
                    });
                }

                return results;
            }

            return {
                list: list,
                add: add,
                detail: detail,
                remove: deleteRole,
                update: update,
                listValues: listValues,
                permissions: permissions,
                getCheckedPermissionIds: getCheckedPermissionIds
            }
        }
    ])
}());
