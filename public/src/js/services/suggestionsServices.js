(function () {
    'use strict';

    angular.module('mdtServices').factory('suggestionsServices', [
        '$q',
        '$rootScope',
        '$location',
        '$translate',
        'repoService',
        'commonService',
        'entityServices',
        function ($q,
            $rootScope,
            $location,
            $translate,
            repoService,
            commonService,
            entityServices) {
            /**
            * Get all suggestions by query strings.
            */
            function list(params, skipBroadcast) {
                var options = {
                    doctor: 0,  // doctor id, 0-> all
                    mdt: 0,  // mdt id, 0-> all
                    history: 0 // history id, 0-> all
                };
                var defer = $q.defer();

                params = angular.extend(options, params);
                repoService.withFunction(repoService.repositories.suggestion, 'list')
                    .exec({
                        data: params,
                        skipBroadcast: true
                        //topic: 'suggestions.list'
                    }).then(function (resp) {
                        if (!resp) {
                            resp = [];
                        }

                        if (resp.map) {
                            resp.map(function (v) {                               
                                return formatSuggestion(v);
                            });
                        }

                        defer.resolve(resp);

                        if (!skipBroadcast) {
                            $rootScope.$broadcast('suggestions.list.done', resp);
                        }                        
                    }, function (err) {                       
                        if (!skipBroadcast) {
                            $rootScope.$broadcast('suggestions.list.failed');
                        }

                        defer.reject(err);
                    });

                return defer.promise;
            };

            /**
            * Add a new suggestion.
            */
            function add(suggestion) {               
                return repoService.withFunction(repoService.repositories.suggestion, 'add')
                    .exec({
                        data: suggestion,
                        topic: 'suggestion.add'
                    });
            }

            /**
            * Update a existing suggestion.
            */
            function update(id, suggestion) {               
                return repoService.withFunction(repoService.repositories.suggestion, 'update')
                    .exec({
                        data: id,
                        options: suggestion,
                        topic: 'suggestion.update'
                    });
            }

            /**
            * Get suggestion information by patient id.
            */
            function detail(id) {
                return repoService.withFunction(repoService.repositories.suggestion, 'detail')
                    .exec({
                        data: id,
                        topic: 'suggestion.detail'
                    });
            }

            /**
             * Convert original data format to expected format for rendering.
             */
            function formatSuggestion(suggestion) {
                if (!suggestion) {
                    return;
                }

                // conver utc date to local time.
                suggestion.createTime = commonService.date.utcToLocal(suggestion.createTime);

                // creator
                if (suggestion.doctor) {
                    // creator avatar
                    suggestion.doctor.avatar = commonService.formatAvatar(suggestion.doctor.avatar, suggestion.doctor.userId);

                    // format gender.
                    suggestion.doctor.gender = commonService.translateGender(suggestion.doctor.sex);
                }

                return suggestion;
            }

            return {
                list: list,
                add: add,
                update: update,
                detail: detail,
                formatSuggestion: formatSuggestion
            }
        }
    ])
}());
