(function () {
    'use strict';

    angular.module('mdtServices').factory('systemMiscServices', [
        '$rootScope', '$location', 'repoService', 'commonService',
        function ($rootScope, $location, repoService, commonService) {
            /**
            * Get all users by query strings.
            */
            function permissionList(options) {
                return repoService.withFunction(repoService.repositories.systemMisc, 'permissionList')
                    .exec({
                        data: options,
                        topic: 'permission.list'
                    });
            };           

            return {
                permissionList: permissionList
            }
        }
    ])
}());
