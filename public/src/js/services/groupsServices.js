(function () {
    'use strict';

    angular.module('mdtServices').factory('groupsServices', [
        '$q',
        '$rootScope',
        '$location',
        '$translate',
        'repoService',
        'commonService',
        'entityServices',
        'strings',
        function ($q,
            $rootScope,
            $location,
            $translate,
            repoService,
            commonService,
            entityServices,
            strings) {

            /*-----------Groups--------------*/
            /**
            * Get all groups by query strings.
            */
            function list(params, skipBroadcast) {
                var options = {
                    count: 20
                };
                var defer = $q.defer();

                params = angular.extend(options, params);
                repoService.withFunction(repoService.repositories.group, 'list')
                    .exec({
                        data: params,
                        skipBroadcast: true
                        //topic: 'group.list'
                    }).then(function (result) {
                        if (result && result.data) {
                            result.data.map(function (item) {
                                item.message = formatMessage(item.message);
                                return item;
                            });
                        }

                        defer.resolve(result);
                        if (!skipBroadcast) {
                            $rootScope.$broadcast('group.list.done', result);
                        }

                    }, function (err) {
                        commonService.progress.hide();
                        defer.reject(err);
                        if (!skipBroadcast) {
                            $rootScope.$broadcast('group.list.failed', err);
                        }
                    });

                return defer.promise;
            };

            /**
            * Add a new group.
            */
            function add(membersId, skipBroadcast) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.group, 'add')
                    .exec({
                        data: membersId,
                        skipBroadcast: true,
                        //topic: 'group.add'
                    }).then(function (resp) {
                        switch (resp.result) {
                            case strings.jsonResults.OK.text: {
                                defer.resolve(resp);
                                if (!skipBroadcast) {
                                    $rootScope.$broadcast('group.add.done', resp);
                                }
                                break;
                            }
                            default: {
                                commonService.resource.getValues(['SHARE_FAILED']).then(function (source) {
                                    resp.text = source['SHARE_FAILED'];
                                    defer.reject(resp);
                                    if (!skipBroadcast) {
                                        $rootScope.$broadcast('group.add.failed', resp);
                                    }
                                });

                                break;
                            }
                        }
                    }, function (err) {
                        commonService.resource.getValues(['UNKNOWN_FAILED']).then(function (source) {
                            err.text = source['UNKNOWN_FAILED'];
                            defer.reject(err);
                            if (!skipBroadcast) {
                                $rootScope.$broadcast('group.message.failed', err);
                            }
                        });
                    });

                return defer.promise;
            }

            /**
            * Get group information by group id.
            */
            function detail(id) {
                var defer = $q.defer();

                repoService.withFunction(repoService.repositories.group, 'detail')
                    .exec({
                        data: id,
                        skipBroadcast: true
                    }).then(function (result) {
                        result = formatGroupItem(result);

                        defer.resolve(result);
                        $rootScope.$broadcast('group.detail.done', result);

                    }, function (err) {
                        commonService.progress.hide();
                        defer.reject(err);
                        $rootScope.$broadcast('group.detail.failed', err);
                    });

                return defer.promise;
            }

            /**
            * Delete group by id
            */
            function del(id) {
                return repoService.withFunction(repoService.repositories.group, 'remove')
                    .exec({
                        data: id,
                        topic: 'group.remove'
                    });
            }

            /**
            * Update group information
            */
            function update(group) {
                return repoService.withFunction(repoService.repositories.group, 'update')
                    .exec({
                        data: group,
                        topic: 'group.update'
                    });
            }
            /*-----------end--------------*/

            /*-----------group memebers--------------*/
            /**
            * Add members to speical group.
            */
            function addMembers(groupId, members) {
                return repoService.withFunction(repoService.repositories.group, 'addMembers')
                    .exec({
                        options: groupId,
                        data: members,
                        topic: 'group.members.add'
                    });
            }

            /**
            * Delete memebers from special group
            */
            function delMembers(groupId, members) {
                return repoService.withFunction(repoService.repositories.group, 'delMembers')
                    .exec({
                        options: groupId,
                        data: members,
                        topic: 'group.members.remove'
                    });
            }

            /**
            * Get all memebers for special group.
            */
            function listMembers(params, skipBroadcast) {
                var options = {
                    count: 20
                };

                var defer = $q.defer();

                params = angular.extend(options, params);
                repoService.withFunction(repoService.repositories.group, 'listMembers')
                    .exec({
                        data: params,
                        skipBroadcast: true
                        //topic: 'group.members.list'
                    }).then(function (result) {
                        if (result) {
                            result.map(function (item) {
                                // avatar
                                item.avatar = commonService.formatAvatar(item.avatar, item.userId);

                                // format gender.
                                item.gender = commonService.translateGender(item.sex);

                                return item;
                            });
                        }

                        defer.resolve(result);
                        if (!skipBroadcast) {
                            $rootScope.$broadcast('group.members.list.done', result);
                        }
                        
                    }, function (err) {
                        commonService.progress.hide();
                        defer.reject(err);
                        if (!skipBroadcast) {
                            $rootScope.$broadcast('group.members.list.failed', err);
                        }
                    });

                return defer.promise;
            };
            /*-----------end--------------*/

            /*-----------group message--------------*/
            /**
            * Send message.
            */
            function sendMessage(groupId, data, skipBroadcast) {
                var defer = $q.defer();
                repoService.withFunction(repoService.repositories.group, 'sendMessage')
                    .exec({
                        options: groupId,
                        data: data,
                        skipBroadcast: true
                        //topic: 'group.message.add'
                    }).then(function (resp) {
                        switch (resp.result) {
                            case strings.jsonResults.OK.text: {
                                defer.resolve(resp);
                                if (!skipBroadcast) {
                                    $rootScope.$broadcast('group.message.add.done', resp);
                                }
                                break;
                            }                           
                            default: {
                                commonService.resource.getValues(['SHARE_FAILED']).then(function (source) {
                                    resp.text = source['SHARE_FAILED'];
                                    defer.reject(resp);
                                    if (!skipBroadcast) {
                                        $rootScope.$broadcast('group.message.add.failed', resp);
                                    }
                                });
                               
                                break;
                            }
                        }
                    }, function (err) {
                        commonService.resource.getValues(['UNKNOWN_FAILED']).then(function (source) {
                            err.text = source['UNKNOWN_FAILED'];
                            defer.reject(err);
                            if (!skipBroadcast) {
                                $rootScope.$broadcast('group.message.add.failed', err);
                            }
                        });                        
                    });

                return defer.promise;
            }

            /**
            * list messages.
            */
            function listMessages(groupId, params) {
                var options = {
                    size: 20,
                    page: 1
                    //ordertype: 1 // 0->desc, 1-> asc
                };

                var defer = $q.defer();

                params = angular.extend(options, params);

                repoService.withFunction(repoService.repositories.group, 'listMessages')
                    .exec({
                        options: params,
                        data: groupId,
                        skipBroadcast: true
                        //topic: 'group.message.list'
                    }).then(function (result) {
                        if (result && result.data) {
                            result.data.map(function (item) {
                                return formatMessage(item);
                            });
                        }

                        defer.resolve(result);
                        $rootScope.$broadcast('group.messages.list.done', result);
                    }, function (err) {
                        commonService.progress.hide();
                        defer.reject(err);
                        $rootScope.$broadcast('group.messages.list.failed', err);
                    });

                return defer.promise;
            }
            /*-----------end--------------*/

            /*-------private-------*/
            function formatGroupItem(groupEntity) {
                if (groupEntity && groupEntity.members) {
                    groupEntity.members.map(function (m) {
                        // format avatar.
                        m.avatar = commonService.formatAvatar(m.avatar, m.userId);
                        return m;
                    });
                }

                if (groupEntity && groupEntity.messages) {
                    groupEntity.messages.map(function (m) {
                        return formatMessage(m);
                    });
                }

                return groupEntity;
            }

            function formatMessage(message) {
                if (!message) {
                    return;
                }

                // format message creator avatar.
                if (message.creator) {
                    message.creator.avatar = commonService.formatAvatar(message.creator.avatar, message.creator.userId);
                }

                // format attachments.
                if (message.attaches && message.attaches.length != 0) {
                    message.attaches.map(function (at) {
                        // healthrecord.
                        if (at.attachtype === 1) {
                            at.url = commonService.formatHealthrecord(at.attachid);
                        } else if (at.attachtype === 2) { // snapshot
                            at.url = commonService.formatSnapshot(at.attachid);
                        }
                        return at;
                    });
                }

                // format patients
                if (message.patients && message.patients.length != 0) {
                    message.patients.map(function (p) {
                        // format gender.
                        p.gender = commonService.translateGender(p.sex);

                        // avatar.
                        p.avatar = commonService.formatAvatar(null, null, true);

                        return p;
                    });
                }

                return message;
            }

            /*----------end--------*/
            return {
                // group
                list: list,
                add: add,
                detail: detail,
                remove: del,
                update: update,

                // members
                addMembers: addMembers,
                delMembers: delMembers,
                listMembers: listMembers,

                // message
                sendMessage: sendMessage,
                listMessages: listMessages,
                formatMessage: formatMessage
            }
        }
    ])
}());
