(function () {
    'use strict';

    angular.module('mdtServices').factory('loadingServices', ['$rootScope', function ($rootScope) {
        return {
            show: function () {
                $rootScope.$broadcast('iloadingbar.loading', {});
            },
            hide: function () {
                $rootScope.$broadcast('iloadingbar.loaded', {});
            }
        };
    }])
}());
