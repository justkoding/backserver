(function () {
    'use-strict';
    angular.module('mdtServices').factory('configModel', [
        '$rootScope',
        '$injector',
        '$templateCache',
        '$location',
        'cachedTemplates',
        function (
            $rootScope,
            $injector,
            $templateCache,
            $location,
            cachedTemplates) {
            var getBaseUrl = function (ws) {
                //'http://localhost:9000'
                var protocol = ws ? 'ws' : $location.protocol();
                var host = $location.host();
                var port = $location.port();

                return protocol + '://' + host + ':' + port;
            };

            var config = {
                initialize: function () {
                    cachedTemplates.init();
                },

                configure: function () { },

                debug: true,
                userImageSize: {
                    min: 10,
                    max: 2048
                },
                defaultTheme: 'skin-blur-chrome',
                baseUrl: getBaseUrl(),
                wsBaseUrl: getBaseUrl(true),
                localUrl: '/localData'
            };

            config.initialize();

            if (config.debug) {
                angular.$debugRootScope = $rootScope;
            }

            return config;
        }
    ]);
}());
