﻿(function () {
    'use strict';

    angular.module('mdtServices').factory('localDataPaginationService', [
		'$q', function ($q) {
		    var params = {
		        dataSource: [],
		        page: 1,
		        size: 5
		    };

		    function paging(options) {
		        var opt = angular.extend(options, params),
		            defer = $q.defer(),
		            arr = [],
		            start = 0,
		            end = 0;

		        if (opt.dataSource.length == 0) {
		            defer.resolve([]);
		            return defer.promise;
		        }

		        start = (opt.page - 1) * opt.size + 1;
		        end = opt.page * opt.size;

		        if (end > opt.dataSource.length) {
		            end = opt.dataSource.length;
		        }

		        for (var i = start; i < end; i++) {
		            arr.push(opt.dataSource[i]);
		        }

		        defer.resolve(arr);

		        return defer.promise;
		    }

		    return {
		        paging: paging
		    }
		}]);
}());