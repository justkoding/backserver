(function() {
    'use strict';

    angular.module('mdtServices').factory('cachedTemplates', ['$templateCache',
        function($templateCache) {
            var templates = [
                new Template('agTypehead/ahead.tpl.html', '<h1>hello, world</h1>')
            ];

            var init = function () {
                angular.forEach(templates, function (tmpl, key) {
                    $templateCache.put(tmpl.url, tmpl.html);
                });               
            };

            function Template(url, html) {
                this.html = html;
                this.url = url;
            };

            return {
                init: init
            };
        }
    ]);
})();
