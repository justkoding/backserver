﻿(function () {
    'use strict';

    angular.module('mdtServices').factory('dataService', [
        '$timeout',
        '$http',
        '$translate',
        'configModel',
        '$window',
        'ajaxMessageService',
        '$exceptionHandler',
        'commonService',
        function ($timeout,
            $http,
            $translate,
            configModel,
            $window,
            ajaxMessageService,
            $exceptionHandler,
            commonService) {
            function post(url, data, options) {
                return $http.post(configModel.baseUrl + url, data, options).success(function (response) {
                    ajaxMessageService.show(response, options);
                    return response;
                }).error(function (error) {
                    ajaxMessageService.show(error, options);
                    return error;
                });
            }

            function get(url, data, options) {
                return getExternal(configModel.baseUrl + url + commonService.getQueryParams(data), options);
            }

            function getLocal(url) {
                return $http.get(configModel.localUrl + url);
            }

            function getExternal(url, options) {
                return $http.get(url).success(function (response) {
                    ajaxMessageService.show(response, options);
                    return response;
                }).error(function (error) {
                    ajaxMessageService.show(error, options);
                    return error;
                });
            }

            function put(url, data, options) {
                return $http.put(configModel.baseUrl + url, data).success(function (response) {
                    ajaxMessageService.show(response, options);
                    return response;
                }).error(function (error) {
                    ajaxMessageService.show(error, options);
                    return error;
                });
            }

            function del(url, data, options) {
                return $http.delete(configModel.baseUrl + url + commonService.getQueryParams(data)).success(function (response) {
                    ajaxMessageService.show(response, options);
                    return response;
                }).error(function (error, status) {
                    ajaxMessageService.show(error, options);
                    return error;
                });
            }

            function parseErrors(errorObject) {

                if (typeof errorObject === 'string') {
                    return errorObject;
                }

                var errors = errorObject.errors,
                    msg = null;

                if (errors) {
                    errors.forEach(function (error) {
                        msg += '<strong>' + error.field + '</strong> : ' + error.message + '<br />';
                    });
                }

                return msg ? msg : '<strong>Invalid data format</strong>';
            }

            return {
                post: post,
                get: get,
                put: put,
                delete: del,

                getLocal: getLocal,
                getExternal: getExternal
            };
        }
    ]);
}());
