﻿(function () {
    'use strict';

    angular.module('mdtServices').factory('ajaxMessageService', ['$translate',
        'strings',
		'messageService',
        function ($translate,
            strings,
            messageService) {

            /**
             * data: object.({result:xxx})
             * options:
             *  - title: message title
             *  - body: message body
             */
            function showMessage(data, options) {
                // check param.
                if (!data) {
                    return;
                }

                // get message property item.
                var item = null;
                for (var jr in strings.jsonResults) {
                    if (data.result === strings.jsonResults[jr].text) {
                        item = strings.jsonResults[jr];
                        break;
                    }
                }

                // show message.
                var defaultOpt = {
                    ignore: true,
                    failIgnore: false
                };

                options = options ? options : defaultOpt;

                if (item) {
                    var title = options.title;
                    var body = options.body;
                    var ignore = options.ignore;

                    if (item.text === 'OK') {
                        if (!ignore) {
                            title = title ? title : item.value;
                            body = body ? body : 'MESSAGE_BODY_SUCCESS';

                            $translate([title, body]).then(function (texts) {
                                messageService.success(texts[title], texts[body]);
                            });
                        }
                    } else {
                    	if (!options.failIgnore) {
	                        title = title ? title : item.value;
	                        body = body ? body : 'MESSAGE_BODY_FAILED';
	
	                        $translate([title, body]).then(function (texts) {
	                            messageService.error(texts[title], texts[body]);
	                        });
                    	}
                    }
                }
            }

            return {
                show: showMessage
            }
        }]);
}());