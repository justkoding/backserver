﻿(function () {
    'use strict';

    angular.module('mdtServices').factory('modalService', [
		'$imodal', function ($imodal) {

		    var options = {
		        data: {},
		        controller: 'ModalInstanceCtrl',
		        backdrop: 'static',
		        closefn: function () { },
		        dismissfn: function () { },
		    };

		    function showModal(url, opts) {
		        opts = angular.extend({}, options, opts);

		        var modalInstance = $imodal.open({
		            templateUrl: url,
		            controller: opts.controller,
		            windowClass: opts.windowClass,
		            backdrop: opts.backdrop,
		            resolve: {
		                data: function () {
		                    return opts.data;
		                }
		            }
		        });

		        modalInstance.result.then(function (data) {
		            opts.closefn(data);
		        }, function (err) {
		            opts.dismissfn(err);
		        });
		    }

		    function toCenter(obj) {
		        if (obj) {
		            var h = (window.document.documentElement.clientHeight - obj.offsetHeight) / 2;
		            obj.style.marginTop = h + 'px';
		            console.log('ch: ' + window.document.documentElement.clientHeight + 'oh: ' + obj.offsetHeight + 'h: ' + h);
		        }
		    };

		    return {
		        showModal: showModal,
		        toCenter: toCenter
		    };
		}]);
}());