﻿(function() {
    'use strict';

    angular.module('mdtServices').factory('commonService', [
        '$rootScope',
        '$translate',
        '$q',
        '$location',
        '$cookieStore',
        '$sessionStorage',
        '$window',
        '$exceptionHandler',
        'entityServices',
        'loadingServices',
        'modalService',
        function($rootScope,
            $translate,
            $q,
            $location,
            $cookieStore,
            $sessionStorage,
            $window,
            $exceptionHandler,
            entityServices,
            loadingServices,
            modalService) {

            /**
             * Setup dropdown list data source
             */
            function buildStatusDataSource(fn) {
                var data = [],
                    defer = $q.defer();

                loadResources(['TEXT_DP_LIST_CLEAR',
                    'USER_STATUS_ENABLE',
                    'USER_STATUS_DISENABLE'
                ]).then(function(texts) {

                    data.push(new entityServices.DpListItem(texts['USER_STATUS_CLEAR'], -1));
                    data.push(new entityServices.DpListItem(texts['USER_STATUS_ENABLE'], 1));
                    data.push(new entityServices.DpListItem(texts['USER_STATUS_DISENABLE'], 2));

                    fn && fn(data);

                    defer.resolve(data);
                });

                return defer.promise;
            }

            /**
             * mock hosptial select control data.
             */
            function buildHosptialDataSource(fn) {
                var data = [],
                    defer = $q.defer();

                loadResources(['TEXT_HOSPITAL_DEFAULT_OPTION']).then(function(texts) {
                    data.push(new entityServices.DpListItem(texts['TEXT_HOSPITAL_DEFAULT_OPTION'], -1));
                    data.push(new entityServices.DpListItem('北京清华长庚医院', 1));

                    fn && fn(data);

                    defer.resolve(data);
                });

                return defer.promise;
            }

            /**
             * build dropdown placeholder item
             */
            function buildDpPlaceHolder(translate, value) {
                var defer = $q.defer();

                loadResources([translate]).then(function(texts) {
                    defer.resolve({
                        name: texts[translate],
                        id: value
                    });
                });

                return defer.promise;
            }

            /**
             * Join array to a string with special symbol, by default ','
             */
            function joinBy(arr, symbol, fn) {
                symbol = symbol || ',';
                var str = '';

                if (fn) {
                    str = fn(arr, symbol);
                } else {
                    str = arr.join(symbol);
                }

                return str;
            }

            /**
             * get index of special item in special array.
             */
            function getIndexOf(item, arr, fn) {
                var index = -1;

                if (item && arr && arr.length != 0) {
                    if (fn) {
                        index = fn(item, arr);
                    } else {
                        //index = arr.indexOf(item);
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i] && arr[i].id === item.id) {
                                index = i;
                                break;
                            }
                        }
                    }
                }

                return index;
            }

            /**
             * Convert byte size to KB or MB.
             */
            function toFileSize(size) {
                var s = size,
                    rate = 1024;

                if (angular.isNumber(size)) {
                    // check if using MB as unit
                    if (size >= rate * rate) {
                        s = size / (rate * rate);
                        s = s.toFixed(2) + 'MB';
                    } else {
                        s = size / rate;
                        s = s.toFixed(0) + 'KB';
                    }
                }

                return s;
            }

            /**
             * Merge and remove duplicate file between two arrays.
             */
            function mergeFiles(arr1, arr2) {
                var files = arr2 || [];

                if (arr1 && arr1.length != 0) {

                    if (files.length != 0) {
                        // check if it is existing in oldVal,
                        // ignore if existed else add it. 
                        arr1.forEach(function(nv) {
                            var index = indexOfFiles(nv, files);

                            // don't find existing same file if index is -1.
                            if (index == -1) {
                                files.push(nv);
                            }
                        });

                    } else {
                        files = arr1;
                    }
                }

                return files;
            }

            /**
             * Compare if two array are equal.
             */
            function equalArray(arr1, arr2) {
                var same = false;

                // get its name, size and type for each file and make a new object
                // and then push it in a new array and return.
                var make = function(files) {
                    var arr = [];

                    if (files && files.length != 0) {
                        files.forEach(function(f) {
                            arr.push({
                                name: f.name,
                                size: f.size,
                                type: f.type
                            });
                        });
                    }

                    return arr;
                };

                arr1 = arr1 || [];
                arr2 = arr2 || [];

                if (arr1.length == 0) {
                    same = arr2.length == 0;
                } else {
                    if (arr2.length == 0) {
                        same = false;
                    } else if (arr1.length != arr2.length) {
                        same = false;
                    } else {
                        // if both are not empty and item count is same, should compare each item in array.
                        // compare the 3 properties for each item.
                        // - name
                        // - size
                        // - type

                        // do deep copy
                        var cArr1 = make(arr1);
                        var cArr2 = make(arr2);

                        while (cArr1.length != 0) {
                            var item = cArr1.pop();

                            cArr2.forEach(function(ca, i) {
                                if (ca.name == item.name &&
                                    ca.size == item.size &&
                                    ca.type == item.type) {
                                    cArr2.splice(i, 1);
                                }
                            });
                        }

                        // if all items in cArr2 are deleted, means the two arrays are same.
                        same = cArr2.length === 0;
                    }
                }

                return same;
            }

            /**
             * Get special file index in speical files array.
             */
            function indexOfFiles(file, files) {
                var index = -1;
                for (var i = 0; i < files.length; i++) {
                    if (files[i].name === file.name) {
                        index = i;
                        break;
                    }
                }

                return index;
            }

            /**
             * Verify if specified value is valid date format.
             */
            function isDate(value) {
                if (!value) {
                    return false;
                }

                var dt = new Date(value);

                return dt.toString() !== 'Invalid Date';
            }

            /**
             * Translate gender code
             * code: 1-> male, 2->female
             */
            function translateGender(code) {
                var gender = 'GENDER_FEMALE';

                if (code && code == 1) {
                    gender = 'GENDER_MALE';
                }

                return gender;
            }

            /**
             * format avatar url.
             */
            function formatAvatar(avatar, userId, isPatient) {
                return avatar ? '/users/' + userId + '/avatar' :
                    (isPatient ? '/assets/src/img/icons/patient.png' : '/assets/src/img/icons/doctor.png');
            }

            /**
             * format snapshots url.
             */
            function formatSnapshot(id) {
                return '/snapshots/' + id + '/download';
            }

            /**
             * format healthrecord url.
             */
            function formatHealthrecord(id) {
                return '/healthrecords/' + id + '/thumbnail';
            }

            /**
             * compare with currently datetime, return the timespan with seconds.
             */
            function getTimespan(first, second) {
                var now = new Date();
                var ts;

                if (typeof(first) === 'string') {
                    first = new Date(first);
                }

                if (second) {
                    if (typeof(second) === 'string') {
                        now = new Date(second);
                    } else if (angular.isDate(second)) {
                        now = second;
                    }
                }

                ts = TimeSpan.FromDates(now, first, true);

                return ts.totalSeconds();
            }

            /**
             * Get current login user.
             */
            function getCurrentUser() {
                var user = $rootScope.currentUser || $cookieStore.get('user');

                if (!$rootScope.currentUser) {
                    $rootScope.currentUser = user;
                }
                return user;
            }

            /**
             * Get role information of current login user.
             */
            function getRoleInfoOfCurrentUser() {
                var user = getCurrentUser() || {
                    account: {}
                };
                return user.account.roles;
            }

            /**
             * Get permission information of current login user.
             */
            function getPermissionOfCurrentUser() {
                var user = getCurrentUser() || {
                    account: {}
                };
                return user.account.permissions;
            }

            /**
             * Get user type of current login user.
             */
            function getTypeInfoOfCurrentUser() {
                var user = getCurrentUser() || {
                    account: {}
                };
                return user.account.userType;
            };

            /**
             * Clear cookie.
             */
            function clearCookie() {
                $rootScope.currentUser = null;
                $cookieStore.put('user', null);
                $cookieStore.put('accounts', null);
            }

            /**
             * Convert utc date to local date.
             */
            function convertUtcToLocal(utcDate) {
                if (!utcDate) {
                    return;
                }

                //if (typeof (utcDate) === 'string') {
                //    utcDate = new Date(utcDate);
                //}

                //var newDate = new Date(utcDate.getTime() + utcDate.getTimezoneOffset() * 60 * 1000);

                //var offset = utcDate.getTimezoneOffset() / 60;
                //var hours = utcDate.getHours();

                //newDate.setHours(hours - offset);

                return moment.utc(utcDate).toDate();
            };

            /**
             * Convert local date to utc.
             */
            function toUtc(date) {
                if (!date) {
                    return;
                }

                return moment.utc(utcDate);
            }

            /**
             * format date to string.
             */
            function formatDate(date, format) {
                var defaultOpt = format || 'l';
                return moment(date).format(defaultOpt).split('-').join('/');
            }

            /**
             * Save value to session
             */
            function setTempData(key, value) {
                if (key) {
                    $sessionStorage[key] = value;
                }
            }

            /**
             * Get value from session
             */
            function getTempData(key) {
                var value = null;
                if (key) {
                    value = $sessionStorage[key];
                }

                return value;
            }

            /**
             * Reset value from session
             */
            function resetTempData(key) {
                key ? $sessionStorage.$reset(key) : $sessionStorage.$reset();
            }

            /**
             * Confirm which its type is 'deleting'.
             */
            function deleteConfirm(opt, fn) {
                var title = (opt && opt.title) || 'COMMON_DELETE_CONFIRM_TITLE';
                var body = (opt && opt.body) || 'COMMON_DELETE_CONFIRM_BODY';

                modalService.showModal('/assets/src/pages/confirm/confirm.html', {
                    controller: 'ConfirmCtrl',
                    data: {
                        title: title,
                        body: body
                    },
                    closefn: function() {
                        fn && fn(opt);
                    }
                });
            }

            /**
             * load source from resource files.
             */
            function loadResources(key) {
                var defer = $q.defer();
                var sessionKey = 'sources';

                // not null.
                if (key) {
                    var arr = [];
                    //if not array.
                    if (!(angular.isArray(key))) {
                        arr.push(key);
                    } else {
                        arr = key;
                    }

                    // check whether was cached in session.
                    var sources = getTempData(sessionKey);
                    var isCached = false;
                    if (sources) {
                        for (var i = 0; i < arr.length; i++) {
                            if (!sources[arr[i]]) {
                                isCached = false;
                                break;
                            }
                            if (i = arr.length - 1) {
                                isCached = true;
                            }
                        }
                    }

                    // if cached, return directly.
                    if (isCached) {
                        defer.resolve(sources);
                    } else {
                        // retrieve by $translate service
                        $translate(arr).then(function(temps) {
                            var data = angular.extend({}, temps, sources);

                            // update session.
                            setTempData(sessionKey, data);

                            // return to caller.
                            defer.resolve(data);
                        }, function(error) {
                            defer.reject(error);
                        });
                    }
                } else {
                    defer.reject();
                }

                return defer.promise;
            }

            /**
             * Full Time a day from 00:00 to 23:30
             */
            function buildFullTimeDataSource() {
                var times = [];
                var mins = ['00', '30'];

                for (var i = 0; i < 24; i++) {
                    mins.forEach(function(m) {
                        var value = (i < 10 ? '0' + i : i) + ':' + m;
                        times.push({
                            value: value,
                            text: value
                        });
                    });
                }

                return times;
            }

            /**
             * Offical working time from 08:00 to 18:00
             */
            function buildWorkingTimeDataSource() {
                var times = [];
                var mins = ['00', '30'];

                var now = new Date();
                var hour = now.getHours(),
                    min = now.getMinutes();

                if (min < 30) {
                    var value = (hour < 10 ? '0' + hour : hour) + ':' + '30';
                    times.push({
                        value: value,
                        text: value
                    });
                }

                for (var i = (hour + 1); i < 19; i++) {
                    mins.forEach(function(m) {
                        var value = (i < 10 ? '0' + i : i) + ':' + m;
                        times.push({
                            value: value,
                            text: value
                        });
                    });
                }

                return times;
            }

            /**
             * Offical working time from 08:00 to 18:00
             */
            function editWorkingTimeDataSource() {
                var times = [];
                var mins = ['00', '30'];

                for (var i = 8; i < 19; i++) {
                    mins.forEach(function(m) {
                        var value = (i < 10 ? '0' + i : i) + ':' + m;
                        times.push({
                            value: value,
                            text: value
                        });
                    });
                }

                return times;
            }

            /**
             * return to last location
             */
            function historyBack() {
                $window.history.back();
            }

            /**
             * Check whether it contains speical role.
             */
            function hasRole(role) {
                var index = -1;
                var userRoles = getRoleInfoOfCurrentUser();

                if (role && userRoles && userRoles.length != 0) {
                    for (var i = 0; i < userRoles.length; i++) {
                        if (userRoles[i].toLowerCase() === role.toLowerCase()) {
                            index = i;
                            break;
                        }
                    }
                }

                return index != -1;
            }

            /**
             * Check whether it contains speical permission.
             */
            function hasPermission(permission) {
                var index = -1;
                var userPermissions = getPermissionOfCurrentUser();

                if (permission && userPermissions && userPermissions.length != 0) {
                    for (var i = 0; i < userPermissions.length; i++) {
                        if (userPermissions[i].toLowerCase() === permission.toLowerCase()) {
                            index = i;
                            break;
                        }
                    }
                }

                return index != -1;
            }

            /**
             * Check whether it equals specifial user type.
             */
            function userTypeEquals(type) {
                var isEqual = false;
                var usertype = getTypeInfoOfCurrentUser();

                if (usertype) {
                    isEqual = usertype.id === type.id;
                }

                return isEqual;
            }

            /**
             * check whether speical user is a attandence for a meeting.
             */
            function hasAttandence(userId) {
                var has = false;

                if (userId) {
                    var meetingAttendees = getTempData('meetingAttendees');

                    if (meetingAttendees && meetingAttendees.length != 0) {
                        for (var i = 0; i < meetingAttendees.length; i++) {
                            if (meetingAttendees[i].userId === userId) {
                                has = true;
                                break;
                            }
                        }
                    }
                }

                return has;
            }

            /**
             * compare dates
             * @param date1
             * @param date2
             * @returns {number}
             */
            function compare(date1, date2) {
                return (new Date(date1.getFullYear(), date1.getMonth(), date1.getDate(), date1.getHours(), date1.getMinutes(), date1.getSeconds()) -
                    new Date(date2.getFullYear(), date2.getMonth(), date2.getDate(), date2.getHours(), date2.getMinutes(), date2.getSeconds()));
            }


            /**
            * convert json object to query string.
            */
            function getQueryParams(data) {
                if (data === null || typeof(data) === "undefined")
                    return '';

                if (!angular.isObject(data)) {
                    $exceptionHandler('Data must be object for dataService to be able to parse as query parameters. You passed in: ', data);
                    return;
                }

                var queryParams = '?';

                for (var paramKey in data) {
                    var qs = '';
                    if (angular.isArray(data[paramKey])) {
                        data[paramKey].forEach(function(v) {
                            qs += paramKey + '=' + v + '&';
                        });
                    } else {
                        qs = paramKey + '=' + data[paramKey] + '&';
                    }
                    queryParams += qs;
                }

                queryParams = queryParams.substring(0, queryParams.length - 1);

                return queryParams;
            }
            /*--------private method--------*/
            function getSubPathByPatter(reg) {
                var id = null;

                reg = reg || /\/user\/(edit|detail)\/(\d+)/i;

                // $0 - whole matched strings(/user/edit/1).
                // $1 - the first matched variable(user or edit).
                // $2 - the second matched variable(user id - 1)
                $location.path().replace(reg, function($0, $1, $2) {
                    id = $2;
                });

                return id;
            }

            /*-----------end---------------*/

            return {
                buildFullTimeDataSource: buildFullTimeDataSource,
                buildWorkingTimeDataSource: buildWorkingTimeDataSource,
                buildStatusDataSource: buildStatusDataSource,
                buildHosptialDataSource: buildHosptialDataSource,
                editWorkingTimeDataSource: editWorkingTimeDataSource,
                joinBy: joinBy,
                getIndexOf: getIndexOf,
                buildDpPlaceHolder: buildDpPlaceHolder,
                toFileSize: toFileSize,
                mergeFiles: mergeFiles,
                indexOfFiles: indexOfFiles,
                equalArray: equalArray,
                isDate: isDate,
                translateGender: translateGender,
                formatAvatar: formatAvatar,
                formatSnapshot: formatSnapshot,
                formatHealthrecord: formatHealthrecord,
                progress: {
                    show: loadingServices.show,
                    hide: loadingServices.hide
                },
                getTimespan: getTimespan,
                getCurrentUser: getCurrentUser,
                getRoleInfoOfCurrentUser: getRoleInfoOfCurrentUser,
                getPermissionOfCurrentUser: getPermissionOfCurrentUser,
                getTypeInfoOfCurrentUser: getTypeInfoOfCurrentUser,
                session: {
                    set: setTempData,
                    get: getTempData,
                    reset: resetTempData,
                    clear: clearCookie
                },
                getQueryParams: getQueryParams,
                date: {
                    format: formatDate,
                    utcToLocal: convertUtcToLocal,
                    toUtc: toUtc
                },
                confirm: {
                    del: deleteConfirm
                },
                resource: {
                    getValues: loadResources
                },
                history: {
                    back: historyBack
                },
                roles: {
                    hasRole: hasRole
                },
                permissions: {
                    hasPermission: hasPermission
                },
                usertypes: {
                    equals: userTypeEquals
                },
                meeting: {
                    hasAttandence: hasAttandence
                },
                compare: compare
            };
        }
    ]);
}());
