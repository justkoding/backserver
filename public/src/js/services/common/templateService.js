(function () {
    'use strict';

    angular.module('mdtServices').factory('templateService', ['$templateCache', '$http',
        function ($templateCache, $http) {
            function put(url, key) {
                return $http.get(url).then(function (result) {
                    $templateCache.put(key, result.data);
                    return result;
                }, function (err) {
                    return err;
                });
            }

            function get(key) {
                return $templateCache.get(key);
            }

            return {
                put: put,
                get: get
            };
        }
    ]);
})();
