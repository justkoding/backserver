(function () {
    'use strict';

    angular.module('mdtServices').factory('clientServices', ['$rootScope',
        '$q',
        '$location',
        '$translate',
        'repoService',
        function ($rootScope,
            $q,
            $location,
            $translate,
            repoService) {
            // defined this variable to mark which type would be launched by pc client.
            var types = {
                meeting: 'OPEN_MDT',
                meterial: 'OPEN_HEALTHRECORD',
                scanner: 'UPLOAD_HEALTHRECORD'
            };

            /**
             * Generate launch meeting path by ticked and mdtId.
             */
            function getPathOfJoinMeeting(ticket, mdtId) {
                return getPath({
                    serverUrl: getServerUrl(),
                    ticket: ticket,
                    action: types.meeting,
                    mdtId: mdtId,
                    lang: getCurrentLang() // en, cn
                }, 'mdt.join');
            };

            /**
            * Generate launch healthrecord path by ticked, id and type.
            * ticket, hrid, type, patientId
            */
            function getPathOfHealthrecord(opt) {
                var mdtId = opt.mdtId ? opt.mdtId : 0;
                return getPath({
                    serverUrl: getServerUrl(),
                    mdtId: mdtId,
                    ticket: opt.ticket,
                    action: types.meterial,
                    healthrecordId: opt.healthrecordId,
                    healthrecordType: opt.healthrecordType,
                    patientId: opt.patientId,
                    snapshotId: opt.snapshotId,
                    historyId: opt.historyId,
                    lang: getCurrentLang() // en, cn
                }, 'hr.open');
            };

            /**
            * Generate start high shot meter.
            */
            function getPathOfStartScanner(opt) {
                return getPath({
                    serverUrl: getServerUrl(),
                    ticket: opt.ticket,
                    action: types.scanner,                   
                    patientId: opt.patientId,
                    historyId: opt.historyId,
                    classification: opt.classification,
                    lang: getCurrentLang() // en, cn
                }, 'scanner.open');
            }

            /**
             * Get current server url.
             */
            function getServerUrl() {
                var url = $location.absUrl();
                var index = url.indexOf('#');

                if (index != -1) {
                    url = url.substring(0, index);
                }

                return url;
            }

            /*-------private------*/
            /**
             * Call base64 api in clientRepositories.
             */
            function getPath(json, topic) {
                return repoService.withFunction(repoService.repositories.client, 'base64')
                     .exec({
                         data: json,
                         topic: topic
                     })
            }

            /**
             * Get current language that used in application.
             *  - en, cn
             */
            function getCurrentLang() {               
                var lang = $translate.preferredLanguage();

                return lang;
            }
            /*--------end----------*/

            return {
                serverUrl: getServerUrl,
                meeting: getPathOfJoinMeeting,
                healthrecord: getPathOfHealthrecord,
                scanner: getPathOfStartScanner
            };
        }
    ])
}());
