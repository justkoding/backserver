(function () {
    'use strict';

    angular.module('mdtServices').factory('accessServices', ['$rootScope',
        '$q',
        '$cookieStore',
        'repoService',
        'commonService',
        'strings',
        function ($rootScope,
            $q,
            $cookieStore,
            repoService,
            commonService,
            strings) {

            var userPermissions = commonService.getPermissionOfCurrentUser();

            /**
             * return true if matched any one.
             */
            function any(permission) {
                var has = false;
                var defer = $q.defer();

                userPermissions = userPermissions || commonService.getPermissionOfCurrentUser();

                if (userPermissions && userPermissions.length != 0) {
                    // permission format: 'meeting-create,meeting-list'
                    if (permission && permission.length != 0) {
                        var arr = permission.split(',');

                        for (var i = 0; i < arr.length; i++) {
                            // true, has specifical permission
                            if (commonService.permissions.hasPermission(arr[i])) {
                                has = true;
                                break;
                            }
                        }

                        if (has) {
                            // has permission
                            defer.resolve(strings.ajaxStatus.OK);
                        } else {
                            // no permission
                            defer.reject(strings.ajaxStatus.FORBIDDEN);
                        }

                    } else {
                        // no permission
                        defer.reject(strings.ajaxStatus.FORBIDDEN);
                    }
                } else {
                    // no permission
                    defer.reject(strings.ajaxStatus.UNAUTHORIZED);
                }

                return defer.promise;
            }

            /**
             * return true when all are matched.
             */
            function all(permissions) {
                var has = false;
                var defer = $q.defer();

                userPermissions = userPermissions || commonService.getPermissionOfCurrentUser();

                if (userPermissions && userPermissions.length != 0) {
                    // permission format: 'meeting-create,meeting-list'
                    if (permission && permission.length != 0) {
                        var arr = permission.split(',');

                        for (var i = 0; i < arr.length; i++) {
                            // true, has specifical permission
                            if (!commonService.permissions.hasPermission(arr[i])) {
                                has = false;
                                break;
                            } else {
                                has = true;
                            }
                        }

                        if (has) {
                            // has permission
                            defer.resolve(strings.ajaxStatus.OK);
                        } else {
                            // no permission
                            defer.reject(strings.ajaxStatus.FORBIDDEN);
                        }

                    } else {
                        // no permission
                        defer.reject(strings.ajaxStatus.FORBIDDEN);
                    }
                } else {
                    // no permission
                    defer.reject(strings.ajaxStatus.UNAUTHORIZED);
                }

                return defer.promise;
            }

            return {
                any: any,
                all: all
            };
        }
    ])
}());
