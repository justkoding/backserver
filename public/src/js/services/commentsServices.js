(function () {
    'use strict';

    angular.module('mdtServices').factory('commentsServices', [
        '$q',
        '$rootScope',
        '$location',
        '$translate',
        'repoService',
        'commonService',
        'entityServices',
        function ($q,
            $rootScope,
            $location,
            $translate,
            repoService,
            commonService,
            entityServices) {
            /**
            * Get all patients by query strings.
            */
            function list(params) {
                var options = {
                    // mdtId,
                    origin: -1, // -1-> all, 0-> default, 1-> healthrecord, 2->sanpshot
                    healthrecordId: 0, // 0->all
                    patientId: 0, // 0->all
                    count: 20 // 0->all
                };
                var defer = $q.defer();

                params = angular.extend(options, params);
                repoService.withFunction(repoService.repositories.comments, 'list')
                    .exec({
                        data: params,
                        skipBroadcast: true
                        //topic: 'comments.list'
                    }).then(function (result) {
                        if (result.data && result.data.length != 0) {
                            result.data.map(function (item) {
                                item.creator.avatar = commonService.formatAvatar(item.creator.avatar, item.creator.userId);

                                // healthrecord
                                if (item.origin === 1) {
                                    if (item.attach) {
                                        item.attach.url = commonService.formatHealthrecord(item.attach.id);
                                    }
                                } else if (item.origin === 2) { // snapshot
                                    if (item.attach) {
                                        item.attach.url = commonService.formatSnapshot(item.attach.id);
                                    }
                                }
                                
                                return item;
                            });
                        }
                        defer.resolve(result);
                        $rootScope.$broadcast('comments.list.done', result);
                    }, function (err) {
                        $rootScope.$broadcast('comments.list.failed');
                        defer.reject(err);
                    });

                return defer.promise;
            };

            /**
            * Add a new patient.
            */
            function add(comment) {
                return repoService.withFunction(repoService.repositories.comments, 'add')
                    .exec({
                        data: comment,
                        topic: 'comments.add'
                    });
            }

            /**
            * Get patient information by patient id.
            */
            function detail(id) {
                return repoService.withFunction(repoService.repositories.comments, 'detail')
                    .exec({
                        data: id,
                        topic: 'comments.detail'
                    });
            }

            return {
                list: list,
                add: add,
                detail: detail
            }
        }
    ])
}());
