(function() {
    'use strict';

    angular.module('mdtServices').factory('homeServices', ['$rootScope',
        'repoService',
        function($rootScope, repoService) {
            function getName() {
                return repoService.withFunction(repoService.repositories.home, 'getName')
                    .exec({
                        topic: 'home.getName',
                        skipBroadcast: true
                    }).then(function(result) {
                        $rootScope.$broadcast('home.getName.done', result);
                        return result;
                    });
            };

            return {
                getName: getName
            };
        }
    ])
}());
