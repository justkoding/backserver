(function () {
    'use strict';

    angular.module('mdtServices').factory('entityServices', ['repoService',
        function (repoService) {

            /**
            * Account entity.
            */
            function Account(opts) {
                var options = {
                    account: '',
                    password: '',
                    status: 2,
                    name: '',
                    sex: 1,
                    birthday: null,
                    usertype: 1,
                    description: '',
                    extinfo: ''
                };

                opts = opts || {};
                opts = angular.extend(opts, options);

                this.account = opts.account;
                this.password = opts.password,
                this.status = opts.status;
                this.name = opts.name;
                this.sex = opts.sex;
                this.birthday = opts.birthday;
                this.usertype = opts.usertype;
                this.description = opts.description;
                this.extinfo = opts.extinfo;
            }

            function DpListItem(name, id) {
                this.name = name;
                this.id = id;
            }

            return {
                Account: Account,
                DpListItem: DpListItem
            };
        }
    ])
}());
