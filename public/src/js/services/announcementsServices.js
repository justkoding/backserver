(function () {
    'use strict';

    angular.module('mdtServices').factory('announcementsServices', [
        '$q',
        '$rootScope',
        '$location',
        '$translate',
        'repoService',
        'commonService',
        'entityServices',
        function ($q,
            $rootScope,
            $location,
            $translate,
            repoService,
            commonService,
            entityServices) {
            /**
            * Get all announcements by query strings.
            */
            function list(params) {
                var options = {
                    // mdtId,
                    size: 10000, // set big value to supose to get all data.
                    page: 1
                };
                var defer = $q.defer();

                params = angular.extend(options, params);
                repoService.withFunction(repoService.repositories.announcement, 'list')
                    .exec({
                        data: params,
                        skipBroadcast: true
                        //topic: 'announcement.list'
                    }).then(function (resp) {
                        if (resp && resp.data) {
                            // conver utc date to local time.
                            if (resp.data.createTime) {
                                resp.data.createTime = commonService.date.utcToLocal(resp.data.createTime);
                            }

                            // creator
                            if (resp.data.creator) {
                                // avatar
                                resp.data.creator.avatar = commonService.formatAvatar(resp.data.creator.avatar, resp.data.creator.userId);
                                                               
                                // format gender.
                                resp.data.creator.gender = commonService.translateGender(resp.data.creator.sex);
                            }
                        }

                        defer.resolve(resp);
                        $rootScope.$broadcast('announcement.list.done', resp);
                    }, function (error) {
                        defer.reject(error);
                        $rootScope.$broadcast('announcement.list.failed', []);
                    });

                return defer.promise;
            };

            /**
            * Add a new announcement.
            */
            function add(comment) {
                return repoService.withFunction(repoService.repositories.announcement, 'add')
                    .exec({
                        data: comment,
                        topic: 'announcement.add'
                    });
            }

            /**
            * Get announcement information by announcement id.
            */
            function detail(id) {
                return repoService.withFunction(repoService.repositories.announcement, 'detail')
                    .exec({
                        data: id,
                        topic: 'announcement.detail'
                    });
            }


            /**
            * Delete announcement by id
            */
            function del(id) {
                return repoService.withFunction(repoService.repositories.announcement, 'remove')
                    .exec({
                        data: id,
                        topic: 'announcement.remove'
                    });
            }

            /**
            * Update announcement information
            */
            function update(announcement) {
                return repoService.withFunction(repoService.repositories.announcement, 'update')
                    .exec({
                        data: announcement,
                        topic: 'announcement.update'
                    });
            }

            return {
                list: list,
                add: add,
                detail: detail,
                remove: del,
                update: update
            }
        }
    ])
}());
