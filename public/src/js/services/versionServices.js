(function () {
    'use strict';

    angular.module('mdtServices').factory('versionServices', ['repoService',    
        function (repoService) {
           
            /**
            * Get web app version
            */
            function detail() {
                return repoService.withFunction(repoService.repositories.version, 'detail')
                    .exec({
                        data: {
                            module: 'web'
                        },
                        skipBroadcast: true
                        //topic: 'comments.detail'
                    });
            }

            return {
               detail: detail
            };
        }
    ])
}());
