(function () {
    'use strict';

    angular.module('mdtServices').factory('navServices', ['$location',
        'repoService',
        'commonService',
        'strings',
        function ($location,
            repoService,
            commonService,
            strings) {

            /**
            * Defined all nav items content, each item contains the two properties:
            * - path: hash value
            * - text: translate key, to show in UI.
            */
            function getNavItems() {
                return [
                  {
                      path: 'dashboard',
                      text: 'NAV_HOME'
                  },
                  {
                      path: 'mdt',
                      text: 'NAV_MANAGEMENT'
                  },
                  //{
                  //    path: 'report',
                  //    text: 'NAV_REPORTS'
                  //},
                  //{
                  //    path: 'docx',
                  //    text: 'NAV_DOCUMENTS'
                  //},
                  {
                      path: 'patient',
                      text: 'NAV_PATIENTS'
                  },
                  {
                      path: 'user',
                      text: 'NAV_USERS'
                  },
                  {
                      path: 'role',
                      text: 'NAV_ROLES'
                  },
                  {
                      path: 'favorite',
                      text: 'TEXT_MY_FAVORITE'
                  },
                  //{
                  //    path: 'log',
                  //    text: 'NAV_LOG'
                  //}
                ];
            }

            /**
            * Return the index of current active nav item.
            */
            function getCurrentActiveNavItem() {
                var path = $location.path(),
                    index = 0,
                    items = [
                        'doctor',
                        'admin',
                        'moderate',
                        'navigator',
                        'meeting',
                        'offline',
                        'patient',
                        'user',
                        'log'
                    ];

                items.forEach(function (state, ind) {
                    if (path && path.toLowerCase().indexOf(state) !== -1) {
                        index = ind;
                    }
                });

                return index;
            }

            /**
             * For different user role has different home page,
             * return home route for each user.
             */
            function getHomeRoute() {
                // mock
                var state = 'page.dashboard';

                // doctor
                if (commonService.permissions.hasPermission(strings.permissions.dashboard.admin)) {
                    state = 'page.user';
                } else if (commonService.permissions.hasPermission(strings.permissions.dashboard.doctor)) { // admin
                    state = 'page.doctorhome';
                }
                else if (commonService.permissions.hasPermission(strings.permissions.dashboard.moderator)) { // moderate
                    state = 'page.moderatehome';
                }
                else if (commonService.permissions.hasPermission(strings.permissions.dashboard.patient)) { // navigator
                    state = 'page.patienthome';
                } else if (commonService.permissions.hasPermission(strings.permissions.dashboard.navigator)) { // navigator
                    state = 'page.patient';
                }

                return state;
            }
            
            return {
                getNavItems: getNavItems,
                getCurrentActiveNavItem: getCurrentActiveNavItem,
                getHomeRoute: getHomeRoute
            };
        }
    ])
}());
