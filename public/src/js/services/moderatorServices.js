(function () {
    'use strict';

    angular.module('mdtServices').factory('moderatorServices', ['$rootScope',
        '$q',
        '$location',
        '$translate',
        'repoService',
        'commonService',
        function ($rootScope,
                  $q,
                  $location,
                  $translate,
                  repoService,
                  commonService) {
            /**
             * Generate launch meeting path by ticked and mdtId.
             */
            function list(params) {
                var defer = $q.defer();
                repoService.withFunction(repoService.repositories.moderator, 'list')
                    .exec({
                        data: params,
                        skipBroadcast: true
                        //topic: 'patient.list'
                    }).then(function (results) {
                        if (results && results.data && results.data.length != 0) {
                            results.data.map(function (v) {
                                return formatMdtData(v);
                            });
                        }

                        $rootScope.$broadcast('moderator.list.done', results.data);
                        defer.resolve(results);
                    }, function (err) {
                        $rootScope.$broadcast('moderator.list.failed', err);
                        defer.reject(err);
                    });

                return defer.promise;
            };


            /*--------private---------*/
            function formatMdtData(rawPatient) {
                var v = rawPatient;
                if (v) {
                    // convert utc to local time.
                    if (v.createTime) {
                        v.createTime = commonService.date.utcToLocal(v.createTime);
                    }

                    // doctor
                    if (v.doctor) {
                        // doctor avatar
                        v.doctor.avatar = commonService.formatAvatar(v.doctor.avatar, v.doctor.userId);

                        // format gender.
                        v.doctor.gender = commonService.translateGender(v.doctor.sex);
                    }

                    // patient
                    if (v.patient) {
                        // convert utc to local time.
                        if (v.birthday) {
                            v.birthday = commonService.date.utcToLocal(v.birthday);
                        }

                        if (v.createTime) {
                            v.createTime = commonService.date.utcToLocal(v.createTime);
                        }

                        // set default patient avatar.
                        v.avatar = commonService.formatAvatar(null, null, true);

                        // format gender.
                        v.patient.gender = commonService.translateGender(v.patient.sex);

                        // creator
                        if (v.patient.creator) {
                            // doctor avatar
                            v.patient.creator.avatar = commonService.formatAvatar(v.patient.creator.avatar, v.patient.creator.userId, true);

                            // format gender.
                            v.patient.creator.gender = commonService.translateGender(v.patient.creator.sex);
                        }
                    }
                }

                return v;
            }

            /*--------end----------*/

            return {
                list: list
            };
        }
    ])
}());
