(function () {
    'use strict';

    var app = angular.module('app');

    app.config(['$provide',
       function ($provide) {

           $provide.decorator('datepickerDirective', [
               '$delegate', '$controller', function ($delegate, $controller) {
                   var directive = $delegate[0];
                   var controllerName = directive.controller;
                   var compare = function (date1, date2) {
                       return (new Date(date1.getFullYear(), date1.getMonth(), date1.getDate()) - new Date(date2.getFullYear(), date2.getMonth(), date2.getDate()));
                   };

                   directive.controller = ['$scope','$attrs', 'dateFilter', '$parse',function ($scope, $attrs, dateFilter, $parse) {

                       var controller = angular.extend(this, $controller(controllerName, {
                           $scope: $scope,
                           $attrs: $attrs
                       }));
                       var self = controller,
                           ngModelCtrl = { $setViewValue: angular.noop };

                       self.step={
                           months:1,
                           years:1
                       };

                       self.initDate = new Date();

                       $scope.$on('meetingsDate.update', function (event, data){
                           if(data){
                              self['meetingsDate'] = data;
                              self.refreshView();
                           }
                       });

                       $scope.$on('moderator.init', function (event, data){
                           if(data){
                               self.initDate = data;
                               self.refreshView();
                           }
                       });


                       $scope.move = function( direction ) {
                           var year = self.activeDate.getFullYear() + direction * (self.step.years || 0),
                               month = self.activeDate.getMonth() + direction * (self.step.months || 0);
                           self.activeDate.setFullYear(year, month, 1);
                           self.refreshView();
                       };

                       this.isDisabled = function( date ) {
                           return ((this.minDate && this.compare(date, this.minDate) < 0) || (this.maxDate && this.compare(date, this.maxDate) > 0) || ($attrs.dateDisabled && $scope.dateDisabled({date: date, mode: $scope.datepickerMode})));
                       };

                       $scope.isActive = function(dateObject) {
                           if (self.compare(dateObject.date, self.activeDate) === 0) {
                               $scope.activeDateId = dateObject.uid;
                               return true;
                           }
                           return false;
                       };

                       // Custome day event.
                       self['meetingsDate'] = angular.isDefined($attrs['meetingsDate']) ? $scope.$parent.$eval($attrs['meetingsDate']) : null;
                       self['iconVisibility'] = angular.isDefined($attrs['iconVisibility']) ? $scope.$parent.$eval($attrs['iconVisibility']) : false;
                       self['minDate'] = angular.isDefined($attrs['minDate']) ? $scope.$parent.$eval($attrs['minDate']) : null;

                       if (self['meetingsDate'] != null) {
                           controller.createDateObject = function (date, format) {
                               var model = ngModelCtrl.$modelValue ? new Date(ngModelCtrl.$modelValue) : null;
                               var hasMeeting = false;

                               for(var i = 0; i< self.meetingsDate.length; i++) {
                                   if (self.compare(date, self.meetingsDate[i]) === 0) {
                                       hasMeeting = true;
                                       break;
                                   }
                               }

                               if (self.compare(date, self.initDate) === 0) {
                                   date.color = '#eef9ff';
                                   date.borderColor = '#93d7ff';
                               }

                               return {
                                   date: date,
                                   label: dateFilter(date, format),
                                   selected: model && this.compare(date, model) === 0,
                                   disabled: hasMeeting? !hasMeeting : this.isDisabled(date),
                                   current: this.compare(date, new Date()) === 0,
                                   hasMeeting: hasMeeting,
                                   iconVisibility: self.iconVisibility,
                                   color: date.color,
                                   borderColor: date.borderColor
                               };
                           };
                       }
                       return controller;
                   }];

                   return $delegate;
               }
           ]);

           $provide.decorator('daypickerDirective', [
               '$delegate', '$controller', function ($delegate, $controller) {

                   var directive = $delegate[0];
                   directive.templateUrl = "/assets/src/templates/dayPicker/day.html";
                   return $delegate;
               }
           ]);
       }]);
}());