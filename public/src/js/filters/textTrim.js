(function() {
    'use strict';

    angular.module('mdtFilters').filter('textTrim', function () {

        return function(text) {
            if (text) {
                text = text.trim();
            }

            return text;
        };
    });
}());
