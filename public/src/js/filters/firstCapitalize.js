(function() {
    'use strict';

    angular.module('mdtFilters').filter('firstCapitalize', function() {

        return function(text) {
            if (text) {
                text = text.charAt(0).toUpperCase() + text.substring(1);
            }

            return text;
        };
    });
}());
