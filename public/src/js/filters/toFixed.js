(function() {
    'use strict';

    angular.module('mdtFilters').filter('toFixed', function () {

        return function (text, len) {
            len = len ? parseInt(len) : 2;
            text = text? parseInt(text) : 0;

            if (text) {
                for(var i=0; i<len; i++){
                    if(text.toString().length <len){
                        text = '0' + text;
                    }
                }
            }

            return text;
        };
    });
}());
