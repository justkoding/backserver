﻿(function () {
    'use strict';

    angular.module('mdtConstants').constant('constraints', getConstraints());

    function getConstraints() {
        return {
            login: {
                account: {
                    'required': {
                        'message': 'VALID_LOGIN_USERNAME'
                    }
                },
                password: {
                    'required': {
                        'message': 'VALID_LOGIN_PASSWORD'
                    }
                }
            },
            changepwd: {
                oldPassword: {
                    'required': {
                        'message': 'VALID_USER_PASSWORD'
                    }
                },
                newPassword: {
                    'required': {
                        'message': 'VALID_USER_NEWPASSWORD'
                    }
                },
                confirmPassword: {
                    'required': {
                        'message': 'VALID_USER_CONFIRMASSWORD'
                    }
                },
            },
            addOrEditUser: {
                accountName: {
                    'required': {
                        'message': 'VALID_ADD_OR_EDIT_ACCOUNT'
                    }
                },
                name: {
                    'required': {
                        'message': 'VALID_ADD_OR_EDIT_FULL_NAME'
                    }
                },
                role:{
                    'select': {
                        'message': 'VALID_USER_ROLE'
                    }
                },
                userTitle:{
                    'select': {
                        'message': 'VALID_USER_TITLE'
                    }
                },
                hospital:{
                    'select': {
                        'message': 'VALID_USER_HOSPITAL'
                    }
                },
                dept:{
                    'select': {
                        'message': 'VALID_USER_DEPT'
                    }
                },
                mobile: {
                    'mobile': {
                        'message': 'VALID_ADD_OR_EDIT_MOBILE'
                    }
                },
                email: {
                    'mail': {
                        'message': 'VALID_ADD_OR_EDIT_MAIL'
                    }
                },
                phone: {
                    'tel': {
                        'message': 'VALID_ADD_OR_EDIT_TEL'
                    }
                }
            },
            addOrEditRole: {
                name: {
                    'required': {
                        'message': 'VALID_ROLE_NAME'
                    }
                }
            },
            addOrEditPatient: {
                name: {
                    'required': {
                        'message': 'VALID_PATIENT_NAME'
                    }
                },
                ptnum: {
                    'required': {
                        'message': 'VALID_PATIENT_PATIENTID'
                    }
                },
                mobile: {
                    'mobile': {
                        'message': 'VALID_ADD_OR_EDIT_MOBILE'
                    }
                },
                idnum: {
                	'required': {
                        'message': 'VALID_PATIENT_ID_REQUIRED'
                    },
                    'idnum': {
                        'message': 'VALID_PATIENT_ID_INVALID'
                    }
                },
                doctor: {
                    'select': {
                        'message': 'VALID_PATIENT_ASSIGN_DOCTOR'
                    }
                }
            },
            addOrEditMdtBasicInfo: {
                title: {
                    'required': {
                        'message': 'VALID_MDT_BASIC_TITLE'
                    }
                },
                remark: {
                    'required': {
                        'message': 'VALID_MDT_BASIC_NOTES'
                    }
                },
                //date: {
                //    'required': {
                //        'message': 'VALID_MDT_BASIC_DATE'
                //    }
                //},
                location: {
                    'required': {
                        'message': 'VALID_MDT_BASIC_LOCATION'
                    }
                }
            },
            anonymous: {
                name: {
                    'required': {
                        'message': 'VALID_ANONYMOUS_NAME'
                    }
                }               
            },
            addOrEditMdtTempAttendees: {
                name: {
                    'required': {
                        'message': 'VALID_MDT_TEMPRARY_NAME'
                    }
                },
                phone: {
                    'required': {
                        'message': 'VALID_MDT_TEMPRARY_PHONE'
                    },
                    'mobile': {
                        'message': 'VALID_ADD_OR_EDIT_MOBILE'
                    }
                },
                email: {
                    'required': {
                        'message': 'VALID_MDT_TEMPRARY_EMAIL'
                    },
                    'mail': {
                        'message': 'VALID_ADD_OR_EDIT_MAIL'
                    }
                },
            },
            addOrEditMeeting: {
                title: {
                    'required': {
                        'message': 'VALID_MDT_BASIC_TITLE'
                    }
                },
                remark: {
                    'required': {
                        'message': 'VALID_MDT_BASIC_NOTES'
                    }
                },
                location: {
                    'required': {
                        'message': 'VALID_MDT_BASIC_LOCATION'
                    }
                }
            }
        }
    };
}());