(function() {
    'use strict';

    angular.module('mdtConstants').constant('strings', getStrings());

    function getStrings() {
        return {
            pageSizes: [5, 10, 20, 30, 50],
            mdtStatusOptions: [{
                text: 'MDT_LIST_STATUS_START_OR_JOIN',
                value: 0
            }, {
                text: 'MDT_LIST_STATUS_FINISHED',
                value: 1
            }, {
                text: 'MDT_LIST_STATUS_REVOKE',
                value: 2
            }],
            mdtTimestampOptions: [
                // auto calc real value by this option value.
                {
                    text: 'TEXT_ALL',
                    value: 0
                }, {
                    text: 'TEXT_LAST_ONE_WEEK',
                    value: 1
                }, {
                    text: 'TEXT_LAST_TWO_WEEK',
                    value: 2
                }, {
                    text: 'TEXT_LAST_ONE_MONTH',
                    value: 3
                }, {
                    text: 'TEXT_LAST_TWO_MONTHS',
                    value: 4
                }, {
                    text: 'TEXT_NEXT_ONE_WEEK',
                    value: 5
                }, {
                    text: 'TEXT_NEXT_TWO_WEEK',
                    value: 6
                }, {
                    text: 'TEXT_NEXT_ONE_MONTH',
                    value: 7
                }, {
                    text: 'TEXT_NEXT_TWO_MONTHS',
                    value: 8
                }
            ],
            jsonResults: {
                OK: {
                    text: 'OK',
                    value: 'OK'
                },
                FAILED: {
                    text: 'FAILED',
                    value: 'FAILED'
                },
                JSON_PARSE_FAILED: {
                    text: 'JSON_PARSE_FAILED',
                    value: 'JSON_PARSE_FAILED'
                },
                NO_DOCTOR: {
                    text: 'NO_DOCTOR',
                    value: 'NO_DOCTOR'
                },
                PATIENT_ERROR_STATUS: {
                    text: 'ERROR_STATUS',
                    value: 'ERROR_STATUS'
                },
                IN_DISCUSSING: {
                    text: 'IN_DISCUSSING',
                    value: 'IN_DISCUSSING'
                },
                IN_MEETING_QUEUE: {
                    text: 'IN_MEETING_QUEUE',
                    value: 'IN_MEETING_QUEUE'
                },
                IN_MEETING: {
                    text: 'IN_MEETING',
                    value: 'IN_MEETING'
                },
                //LOGIN_NO_USER_OR_PASSWORD: { text: 'NO_USER_OR_PASSWORD', value: 'NO_USER_OR_PASSWORD' },
                WEB_SOCKET_NOT_CONNECTED: {
                    text: 'WEB_SOCKET_NOT_CONNECTED',
                    value: 'WEB_SOCKET_NOT_CONNECTED'
                },
                MDT_NOT_STARTED: {
                    text: 'MDT_NOT_STARTED',
                    value: 'MDT_NOT_STARTED'
                },
                RECORD_NOT_EXIST: {
                    text: 'RECORD_NOT_EXIST',
                    value: 'RECORD_NOT_EXIST'
                },
                RECORD_NOT_FOUND: {
                    text: 'RECORD_NOT_FOUND',
                    value: 'RECORD_NOT_FOUND'
                },
                RECORD_ALREADY_EXIST: {
                    text: 'RECORD_ALREADY_EXIST',
                    value: 'RECORD_ALREADY_EXIST'
                },
                FORBIDDEN_FOR_SYSTEM_RECORD: {
                    text: 'FORBIDDEN_FOR_SYSTEM_RECORD',
                    value: 'FORBIDDEN_FOR_SYSTEM_RECORD'
                },
                ACCOUNT_CANNOT_EMPTY: {
                    text: 'ACCOUNT_CANNOT_EMPTY',
                    value: 'ACCOUNT_CANNOT_EMPTY'
                },
                USER_ALREADY_EXISTS: {
                    text: 'USER_ALREADY_EXISTS',
                    value: 'USER_ALREADY_EXISTS'
                },
                IDENTITY_CANNOT_READ: {
                    text: 'IDENTITY_CANNOT_READ',
                    value: 'IDENTITY_CANNOT_READ'
                },
                PARAM_CANNOT_EMPTY: {
                    text: 'PARAM_CANNOT_EMPTY',
                    value: 'PARAM_CANNOT_EMPTY'
                },
                ERROR_DATA: {
                    text: 'ERROR_DATA',
                    value: 'ERROR_DATA'
                },
                ERROR_STATUS: {
                    text: 'ERROR_STATUS',
                    value: 'ERROR_STATUS'
                },
                INAVAILABLE_ATTENDEE: {
                    text: 'INAVAILABLE_ATTENDEE',
                    value: 'INAVAILABLE_ATTENDEE'
                },
                MUST_BE_HOST: {
                    text: 'MUST_BE_HOST',
                    value: 'MUST_BE_HOST'
                },
                ALREADY_BE_HOST: {
                    text: 'ALREADY_BE_HOST',
                    value: 'ALREADY_BE_HOST'
                },
                MDT_ALREADY_ONGOING: {
                    text: 'MDT_ALREADY_ONGOING',
                    value: 'MDT_ALREADY_ONGOING'
                },
                ALREADY_JOIN: {
                    text: 'ALREADY_JOIN',
                    value: 'ALREADY_JOIN'
                },
                NEED_JOIN: {
                    text: 'NEED_JOIN',
                    value: 'NEED_JOIN'
                },
                NO_ACCOUNT: {
                    text: 'NO_ACCOUNT',
                    value: 'NO_ACCOUNT'
                },
                NO_CONTENT: {
                    text: 'NO_CONTENT',
                    value: 'NO_CONTENT'
                },
                UNKNOWN_COMMAND: {
                    text: 'UNKNOWN_COMMAND',
                    value: 'UNKNOWN_COMMAND'
                }
            },
            ajaxStatus: {
                OK: 200,
                UNAUTHORIZED: 401,
                FORBIDDEN: 403,
                NOTFOUND: 404
            },
            roles: {
                admin: 'role-admin',
                doctor: 'role-doctor',
                moderator: 'role-moderator',
                navigator: 'role-navigator',
                patient: 'role-patient',
                test: 'role-test',
                anonymous: 'role-anonymous'
            },
            permissions: {
                patient: {
                    create: 'patient-create',
                    list: 'patient-list',
                    remove: 'patient-remove',
                    update: 'patient-update',
                    detail: 'patient-detail',
                    healthrecordCreate: 'patient-healthrecord-create',
                    healthrecordList: 'patient-healthrecord-list',
                    healthrecordRemove: 'patient-healthrecord-remove',
                    healthrecordView: 'patient-healthrecord-view',
                    healthrecoedSnapshot: 'patient-healthrecoed-snapshot',
                    consulate: 'patient-consulate',
                    assign: 'patient-assign'
                },
                user: {
                    create: 'user-create',
                    list: 'user-list',
                    remove: 'user-remove',
                    update: 'user-update',
                    view: 'user-view',
                    profile: 'user-profile',
                    pwdReset: 'user-pwd-reset'
                },
                mdt: {
                    create: 'MDT-create',
                    list: 'MDT-list',
                    update: 'MDT-update',
                    detail: 'MDT-detail'
                },
                meeting: {
                    create: 'meeting-create',
                    list: 'meeting-list',
                    remove: 'meeting-remove',
                    update: 'meeting-update',
                    detail: 'meeting-detail'
                },
                role: {
                    create: 'role-create'
                },
                audit: {
                    list: 'audit-list'
                },
                favorite: {
                    list: 'favorite-list'
                },
                social: {
                    doctor: {
                        create: 'social-create',
                        list: 'social-list'
                    }
                },
                dashboard: {
                    doctor: 'dashboard-doctor',
                    moderator: 'dashboard-moderator',
                    navigator: 'dashboard-navigator',
                    patient: 'dashboard-patient',
                    admin: 'dashboard-admin'
                }
            },
            usertypes: {
                admin: {
                    id: 0,
                    type: 'admin'
                },
                doctor: {
                    id: 1,
                    type: 'doctor'
                },
                moderator: {
                    id: 2,
                    type: 'moderator'
                },
                navigator: {
                    id: 3,
                    type: 'navigator'
                }
            },
            // to filter by doctor title. only show the users whose title is 'doctor'
            // 1-> 主任医师,2-> 副主任医师,3-> 主治医师,4-> 医师,5-> 医士
            doctorTitles: [1, 2, 3, 4, 5],
            defaultAvatar: '/assets/src/img/icons/defaultuser.png',
            websocket: {
                dataTypes: {
                    notification: 'NOTIFICATION',
                    doctorSocial: 'DOCTOR_SOCIAL',
                    discussion: 'DISCUSSION'
                },
                scope: {
                    'public': 'PUBLIC',
                    'private': 'PRIVATE'
                },
                topic: {
                    notification: 'websocket.nofiticaion',
                    doctorSocial: 'websocket.doctor.social',
                    discussion: 'websocket.discussion'
                }
            }

        }
    };
}());
