(function () {
    'use strict';

    angular.module('mdtConstants').constant('apiUrls', getUrls());

    function getUrls() {
        return {
            signIn: '/token',

            //accounts
            accounts: '/accounts',
            checkAccount: '/accounts/checkexistence',

            //password
            changepwd: '/accounts/password',

            // user management
            users: '/users',

            // dept
            depts: '/depts',

            // title - usertype
            usertypes: '/usertypes',

            // usertitles
            usertitles: '/usertitles',

            // permission
            permissions: '/permissions',

            // role management
            roles: '/roles',
            rolesValues: '/roles/values',

            // healthrecord
            healthrecords: '/healthrecords',

            // patient management
            patients: '/patients',

            // history management
            histories: '/histories',

            // suggestions
            suggestions: '/suggestions',

            // classifications management
            classifications: '/classifications',

            // mdt 
            mdts: '/mdts',

            // snapshots
            snapshots: '/snapshots',

            // comments 
            comments: '/comments',

            // announcements
            announcements: '/announcements',

            // favorite
            patientFavorites: '/favorite/patients',

            // doctor groups
            doctorGroups: '/group/doctors',

            // icp
            icp: '/icp',

            // temprary attendees.
            tempAttendees: '/tempattendees',

            //anonymous
            anonymous: '/anonymous',

            //dashboard
            dashboard: '/dashboard',

            //moderator
            moderator: '/moderator',

            //hospitals
            hospitals: '/hospitals',
            
            //version
            version: '/version'
        }
    };
}());