﻿(function () {
    'use strict';

    angular.module('mdtConstants').constant('routes', getRoutes());

    function getRoutes() {
        return [
            {
                state: 'login',
                config: {
                    controller: 'LoginCtrl',
                    templateUrl: 'assets/src/pages/login/login.html',
                    url: '/login?returnUrl',
                    params: { 'dest': null }
                }
            },            
            {
                state: 'login.forgetpassword',
                config: {
                    controller: 'ForgetPasswordController',
                    templateUrl: 'assets/src/pages/forgetpassword/forgetpassword.html',
                    url: '/forgetpassword'
                }
            },
            {
                state: 'anonymous',
                config: {
                    controller: 'AnonymousCtrl',
                    templateUrl: 'assets/src/pages/anonymous/anonymous.html',
                    url: '/anonymous?token'
                }
            },
            {
                state: 'error',
                config: {
                    url: '/error',
                    controller: 'ErrorController',
                    templateUrl: 'assets/src/pages/error/error.html',
                    params: { 'dest': null }
                }
            },
            {
                state: 'error.401',
                config: {
                    url: '/401',
                    controller: 'Error401Ctrl',
                    templateUrl: 'assets/src/pages/common/error/401/401.html'
                }
            },
            {
                state: 'error.403',
                config: {
                    url: '/403',
                    controller: 'Error403Ctrl',
                    templateUrl: 'assets/src/pages/common/error/403/403.html'
                }
            },
            {
                state: 'error.404',
                config: {
                    url: '/404',
                    controller: 'Error404Ctrl',
                    templateUrl: 'assets/src/pages/common/error/404/404.html'
                }
            },
            {
                state: 'page',
                config: {
                    url: '/page',
                    controller: 'PageController',
                    templateUrl: 'assets/src/pages/page/page.html',
                    params: { 'dest': null }
                }
            },
            {
                state: 'page.forbidden',
                config: {
                    url: '/forbidden',
                    templateUrl: 'assets/src/pages/forbidden/forbidden.html',
                    controller: 'ForbiddenCtrl'
                }
            },
            {
                state: 'page.adminhome',
                config: {
                    url: '/admin',
                    templateUrl: 'assets/src/pages/home/admin/admin.html',
                    controller: 'AdminHomeCtrl',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('dashboard-admin');
                        }]
                    }
                }                
            },
           {
               state: 'page.doctorhome',
               config: {
                   url: '/doctor',
                   templateUrl: 'assets/src/pages/home/doctor/doctor.html',
                   controller: 'DoctorHomeCtrl',
                   resolve: {
                       hasPermission: ['accessServices', function (access) {
                           return access.any('dashboard-doctor');
                       }]
                   }
               }               
           },
            {
                state: 'page.moderatehome',
                config: {
                    url: '/moderate',
                    templateUrl: 'assets/src/pages/home/moderate/moderate.html',
                    controller: 'ModerateHomeCtrl',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('dashboard-moderator');
                        }]
                    }
                }                
            },
            {
                state: 'page.navigatorhome',
                config: {
                    url: '/navigator',
                    templateUrl: 'assets/src/pages/home/navigator/navigator.html',
                    controller: 'NavigatorHomeCtrl',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('dashboard-navigator');
                        }]
                    }
                }                
            },
            {
                state: 'page.doctorsocial',
                config: {
                    url: '/doctorsocial',
                    templateUrl: 'assets/src/pages/groups/list/list.html',
                    controller: 'ListDoctorGroupsCtrl',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('social-list');
                        }]
                    }
                }                
            },
            {
                state: 'page.dashboard',
                config: {
                    url: '/dashboard',
                    templateUrl: 'assets/src/pages/home/dashboard/dashboard.html',
                    controller: 'DashboardCtrl',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('dashboard-admin');
                        }]
                    }
                }                
            },
            {
                state: 'page.meeting',
                config: {
                    url: '/meeting?search',
                    controller: 'ListMeetingCtrl',
                    templateUrl: 'assets/src/pages/meeting/list/list.html',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('meeting-list');
                        }]
                    }
                }                
            },
            {
                state: 'page.meeting.detail',
                config: {
                    controller: 'DetailMeetingCtrl',
                    templateUrl: 'assets/src/pages/meeting/detail/detail.html',
                    url: '/detail/:id',
                    params: { id: null },
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('meeting-detail');
                        }]
                    }
                }
            },
            {
                state: 'page.meeting.patients',
                config: {
                    controller: 'MeetingPatientsCtrl',
                    templateUrl: 'assets/src/pages/meeting/patients/patients.html',
                    url: '/:mid/patients?hid&pid',
                    params: {
                        mid: null, // meeting id
                        hid: null, // history id
                        pid: null // patient id
                    },
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('meeting-list');
                        }]
                    }
                }
            },
            {
                state: 'page.meeting.edit',
                config: {
                    controller: 'AddMeetingCtrl',
                    templateUrl: 'assets/src/pages/meeting/add/add.html',
                    url: '/edit/:id',
                    params: { id: null },
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('meeting-update');
                        }]
                    }
                }
            },
            {
                state: 'page.meeting.add',
                config: {
                    controller: 'AddMeetingCtrl',
                    templateUrl: 'assets/src/pages/meeting/add/add.html',
                    url: '/add?date',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('meeting-create');
                        }]
                    }
                }
            },           
             {
                 state: 'page.offline',
                 config: {
                     url: '/offline?search',
                     controller: 'ListOfflineCtrl',
                     templateUrl: 'assets/src/pages/offline/list/list.html'
                 }
             },
            {
                state: 'page.report',
                config: {
                    controller: 'ReportsCtrl',
                    templateUrl: 'assets/src/pages/reports/reports.html',
                    url: '/report?search'
                }
            },
            {
                state: 'page.docx',
                config: {
                    controller: 'DocxCtrl',
                    templateUrl: 'assets/src/pages/docx/docx.html',
                    url: '/docx?search'
                }
            },
            {
                state: 'page.patient',
                config: {
                    controller: 'ListPatientsCtrl',
                    templateUrl: 'assets/src/pages/patients/list/list.html',
                    url: '/patient?search',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('patient-list');
                        }]
                    }
                }
            },
            {
                state: 'page.patient.add',
                config: {
                    controller: 'AddorEditPatientCtrl',
                    templateUrl: 'assets/src/pages/patients/addOrEdit/addOrEdit.html',
                    url: '/add',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('patient-create');
                        }]
                    }
                }
            },
            {
                state: 'page.patient.edit',
                config: {
                    controller: 'AddorEditPatientCtrl',
                    templateUrl: 'assets/src/pages/patients/addOrEidt/addOrEidt.html',
                    url: '/edit/:id',
                    params: { id: null },
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('patient-update');
                        }]
                    }
                }
            },
            {
                state: 'page.patient.detail',
                config: {
                    controller: 'DetailPatientCtrl',
                    templateUrl: 'assets/src/pages/patients/detail/detail.html',
                    url: '/detail/:id',
                    params: {
                        id: null,
                        fs: null // fromShare
                    },
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('patient-detail');
                        }]
                    }
                }
            },
            {
                state: 'page.pwd',
                config: {
                    templateUrl: 'assets/src/pages/password/reset.html',
                    url: '/pwd',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('user-pwd-reset');
                        }]
                    }
                }                
            },
            {
                state: 'page.user',
                config: {
                    controller: 'ListUserCtrl',
                    templateUrl: 'assets/src/pages/users/list/list.html',
                    url: '/user?search',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('user-list');
                        }]
                    }
                }
            },
            {
                state: 'page.user.add',
                config: {
                    controller: 'AddOrEidtUserCtrl',
                    templateUrl: 'assets/src/pages/users/addOrEidt/addOrEidt.html',
                    url: '/add',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('user-create');
                        }]
                    }
                }
            },
            {
                state: 'page.user.edit',
                config: {
                    controller: 'AddOrEidtUserCtrl',
                    templateUrl: 'assets/src/pages/users/addOrEidt/addOrEidt.html',
                    url: '/edit/:id',
                    params: { id: null },
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('user-update');
                        }]
                    }
                }
            },
            {
                state: 'page.user.detail',
                config: {
                    controller: 'DetailUserCtrl',
                    templateUrl: 'assets/src/pages/users/detail/detail.html',
                    url: '/detail/:id',
                    params: { id: null },
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('user-view');
                        }]
                    }
                }
            },
            {
                state: 'page.profile',
                config: {
                    controller: 'UserProfileCtrl',
                    templateUrl: 'assets/src/pages/users/profile/profile.html',
                    url: '/profile/:id',
                    params: { id: null },
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('user-profile');
                        }]
                    }
                }
            },
            {
                state: 'page.role',
                config: {
                    controller: 'ListRoleCtrl',
                    templateUrl: 'assets/src/pages/roles/list/list.html',
                    url: '/role',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('role-create');
                        }]
                    }
                }                
            },
            {
                state: 'page.role.add',
                config: {
                    controller: 'AddOrEidtRoleCtrl',
                    templateUrl: 'assets/src/pages/roles/addOrEidt/addOrEidt.html',
                    url: '/add',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('role-create');
                        }]
                    }
                }                
            },
            {
                state: 'page.role.edit',
                config: {
                    controller: 'AddOrEidtRoleCtrl',
                    templateUrl: 'assets/src/pages/roles/addOrEidt/addOrEidt.html',
                    url: '/edit/:id',
                    params: { id: null },
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('role-create');
                        }]
                    }
                }               
            },
            {
                state: 'page.role.detail',
                config: {
                    controller: 'DetailRoleCtrl',
                    templateUrl: 'assets/src/pages/roles/detail/detail.html',
                    url: '/detail/:id',
                    params: { id: null },
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('role-create');
                        }]
                    }
                }                
            },
            {
                state: 'page.favorite',
                config: {
                    controller: 'ListFavoriteCtrl',
                    templateUrl: 'assets/src/pages/favorites/list/list.html',
                    url: '/favorite',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('favorite-list');
                        }]
                    }
                }                
            },
            {
                state: 'page.group',
                config: {
                    controller: 'ListDoctorGroupsCtrl',
                    templateUrl: 'assets/src/pages/groups/list/list.html',
                    url: '/group?id',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('doctor-social-create');
                        }]
                    }
                }                
            },
            {
                state: 'page.log',
                config: {
                    controller: 'LogsCtrl',
                    templateUrl: 'assets/src/pages/logs/logs.html',
                    url: '/log?search',
                    resolve: {
                        hasPermission: ['accessServices', function (access) {
                            return access.any('audit-list');
                        }]
                    }
                }                
            }
        ];
    };
}());