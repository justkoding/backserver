(function () {
    'use strict';

    angular.module('mdtRepositories').factory('passwordRepository', [
        '$q', 'dataService', 'apiUrls', 'commonRepository',
        function ($q, dataService, apiUrls, commonRepository) {

            function changePwd(data) {
                return commonRepository.putData(apiUrls.accounts + '/password', data, {
                    title: 'MESSAGE_PASSWORD_RESET_TITLE'
                });
            }

            ///account/resetpwd
            function resetPwd(id) {
                return commonRepository.putData(apiUrls.accounts + '/' + id + '/resetpwd', {}, {
                    title: 'MESSAGE_PASSWORD_RESET_TITLE'
                });
            }

            return {
                changePwd: changePwd,
                resetPwd: resetPwd
            }
        }
    ]);
}());
