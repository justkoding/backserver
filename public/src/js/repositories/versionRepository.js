(function () {
    'use strict';

    angular.module('mdtRepositories').factory('versionRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /**
            * Get web app version
            */
            function detail(opt) {
                return commonRepository.getData(apiUrls.version, opt);
            }            

            return {
                detail: detail          
            }
        }
    ]);
}());
