(function () {
    'use strict';

    angular.module('mdtRepositories').factory('dashboardRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /**
            * Get all meetings' info by time bucket
            */
            function list() {
                return commonRepository.getData(apiUrls.mdts + apiUrls.dashboard + '/cs');
            }

            return {
                list: list
            }
        }
    ]);
}());
