(function () {
    'use strict';

    angular.module('mdtRepositories').factory('mdtRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /**
            * Get all mdt by query strings.
            */
            function list(data) {
                return commonRepository.postData(apiUrls.mdts + '/list', data);
            }

            /**
            * Add a new mdt.
            */
            function add(mdt) {
                return commonRepository.postData(apiUrls.mdts, mdt);
            }

            /**
            * Get mdt information by mdt id.
            */
            function detail(id) {
                return commonRepository.getData(apiUrls.mdts + '/' + id);
            }

            /**
            * Delete mdt by mdt id
            */
            function deleteUser(id) {
                return commonRepository.deleteData(apiUrls.mdts + '/' + id);
            }

            /**
            * Update mdt information
            */
            function update(mdt) {
                return commonRepository.putData(apiUrls.mdts + '/' + mdt.id, mdt, {
                    title: 'MESSAGE_MDT_EDIT_DONE_TITLE'
                });
            }
            
            /**
             * Get mdt infomation for doctor home dashboard.
             */
            function doctorDashboardMdt() {
                return commonRepository.getData(apiUrls.mdts + '/dashboard/cs');
            }
            
            /**
            * Get all patients in speical mdt.
            */
            function patientsList(options) {
                return commonRepository.getData(apiUrls.mdts + '/' + options.mdtid + '/patients', options);
            }

            /**
            * Get all healthrecords in speical mdt and speical patient.
            */
            function healthrecordsList(options) {
                return commonRepository.getData(apiUrls.mdts + '/' + options.mdtid + '/patients/' + options.patientId + '/healthrecords', options);
            }

            /**
            * Get special patient information.
            */
            function patient(options) {
                return commonRepository.getData(apiUrls.mdts + '/' + options.mdtid + '/patients/' + options.patientId, options);
            }

            /**
             * Delete special patient from speical mdt.
             */
            function deletePatient(options) {
                return commonRepository.deleteData(apiUrls.mdts + '/' + options.mdtid + '/patients/' + options.patientId);
            }

            /**
            * Update special patient from speical mdt.
            * data: only contain one field named 'reason'.
            */
            function addOrUpdatePatient(data, options) {
                return commonRepository.putData(apiUrls.mdts + '/' + options.mdtid + '/patients/' + options.patientId, data);
            }

            /**
            * Add healthrecords to special mdt.
            * data: healthrecords array.
            */
            function addHealthrecords(data, options) {
                return commonRepository.postData(apiUrls.mdts + '/' + options.mdtid + '/healthrecords', data);
            }

            /**
            * Delete special patient from speical mdt.
            */
            function deleteHealthrecord(options) {
                return commonRepository.deleteData(apiUrls.mdts + '/' + options.mdtid + '/healthrecords/' + options.hrdId);
            }

            /**
             * Add attendees
             */
            function addAttendees(data, options) {
                return commonRepository.postData(apiUrls.mdts + '/' +  options.id + '/attendees', data);
            }

            /**
             * get attendees
             */
            function getAttendees(id) {
                return commonRepository.getData(apiUrls.mdts + '/' + id + '/attendees');
            }

            /**
            * Add temprary attendees
            */
            function addTempattendees(data) {
                return commonRepository.postData(apiUrls.tempAttendees, data);
            }

            /**
            * Delete special temprary attendence.
            */
            function deleteTempattendence(id) {
                return commonRepository.deleteData(apiUrls.tempAttendees + '/' + id);
            }

            /**
             * Update special temprary attendence.
             */
            function updateTempAttendee(options, data) {
                return commonRepository.putData(apiUrls.tempAttendees + '/' + options.id, data, {
                    title: 'MESSAGE_ANONYMOUS_EDIT_DONE_TITLE'
                });
            }

            /**
             * Get temprary attendence's information by token.
             */
            function getTempattendees(options) {
                return commonRepository.getData(apiUrls.mdts + apiUrls.anonymous + '/' + options.token);
            }

            /**
             * send a mail to temp attendees
             */
            function mailTempattendees(mdtId) {
                return commonRepository.getData(apiUrls.mdts + '/' + mdtId + '/mail');
            }

            /**
             * Add patients
             */
            function addPatients(data, options) {
                return commonRepository.putData(apiUrls.mdts + '/' +  options.id + '/addcase', data);
            }

            /**
             * Delete special patients from speical mdts.
             */
            function deletePatients(data, options) {
                return commonRepository.putData(apiUrls.mdts + '/' + options.id + '/removecase', data);
            }


            return {
                // mdt
                list: list,
                add: add,
                detail: detail,
                remove: deleteUser,
                update: update,
                doctorDashboardMdt: doctorDashboardMdt,

                // materials
                patientsList: patientsList,
                healthrecordsList: healthrecordsList,
                patient: patient,
                deletePatient: deletePatient,
                addOrUpdatePatient: addOrUpdatePatient,

                // healthrecords
                addHealthrecords: addHealthrecords,
                deleteHealthrecord: deleteHealthrecord,

                //attendence
                addAttendees: addAttendees,
                getAttendees: getAttendees,

                // temprary attendence
                addTempattendees: addTempattendees,
                deleteTempattendence: deleteTempattendence,
                updateTempAttendee: updateTempAttendee,
                getTempattendees: getTempattendees,
                mailTempattendees: mailTempattendees,

                //patients
                addPatients: addPatients,
                deletePatients:deletePatients

            }
        }
    ]);
}());
