(function () {
    'use strict';

    angular.module('mdtRepositories').factory('patientRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /**
            * Get all patients by query strings.
            */
            function list(options) {
                return commonRepository.getData(apiUrls.patients, options);
            }

            /**
             * Get all reference patients in discussion.
             */
            function discussion(options) {
                return commonRepository.getData(apiUrls.patients + '/discussion', options);
            }

            /**
             * Get all reference patients in meeting.
             */
            function meeting(options) {
                return commonRepository.getData(apiUrls.patients + '/meeting', options);
            }

            /**
            * Add a new patient.
            */
            function add(patient) {
                return commonRepository.postData(apiUrls.patients, patient, {
                    title: 'MESSAGE_PATIENT_ADD_DONE_TITLE'
                });
            }

            /**
            * Get patient information by patient id.
            */
            function detail(id) {
                return commonRepository.getData(apiUrls.patients + '/' + id, {}, {
                    title: 'MESSAGE_PATIENT_DETAIL_DONE_TITLE'
                });
            }

            /**
            * Delete patient by patient id
            */
            function deletePatient(id) {
                return commonRepository.deleteData(apiUrls.patients + '/' + id, {}, {
                    title: 'MESSAGE_PATIENT_REMOVE_DONE_TITLE'
                });
            }

            /**
            * Update patient information
            */
            function update(patient) {
                return commonRepository.putData(apiUrls.patients + '/' + patient.id, patient, {
                    title: 'MESSAGE_PATIENT_UPDATE_DONE_TITLE'
                });
            }

            /**
            * Check if the special patient is existing
            */
            function checkPatient(data) {
                return commonRepository.getData(apiUrls.patients + '/check/' + data.idnum, {}, { 'failIgnore': true });
            }

            /**
            * Get all mdt list for special patient
            */
            function mdts(patientId) {
                return commonRepository.getData(apiUrls.patients + '/' + patientId + '/mdts');
            }

            /**
            * Get all patients which can be accessing by special doctor.
            * @param options[object] query string.
            * - filter: search by patient name
            * - doctor: doctor id(user id)
            * - size: records number each page.
            * - page: the current page index.
            */
            function relation(options) {
                return commonRepository.getData(apiUrls.patients + '/relation', options);
            }

            /**
            * Get diagnosis histories summary of special patient.         
            */
            function summary(id) {
                return commonRepository.getData(apiUrls.patients + '/' + id + '/summary');
            }

            return {
                list: list,
                add: add,
                detail: detail,
                discussion: discussion,
                meeting: meeting,
                remove: deletePatient,
                update: update,
                checkPatient: checkPatient,
                mdts: mdts,
                relation: relation,
                summary: summary
            }
        }
    ]);
}());
