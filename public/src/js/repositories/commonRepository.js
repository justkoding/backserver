﻿(function () {
    'use strict';

    angular.module('mdtRepositories').factory('commonRepository', ['$q', 'dataService', function ($q, dataService) {

        function getData(url, data, options) {
            var defer = $q.defer();

            dataService.get(url, data, options).then(function (response) {
                defer.resolve(response.data);
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;
        }

        function postData(url, data, opt) {
            var defer = $q.defer();

            dataService.post(url, data, opt).then(function (response) {
                defer.resolve(response.data);
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;
        }

        function putData(url, data, options) {
            var defer = $q.defer();

            dataService.put(url, data, options).then(function (response) {
                defer.resolve(response.data);
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;
        }

        function deleteData(url, data, options) {
            var defer = $q.defer();

            dataService.delete(url, data, options).then(function (response) {
                defer.resolve(response.data);
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;
        }

        return {
            getData: getData,
            postData: postData,
            putData: putData,
            deleteData: deleteData
        }
    }
    ]);
}());
