(function () {
    'use strict';

    angular.module('mdtRepositories').factory('systemMiscRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /**
            * Get all users by query strings.
            */
            function permissionList() {
                return commonRepository.getData(apiUrls.permissions);
            }
            
            return {
                permissionList: permissionList
            }
        }
    ]);
}());
