(function () {
    'use strict';

    angular.module('mdtRepositories').factory('homeRepository', [
        '$q', 'dataService', 'configModel',
        function ($q, dataService, configModel) {
            //function getAssets() {
            //    return dataService.get(configModel.getUrl().assets).then(function(response) {
            //        return response.data;
            //    });
            //};

            // test
            function getName() {
                var deferred = $q.defer();

                dataService.getExternal('/assets/src/localData/t.json').then(function (response) {
                    deferred.resolve(response.data);
                }, function (err) {
                    deferred.reject(err);
                });

                return deferred.promise;
            }

            return {
                getName: getName
            }
        }
    ]);
}());
