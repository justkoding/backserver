(function () {
    'use strict';

    angular.module('mdtRepositories').factory('suggestionsRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /**
            * Get all suggestions by query strings.
            */
            function list(options) {
                return commonRepository.getData(apiUrls.suggestions, options);
            }

            /**
            * Add a new suggestion.
            */
            function add(suggestion) {
                return commonRepository.postData(apiUrls.suggestions, suggestion);
            }

            /**
            * Update a existing suggestion.
            */
            function update(id, suggestion) {
                return commonRepository.putData(apiUrls.suggestions + '/' + id, suggestion);
            }

            /**
            * Get suggestion information by id.
            */
            function detail(id) {
                return commonRepository.getData(apiUrls.suggestions + '/' + id);
            }

            return {
                // mdt
                list: list,
                add: add,
                update: update,
                detail: detail
            }
        }
    ]);
}());
