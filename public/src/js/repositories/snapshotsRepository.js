(function () {
    'use strict';

    angular.module('mdtRepositories').factory('snapshotsRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {
            /**
            * Get all snapshots by query strings.
            */
            function list(options) {
                return commonRepository.getData(apiUrls.snapshots, options);
            }

            return {
                list: list
            }
        }
    ]);
}());
