(function () {
    'use strict';

    angular.module('mdtRepositories').factory('userRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /**
            * Get all users by query strings.
            */
            function list(param) {
                return commonRepository.getData(apiUrls.users, param);
            }

            /**
            * Add a new user.
            */
            function add(user) {
                return commonRepository.postData(apiUrls.users, user, {
                    title: 'MESSAGE_USER_ADD_DONE_TITLE'
                });
            }

            /**
            * Get user information by user id.
            */
            function detail(id) {
                return commonRepository.getData(apiUrls.users + '/' + id, {}, {
                    title: 'MESSAGE_USER_DETAIL_DONE_TITLE'
                });
            }

            /**
            * Delete user by user id
            */
            function deleteUser(id) {
                return commonRepository.deleteData(apiUrls.users + '/' + id, {}, {
                    title: 'MESSAGE_USER_REMOVE_DONE_TITLE'
                });
            }

            /**
            * Update user information
            */
            function update(user) {
                return commonRepository.putData(apiUrls.users + '/' + user.id, user, {
                    title: 'MESSAGE_USER_UPDATE_DONE_TITLE'
                });
            }

            /**
            * Get all dept list
            */
            function getDeptlist() {
                return commonRepository.getData(apiUrls.depts);
            }

            /**
            * Get all userType list
            */
            function getUserTypeList() {
                return commonRepository.getData(apiUrls.usertypes);
            }

            /**
            * Get all user titles list
            */
            function getUserTitleList() {
                return commonRepository.getData(apiUrls.usertitles);
            }

            /**
            * Get all permission list
            */
            function getPermissionList() {
                return commonRepository.getData(apiUrls.permissions);
            }

            /**
            * Enable speical account
            */
            function enableAccount(id) {
                return commonRepository.putData(apiUrls.accounts + '/' + id + '/enable', {}, {
                    title: 'MESSAGE_ACCOUNT_ENABLE_DONE_TITLE'
                });
            }

            /**
            * Disable speical account
            */
            function disableAccount(id) {
                return commonRepository.putData(apiUrls.accounts + '/' + id + '/disable', {}, {
                    title: 'MESSAGE_ACCOUNT_DISABLE_DONE_TITLE'
                });
            }

            /**
            * Check if the special account is existing
            */
            function checkAccount(data) {
                return commonRepository.getData(apiUrls.checkAccount, data);
            }

            return {
                list: list,
                add: add,
                detail: detail,
                remove: deleteUser,
                update: update,
                enableAccount: enableAccount,
                disableAccount: disableAccount,
                getDeptlist: getDeptlist,
                getUserTypeList: getUserTypeList,
                getPermissionList: getPermissionList,
                getUserTitleList: getUserTitleList,
                checkAccount: checkAccount
            }
        }
    ]);
}());
