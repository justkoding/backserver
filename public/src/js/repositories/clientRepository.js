(function () {
    'use strict';

    angular.module('mdtRepositories').factory('clientRepository', [
        'commonRepository', 'apiUrls',
        function (commonRepository, apiUrls) {

            /**
            * Get all healthrecord by query strings.
            */
            function base64(json) {
                return commonRepository.postData(apiUrls.icp, json);
            }
          
            return {
                base64: base64
            }
        }
    ]);
}());
