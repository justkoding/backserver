(function () {
    'use strict';

    angular.module('mdtRepositories').factory('historiesRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {
            /**
            * Add a new history.
            */
            function add(patientId, history) {
                return commonRepository.postData(apiUrls.patients + '/' + patientId + apiUrls.histories, history);
            }

            /**
            * Get all diagonistic histories for specifical patient.
            */
            function list(patientId) {
                return commonRepository.getData(apiUrls.patients + '/' + patientId + apiUrls.histories);
            }

            /**
            * Get history detail information.
            */
            function detail(historyId) {
                return commonRepository.getData(apiUrls.histories + '/' + historyId);
            }

            /**
            * Update history
            */
            function update(history) {
                return commonRepository.putData(apiUrls.histories + '/' + history.id, history);
            }

            /**
            * Make a decision by doctor.
            */
            function decision(historyId, decision) {
                return commonRepository.putData(apiUrls.histories + '/' + historyId, decision);
            }

            /**
            * assign patient to doctor
            * @param historyId[string] the history id of patient
            * @param doctorId[string] the account id of doctor
            */
            function assign(historyId, doctorId) {
                return commonRepository.putData(apiUrls.histories + '/' + historyId + '/assign/' + doctorId);
            }

            /**
            * requet offline discussion
            * @param data[object] posted data   
            * @param historyId[string] the history id of patient     
            */
            function requestOffline(data, historyId) {
                return commonRepository.postData(apiUrls.histories + '/' + historyId + '/discussions', data);
            }

            /**
            * make decision
            * @param data[object] posted data   
            * @param historyId[string] the history id of patient     
            */
            function makeDecision(data, historyId) {
                return commonRepository.putData(apiUrls.histories + '/' + historyId + '/decision', data);
            }

            /**
            * request a online meeting 
            * @param data[object] posted data   
            * @param historyId[string] the history id of patient     
            */
            function requestMeeting(data, historyId) {
                return commonRepository.postData(apiUrls.histories + '/' + historyId + '/meetings', data);
            }

            /**
            * transform patient to another doctor
            * @param data[object] posted data   
            * @param historyId[string] the history id of patient     
            */
            function transform(data, historyId) {
                return commonRepository.postData(apiUrls.histories + '/' + historyId + '/referral', data);
            }

            /**
             * get all mdts(offline and online) for specifical patient.
             */
            function mdtsList(historyId) {
                return commonRepository.getData(apiUrls.histories + '/' + historyId + '/mdts');
            }

            /**
            * Get history diagnosis summary.
            */
            function summary(historyId) {
                return commonRepository.getData(apiUrls.histories + '/' + historyId + '/summary');
            }

            return {
                add: add,
                list: list,
                update: update,
                detail: detail,
                decision: decision,
                assign: assign,
                requestOffline: requestOffline,
                requestMeeting: requestMeeting,
                makeDecision: makeDecision,
                transform: transform,
                mdtsList: mdtsList,
                summary: summary
            }
        }
    ]);
}());
