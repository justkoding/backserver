(function () {
    'use strict';

    angular.module('mdtRepositories').factory('announcementRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /**
            * Get all announcements by query strings.
            */
            function list(options) {
                return commonRepository.getData(apiUrls.announcements, options);
            }

            /**
            * Add a new announcement.
            */
            function add(announcement) {
                return commonRepository.postData(apiUrls.announcements, announcement);
            }

            /**
            * Get announcement information by id.
            */
            function detail(id) {
                return commonRepository.getData(apiUrls.announcements + '/' + id);
            }

            /**
            * Delete announcement by id
            */
            function del(id) {
                return commonRepository.deleteData(apiUrls.announcements + '/' + id);
            }

            /**
            * Update announcement information
            */
            function update(announcement) {
                return commonRepository.putData(apiUrls.announcements + '/' + announcement.id, announcement);
            }

            return {
                list: list,
                add: add,
                detail: detail,
                remove: del,
                update: update
            }
        }
    ]);
}());
