(function () {
    'use strict';

    angular.module('mdtRepositories').factory('favoriteRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /**
            * Get all favorites by query strings.
            */
            function list(options) {
                return commonRepository.getData(apiUrls.patientFavorites, options);
            }

            /**
            * Add a new favorite.
            */
            function add(favorite, patientId) {
                return commonRepository.postData(apiUrls.patientFavorites + '/' + patientId, favorite);
            }

            /**
            * Get favorite information by id.
            */
            function detail(id) {
                return commonRepository.getData(apiUrls.patientFavorites + '/' + id);
            }

            /**
            * Delete favorite by id
            */
            function del(id) {
                return commonRepository.deleteData(apiUrls.patientFavorites + '/' + id);
            }

            /**
            * Update favorite information
            */
            function update(favorite) {
                return commonRepository.putData(apiUrls.patientFavorites + '/' + favorite.id, favorite);
            }

            return {
                list: list,
                add: add,
                detail: detail,
                remove: del,
                update: update
            }
        }
    ]);
}());
