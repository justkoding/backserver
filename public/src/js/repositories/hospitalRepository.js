(function () {
    'use strict';

    angular.module('mdtRepositories').factory('hospitalRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /**
            * Get all hospitals by query strings.
            */
            function list(options) {
                return commonRepository.getData(apiUrls.hospitals, options);
            }

            return {               
                list: list               
            }
        }
    ]);
}());
