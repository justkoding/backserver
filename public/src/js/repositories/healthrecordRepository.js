(function () {
    'use strict';

    angular.module('mdtRepositories').factory('healthrecordRepository', [
        'commonRepository', 'apiUrls',
        function (commonRepository, apiUrls) {

            /**
            * Get all healthrecord by query strings.
            */
            function list(historyId) {
                return commonRepository.getData(apiUrls.histories + '/' + historyId + apiUrls.healthrecords);
            }                      

            /**
            * Download healthrecord by healthrecordid.
            */
            function download(id) {
                return commonRepository.getData(apiUrls.healthrecords + '/' + id);
            }

            /**
            * Delete healthrecord by healthrecordid.
            */
            function del(id) {
                return commonRepository.deleteData(apiUrls.healthrecords + '/' + id, {}, {
                    title: 'MESSAGE_HEALTH_RECORD_REMOVE_DONE_TITLE'
                });
            }

            /**
            * Get thumbnail
            */
            function thumbnail(id) {
                return commonRepository.getData(apiUrls.healthrecords + '/' + id + '/thumbnail');
            }

            /**
            * Get healthrecord password
            */
            function password(id) {
                return commonRepository.getData(apiUrls.healthrecords + '/' + id + '/password');
            }

            /**
             * Update healthrecord by healthrecordid.
             */
            function update(options) {
                return commonRepository.putData(apiUrls.healthrecords + '/' + options.id + '/note', options.data);
            }

            /**
            * Get all classifications of patient healthrecords 
            */
            function classifications() {
                return commonRepository.getData(apiUrls.classifications);
            }

            return {
                list: list,                
                download: download,
                thumbnail: thumbnail,
                password: password,
                del: del,
                update: update,
                classifications: classifications
            }
        }
    ]);
}());
