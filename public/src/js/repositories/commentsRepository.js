(function () {
    'use strict';

    angular.module('mdtRepositories').factory('commentsRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /**
            * Get all comments by query strings.
            */
            function list(options) {
                return commonRepository.getData(apiUrls.comments, options);
            }

            /**
            * Add a new comment.
            */
            function add(mdt) {
                return commonRepository.postData(apiUrls.comments, mdt);
            }

            /**
            * Get comment information by id.
            */
            function detail(id) {
                return commonRepository.getData(apiUrls.comments + '/' + id);
            }            

            return {
                // mdt
                list: list,
                add: add,
                detail: detail               
            }
        }
    ]);
}());
