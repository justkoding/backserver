(function () {
    'use strict';

    angular.module('mdtRepositories').factory('groupRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /*----------Doctor groups------------*/
            /**
            * Get all groups by query strings.
            */
            function list(options) {
                return commonRepository.getData(apiUrls.doctorGroups, options);
            }

            /**
            * Add a new group.
            */
            function add(membersId) {
                return commonRepository.postData(apiUrls.doctorGroups, membersId);
            }

            /**
            * Get group information by id.
            */
            function detail(id) {
                return commonRepository.getData(apiUrls.doctorGroups + '/' + id);
            }

            /**
            * Delete group by id
            */
            function del(id) {
                return commonRepository.deleteData(apiUrls.doctorGroups + '/' + id);
            }

            /**
            * Update group information
            */
            function update(group) {
                return commonRepository.putData(apiUrls.doctorGroups + '/' + group.id, group);
            }

            /**
            * Add memebers to speical group
            */
            function addMembers(members, groupId) {
                return commonRepository.putData(apiUrls.doctorGroups + '/' + groupId + '/members', members);
            }

            /**
            * Delete memebers from special group
            */
            function delMembers(memberId, groupId) {
                return commonRepository.deleteData(apiUrls.doctorGroups + '/' + groupId + '/members/' + memberId);
            }

            /**
            * Get all memebers for special group.
            */
            function listMembers(options) {
                var groupId = options.groupId;
                return commonRepository.getData(apiUrls.doctorGroups + '/' + groupId + '/members');
            }

            /**
            * Send message.
            */
            function sendMessage(data, groupId) {
                return commonRepository.postData(apiUrls.doctorGroups + '/' + groupId, data);
            }

            /**
            * Get messages by query string.
            */
            function listMessages(id, options) {
                return commonRepository.getData(apiUrls.doctorGroups + '/' + id + '/messages', options);
            }
            /*----------End------------*/

            return {
                // group
                list: list,
                add: add,
                detail: detail,
                remove: del,
                update: update,

                // members
                addMembers: addMembers,
                delMembers: delMembers,
                listMembers: listMembers,

                // message
                sendMessage: sendMessage,
                listMessages: listMessages
            }
        }
    ]);
}());
