(function () {
    'use strict';

    angular.module('mdtRepositories').factory('loginRepository', [
        '$q', 'dataService', 'apiUrls', '$cookieStore',
        function ($q, dataService, apiUrls, $cookieStore) {
            function login(data) {
                var deferred = $q.defer();

                dataService.post(apiUrls.accounts + '/login', data).then(function (response) {
                    deferred.resolve(response.data);
                }, function (err) {
                    deferred.reject(err);
                });

                return deferred.promise;
            };

            function logout(data) {
                var deferred = $q.defer();

                dataService.get(apiUrls.accounts + '/logout').then(function (response) {
                    deferred.resolve(response.data);
                }, function (err) {
                    deferred.reject(err);
                });

                return deferred.promise;
            };

            return {
                login: login,
                logout: logout,
            }
        }
    ]);
}());
