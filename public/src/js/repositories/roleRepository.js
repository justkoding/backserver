(function () {
    'use strict';

    angular.module('mdtRepositories').factory('roleRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /**
            * Get all users by query strings.
            */
            function list() {
                return commonRepository.getData(apiUrls.roles);
            }

            /**
            * Get all users by query strings.
            */
            function listValues() {
                return commonRepository.getData(apiUrls.rolesValues);
            }

            /**
            * Add a new user.
            */
            function add(role) {
                return commonRepository.postData(apiUrls.roles, role, {
                    title: 'MESSAGE_ROLE_ADD_DONE_TITLE'
                });
            }

            /**
            * Get user information by user id.
            */
            function detail(id) {
                return commonRepository.getData(apiUrls.roles + '/' + id);
            }

            /**
            * Delete user by user id
            */
            function deleteRole(id) {
                return mdtServices.deleteData(apiUrls.roles + '/' + id, {}, {
                    title: 'MESSAGE_ROLE_REMOVE_DONE_TITLE'
                });
            }

            /**
            * Update user information
            */
            function update(role) {
                return commonRepository.putData(apiUrls.roles + '/' + role.id, role, {
                    title: 'MESSAGE_ROLE_UPDATE_DONE_TITLE'
                });
            }

            return {
                list: list,
                add: add,
                detail: detail,
                remove: deleteRole,
                update: update,
                listValues: listValues
            }
        }
    ]);
}());
