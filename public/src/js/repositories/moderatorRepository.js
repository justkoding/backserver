(function () {
    'use strict';

    angular.module('mdtRepositories').factory('moderatorRepository', [
        '$q', 'commonRepository', 'apiUrls',
        function ($q, commonRepository, apiUrls) {

            /**
             * Get all announcements by query strings.
             */
            function list(options) {
                return commonRepository.getData(apiUrls.patients + '/queue', options);
            }



            return {
                list: list
            }
        }
    ]);
}());
