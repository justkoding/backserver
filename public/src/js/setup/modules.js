(function () {
    'use strict';

    /* Modules */
    angular.module('mdtConstants', []);
    angular.module('mdtServices', []);
    angular.module('mdtFilters', []);
    angular.module('mdtDrectives', []);
    angular.module('mdtRepositories', []);
    angular.module('mdtTemplates', []);

    angular.module('app', [
        /*base module*/
        'ui.router',
        'ngSanitize',      

        /*bootstrap*/
        'ui.bootstrap',
        'ui.bootstrap.tpls',

        'angularMoment',
        'ngAnimate',
        'toaster',

        /*upload*/
        'ngFileUpload',
               
        /*translate*/
        'ngCookies',
        'pascalprecht.translate',

        /*ngStorage*/
        'ngStorage',

        /*valdr*/
        'valdr',

        /*xeditable*/
        'xeditable',
       
        /*autocomplete*/
        'angucomplete-alt',

        /*customizable plugin*/
        'mybootstrap.modal',

        /*application module*/
        'mdtConstants',
        'mdtTemplates',
        'mdtRepositories',
        'mdtServices',
        'mdtDrectives',
        'mdtFilters'
    ]);
}());
