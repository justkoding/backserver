angular.module('mdtTemplates').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('src/templates/dayPicker/day.html',
    "<table role=\"grid\" aria-labelledby=\"{{uniqueId}}-title\" aria-activedescendant=\"{{activeDateId}}\">\r" +
    "\n" +
    "      <thead>\r" +
    "\n" +
    "        <tr>\r" +
    "\n" +
    "            <th><button type=\"button\" class=\"btn pull-left\" ng-click=\"move(-1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-left\"></i></button></th>\r" +
    "\n" +
    "            <th colspan=\"{{5}}\"><button id=\"{{uniqueId}}-title\" role=\"heading\" aria-live=\"assertive\" aria-atomic=\"true\" type=\"button\" class=\"btn\" ng-click=\"toggleMode()\" tabindex=\"-1\" style=\"width:100%;\"><strong>{{title}}</strong></button></th>\r" +
    "\n" +
    "            <th><button type=\"button\" class=\"btn pull-right\" ng-click=\"move(1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-right\"></i></button></th>\r" +
    "\n" +
    "        </tr>\r" +
    "\n" +
    "        <tr>\r" +
    "\n" +
    "            <th ng-repeat=\"label in labels track by $index\" class=\"text-center\"><small aria-label=\"{{label.full}}\">{{label.abbr}}</small></th>\r" +
    "\n" +
    "        </tr>\r" +
    "\n" +
    "      </thead>\r" +
    "\n" +
    "      <tbody>\r" +
    "\n" +
    "        <tr ng-repeat=\"row in rows track by $index\">\r" +
    "\n" +
    "          <td ng-repeat=\"dt in row track by dt.date\" class=\"text-center\" role=\"gridcell\" id=\"{{dt.uid}}\" aria-disabled=\"{{!!dt.disabled}}\">\r" +
    "\n" +
    "            <button type=\"button\" ng-style=\"{'background-color': dt.color, 'border-color': dt.borderColor}\" style=\"width:100%;\" class=\"btn\"  ng-class=\"{'btn-info': dt.selected, active: isActive(dt), 'btn-cannot-create-meeting':dt.disabled && dt.iconVisibility,'btn-meeting-default': !dt.disabled && !dt.hasMeeting && dt.iconVisibility}\" ng-click=\"select(dt.date)\" ng-disabled=\"dt.disabled\" tabindex=\"-1\">\r" +
    "\n" +
    "                    <span ng-class=\"{'text-muted': dt.secondary, 'text-info': dt.current}\">{{dt.label}}</span>\r" +
    "\n" +
    "                    <span style=\"padding-left:35px;\" ng-if=\"dt.hasMeeting && dt.iconVisibility\"><img src=\"/assets/src/img/icons/meeting.png\" /></span>\r" +
    "\n" +
    "             </button>\r" +
    "\n" +
    "          </td>\r" +
    "\n" +
    "        </tr>\r" +
    "\n" +
    "      </tbody>\r" +
    "\n" +
    "</table>"
  );


  $templateCache.put('src/templates/login/login.tpl.html',
    "<div class=\"app-modal\">\r" +
    "\n" +
    "    <div class=\"app-modal-wrap\">\r" +
    "\n" +
    "        <div class=\"app-modal-header\">\r" +
    "\n" +
    "            <h3 class=\"app-modal-title\">I'm a middle modal!</h3>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"app-modal-body\">\r" +
    "\n" +
    "            <form novalidate name=\"login\" valdr-type=\"login\">\r" +
    "\n" +
    "                <div valdr-form-group class=\"form-group\">\r" +
    "\n" +
    "                    <label for=\"name\">Name</label>\r" +
    "\n" +
    "                    <input class=\"form-control\" name=\"firstName\" ng-model=\"data.name\" />\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div valdr-form-group class=\"form-group\">\r" +
    "\n" +
    "                    <label for=\"name\">Pwd</label>\r" +
    "\n" +
    "                    <input class=\"form-control\" name=\"lastName\" ng-model=\"data.pwd\" />\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <input type=\"submit\" value=\"submit\" />\r" +
    "\n" +
    "            </form>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"app-modal-footer\">\r" +
    "\n" +
    "            <div class=\"data-btns\">\r" +
    "\n" +
    "                <button class=\"btn cancel\" ng-click=\"cancel()\">\r" +
    "\n" +
    "                    {{'TEXT_CANCEL'|translate}}\r" +
    "\n" +
    "                </button>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <button class=\"btn ok\" ng-click=\"ok(login)\">\r" +
    "\n" +
    "                    {{'TEXT_OK'|translate}}\r" +
    "\n" +
    "                </button>\r" +
    "\n" +
    "            </div>         \r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/login/logoutpanel.tpl.html',
    "<div ng-show=\"isShown\" class=\"logout-panel\">\r" +
    "\n" +
    "    <ul class=\"logout-dropdown-menu\">\r" +
    "\n" +
    "        <li ng-click=\"changePwd($event)\">\r" +
    "\n" +
    "            <a href=\"javascript:void(0)\">{{'LOGIN_CHANGE_PASSWORD' | translate}}\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </li>\r" +
    "\n" +
    "        <li ng-click=\"gotoUserProfile()\">\r" +
    "\n" +
    "            <a href=\"javascript:void(0)\">{{'LOGIN_USER_PROFILE' | translate}}\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </li>\r" +
    "\n" +
    "        <li ng-click=\"logout($event)\">\r" +
    "\n" +
    "            <a href=\"javascript:void(0)\">{{'LOGIN_LOGOUT' | translate}}\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </li>\r" +
    "\n" +
    "    </ul>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/pageFooter/pageFooter.tpl.html',
    "<footer class=\"footer-bar zindex-11\" ng-class=\"{'bg-white': loginPage}\">\r" +
    "\n" +
    "    <p class=\"txt-c\">\r" +
    "\n" +
    "        <i class=\"fa fa-copyright\"></i>\r" +
    "\n" +
    "        2015\r" +
    "\n" +
    "        <label class=\"blue\">Medtronic</label>\r" +
    "\n" +
    "        <span>|</span>\r" +
    "\n" +
    "        <span>{{'WEB_VERSION'|translate}}</span>\r" +
    "\n" +
    "        <span>{{version}}</span>\r" +
    "\n" +
    "    </p>\r" +
    "\n" +
    "</footer>\r" +
    "\n"
  );


  $templateCache.put('src/templates/user/addOrEditUser.tpl.html',
    "<div class=\"app-modal add-edit-user\">\r" +
    "\n" +
    "    <div class=\"app-modal-wrap\">\r" +
    "\n" +
    "        <div class=\"app-modal-header\">\r" +
    "\n" +
    "            <h3 class=\"app-modal-title\" translate=\"USER_ADD_EDIT_FORM_TITLE\">\r" +
    "\n" +
    "                <!--Create a new doctor-->\r" +
    "\n" +
    "            </h3>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"app-modal-body\">\r" +
    "\n" +
    "            <form novalidate name=\"addOrEditUserForm\" valdr-type=\"addOrEditUser\" role=\"form\">\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <div valdr-form-group class=\"form-group col-md-4 col-sm-6\">\r" +
    "\n" +
    "                        <label for=\"name\" translate=\"USER_ADD_EDIT_LOGIN_NAME\">\r" +
    "\n" +
    "                            <!--login Name-->\r" +
    "\n" +
    "                        </label>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"account\" ng-model=\"data.account\" />\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div valdr-form-group class=\"form-group col-md-4 col-sm-6\">\r" +
    "\n" +
    "                        <label for=\"name\" translate=\"USER_ADD_EDIT_PASSWORD\">\r" +
    "\n" +
    "                            <!--Password-->\r" +
    "\n" +
    "                        </label>\r" +
    "\n" +
    "                        <input type=\"password\" class=\"form-control\" name=\"password\" ng-model=\"data.password\" />\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div valdr-form-group class=\"form-group col-md-4 col-sm-6\">\r" +
    "\n" +
    "                        <label for=\"name\" translate=\"USER_ADD_EDIT_ACCOUNT_STATE\">\r" +
    "\n" +
    "                            <!--Account State-->\r" +
    "\n" +
    "                        </label>\r" +
    "\n" +
    "                        <div class=\"row\">\r" +
    "\n" +
    "                            <div class=\"col-md-6 col-sm-6\">\r" +
    "\n" +
    "                                <div class=\"input-group\">\r" +
    "\n" +
    "                                    <span class=\"input-group-addon\">\r" +
    "\n" +
    "                                        <input type=\"radio\" id=\"status1\" name=\"status\" value=\"1\" ng-model=\"data.status\" ng-change=\"changed(1)\" />\r" +
    "\n" +
    "                                    </span>\r" +
    "\n" +
    "                                    <div class=\"form-control\">启用</div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"col-md-6  col-sm-6\">\r" +
    "\n" +
    "                                <div class=\"input-group\">\r" +
    "\n" +
    "                                    <span class=\"input-group-addon\">\r" +
    "\n" +
    "                                        <input type=\"radio\" id=\"status2\" name=\"status\" value=\"2\" ng-model=\"data.status\" ng-change=\"changed(2)\" />\r" +
    "\n" +
    "                                    </span>\r" +
    "\n" +
    "                                    <div class=\"form-control\">禁用</div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div valdr-form-group class=\"form-group col-md-4 col-sm-6\">\r" +
    "\n" +
    "                        <label for=\"name\" translate=\"USER_ADD_EDIT_FULL_NAME\">\r" +
    "\n" +
    "                            <!--Full Name-->\r" +
    "\n" +
    "                        </label>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"name\" ng-model=\"data.name\" />\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div valdr-form-group class=\"form-group col-md-4 col-sm-6\">\r" +
    "\n" +
    "                        <label for=\"name\" translate=\"USER_ADD_EDIT_GENDER\">\r" +
    "\n" +
    "                            <!--Gender-->\r" +
    "\n" +
    "                        </label>\r" +
    "\n" +
    "                        <div class=\"row\">\r" +
    "\n" +
    "                            <div class=\"col-md-6 col-sm-6\">\r" +
    "\n" +
    "                                <div class=\"input-group\">\r" +
    "\n" +
    "                                    <span class=\"input-group-addon\">\r" +
    "\n" +
    "                                        <input type=\"radio\" name=\"sex\" value=\"1\" ng-model=\"data.sex\" />\r" +
    "\n" +
    "                                    </span>\r" +
    "\n" +
    "                                    <div class=\"form-control\">男</div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"col-md-6  col-sm-6\">\r" +
    "\n" +
    "                                <div class=\"input-group\">\r" +
    "\n" +
    "                                    <span class=\"input-group-addon\">\r" +
    "\n" +
    "                                        <input type=\"radio\" name=\"sex\" value=\"2\" ng-model=\"data.sex\" />\r" +
    "\n" +
    "                                    </span>\r" +
    "\n" +
    "                                    <div class=\"form-control\">女</div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div valdr-form-group class=\"form-group col-md-4 col-sm-6\">\r" +
    "\n" +
    "                        <label for=\"name\" translate=\"USER_ADD_EDIT_BIRTHDAY\">\r" +
    "\n" +
    "                            <!--Birthday-->\r" +
    "\n" +
    "                        </label>\r" +
    "\n" +
    "                        <p>\r" +
    "\n" +
    "                            <input type=\"date\"\r" +
    "\n" +
    "                                   class=\"form-control\"\r" +
    "\n" +
    "                                   name=\"birthday\"\r" +
    "\n" +
    "                                   ng-model=\"data.birthday\" />\r" +
    "\n" +
    "                        </p>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div valdr-form-group class=\"form-group col-md-4 col-sm-6\">\r" +
    "\n" +
    "                        <label for=\"name\" translate=\"USER_ADD_EDIT_TITLE\">\r" +
    "\n" +
    "                            <!--Title-->\r" +
    "\n" +
    "                        </label>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"usertype\" ng-model=\"data.usertype\" />\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div valdr-form-group class=\"form-group col-md-4 col-sm-6\">\r" +
    "\n" +
    "                        <label for=\"name\" translate=\"USER_ADD_EDIT_DESCRIPTION\">\r" +
    "\n" +
    "                            <!--Description-->\r" +
    "\n" +
    "                        </label>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"description\" ng-model=\"data.description\" />\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div valdr-form-group class=\"form-group col-md-4 col-sm-6\">\r" +
    "\n" +
    "                        <label for=\"name\" translate=\"USER_ADD_EDIT_OTHERS\">\r" +
    "\n" +
    "                            <!--Others-->\r" +
    "\n" +
    "                        </label>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"extinfo\" ng-model=\"data.extinfo\" />\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </form>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"app-modal-footer\">\r" +
    "\n" +
    "            <div class=\"data-btns\">\r" +
    "\n" +
    "                <button class=\"btn cancel\" ng-click=\"cancel()\">\r" +
    "\n" +
    "                    {{'TEXT_CANCEL'|translate}}\r" +
    "\n" +
    "                </button>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <button class=\"btn ok\" ng-click=\"ok()\" ng-disabled=\"addOrEditUserForm.$invalid\" ng-click=\"ok(addOrEditUserForm)\">\r" +
    "\n" +
    "                    {{'TEXT_OK'|translate}}\r" +
    "\n" +
    "                </button>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgCircle/xgCircle.tpl.html',
    "<svg class=\"countdown\"\r" +
    "\n" +
    "     ng-attr-width=\"{{diameter}}\"\r" +
    "\n" +
    "     ng-attr-height=\"{{diameter}}\"\r" +
    "\n" +
    "     ng-attr-viewbox=\"0 0 {{diameter}} {{diameter}}\">\r" +
    "\n" +
    "    <defs>\r" +
    "\n" +
    "        <mask ng-attr-id=\"countdown-mask-{{$id}}\">\r" +
    "\n" +
    "            <circle cx=\"50%\" cy=\"50%\" ng-attr-r=\"{{radius}}\" fill=\"#ddd\" />\r" +
    "\n" +
    "            <circle cx=\"50%\" cy=\"50%\" ng-attr-r=\"{{radius - stroke}}\" fill=\"#000\" />            \r" +
    "\n" +
    "        </mask>\r" +
    "\n" +
    "    </defs>\r" +
    "\n" +
    "    <g ng-attr-mask=\"url(#countdown-mask-{{$id}})\">       \r" +
    "\n" +
    "        <circle cx=\"50%\" cy=\"50%\" fill=\"none\" ng-attr-r=\"{{cradius}}\" ng-attr-stroke=\"{{cfill}}\" ng-attr-stroke-width=\"{{stroke}}\" />\r" +
    "\n" +
    "        <path class=\"countdown-border\" ng-attr-id=\"loader-{{$id}}\" />\r" +
    "\n" +
    "    </g>\r" +
    "\n" +
    "    <g transform=\"translate(0, 10)\">\r" +
    "\n" +
    "        <text x=\"50%\" y=\"40%\" font-size=\"16\" text-anchor=\"middle\" fill=\"#27c3f0\">离预约会诊还有</text>\r" +
    "\n" +
    "        <text x=\"50%\" y=\"55%\" font-size=\"16\" text-anchor=\"middle\" fill=\"#bdbdbd\">2天18小时25分</text>\r" +
    "\n" +
    "    </g>    \r" +
    "\n" +
    "</svg>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgCountdown/xgCountdown.tpl.html',
    "<svg class=\"countdown\"\r" +
    "\n" +
    "     ng-attr-width=\"{{diameter}}\"\r" +
    "\n" +
    "     ng-attr-height=\"{{diameter}}\"\r" +
    "\n" +
    "     ng-attr-viewbox=\"0 0 {{diameter}} {{diameter}}\">\r" +
    "\n" +
    "    <defs>\r" +
    "\n" +
    "        <mask ng-attr-id=\"countdown-mask-{{$id}}\">\r" +
    "\n" +
    "            <circle cx=\"50%\" cy=\"50%\" ng-attr-r=\"{{radius}}\" fill=\"#ddd\" />\r" +
    "\n" +
    "            <circle cx=\"50%\" cy=\"50%\" ng-attr-r=\"{{radius - stroke}}\" fill=\"#000\" />\r" +
    "\n" +
    "        </mask>\r" +
    "\n" +
    "    </defs>\r" +
    "\n" +
    "    <g ng-attr-mask=\"url(#countdown-mask-{{$id}})\">\r" +
    "\n" +
    "        <circle cx=\"50%\" cy=\"50%\" fill=\"none\" ng-attr-r=\"{{cradius}}\" ng-attr-stroke=\"{{fill}}\" ng-attr-stroke-width=\"{{stroke}}\" />\r" +
    "\n" +
    "        <path class=\"countdown-border\" ng-attr-id=\"loader-{{$id}}\" />\r" +
    "\n" +
    "    </g>\r" +
    "\n" +
    "    <g transform=\"translate(0, 8)\">\r" +
    "\n" +
    "        <text x=\"50%\" y=\"30%\" font-size=\"12\" text-anchor=\"middle\" ng-attr-fill=\"{{textColor}}\">{{text|translate}}</text>\r" +
    "\n" +
    "        <text ng-if=\"day!=0\" x=\"50%\" y=\"50%\" font-size=\"18\" text-anchor=\"middle\" ng-attr-fill=\"{{textColor}}\">\r" +
    "\n" +
    "            {{day}}{{'TEXT_DAY'|translate}}\r" +
    "\n" +
    "        </text>      \r" +
    "\n" +
    "        <text ng-if=\"day==0\" x=\"50%\" y=\"50%\" font-size=\"18\" text-anchor=\"middle\" ng-attr-fill=\"{{textColor}}\">\r" +
    "\n" +
    "            {{hour|toFixed}}:{{min|toFixed}}\r" +
    "\n" +
    "        </text>       \r" +
    "\n" +
    "    </g>\r" +
    "\n" +
    "</svg>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgCountdown/xgCountdown.tpl_2.html',
    "<svg class=\"countdown\"\r" +
    "\n" +
    "     ng-attr-width=\"{{diameter}}\"\r" +
    "\n" +
    "     ng-attr-height=\"{{diameter}}\"\r" +
    "\n" +
    "     ng-attr-viewbox=\"0 0 {{diameter}} {{diameter}}\">   \r" +
    "\n" +
    "    <g>       \r" +
    "\n" +
    "        <circle cx=\"50%\" cy=\"50%\" fill=\"none\" ng-attr-r=\"{{radius}}\" ng-attr-stroke=\"{{cfill}}\" ng-attr-stroke-width=\"{{stroke}}\" />\r" +
    "\n" +
    "        <path class=\"countdown-border\" ng-attr-id=\"loader-{{$id}}\" fill=\"none\" ng-attr-stroke=\"{{color}}\" ng-attr-stroke-width=\"{{stroke}}\" />\r" +
    "\n" +
    "    </g>\r" +
    "\n" +
    "    <g transform=\"translate(0, 10)\">\r" +
    "\n" +
    "        <text x=\"50%\" y=\"40%\" font-size=\"16\" text-anchor=\"middle\" fill=\"#27c3f0\">{{text|translate}}</text>\r" +
    "\n" +
    "        <text x=\"50%\" y=\"55%\" font-size=\"16\" text-anchor=\"middle\" fill=\"#bdbdbd\">{{day}}{{'TEXT_DAY'|translate}}{{hour}}{{'TEXT_HOUR'|translate}}{{min}}{{'TEXT_MIN'|translate}}</text>\r" +
    "\n" +
    "    </g>    \r" +
    "\n" +
    "</svg>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgCountdown/xgCountdown.tpl_3.html',
    "<div>\r" +
    "\n" +
    "    <span>{{text}}</span>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgEditTrash/xgEditTrash.tpl.html',
    "<div class=\"xg-edit-trash\">\r" +
    "\n" +
    "    <div class=\"xg-edit\"\r" +
    "\n" +
    "         ng-hide=\"onlyTrash\"\r" +
    "\n" +
    "         ng-click=\"onEdit(entity)\">\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"xg-trash\"\r" +
    "\n" +
    "         ng-hide=\"onlyEdit\"\r" +
    "\n" +
    "         ng-click=\"onTrash(entity)\">\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgEndTimePicker/xgEndTimePicker.tpl.html',
    "<div class=\"xg-time-picker\">\r" +
    "\n" +
    "    <select\r" +
    "\n" +
    "        class=\"form-control f-l\"\r" +
    "\n" +
    "        ng-model=\"from\"\r" +
    "\n" +
    "        ng-change=\"onTimeChanged(from)\"\r" +
    "\n" +
    "        ng-options=\"opt.text for opt in fromData\"\r" +
    "\n" +
    "        name=\"from\">\r" +
    "\n" +
    "    </select>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgFavorite/xgFavorite.tpl.html',
    "<div class=\"favorite-icon\">   \r" +
    "\n" +
    "    <span class=\"hidden-sm\"\r" +
    "\n" +
    "          ng-if=\"!favorite.isFavorite\"\r" +
    "\n" +
    "          ng-click=\"onFavorite(true); $event.stopPropagation();\">\r" +
    "\n" +
    "        <i class=\"fa fa-heart-o\"></i>\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    <span class=\"hidden-sm\" ng-if=\"favorite.isFavorite\" ng-click=\"onFavorite(false);$event.stopPropagation();\">\r" +
    "\n" +
    "        <i class=\"fa fa-heart active\"></i>\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgFilter/xgFilter.tpl.html',
    "<div class=\"xg-filter\">\r" +
    "\n" +
    "    <label class=\"f-l\">\r" +
    "\n" +
    "        <label class=\"f-l\">\r" +
    "\n" +
    "            <label>{{title}}</label>\r" +
    "\n" +
    "        </label>\r" +
    "\n" +
    "        <label class=\"selected-item f-l\">\r" +
    "\n" +
    "            {{sItem}}\r" +
    "\n" +
    "        </label>\r" +
    "\n" +
    "    </label>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <label class=\"f-r f-icon-wrap\">\r" +
    "\n" +
    "        <span class=\"dropdown xg-filter\" dropdown on-toggle=\"toggled(open)\">\r" +
    "\n" +
    "            <label class=\"dropdown-toggle\" dropdown-toggle>\r" +
    "\n" +
    "                <img ng-show=\"!isActive\" src=\"/assets/src/img/icons/filter0.png\" />\r" +
    "\n" +
    "                <img ng-show=\"isActive\" src=\"/assets/src/img/icons/filter1.png\" />\r" +
    "\n" +
    "            </label>\r" +
    "\n" +
    "            <ul class=\"dropdown-menu\">\r" +
    "\n" +
    "                <li ng-repeat=\"item in dpData\" ng-click=\"selected(item)\">\r" +
    "\n" +
    "                    <a href>{{item.name}}</a>\r" +
    "\n" +
    "                </li>\r" +
    "\n" +
    "            </ul>\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </label>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgLine/xgLine.tpl.html',
    "<div class=\"xg-line\"></div>"
  );


  $templateCache.put('src/templates/xgLoading/xgLoading.tpl.html',
    "<div class=\"hide loading\">\r" +
    "\n" +
    "    <div class=\"loading-backdrop\"></div>\r" +
    "\n" +
    "    <div class=\"circular-wrap\">\r" +
    "\n" +
    "        <div id=\"circularG\">\r" +
    "\n" +
    "            <div id=\"circularG_1\" class=\"circularG\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div id=\"circularG_2\" class=\"circularG\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div id=\"circularG_3\" class=\"circularG\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div id=\"circularG_4\" class=\"circularG\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div id=\"circularG_5\" class=\"circularG\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div id=\"circularG_6\" class=\"circularG\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div id=\"circularG_7\" class=\"circularG\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div id=\"circularG_8\" class=\"circularG\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>    \r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgLocked/xgLocked.tpl.html',
    "<div>\r" +
    "\n" +
    "    <div ng-click=\"onClick()\" class=\"xg-btn lock\">\r" +
    "\n" +
    "        <p>{{'TEXT_EDIT_LOCKED' | translate}}</p>\r" +
    "\n" +
    "    </div>  \r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgMeetingTimeLocation/xgMeetingTimeLocation.tpl.html',
    "<div>\r" +
    "\n" +
    "    <div ng-if=\"isChinese\">\r" +
    "\n" +
    "        <span>{{startDateTime}}</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div ng-if=\"!isChinese\">\r" +
    "\n" +
    "        <span>{{startDateTimeEng}}</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgMessage/xgMessage.html',
    "<h1>{{msg}} <div data-ng-transclude></div></h1>"
  );


  $templateCache.put('src/templates/xgPagination/xgPagination.tpl.html',
    "<div class=\"xg-pagination\" ng-show=\"totalItems != 0\">\r" +
    "\n" +
    "    <div class=\"row\">\r" +
    "\n" +
    "        <div class=\"col-md-6\">\r" +
    "\n" +
    "            <div class=\"f-l\">\r" +
    "\n" +
    "                <label>\r" +
    "\n" +
    "                    {{'TEXT_PAGINATION_PER_PAGE' | translate}}\r" +
    "\n" +
    "                </label>\r" +
    "\n" +
    "                <select\r" +
    "\n" +
    "                    class=\"num-pages form-control inline-block\"\r" +
    "\n" +
    "                    ng-model=\"pageSize\"\r" +
    "\n" +
    "                    ng-change=\"pageSizeChanged()\"\r" +
    "\n" +
    "                    ng-options=\"opt.text for opt in sizeOptions\">\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"f-l m-l-10\">\r" +
    "\n" +
    "                <label class=\"text-entries\" ng-show=\"showEntitiesText == 'true'\">\r" +
    "\n" +
    "                    <label>{{'TEXT_PAGINATION_CURRENT' | translate}}</label>\r" +
    "\n" +
    "                    <label>{{start}}</label>\r" +
    "\n" +
    "                    <label>-</label>\r" +
    "\n" +
    "                    <label>{{end}}</label>\r" +
    "\n" +
    "                    <label>{{'TEXT_PAGINATION_UNIT' | translate}}, </label>\r" +
    "\n" +
    "                    <label>{{'TEXT_PAGINATION_TOTAL' | translate}}</label>\r" +
    "\n" +
    "                    <label>{{totalItems}}</label>\r" +
    "\n" +
    "                    <label>{{'TEXT_PAGINATION_UNIT' | translate}}</label>\r" +
    "\n" +
    "                </label>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"col-md-6 txt-r\" ng-show=\"visibility\">\r" +
    "\n" +
    "            <div class=\"m-r-10 m-l-10 inline-flex\" ng-show=\"showPageText == 'true'\">\r" +
    "\n" +
    "                <label class=\"f-l m-r-5\">{{'TEXT_PAGINATION_CURRENT' | translate}}</label>\r" +
    "\n" +
    "                <input type=\"number\" min=\"1\" max=\"{{numPages}}\" ng-model=\"currentPage\" class=\"form-control current-page f-l\" />\r" +
    "\n" +
    "                <label class=\"f-l m-l-5\">/</label>\r" +
    "\n" +
    "                <label class=\"f-l m-r-5\">{{numPages}} </label>\r" +
    "\n" +
    "                <label class=\"f-l\">{{'TEXT_PAGINATION_PAGE' | translate}}</label>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <input type=\"button\" class=\"btn btn-default page-btn\" value=\"{{'TEXT_PAGINATION_PREVIOUS' | translate}}\" ng-click=\"previousPage()\" ng-disabled=\"currentPage === 1\" />\r" +
    "\n" +
    "            <input type=\"button\" class=\"btn btn-default page-btn\" value=\"{{'TEXT_PAGINATION_NEXT' | translate}}\" ng-click=\"nextPage()\" ng-disabled=\"currentPage === numPages\" />\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgPatientPanel/xgPatientPanel.tpl.html',
    "<!--existing patient info-->\r" +
    "\n" +
    "<div class=\"panel panel-default\">\r" +
    "\n" +
    "    <div class=\"panel-heading\">\r" +
    "\n" +
    "        <h3 class=\"panel-title\">\r" +
    "\n" +
    "            <label>{{'XG_PATIENT_PANEL_TITLE' | translate}}</label>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <input type=\"button\" class=\"btn btn-primary f-r\" value=\"{{'TEXT_YES' | translate}}\" ng-click=\"accept()\" />\r" +
    "\n" +
    "            <input type=\"button\" class=\"btn btn-default f-r\" value=\"{{'TEXT_NO' | translate}}\" ng-click=\"reject()\" />\r" +
    "\n" +
    "        </h3>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"panel-body\">\r" +
    "\n" +
    "        <div class=\"panel-content\">\r" +
    "\n" +
    "            <div class=\"row\">\r" +
    "\n" +
    "                <!--patient id-->\r" +
    "\n" +
    "                <div class=\"form-group col-md-12\">\r" +
    "\n" +
    "                    <label class=\"col-md-4 control-label\">{{'PATIENT_DETAIL_ID' | translate}}</label>\r" +
    "\n" +
    "                    <label class=\"col-md-8\">\r" +
    "\n" +
    "                        {{patient.patientid}}\r" +
    "\n" +
    "                    </label>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <!--name-->\r" +
    "\n" +
    "                <div class=\"form-group col-md-12\">\r" +
    "\n" +
    "                    <label class=\"col-md-4 control-label\">{{'PATIENT_LIST_NAME' | translate}}</label>\r" +
    "\n" +
    "                    <label class=\"col-md-8\">\r" +
    "\n" +
    "                        {{patient.name}}\r" +
    "\n" +
    "                    </label>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <!--hosiptal id-->\r" +
    "\n" +
    "                <div class=\"form-group col-md-12\">\r" +
    "\n" +
    "                    <label class=\"col-md-4 control-label\">{{'PATIENT_LIST_SEEDOCTOR_HOSPITAL_ID' | translate}}</label>\r" +
    "\n" +
    "                    <label class=\"col-md-8\">\r" +
    "\n" +
    "                        {{patient.hospital}}\r" +
    "\n" +
    "                    </label>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <!--dept-->\r" +
    "\n" +
    "                <div class=\"form-group col-md-12\">\r" +
    "\n" +
    "                    <label class=\"col-md-4 control-label\">{{'PATIENT_LIST_DEPT' | translate}}</label>\r" +
    "\n" +
    "                    <label class=\"col-md-8\">\r" +
    "\n" +
    "                        {{patient.dept.name}}\r" +
    "\n" +
    "                    </label>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <!--birthday-->\r" +
    "\n" +
    "                <div class=\"form-group col-md-12\">\r" +
    "\n" +
    "                    <label class=\"col-md-4 control-label\">{{'PATIENT_DETAIL_BIRTHDAY' | translate}}</label>\r" +
    "\n" +
    "                    <label class=\"col-md-8\">\r" +
    "\n" +
    "                        {{patient.dept.birthday | amDateFormat:'YYYY-MM-DD'}}\r" +
    "\n" +
    "                    </label>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <!--error-->\r" +
    "\n" +
    "    <div class=\"error-section\">\r" +
    "\n" +
    "        <div ng-show=\"hasError\" class=\"error valdr-message ng-invalid custom-error\">\r" +
    "\n" +
    "            <span>\r" +
    "\n" +
    "                {{'PATIENT_DETAIL_LOAD_FAILED_MESSAGE' | translate}}\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgProgress/xgProgress.tpl.html',
    "<div class=\"xg-progress-bar\">\r" +
    "\n" +
    "    <div class=\"xg-progress\">\r" +
    "\n" +
    "        <span class=\"xg-progress-value\" ng-show=\"!smallMode\">{{text}}</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgRadio/xgRadio.tpl.html',
    "<label>\r" +
    "\n" +
    "    <input\r" +
    "\n" +
    "        type=\"radio\"\r" +
    "\n" +
    "        name=\"{{name}}\"   \r" +
    "\n" +
    "        value=\"{{value}}\"    \r" +
    "\n" +
    "        ng-click=\"onClick()\"/>\r" +
    "\n" +
    "    <span>{{text}}\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "</label>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgSearch/xgSearch.tpl.html',
    "<div class=\"xg-search-control\">\r" +
    "\n" +
    "    <span class=\"search-icon\"\r" +
    "\n" +
    "        ng-click=\"search()\">\r" +
    "\n" +
    "        <i class=\"fa fa-search\"></i>\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    <input class=\"search-text\"\r" +
    "\n" +
    "        ng-model=\"data.text\"\r" +
    "\n" +
    "        type=\"search\"\r" +
    "\n" +
    "        placeholder=\"search\" />\r" +
    "\n" +
    "    <select\r" +
    "\n" +
    "        class=\"search-options\"\r" +
    "\n" +
    "        ng-model=\"data.option\"\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        ng-options=\"opt.name for opt in data.options track by opt.id\">\r" +
    "\n" +
    "    </select>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgSelect/xgSelect.tpl.html',
    "<div class=\"xg-select\" ng-mouseenter=\"\" ng-mouseleave=\"opened=false\">\r" +
    "\n" +
    "    <input type=\"text\"\r" +
    "\n" +
    "           ng-focus=\"opened=false\"\r" +
    "\n" +
    "           class=\"form-control\"\r" +
    "\n" +
    "           name=\"{{name}}\"\r" +
    "\n" +
    "           ng-model=\"data\"\r" +
    "\n" +
    "           placeholder=\"{{'LOGIN_USER_NAME' | translate}}\" />\r" +
    "\n" +
    "    <span class=\"select-caret-down\" ng-click=\"toggle()\">\r" +
    "\n" +
    "        <i class=\"fa fa-caret-down\"></i>\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    <ul class=\"dropdown-menu\" ng-class=\"{'show': opened}\">\r" +
    "\n" +
    "        <li ng-if=\"items==null || items.length ===0\">\r" +
    "\n" +
    "            <a href=\"javascript:void(0)\">{{'XGSELECT_NONE_USER'|translate}}</a>\r" +
    "\n" +
    "        </li>\r" +
    "\n" +
    "        <li ng-repeat=\"item in items\">\r" +
    "\n" +
    "            <a href=\"javascript:void(0)\"\r" +
    "\n" +
    "                data-value=\"{{item.value}}\"\r" +
    "\n" +
    "                ng-click=\"onClickItem(item)\">{{item.text}}</a>\r" +
    "\n" +
    "        </li>\r" +
    "\n" +
    "    </ul>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgShare/xgShare.tpl.html',
    "<div>\r" +
    "\n" +
    "    <div class=\"xg-share\"\r" +
    "\n" +
    "         ng-click=\"onShare(entity)\">\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgSmartEditTrash/xgSmartEditTrash.tpl.html',
    "<div class=\"xg-smart-edit-trash\">\r" +
    "\n" +
    "    <div class=\"xg-edit\"\r" +
    "\n" +
    "        ng-click=\"edit()\">\r" +
    "\n" +
    "        <i class=\"fa fa-edit\"></i>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"xg-trash\"\r" +
    "\n" +
    "        ng-click=\"trash()\">\r" +
    "\n" +
    "        <i class=\"fa fa-trash\"></i>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgSort/xgSort.tpl.html',
    "<div class=\"xg-sort\">\r" +
    "\n" +
    "    <label class=\"xg-text\">{{text}}</label>\r" +
    "\n" +
    "    <span\r" +
    "\n" +
    "        ng-show=\"btnStatus.sDefault\"\r" +
    "\n" +
    "        class=\"sort-icon\"\r" +
    "\n" +
    "        ng-click=\"onDefault()\">\r" +
    "\n" +
    "        <img ng-show=\"!isActive\" src=\"/assets/src/img/icons/order0.png\" />\r" +
    "\n" +
    "        <img ng-show=\"isActive\" src=\"/assets/src/img/icons/order1.png\" />\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    <span ng-show=\"!btnStatus.sDefault\"\r" +
    "\n" +
    "        class=\"sort-icon\"\r" +
    "\n" +
    "        ng-click=\"sort()\">\r" +
    "\n" +
    "        <img src=\"/assets/src/img/icons/order3.png\" ng-show=\"!btnStatus.sDefault && btnStatus.sAsc\" />\r" +
    "\n" +
    "        <img src=\"/assets/src/img/icons/order2.png\" ng-show=\"!btnStatus.sDefault && !btnStatus.sAsc\" />\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgTab/xgTab.tpl.html',
    "<div class=\"xg-tab\">\r" +
    "\n" +
    "    <ul class=\"nav nav-tabs\">\r" +
    "\n" +
    "        <!--placeholder-->\r" +
    "\n" +
    "        <li class=\"\">\r" +
    "\n" +
    "            <a href=\"javascript:void(0)\"></a>\r" +
    "\n" +
    "        </li>\r" +
    "\n" +
    "        <li ng-repeat=\"tab in tabs\" ng-class=\"{'active': tab.active}\" ng-click=\"clickTab(tab, $index)\">\r" +
    "\n" +
    "            <a href=\"javascript:void(0)\">{{tab.name}}\r" +
    "\n" +
    "                <span ng-if=\"tab.removed\" ng-click=\"deleteTab(tab, $index)\">\r" +
    "\n" +
    "                    <i class=\"fa fa-remove\"></i>\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "            </a>           \r" +
    "\n" +
    "        </li>\r" +
    "\n" +
    "        <li ng-click=\"addTab()\">\r" +
    "\n" +
    "            <a href=\"javascript:void(0)\">\r" +
    "\n" +
    "                <i class=\"fa fa-plus\"></i>\r" +
    "\n" +
    "            </a>\r" +
    "\n" +
    "        </li>\r" +
    "\n" +
    "    </ul>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgTime/xgTime.tpl.html',
    "<div>\r" +
    "\n" +
    "    <div ng-if=\"isChinese\">\r" +
    "\n" +
    "        <span>{{date}}</span>\r" +
    "\n" +
    "        <span>{{week}}</span>\r" +
    "\n" +
    "        <span>{{time}}</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div ng-if=\"!isChinese\">\r" +
    "\n" +
    "        <span>{{shortTime}}</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgTimePicker/xgTimePicker.tpl.html',
    "<div class=\"xg-time-picker\">\r" +
    "\n" +
    "    <select\r" +
    "\n" +
    "        class=\"form-control f-l\"\r" +
    "\n" +
    "        ng-model=\"from\"\r" +
    "\n" +
    "        ng-change=\"onFromChanged(from)\"\r" +
    "\n" +
    "        ng-options=\"opt.text for opt in fromData\"\r" +
    "\n" +
    "        name=\"from\">\r" +
    "\n" +
    "    </select>\r" +
    "\n" +
    "    <label class=\"time-sep f-l\">-</label>\r" +
    "\n" +
    "    <select\r" +
    "\n" +
    "        class=\"form-control f-l\"\r" +
    "\n" +
    "        ng-model=\"to\"\r" +
    "\n" +
    "        ng-change=\"onToChanged(to)\"\r" +
    "\n" +
    "        ng-options=\"opt.text for opt in toData\"\r" +
    "\n" +
    "        name=\"to\">\r" +
    "\n" +
    "    </select>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgTrash/xgTrash.tpl.html',
    "<div class=\"xg-edit-trash\">\r" +
    "\n" +
    "    <div class=\"xg-trash\"\r" +
    "\n" +
    "         ng-click=\"trash()\">\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgUnlock/xgUnlock.tpl.html',
    "<div>   \r" +
    "\n" +
    "    <div ng-click=\"onClick()\" class=\"xg-btn unlock\">\r" +
    "\n" +
    "        <p>{{'TEXT_EDIT_UNLOCK' | translate}}</p>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgUpload/xgUpload.tpl.html',
    "<div class=\"xg-upload\"   \r" +
    "\n" +
    "     ng-click=\"upload()\">\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgUserAssistants/xgUserAssistants.tpl.html',
    "<div class=\"user-assistants-container\">\r" +
    "\n" +
    "    <label class=\"user-assistant-item list-item\" ng-repeat=\"assistant in assistants\">\r" +
    "\n" +
    "        <label>{{assistant.userName}}</label>\r" +
    "\n" +
    "        <label class=\"cursor-p\"\r" +
    "\n" +
    "               ng-click=\"remove(assistant)\"\r" +
    "\n" +
    "               ng-if=\"assistant.id !=user.account.userId\">\r" +
    "\n" +
    "            <i class=\"fa fa-times\"></i>\r" +
    "\n" +
    "        </label>\r" +
    "\n" +
    "    </label>\r" +
    "\n" +
    "    <label class=\"user-assistant-item cursor-p\" ng-click=\"add()\">\r" +
    "\n" +
    "        <span>\r" +
    "\n" +
    "            <i class=\"fa fa-plus\"></i>\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </label>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );


  $templateCache.put('src/templates/xgUserRoles/xgUserRoles.tpl.html',
    "<div class=\"user-roles-container\">\r" +
    "\n" +
    "    <label class=\"user-role-item list-item\" ng-repeat=\"role in roles\">\r" +
    "\n" +
    "        <label>{{role.name}}</label>\r" +
    "\n" +
    "        <label class=\"cursor-p\" ng-click=\"remove(role)\">\r" +
    "\n" +
    "            <i class=\"fa fa-times\"></i>\r" +
    "\n" +
    "        </label>\r" +
    "\n" +
    "    </label>\r" +
    "\n" +
    "    <label class=\"user-role-item clear-padding\" ng-show=\"aroles.length !=0\">\r" +
    "\n" +
    "        <label>\r" +
    "\n" +
    "            <span class=\"dropdown\" dropdown on-toggle=\"toggled(open)\">\r" +
    "\n" +
    "                <label class=\"dropdown-toggle\" dropdown-toggle>\r" +
    "\n" +
    "                    <i class=\"fa fa-plus\"></i>\r" +
    "\n" +
    "                </label>\r" +
    "\n" +
    "                <ul class=\"dropdown-menu\">\r" +
    "\n" +
    "                    <li ng-repeat=\"item in aroles\" ng-click=\"add(item)\">\r" +
    "\n" +
    "                        <a href>{{item.name}}</a>\r" +
    "\n" +
    "                    </li>\r" +
    "\n" +
    "                </ul>\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "        </label>\r" +
    "\n" +
    "    </label>\r" +
    "\n" +
    "</div>\r" +
    "\n"
  );

}]);
