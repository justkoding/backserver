﻿//xgLogoutPanel

(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgLogoutPanel', ['$state',
        '$cookieStore',
        'authApi',
        'commonService',
        'loginServices',
        'websocketServices',
        function ($state,          
            $cookieStore,
            authApi,
            commonService,
            loginServices,
            websocketServices) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    isShown: '=',
                    changePassword: '&'
                },
                templateUrl: 'src/templates/login/logoutpanel.tpl.html',
                link: function (scope, element, attrs) {
                    scope.changePwd = function () {
                        scope.isShown = false;

                        scope.changePassword();
                        return false;
                    };

                    scope.gotoUserProfile = function () {
                        scope.isShown = false;

                        var user = commonService.getCurrentUser().account;
                        $state.go('page.profile', { id: user.accountId });

                        return false;
                    };

                    scope.logout = function () {
                        loginServices.logout();
                        return false;
                    };

                    /*--------events------*/
                    scope.$on('user.logout.failed', userLogout);
                    scope.$on('user.logout.done', userLogout);
                    /*--------end------*/

                    /*--------private methods------*/
                    function userLogout() {
                        $cookieStore.remove('token');
                        $cookieStore.remove('user');
                        authApi.init(null);

                        // closed websocket.
                        websocketServices.close();

                        scope.isShown = false;
                        $state.go('login');
                    }
                    /*--------end------*/
                }
            };
        }]);
}());
