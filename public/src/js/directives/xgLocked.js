(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgLocked', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            scope: {
                onClick: '&xgOnClick'
            },
            replace: true,
            templateUrl: 'src/templates/xgLocked/xgLocked.tpl.html',
            link: function (scope, element, attrs) {
                scope.onClick = scope.onClick || function () { };
            }
        };
    }]);
}());
