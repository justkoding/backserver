﻿(function () {
    'use strict';

    /**
    * xg-scroll-bar
    *  parameters:
    *  - xg-scroll-parent: parent id, default value is document.
    */
    angular.module('mdtDrectives').directive('xgScrollBar', function () {
        return {
            restrict: 'A',
            scope: {
                scrollParent: '@xgScrollParent'
            },
            link: function (scope, element, attrs) {
                var parent = scope.scrollParent ? document.getElementById(scope.scrollParent) : window.document.documentElement;

                var setMaxHeight = function () {
                    var h = parent.clientHeight - element[0].offsetTop - 40;
                                        
                    element.css('max-height', h + 'px');
                };

                window.addEventListener("resize", function () {
                    setMaxHeight();
                });

                setMaxHeight();
            }
        };
    });
}());
