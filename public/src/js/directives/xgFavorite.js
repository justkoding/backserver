(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgFavorite', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            scope: {
                favorite: '=xgFavorite',
                xgOnClick: '&xgOnClick'
            },
            replace: true,
            templateUrl: 'src/templates/xgFavorite/xgFavorite.tpl.html',
            link: function (scope, element, attrs) {
                scope.onFavorite = function () {
                    scope.xgOnClick && scope.xgOnClick({
                        favorite: {
                            data: scope.favorite,
                            isAdd: !scope.favorite.isFavorite
                        }
                    });
                };
            }
        };
    }]);
}());
