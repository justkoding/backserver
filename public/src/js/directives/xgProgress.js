(function () {
    'use strict';

    /**
    * xgProgress
    * - xgClassName, default:  normal-bar success-bar.
    *       a. size: normal-bar,  small-bar, large-bar
    *       b. style: success-bar, error-bar, gray-bar
    * - xgRate, greate than 0 and less or equal 1.
    */
    angular.module('mdtDrectives').directive('xgProgress', ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            scope: {
                className: '@xgClassName',           
                rate: '=xgRate'
            },
            replace: true,
            templateUrl: 'src/templates/xgProgress/xgProgress.tpl.html',
            link: function (scope, ele, attrs) {
                var progress = angular.element(ele).find('.xg-progress')[0];

                init();

                /*---------event-----------*/
                scope.$watch('rate', function (newVal, oldVal) {
                    updateProgressUI(newVal);
                });

                /*---------end-----------*/

                /*---------private-----------*/
                function init() {
                    var defaultClassName = 'normal-bar success-bar';
                    scope.className = scope.className || defaultClassName;

                    angular.element(ele).addClass(scope.className);
                   
                    // check if small mode. would hide text in this mode.
                    scope.smallMode = scope.className.indexOf('small-bar') !== -1 || scope.className.indexOf('small-ex-bar') !== -1;

                    // update progress UI.
                    updateProgressUI(scope.rate);
                }

                function updateProgressUI(val) {
                    if (val) {
                        scope.text = Math.ceil(val * 100) + '%';

                        // update progress UI.
                        progress.style.width = scope.text;
                    }
                }
                /*---------end-----------*/
            }
        };
    }]);
}());
