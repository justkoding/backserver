(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgSearch', ['$translate', '$rootScope', 'commonService',
        function ($translate, $rootScope, commonService) {
            return {
                restrict: 'E',
                scope: {
                    option: '=xgOption',
                    onSearch: '&xgOnSearch'
                },
                templateUrl: 'src/templates/xgSearch/xgSearch.tpl.html',
                link: function (scope, ele, attrs) {

                    scope.data = {
                        text: '',
                        option: {},
                        options: []
                    };

                	$rootScope.changeSearchOption = function(id){
                		commonService.session.reset('searchOption');
                		commonService.session.set('searchOption',findOptionById(id));
                		scope.data.option = commonService.session.get('searchOption');
                	};
                	
                    scope.search = function () {
                        onSearch();
                    };
                    
                    var userPermissions = commonService.getPermissionOfCurrentUser();
                    
                    setupOptions();
                    
                    /*--------Events-----------*/
                    var oInput = ele.find('input');

                    angular.element(oInput).bind('keyup', function (e) {
                        // enter: 13
                        if (e.keyCode === 13) {
                            onSearch();
                        }
                    });

                    scope.$watch('option', function (newVal, oldVal) {
                        //reset user input.
                        //scope.data.text = '';
                        scope.data.option = findOptionById(scope.option);
                        scope.option = scope.data.option.id;
                    });

                    // Once translate get changed, will fire this handler.
                    $rootScope.$on('$translateChangeSuccess', function () {
                        setupOptions();
                    });

                    /*--------end-----------*/

                    /*--------private method-----------*/
                    function onSearch() {
                        var data = {
                            text: scope.data.text,
                            opt: scope.data.option
                        };

                        scope.onSearch && scope.onSearch({
                            data: data
                        });
                    }

                    function setupOptions() {
                        // setup select control data.
                        $translate(['NAV_MANAGEMENT',
//                            'NAV_REPORTS',
//                            'NAV_DOCUMENTS',
                            'NAV_PATIENTS',
                            'NAV_USERS',
//                            'NAV_ROLES',
                            'NAV_LOG'
                        ]).then(function (texts) {
                            var items = [
//                                { id: 1, name: texts['NAV_MANAGEMENT'] },
//                                { id: 2, name: texts['NAV_REPORTS'] },
//                                { id: 3, name: texts['NAV_DOCUMENTS'] },
//                                { id: 4, name: texts['NAV_PATIENTS'] },
//                                { id: 5, name: texts['NAV_USERS'] }
//                                { id: 6, name: texts['NAV_LOG'] }
                            ];
                            if(userPermissions.indexOf('meeting-list') != -1){
                            	items.push({ id: 1, name: texts['NAV_MANAGEMENT'] });
                            }
                            if(userPermissions.indexOf('patient-list') != -1){
                            	items.push({ id: 4, name: texts['NAV_PATIENTS'] });
                            }
                            if(userPermissions.indexOf('user-list') != -1){
                            	items.push({ id: 5, name: texts['NAV_USERS'] });
                            }
                            if(userPermissions.indexOf('audit-list') != -1){
                            	items.push({ id: 6, name: texts['NAV_LOG'] });
                            }
                           
                            scope.data.options = items;
                            // set first option as selected by default.
                            scope.data.option = findOptionById(scope.option);
                        });
                    }

                    function findOptionById(id) {
                        var opt = commonService.session.get('searchOption');

                        if(!opt){
                        	 for (var i = 0; i < scope.data.options.length; i++) {
                            if (scope.data.options[i].id === id) {
                                opt = scope.data.options[i];
                                break;
                            }
                        }

                        if (opt == null) {
                            opt = scope.data.options[0];
                        }
                        }

                        return opt;
                    }
                    /*--------end-----------*/
                }
            };
        }]);
}());
