﻿(function () {
    'use strict';

    angular.module('mdtDrectives').directive('xgScrollTo', ["$rootScope", function ($rootScope) {
        return {
            restrict: "A", // attribute
            scope: {
                scrollEle: "@", // scroll element id with.
                fixedTop:"@",
                scrollTo: "@",  // scroll to element id.     
                scrollDuration: "@", // animation duration
                scrollTopic: "@"  // broadcast topic, by default: "scroll.done"
            },
            link: function (scope, element, attrs) {
                element.bind("click", function (event) {
                    event.stopPropagation();
                    event.preventDefault();

                    var top = scope.fixedTop ? scope.fixedTop : document.getElementById(scope.scrollTo).offsetTop;

                    animatedScrollTo(
                       document.getElementById(scope.scrollEle),
                       top,
                       scope.scrollDuration ? new Number(scope.scrollDuration) : 100,
                       function () {
                           $rootScope.$broadcast(scope.scrollTopic ? scope.scrollTopic : 'scroll.done', { message: 'scroll.done' });

                           // the broadcast will be catch by xgScrollToFixed directive.
                           $rootScope.$broadcast('scroll.progress', { scrollTop: top });
                       });
                });
            }
        };
    }]);
}());