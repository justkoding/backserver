(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgTime', ['$interval', function ($interval) {
        return {
            restrict: 'E',
            scope: {
            },
            replace: true,
            templateUrl: 'src/templates/xgTime/xgTime.tpl.html',
            link: function (scope, element, attrs) {
                updateDateTime();

                // update time every 60 seconds.
                $interval(updateDateTime, 60 * 1000);

                function updateDateTime() {
                    // get current datetime.
                    var now = new Date();

                    // identity current language is chinese or not.
                    scope.isChinese = moment.locale().indexOf('cn') != -1;

                    // fileds for chinese locale.
                    scope.date = moment(now).format('ll');
                    scope.week = moment(now).format('ddd');
                    scope.time = moment(now).format('HH:mm');

                    // fileds for other language like english locale.
                    scope.shortTime = moment(now).format('llll');
                }
            }
        };
    }]);
}());
