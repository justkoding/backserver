(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgScrollToFixed', ['$window', function ($window) {
        return {
            restrict: 'A',
            scope: {
                id: '@',
                topGap: '@'
            },
            link: function (scope, ele, attrs) {
                var element = angular.element(document.getElementById(scope.id));

                // store the original value, used to restore.
                var width = element.width();
                var height = element.height();

                scope.top = element.offset().top;

                scope.$on('scroll.progress', function (event, data) {
                    var element = angular.element(document.getElementById(scope.id));
                    var offset = element.offset();

                    // compare with original top value.
                    if (data.scrollTop > scope.top) {
                        initialize();

                        var topGap = scope.topGap ? parseInt(scope.topGap) : 0;
                        element[0].style.top = (element.height() + topGap) + 'px';
                        element.addClass('xg-scroll-to-fixed');
                    } else {
                        element.removeClass('xg-scroll-to-fixed');

                        // top of original value is empty once not rendering.
                        // assign its value once get value.
                        if (scope.top == 0 && offset.top != 0) {
                            scope.top = offset.top;
                        }

                        // restore original value.
                        width = width != 0 ? width + 'px' : '';
                        height = height != 0 ? height + 'px' : '';
                        scope.top = scope.top != 0 ? scope.top : '0';
                        element[0].style.width = width;
                        element[0].style.height = height;
                        element[0].style.top = scope.top + 'px';
                    }

                    console.log('top:' + scope.top);
                });

                function initialize() {
                    var element = angular.element(document.getElementById(scope.id));
                    if (element.width() != 0) {
                        element[0].style.width = element.width() + 'px';
                        element[0].style.height = element.height() + 'px';
                    }
                }
            }
        };
    }]);
}());
