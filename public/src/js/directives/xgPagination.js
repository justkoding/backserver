(function () {
    'use strict';

    /**
    * xgPagination
    * Usages:
    *    <xg-pagination
    *        xg-total-items="50"        
    *        xg-page-changed="pageChanged(item)"
    *        xg-page-size-changed="pageChanged(item)"
    *        xg-previous-text="上一页"
    *        xg-next-text="下一页"
    *        xg-entities-text="共\{\{totalItems\}\} 当前显示:\{\{start\}\} 到 \{\{end\}\}"
    *        xg-page-text="页"
    *        xg-items-per-page="itemsPerPage"
    *        xg-current-page="currentPage"
    *        xg-show-page-text="true"
    *        xg-show-entities-text="true">
    *    </xg-pagination>
    */
    angular.module('mdtDrectives').directive('xgPagination', ['$interpolate', function ($interpolate) {
        return {
            restrict: 'E',
            scope: {
                totalItems: '=xgTotalItems',
                previousText: '@xgPreviousText',
                nextText: '@xgNextText',
                showPageText: '@xgShowPageText',
                showEntitiesText: '@xgShowEntitiesText',
                entitiesText: '@xgEntitiesText',
                pageText: '@xgPageText',
                afterPageChanged: '&xgPageChanged',
                afterPageSizeChanged: '&xgPageSizeChanged',
                itemsPerPage: '=xgItemsPerPage',
                icurrentPage: '=xgCurrentPage',
            },
            templateUrl: 'src/templates/xgPagination/xgPagination.tpl.html',
            link: function (scope, element, attrs) {
                // page size
                scope.sizeOptions = [
                    { text: 5, value: 5 },
                    { text: 10, value: 10 },
                    { text: 20, value: 20 },
                    { text: 30, value: 30 },
                    { text: 50, value: 50 }
                ];

                // pagination control visibility. will be hidden when only has one page.
                scope.visibility = true;

                // init default settings               
                scope.pageSize = scope.sizeOptions[indexOfDefaultPageSize(scope.itemsPerPage)];
                scope.previousText = scope.previousText || '<';
                scope.nextText = scope.nextText || '>';
                scope.showPageText = scope.showPageText || 'true';
                scope.pageText = scope.pageText || 'Page';
                scope.currentPage = 1;
                scope.icurrentPage = scope.currentPage;

                scope.entitiesText = scope.entitiesText || 'Showing {{start}} to {{end}} of {{totalItems}} entries';
                var et = $interpolate(scope.entitiesText, false, null, true);

                // total pages
                scope.numPages = Math.ceil(scope.totalItems / scope.pageSize.value);

                // Entity index
                scope.start = scope.pageSize.value * (scope.icurrentPage - 1) + 1;
                scope.end = scope.pageSize.value * scope.icurrentPage;

                // event handler
                scope.nextPage = function () {
                    var tmp = scope.currentPage + 1;
                    if (tmp <= scope.numPages) {
                        scope.currentPage = tmp;
                        scope.icurrentPage = scope.currentPage;
                        showEntitiesText();
                    }
                };

                scope.previousPage = function () {
                    var tmp = scope.currentPage - 1;
                    if (tmp > 0) {
                        scope.currentPage = tmp;
                        scope.icurrentPage = scope.currentPage;
                        showEntitiesText();
                    }
                };

                scope.pageSizeChanged = function () {
                    scope.currentPage = 1;
                    scope.itemsPerPage = scope.pageSize.value;
                    scope.numPages = Math.ceil(scope.totalItems / scope.pageSize.value);
                    scope.visibility = scope.numPages != 1;

                    scope.icurrentPage = scope.currentPage;
                    showEntitiesText();

                    scope.afterPageSizeChanged && scope.afterPageSizeChanged({
                        item: {
                            page: scope.currentPage,
                            size: scope.pageSize.value
                        }
                    });
                };

                scope.$watch('totalItems', function (newVal, oldVal) {
                    scope.numPages = Math.ceil(scope.totalItems / scope.pageSize.value);
                    scope.visibility = scope.numPages != 1;
                    showEntitiesText();
                });

                scope.$watch('currentPage', function (newVal, oldVal) {
                    scope.icurrentPage = scope.currentPage;
                    showEntitiesText();

                    scope.afterPageChanged && scope.afterPageChanged({
                        item: {
                            page: scope.currentPage ? scope.currentPage : 1,
                            size: scope.pageSize.value
                        }
                    });
                });

                showEntitiesText();

                // using $interpolate service to dynamic update value in dom.
                function showEntitiesText() {
                    // Entity index
                    scope.start = scope.pageSize.value * (scope.icurrentPage - 1) + 1;
                    scope.end = scope.pageSize.value * scope.icurrentPage;

                    if (scope.totalItems == 0) {
                        scope.start = 0;
                    }

                    if (scope.end > scope.totalItems) {
                        scope.end = scope.totalItems;
                    }

                    scope.entityText = et({
                        start: scope.start,
                        end: scope.end,
                        totalItems: scope.totalItems
                    });
                };

                // set default selected page size
                function indexOfDefaultPageSize(size) {
                    var index = 0;

                    if (angular.isNumber(size)) {
                        scope.sizeOptions.forEach(function (opt, ind) {
                            if (opt.value == new Number(size)) {
                                index = ind;
                            }
                        });
                    }

                    return index;
                }
            }
        };
    }]);
}());
