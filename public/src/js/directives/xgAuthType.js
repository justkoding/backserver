(function () {
    'use strict';

    //xgEditTrash
    angular.module('mdtDrectives').directive('xgAuthType', ['$rootScope',
        'commonService',
        function ($rootScope,
            commonService) {
            return {
                restrict: 'A',
                scope: {
                    type: '@xgAuthType'
                },
                link: function (scope, ele, attrs) {
                    var usertype = commonService.getTypeInfoOfCurrentUser();
                   
                    // hide element if current user don't belong to expected role.
                    if (usertype && usertype.id && usertype.id !== scope.type) {
                        angular.element(ele).addClass('ng-hide');
                    }
                }
            };
        }]);
}());
