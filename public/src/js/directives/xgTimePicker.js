(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgTimePicker', function () {
        return {
            restrict: 'E',
            scope: {
                xgDiscussionDate: '=',
                xgDiscussionTo: '=',
                xgStartDate: '=',
                xgFrom: '=',
                xgTo: '=',
                xgLessThan: '=',
                xgMoreThan: '=',
                xgCheck: '=',
                onTimeChanged: '&xgOnChanged'
            },
            replace: true,
            templateUrl: 'src/templates/xgTimePicker/xgTimePicker.tpl.html',
            link: function (scope, element, attrs) {
                scope.fromData = buildOptions();
                scope.toData = buildOptions();

                scope.onFromChanged = function (time) {
                    scope.xgFrom = time.value;
                    // auto update to filed data.
                    updateToDateByFromDate();

                    scope.onTimeChanged && scope.onTimeChanged({
                        item: {
                            time: time,
                            from: true
                        }
                    });
                };

                scope.onToChanged = function (time) {
                    scope.xgTo = time.value;
                    scope.onTimeChanged && scope.onTimeChanged({
                        item: {
                            time: time,
                            from: false
                        }
                    });
                };

                /*----------events-------------*/
                scope.$watch('xgFrom', function (newVal, oldVal) {
                    if (newVal) {
                        if (scope.from == null || newVal != scope.from.value) {
                            findItem(newVal, scope.fromData, function (result) {
                                scope.from = result.item;
                            });
                        }
                    }

                    if(scope.xgCheck){
                        var disDate = '',
                            startDate = '';

                        disDate = moment(scope.xgDiscussionDate).format('YYYYMMDD') + scope.xgDiscussionTo.replace(':','');
                        startDate = moment(scope.xgStartDate).format('YYYYMMDD') + scope.xgTo.replace(':','');

                        if(disDate < startDate){
                            scope.xgMoreThan = true;
                        }else{
                            scope.xgMoreThan = false;
                        }

                        scope.xgLessThan = false;
                    }
                });

                scope.$watch('xgTo', function (newVal, oldVal) {
                    if (newVal) {
                        if (scope.to == null || newVal != scope.to.value) {
                            findItem(newVal, scope.fromData, function (result) {
                                scope.toData = buildOptions(result.index);

                                if (scope.toData.length >= 2) {
                                    scope.to = scope.toData[1];
                                } else {
                                    scope.to = scope.toData[0];
                                }
                            });
                        }
                    }

                    if(scope.xgCheck){
                        var disDate = '',
                            startDate = '';

                        disDate = moment(scope.xgDiscussionDate).format('YYYYMMDD') + scope.xgDiscussionTo.replace(':','');
                        startDate = moment(scope.xgStartDate).format('YYYYMMDD') + scope.xgTo.replace(':','');

                        if(disDate < startDate){
                            scope.xgMoreThan = true;
                        }else{
                            scope.xgMoreThan = false;
                        }

                        scope.xgLessThan = false;
                    }
                });
                /*----------end events-------------*/

                /*----------private-------------*/
                function buildOptions(start) {
                    var options = [];
                    var mins = ['00', '30'];
                    var iNow = 0;

                    start = start ? start : 0;

                    for (var i = 0; i < 24; i++) {
                        mins.forEach(function (m) {
                            if (++iNow >= start) {
                                if(i < 10){
                                    var temp = '0' + i;
                                    options.push({
                                        text: temp + ':' + m,
                                        value: temp + ':' + m
                                    });

                                }else{
                                    options.push({
                                        text: i + ':' + m,
                                        value: i + ':' + m
                                    });
                                }
                            }
                        });
                    }

                    return options;
                }

                function findItem(text, data, fn) {
                    var result = {
                        item: data[0],
                        index: 0
                    };
                    if (text != null && text != '') {
                        data.forEach(function (d, i) {
                            if (d.value === text) {
                                result.item = d;
                                result.index = i;
                            }
                        });
                    }

                    fn && fn(result);
                }

                /**
                * By default, to date is later than from date 1 hour.
                */
                function updateToDateByFromDate() {
                    findItem(scope.from.value, scope.fromData, function (result) {
                        scope.toData = buildOptions(result.index + 1);

                        if (scope.toData.length >= 3) {
                            scope.to = scope.toData[2];
                        } else {
                            scope.to = scope.toData[scope.toData.length - 1];
                        }

                        scope.onTimeChanged && scope.onTimeChanged({
                            item: {
                                time: scope.to,
                                from: false
                            }
                        });
                    });
                }

                function init() {
                }
                /*----------end-------------*/

                // init
                init();
            }
        };
    });
}());
