(function () {
    'use strict';

    //xgCountdown
    angular.module('mdtDrectives').directive('xgCountdown3', ['$rootScope', '$interval', 'commonService', '$q',
        function ($rootScope, $interval, commonService, $q) {
            return {
                restrict: 'E',
                scope: {
                    style: '@', // text color
                    date: '=',
                    completed: '&xgCompleted'
                },
                replace: true,
                templateUrl: 'src/templates/xgCountdown/xgCountdown.tpl_3.html',
                link: function (scope, element, attrs) {

                    var future = null,
                        timer = null,
                        diff = null;

                    scope.$watch('date', function (newVal, oldVal) {
                        if (newVal != 0 && newVal != undefined) {
                            future = scope.date;
                            if (compare(future, new Date()) < 0) {
                                scope.text = "Meeting has expired";
                            } else {
                                timer = $interval(function () {
                                    diff = Math.floor(future.getTime() - new Date().getTime()) / 1000;

                                    dhms(diff).then(function (value) {
                                        if (diff < 1) {
                                            // callback when countdown is done.
                                            scope.completed && scope.completed();
                                        } else {
                                            scope.text = value;
                                        }
                                    });
                                }, 1000);
                            }
                        }
                    });


                    function dhms(t) {
                        var defer = $q.defer(),
                            days = null,
                            hours = null,
                            minutes = null,
                            seconds = null;

                        days = Math.floor(t / 86400);
                        t -= days * 86400;
                        hours = Math.floor(t / 3600) % 24;
                        t -= hours * 3600
                        minutes = Math.floor(t / 60) % 60;
                        t -= minutes * 60;
                        seconds = Math.floor(t % 60);

                        commonService.resource.getValues(['TEXT_DAY', 'TEXT_HOURS', 'TEXT_MIN', 'TEXT_SEC']).then(function (source) {
                            if (days > 0) {
                                defer.resolve(days + source['TEXT_DAY'] + ' ' + hours + source['TEXT_HOURS']);
                            } else {
                                defer.resolve(hours + source['TEXT_HOURS'] + ' ' + minutes + source['TEXT_MIN']);
                            }
                        });

                        return defer.promise;
                    }

                    function compare(date1, date2) {
                        return (new Date(date1.getFullYear(), date1.getMonth(), date1.getDate(), date1.getHours(), date1.getMinutes(), date1.getSeconds()) -
                        new Date(date2.getFullYear(), date2.getMonth(), date2.getDate(), date2.getHours(), date2.getMinutes(), date2.getSeconds()));
                    };
                }
            };
        }]);
}());
