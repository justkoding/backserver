(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgPatientPanel', ['$translate', '$rootScope', 'patientServices', '$timeout',
        function ($translate, $rootScope, patientServices, $timeout) {
            return {
                restrict: 'E',
                scope: {
                    parent: '@xgParent',
                    onAccept: '&xgOnAccept',
                    onReject: '&xgOnReject',
                    className: '@xgClass'
                },
                templateUrl: 'src/templates/xgPatientPanel/xgPatientPanel.tpl.html',
                link: function (scope, ele, attrs) {
                    // mark.
                    var iNow = 0;

                    // original styles values.
                    var oValue = {};

                    // find panel element.
                    var panel = angular.element(ele).find('.panel');

                    // deep copy to aviod unexpected changed.
                    scope.patient = {};

                    // variable to mark if some error happens.
                    scope.hasError = false;

                    // reject hanlder
                    scope.reject = function () {
                        scope.hasError = false;
                        restoreStyle();
                        scope.onReject & scope.onReject();
                    };

                    // accept handler.
                    scope.accept = function () {
                        scope.hasError = false;
                        restoreStyle();

                        scope.onAccept & scope.onAccept({ patient: scope.patient });
                    };

                    init();

                    /*----------private method----------*/
                    function init() {
                        // set classes. xg-patient-panel       
                        var className = 'xg-patient-panel';
                        if (scope.className) {
                            className += ' ' + scope.className;
                        }
                        angular.element(ele).addClass(className);

                        // update panel css after DOM rendering.
                        //$timeout(function () {
                        //    setPanelStyle();
                        //});
                    }

                    //function setPanelStyle() {
                    //    var parent = angular.element(document).find(scope.parent);

                    //    var h = parent.height();

                    //    angular.element(ele).css('height', h);
                    //    angular.element(ele).css('bottom', -(h + 30));

                    //    panel.css('height', h);
                    //}

                    function restoreStyle() {
                        var oEle = angular.element(ele);

                        for (var s in oValue) {
                            oEle.css(s, oValue[s]);
                        }
                    }
                    /*---------------end---------------*/
                    
                    /*------------events-----------------*/
                    //scope.$on('modal.render.done', function (event, data) {
                    //    setPanelStyle();
                    //});

                    scope.$on('xgPatientPanel.shown', function (event, results) {                      
                        var css = results.css;
                        var oEle = angular.element(ele);

                        scope.patient = results.data;

                        // update element panel style to show.
                        css.forEach(function (item) {
                            if (iNow === 0) {
                                // store old value, will be used on restore action.
                                oValue[item.key] = oEle.css(item.key);
                            }

                            oEle.css(item.key, item.value);
                        });

                        iNow++;
                    });
                    /*-------------end-------------------*/
                }
            };
        }]);
}());
