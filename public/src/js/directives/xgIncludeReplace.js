(function () {
    'use strict';

    //xgEditTrash
    angular.module('mdtDrectives').directive('xgIncludeReplace', ['$rootScope', function ($rootScope) {
        return {
            require: 'ngInclude',
            restrict: 'A',
            link: function (scope, ele, attrs) {
                ele.replaceWith(ele.children());
            }
        };
    }]);
}());
