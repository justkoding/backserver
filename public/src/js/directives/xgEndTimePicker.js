(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgEndTimePicker', function () {
        return {
            restrict: 'E',
            scope: {
                xgDiscussionDate: '=',
                xgFrom: '=',
                xgStartDate: '=',
                xgTo: '=',
                xgLessThan: '=',
                xgMoreThan: '=',
                xgCheck: '=',
                onEndTimeChanged: '&xgOnChanged'
            },
            replace: true,
            templateUrl: 'src/templates/xgEndTimePicker/xgEndTimePicker.tpl.html',
            link: function (scope, element, attrs) {
                scope.fromData = buildOptions();

                scope.onTimeChanged = function (time) {
                    scope.xgFrom = time.value;

                    scope.onEndTimeChanged && scope.onEndTimeChanged({
                        item: {
                            time: time,
                            from: true
                        }
                    });
                };

                /*----------events-------------*/
                scope.$watch('xgFrom', function (newVal, oldVal) {
                    if (newVal) {
                        if (scope.from == null || newVal != scope.from.value) {
                            findItem(newVal, scope.fromData, function (result) {
                                scope.from = result.item;
                            });
                        }

                        if(scope.xgCheck){
                            var disDate = '',
                                startDate = '';

                            disDate = moment(scope.xgDiscussionDate).format('YYYYMMDD') + scope.xgFrom.replace(':','');
                            startDate = moment(scope.xgStartDate).format('YYYYMMDD') + scope.xgTo.replace(':','');

                            if(disDate < startDate){
                                scope.xgLessThan = true;
                            }else{
                                scope.xgLessThan = false;
                            }

                            scope.xgMoreThan = false;
                        }
                    }
                });


                /*----------end events-------------*/

                /*----------private-------------*/
                function buildOptions(start) {
                    var options = [];
                    var mins = ['00', '30'];
                    var iNow = 0;

                    start = start ? start : 0;

                    for (var i = 0; i < 24; i++) {
                        mins.forEach(function (m) {
                            if (++iNow >= start) {
                                if(i < 10){
                                    var temp = '0' + i;
                                    options.push({
                                        text: temp + ':' + m,
                                        value: temp + ':' + m
                                    });

                                }else{
                                    options.push({
                                        text: i + ':' + m,
                                        value: i + ':' + m
                                    });
                                }
                            }
                        });
                    }

                    return options;
                }

                function findItem(text, data, fn) {
                    var result = {
                        item: data[0],
                        index: 0
                    };
                    if (text != null && text != '') {
                        data.forEach(function (d, i) {
                            if (d.value === text) {
                                result.item = d;
                                result.index = i;
                            }
                        });
                    }

                    fn && fn(result);
                }


                function init() {
                }
                /*----------end-------------*/

                // init
                init();
            }
        };
    });
}());
