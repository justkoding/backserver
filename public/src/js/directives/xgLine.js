(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgLine', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            scope: {
                color: '@',
                toAfter: '@',
                width: '@'
            },
            replace: true,
            templateUrl: 'src/templates/xgLine/xgLine.tpl.html',
            link: function (scope, element, attrs) {
                var pos = getRelativePosition();

                // set background color.
                element.css('background-color', scope.color ? scope.color : '#e4e1e1');

                // set width
                element.css('width', scope.width ? scope.width : '2px');

                // postion
                element.css('top', 0);
                element.css('left', pos.x);

                // get relative element position by 'toAfter' value.
                function getRelativePosition() {
                    var x = 0, y = 0;

                    if (scope.toAfter) {
                        var ele = angular.element(scope.toAfter);
                        if (ele[0]) {
                            x = ele[0].clientWidth;
                        }             
                    }

                    return {
                        x: x,
                        y: y
                    };
                }
            }
        };
    }]);
}());
