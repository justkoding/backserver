(function() {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgMessage', function() {
        return {
            restrict: 'E',
            transclude: true,
            replace: 'true',
            scope: {
                msg: '@'
            },
            template: '<h1>{{msg | firstCapitalize}} <div data-ng-transclude></div></h1>'
                //templateUrl: '/assets/src/partials/_xgMessage.html'
        };
    });
}());
