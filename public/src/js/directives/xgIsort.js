(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgIsort', function () {
        return {
            restrict: 'A',
            scope: {
                text: '@xgItext',
                onSort: '&xgOnIsort'
            },
            templateUrl: 'src/templates/xgSort/xgSort.tpl.html',
            link: function (scope, element, attrs) {
                var sDefault = element.find('.sort-icon-default');
                var sAsc = element.find('.sort-icon-asc');
                var sDesc = element.find('.sort-icon-desc');
                
                // to mark if the default icon is active.
                scope.isActive = false;

                scope.btnStatus = {
                    sDefault: true,
                    sAsc: false
                };

                scope.onDefault = function () {
                    scope.btnStatus.sDefault = false;
                    scope.btnStatus.sAsc = true;

                    scope.onSort && scope.onSort({ asc: true });                   
                };

                scope.sort = function () {
                    scope.btnStatus.sAsc = !scope.btnStatus.sAsc;                   
                    scope.onSort && scope.onSort({ asc: scope.btnStatus.sAsc });                    
                };
            }
        };
    });
}());
