(function () {
    'use strict';

    //xgEditTrash
    angular.module('mdtDrectives').directive('xgUpload', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            scope: {
                upload: '&xgOnUpload'               
            },
            replace: true,
            templateUrl: 'src/templates/xgUpload/xgUpload.tpl.html',
            link: function (scope, element, attrs) {
                scope.upload = scope.upload || angular.noop;               
            }
        };
    }]);
}());
