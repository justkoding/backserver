﻿(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgToCenter', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var toCenter = function () {
                    var w = (window.document.documentElement.clientWidth - element[0].offsetWidth) / 2;
                    var h = (window.document.documentElement.clientHeight - element[0].offsetHeight) / 2;

                    if (h < 80) {
                        h = 80;
                    }

                    if (w < 0) {
                        w = 0;
                    }

                    //element.css('margin-left', w + 'px');
                    //element.css('margin-top', h + 'px');
                    element.css('left', w + 'px');
                    element.css('top', h + 'px');
                };

                window.addEventListener("resize", function () {
                    toCenter();
                });

                toCenter();
            }
        };
    });
}());
