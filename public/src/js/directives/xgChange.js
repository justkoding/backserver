(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgChange', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            scope: {
                duration: '@xgDuration',
                onChange: '&xgOnChange'
            },
            link: function (scope, ele, attrs) {
                var timer = null;

                scope.duration = scope.duration && angular.isNumber(scope.duration) ? scope.duration : 500;

                angular.element(ele).bind('keyup', function () {
                    var value = angular.element(this).val();

                    $timeout.cancel(timer);

                    timer = $timeout(function () {
                        scope.onChange && scope.onChange({ value: value });
                    }, scope.duration);
                });
            }
        };
    }]);
}());
