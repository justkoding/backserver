﻿(function () {
    'use strict';

    //xgLoading
    angular.module('mdtDrectives').directive('xgLoading', ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                onShow: '&xgShow',
                onHide: '&xgHide',
            },
            templateUrl: 'src/templates/xgLoading/xgLoading.tpl.html',
            link: function (scope, element, attrs) {                
                scope.show = function () {
                    $timeout(function () {
                        angular.element(element).removeClass('hide');

                        scope.onShow && scope.onShow({
                            status: {
                                show: true
                            }
                        });
                    }, 100);
                };

                scope.hide = function () {
                    $timeout(function () {
                        angular.element(element).addClass('hide');

                        scope.onHide && scope.onHide({
                            status: {
                                show: false
                            }
                        });
                    }, 300);
                };

                /*-----------events-----------*/
                //listening broadcast from libs/iLoadingInterceptor.js
                scope.$on('iloadingbar.loading', function (event, data) {
                    if (!scope.isShow) {
                        scope.show();
                    }
                });

                scope.$on('iloadingbar.loaded', function (event, data) {
                    scope.hide();
                });
                /*-----------end-------------*/             
            }
        };
    }]);
}());
