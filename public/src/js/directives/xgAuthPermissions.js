(function () {
    'use strict';

    //xgEditTrash
    angular.module('mdtDrectives').directive('xgAuthPermissions', ['$rootScope',
        'commonService',
        function ($rootScope,
            commonService) {
            return {
                restrict: 'A',
                scope: {
                    permission: '@xgAuthPermissions'
                },
                link: function (scope, ele, attrs) {
                    var permissions = [];
                    var userPermissions = commonService.getPermissionOfCurrentUser();
                    var index = -1;

                    if (scope.permission) {
                        permissions = scope.permission.split(',');
                    }

                    if (userPermissions && permissions.length != 0) {
                        for (var i = 0; i < permissions.length; i++) {
                            index = userPermissions.indexOf(permissions[i]);

                            if (index != -1) {
                                break;
                            }
                        }
                    }

                    // hide element if current user don't contain expected permissions.
                    if (index === -1) {
                        angular.element(ele).addClass('ng-hide');
                    }
                }
            };
        }]);
}());
