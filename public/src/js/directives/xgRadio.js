(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgRadio', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            scope: {
                name: '@xgName',
                text: '@xgText',
                value:'@xgValue',
                checked: '@xgChecked',
                xgOnClick: '&xgOnClick'
            },
            replace: true,
            templateUrl: 'src/templates/xgRadio/xgRadio.tpl.html',
            link: function (scope, element, attrs) {
                var oInput = angular.element(element).find('input');

                if (scope.checked === 'true') {
                    oInput.attr('checked', 'checked');
                }

                scope.onClick = function () {
                    scope.xgOnClick && scope.xgOnClick({
                        value: scope.value
                    });
                };
            }
        };
    }]);
}());
