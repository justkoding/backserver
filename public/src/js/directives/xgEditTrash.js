(function () {
    'use strict';

    //xgEditTrash
    angular.module('mdtDrectives').directive('xgEditTrash', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            scope: {
                edit: '&xgOnEdit',
                trash: '&xgOnTrash',
                onlyEdit: '@',
                onlyTrash: '@',
                entity:'@' // the entity which is edited/deleted.
            },
            replace: true,
            templateUrl: 'src/templates/xgEditTrash/xgEditTrash.tpl.html',
            link: function (scope, element, attrs) {
                scope.onEdit = function () {
                    if (scope.edit) {
                        var entity = scope.entity ? JSON.parse(scope.entity) : {};
                        scope.edit({
                            entity: entity
                        });
                    }
                };

                scope.onTrash = function () {
                    if (scope.trash) {
                        var entity = scope.entity ? JSON.parse(scope.entity) : {};
                        scope.trash({
                            entity: entity
                        });
                    }
                };
            }
        };
    }]);
}());
