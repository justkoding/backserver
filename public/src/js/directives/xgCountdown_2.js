(function () {
    'use strict';

    //xgCountdown
    angular.module('mdtDrectives').directive('xgCountdown2', ['$rootScope', '$interval', function ($rootScope, $interval) {
        return {
            restrict: 'E',
            scope: {
                total: '@', // total numbers
                base: '@', // completed numbers
                interval: '@', // milliseconds
                text: '@',  // display in middle
                radius: '@',
                stroke: '@',
                color: '@'
            },
            replace: true,
            templateUrl: 'src/templates/xgCountdown/xgCountdown.tpl.html',
            link: function (scope, element, attrs) {
                scope.total = parseInt(scope.total);
                scope.base = parseInt(scope.base);
                scope.interval = scope.interval ? parseInt(scope.interval) : 60 * 1000; // milliseconds
                scope.radius = parseInt(scope.radius);

                // assign default values.
                scope.diameter = scope.radius * 2 + 10;
                scope.color = scope.color ? scope.color : 'red'

                // backend circle.
                scope.cfill = '#e9e9e9';

                // left times
                scope.day = 0;
                scope.hour = 0;
                scope.min = 0;

                // init
                var pos = calcPosition();
                reset(pos.x, pos.y);

                function reset(x, y) {
                    /*
                     *  <circle cx="200" cy="200" r="50" fill="none" stroke="#efefef" stroke-width="20"></circle>
                     *  <path d="M200 150 A50 50 0 0 1 250 200" fill="none" stroke="red" stroke-width="20"></path>
                     * 
                     */

                    var rr = y <= 0 ? 0 : 1;

                    var transform = 'translate(' + scope.radius + ',' + scope.radius + ')';
                    var d = 'M ' + scope.radius + ' 0 A' + scope.radius + ' ' + scope.radius + ' 0 ' + rr + ' 1 ' + x + ' ' + y;

                    var svg = element[0];
                    var path = element.find('path')[0];

                    svg.setAttribute('transform', transform);
                    path.setAttribute('transform', transform);
                    path.setAttribute('d', d);

                    // update UI for time.
                    var ts = TimeSpan.FromSeconds(scope.total - scope.base);
                    scope.day = ts.days();
                    scope.hour = ts.hours();
                    scope.min = ts.minutes();
                }

                function calcPosition() {
                    var angle = ((scope.interval / 1000 + scope.base) / scope.total) * 360;
                    var x = Math.cos(angle * Math.PI / 180) * scope.radius;
                    var y = Math.sin(angle * Math.PI / 180) * scope.radius;

                    return {
                        x: x,
                        y: y
                    };
                }

                var timer = null;

                timer = $interval(function () {
                    if (scope.base === scope.total) {
                        $interval.cancel(timer);
                        scope.cfill = scope.color;
                        return;
                    } 

                    // increase base.
                    scope.base += scope.interval / 1000;

                    // update photograph.
                    var pos = calcPosition();
                    reset(pos.x, pos.y);

                }, scope.interval);

                /*-------events------*/


                /*-------end------*/

                //scope.radius = parseInt(scope.iradius);
                //scope.angle = 0;
                //scope.done = false;

                //// radius of backend circle.
                //scope.cfill = '#e9e9e9';
                //scope.cradius = scope.radius - parseInt(scope.stroke) / 2;

                //// left times
                //scope.day = 0;
                //scope.hour = 0;
                //scope.min = 0;                

                //initialize();

                //scope.$on('tick', function (event, data) {
                //    var ticked = data.ticked;
                //    var count = data.count; // unit seconds
                //    if (scope.done) {
                //        scope.done = false;
                //        scope.cfill = '#e9e9e9';
                //    }

                //    // update UI for time.
                //    var ts = TimeSpan.FromSeconds(count);
                //    scope.day = ts.days();
                //    scope.hour = ts.hours();
                //    scope.min = ts.minutes();                   

                //    scope.angle += 2 * Math.PI * ticked;
                //    initialize();                   
                //});

                //scope.$on('done', function () {
                //    scope.done = true;
                //    scope.cfill = scope.color;
                //});

                //function initialize() {
                //    scope.diameter = scope.radius * 2;
                //    scope.mid = ~~(scope.angle > Math.PI);
                //    scope.x = Math.sin(scope.angle) * scope.radius;
                //    scope.y = Math.cos(scope.angle) * -scope.radius;

                //    var transform = 'translate(' + scope.radius + ',' + scope.radius + ')';
                //    var d = 'M 0 0 v -' + scope.radius + ' A ' + scope.radius + ' ' + scope.radius + ' 1 ' + scope.mid + ' 1 ' + scope.x + ' ' + scope.y + ' z';
                //    var fill = scope.color ? scope.color : 'red';

                //    var svg = element[0];
                //    var path = element.find('path')[0];

                //    svg.setAttribute('transform', transform);
                //    path.setAttribute('transform', transform);
                //    path.setAttribute('d', d);
                //    path.setAttribute('fill', fill);
                //}

                //// start timer
                //scope.timer.start();
            }
        };
    }]);
}());
