(function () {
    'use strict';

    //xgFilter
    angular.module('mdtDrectives').directive('xgFilter', ['$window', function ($window) {
        return {
            restrict: 'A',
            scope: {
                title: '@xgTitle',
                sItem: '=xgSText',
                dpData: '=',
                onToggle: '&xgToggle',
                onSelected: '&xgSelected'
            },
            templateUrl: 'src/templates/xgFilter/xgFilter.tpl.html',
            link: function (scope, element, attrs) {
                scope.isActive = false;
                scope.sItem = '';
                scope.selected = function (item) {
                    // remove selected item if value is -1.
                    if (item.id === -1) {
                        scope.sItem = '';
                    } else {
                        scope.sItem = ': ' + item.name;
                    }

                    scope.onSelected && scope.onSelected({ item: item });
                };

                scope.toggled = function (open) {
                    if (open) {
                        setDropDownMenuPosition();
                    }
                    scope.onToggle && scope.onToggle({ open: open });
                };

                init();

                function init() {
                    setDropDownMenuPosition();
                }

                /*----------events-------------*/
                angular.element(element).bind('mouseenter', function () {
                    scope.isActive = true;
                    scope.$digest();
                });

                angular.element(element).bind('mouseleave', function () {
                    scope.isActive = false;
                    scope.$digest();
                });

                $window.addEventListener('resize', function () {
                    setDropDownMenuPosition();
                });
                /*----------end events-------------*/

                function setDropDownMenuPosition() {
                    var filterIcon = element.find('.dropdown-toggle')[0];
                    var dropdownMenu = element.find('.dropdown-menu')[0];

                    var offset = angular.element(filterIcon).offset();
                    var width = angular.element(filterIcon).width();
                    var height = angular.element(filterIcon).height();

                    dropdownMenu.style.left = (offset.left + width - 10) + 'px';
                    dropdownMenu.style.top = (offset.top + height) + 'px';
                }
            }
        };
    }]);
}());
