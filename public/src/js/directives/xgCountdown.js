(function () {
    'use strict';

    //xgCountdown
    angular.module('mdtDrectives').directive('xgCountdown', ['$rootScope',
        function ($rootScope) {
            return {
                restrict: 'E',
                scope: {
                    total: '=', // seconds
                    basic: '=', // seconds
                    ticketInterval: '@', // seconds
                    text: '@',
                    iradius: '@radius',
                    stroke: '@',
                    color: '@', // progress color.
                    textColor: '@', // text color
                    fill:'@', // backend circle color.
                    topic: '@',
                    completed: '&xgCompleted',
                    inprogress: '&'
                },
                replace: true,
                templateUrl: 'src/templates/xgCountdown/xgCountdown.tpl.html',
                link: function (scope, element, attrs) {
                    var timer = null;

                    scope.topic = scope.topic ? scope.topic : 'xg.countdown';

                    // check fill value.
                    scope.textColor = scope.textColor ? scope.textColor : '#27c3f0';

                    scope.radius = parseInt(scope.iradius);
                    scope.angle = 0;
                    scope.done = false;

                    // radius of backend circle.
                    scope.fill = scope.fill ? scope.fill : '#e9e9e9';
                    scope.cradius = scope.radius - parseInt(scope.stroke) / 2;

                    // left times
                    scope.day = 0;
                    scope.hour = 0;
                    scope.min = 0;

                    initialize();

                    scope.$on(scope.topic + '.tick', function (event, data) {
                        var ticked = data.ticked;
                        var count = data.count; // unit seconds
                        if (scope.done) {
                            scope.done = false;
                            scope.cfill = '#e9e9e9';
                        }

                        // update UI for time.
                        var ts = TimeSpan.FromSeconds(count);
                        scope.day = ts.days();
                        scope.hour = ts.hours();
                        scope.min = ts.minutes();

                        scope.angle += 2 * Math.PI * ticked;
                        initialize();
                    });

                    scope.$watch('total', function (newVal, oldVal) {
                        if (newVal != 0 && newVal != oldVal) {
                            if (timer) {
                                clearInterval(timer);
                                timer = null;
                            }
                            start();
                        }
                    });

                    scope.$on(scope.topic + '.done', function () {
                        scope.done = true;
                        scope.cfill = scope.color;

                        // callback when countdown is done.
                        scope.completed && scope.completed();
                    });

                    function initialize() {
                        scope.diameter = scope.radius * 2;
                        scope.mid = ~~(scope.angle > Math.PI);
                        scope.x = Math.sin(scope.angle) * scope.radius;
                        scope.y = Math.cos(scope.angle) * -scope.radius;

                        var transform = 'translate(' + scope.radius + ',' + scope.radius + ')';
                        var d = 'M 0 0 v -' + scope.radius + ' A ' + scope.radius + ' ' + scope.radius + ' 1 ' + scope.mid + ' 1 ' + scope.x + ' ' + scope.y + ' z';
                        var fill = scope.color ? scope.color : 'red';

                        var svg = element[0];
                        var path = element.find('path')[0];

                        //svg.setAttribute('transform', transform);
                        path.setAttribute('transform', transform);
                        path.setAttribute('d', d);
                        path.setAttribute('fill', fill);
                    }

                    function start() {
                        // seconds.
                        var tickInterval = scope.ticketInterval ? parseInt(scope.ticketInterval) : 60; // 1min
                        var total = scope.total ? parseInt(scope.total) : 0; // 1hour by default.
                        var basic = scope.basic ? parseInt(scope.basic) : 0;
                        var count = total- basic;

                        // default value with passed time.
                        scope.angle = 2 * Math.PI * (basic / total);

                        timer = setInterval(function () {
                            count -= tickInterval;
                            if (count > 0) {
                                $rootScope.$broadcast(scope.topic + '.tick', {
                                    ticked: tickInterval / total,
                                    count: count
                                });

                                scope.inprogress && scope.inprogress({
                                    ticked: tickInterval / total,
                                    count: count
                                });
                            } else {
                                if (count == 0) {
                                    $rootScope.$broadcast(scope.topic + '.tick', {
                                        ticked: tickInterval / total,
                                        count: count
                                    });
                                }
                                if (timer) {
                                    clearInterval(timer);
                                    $rootScope.$broadcast(scope.topic + '.done');
                                }
                            }
                        }, tickInterval * 1000);

                        // get started to count down.
                        $rootScope.$broadcast(scope.topic + '.tick', {
                            ticked: tickInterval / total,
                            count: count
                        });
                    }

                    if (scope.total != 0) {
                        start();
                    }                    
                }
            };
        }]);
}());
