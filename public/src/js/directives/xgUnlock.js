(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgUnlock', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            scope: {
                onClick: '&xgOnClick'
            },
            replace: true,
            templateUrl: 'src/templates/xgUnlock/xgUnlock.tpl.html',
            link: function (scope, element, attrs) {
                scope.onClick = scope.onClick || function () { };
            }
        };
    }]);
}());
