(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgUserAssistants', ['entityServices',
        'commonService',
        function (entityServices, commonService) {
            return {
                restrict: 'E',
                scope: {
                    assistants: '=xgAssistants',
                    removeAssistant: '&xgRemoveAssistant',
                    addAssistants: '&xgAddAssistants'
                },
                templateUrl: 'src/templates/xgUserAssistants/xgUserAssistants.tpl.html',
                link: function (scope, element, attrs) {
                    scope.user = commonService.getCurrentUser();

                    scope.remove = function (ass) {
                        // removed selected assistant.
                        removeAssistant(ass, scope.assistants);
                        scope.removeAssistant && scope.removeAssistant(ass);
                    };

                    scope.add = function () {
                        scope.addAssistants && scope.addAssistants();
                    };

                    /*---------private method------------*/
                    /**
                    * remove a role item from special assistants array.
                    */
                    function removeAssistant(ass, arr, p) {
                        var index = -1,
                            p = p || 'id';
                        if (ass) {
                            arr.forEach(function (r, i) {
                                if (r[p] == ass[p]) {
                                    index = i;
                                }
                            });

                            if (index != -1) {
                                arr.splice(index, 1);
                            }
                        }
                    }
                    /*---------end------------*/
                }
            };
        }]);
}());
