(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgUserRoles', ['entityServices', function (entityServices) {
        return {
            restrict: 'E',
            scope: {
                roles: '=xgRoles',
                availableRoles: '=xgAvailableRoles',
                removeRole: '&xgRemoveRole',
                addRole: '&xgAddRole'
            },
            templateUrl: 'src/templates/xgUserRoles/xgUserRoles.tpl.html',
            link: function (scope, element, attrs) {
                scope.remove = function (role) {
                    // removed selected roles.
                    removeRole(role, scope.roles, 'id');

                    // update dropdown list items
                    var dpItem = new entityServices.DpListItem(role.name, role.id);
                    updateDpDataSource(dpItem, false);

                    // callback
                    scope.removeRole && scope.removeRole({ role: role });
                };

                scope.add = function (item) {
                    // updated selected roles
                    var role = {
                        name: item.name,
                        id: item.id
                    };
                    scope.roles.push(role);

                    // update dropdown list items
                    updateDpDataSource(item, true);

                    // call back
                    scope.addRole && scope.addRole({ role: role });
                };

                // data source for all available roles, which passed from directive.
                if (scope.availableRoles) {
                    var aroles = JSON.parse(JSON.stringify(scope.availableRoles));
                    scope.aroles = buildDpDataSource(aroles);
                }

                scope.$watch('availableRoles', function (newVal, oldVal) {
                    // do deeply copy
                    if (newVal) {
                        var aroles = JSON.parse(JSON.stringify(newVal));
                        scope.aroles = buildDpDataSource(aroles);
                    }
                });

                scope.$watch('roles', function (newVal, oldVal) {
                    // do deeply copy
                    if (newVal && scope.availableRoles) {
                        var aroles = JSON.parse(JSON.stringify(scope.availableRoles));
                        scope.aroles = buildDpDataSource(aroles);
                    }
                });

                /*----private method------*/
                /**
                * Setup dropdown list data source.
                */
                function buildDpDataSource(arr) {
                    var data = [];

                    if (arr.length != 0) {
                        data = arr.map(function (r) {
                            return new entityServices.DpListItem(r.name, r.id);
                        });
                    }

                    if (data.length != 0) {
                        // exclude selected roles in dropdown data source.
                        if (scope.roles) {
                            data.forEach(function (r, i) {
                                scope.roles.forEach(function (role) {
                                    if (r.id == role.id) {
                                        data.splice(i, 1);
                                    }
                                });
                            });
                        }

                        data.sort(function (a, b) {
                            return a.value < b.value;
                        });
                    }

                    return data;
                }

                /**
                * add or remove a item from array.
                * - item: the item need to remove or add
                * - remove: boolean, to mark it is remove or push action.
                */
                function updateDpDataSource(item, remove) {
                    if (remove) {
                        removeRole(item, scope.aroles);
                    } else {
                        scope.aroles.push(item);

                        scope.aroles.sort(function (a, b) {
                            return a.id < b.id;
                        });
                    }
                }

                /**
                * remove a role item from special roles array.
                */
                function removeRole(role, arr, p) {
                    var index = -1,
                        p = p || 'id';
                    if (role) {
                        arr.forEach(function (r, i) {
                            if (r[p] == role[p]) {
                                index = i;
                            }
                        });

                        if (index != -1) {
                            arr.splice(index, 1);
                        }
                    }
                }
                /*---------end------------*/
            }
        };
    }]);
}());
