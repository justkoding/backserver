(function () {
    'use strict';

    //xgEditTrash
    angular.module('mdtDrectives').directive('xgSmartEditTrash', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            scope: {
                edit: '&xgOnEdit',
                trash: '&xgOnTrash',
                className:'@xgStyle'
            },
            replace: true,
            templateUrl: 'src/templates/xgSmartEditTrash/xgSmartEditTrash.tpl.html',
            link: function (scope, element, attrs) {
                if (scope.className) {
                    element.addClass(scope.className);
                }

                scope.edit = scope.edit || angular.noop;
                scope.trash = scope.trash || angular.noop;
            }
        };
    }]);
}());
