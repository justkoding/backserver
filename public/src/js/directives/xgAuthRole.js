(function () {
    'use strict';

    //xgEditTrash
    angular.module('mdtDrectives').directive('xgAuthRole', ['$rootScope',
        'commonService',
        function ($rootScope,
            commonService) {
            return {
                restrict: 'A',
                scope: {
                    role: '@xgAuthRole'
                },
                link: function (scope, ele, attrs) {
                    var roles = [];
                    var userRoles = commonService.getRoleInfoOfCurrentUser();
                    var index = -1;

                    if (scope.role) {
                        roles = scope.role.split(',');
                    }

                    if (userRoles && roles.length != 0) {
                        for (var i = 0; i < roles.length; i++) {
                            index = userRoles.indexOf(roles[i]);

                            if (index != -1) {
                                break;
                            }
                        }
                    }

                    // hide element if current user don't belong to expected role.
                    if (index === -1) {
                        angular.element(ele).addClass('ng-hide');
                    }
                }
            };
        }]);
}());
