(function () {
    'use strict';

    //xgEditTrash
    angular.module('mdtDrectives').directive('xgTrash', [
        '$rootScope',
        'modalService',
        function ($rootScope,
            modalService) {
            return {
                restrict: 'E',
                scope: {
                    id: '@xgId',
                    mdtId: '@xgMdtId',
                    onTrash: '&xgOnTrash',
                    title: '@xgTitle',
                    body: '@xgBody'
                },
                replace: true,
                templateUrl: 'src/templates/xgTrash/xgTrash.tpl.html',
                link: function (scope, element, attrs) {
                    scope.title = scope.title || 'COMMON_DELETE_CONFIRM_TITLE';
                    scope.body = scope.body || 'COMMON_DELETE_CONFIRM_BODY';

                    scope.trash = function () {
                        modalService.showModal('/assets/src/pages/confirm/confirm.html',
                          {
                              controller: 'ConfirmCtrl',
                              data: {
                                  title: scope.title,
                                  body: scope.body
                              },
                              closefn: function () {
                                  scope.onTrash && scope.onTrash({
                                      id: scope.id,
                                      mdtId: scope.mdtId
                                  });
                              }
                          });
                    };
                }
            };
        }]);
}());
