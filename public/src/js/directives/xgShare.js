(function () {
    'use strict';

    //xgEditTrash
    angular.module('mdtDrectives').directive('xgShare', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            scope: {
                share: '&share',
                entity: '@' // the entity which is edited/deleted.
            },
            replace: true,
            templateUrl: 'src/templates/xgShare/xgShare.tpl.html',
            link: function (scope, element, attrs) {
                scope.onShare = function () {
                    if (scope.share) {
                        var entity = scope.entity ? JSON.parse(scope.entity) : {};
                        scope.share({
                            entity: entity
                        });
                    }
                };
            }
        };
    }]);
}());
