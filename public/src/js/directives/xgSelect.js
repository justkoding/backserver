(function () {
    'use strict';

    //xgFilter
    angular.module('mdtDrectives').directive('xgSelect', function () {
        return {
            restrict: 'E',
            scope: {
                name: '@xgName',
                data: '=xgData',
                change: '&xgOnChanged',
                items: '=xgItems'
            },
            templateUrl: 'src/templates/xgSelect/xgSelect.tpl.html',
            link: function (scope, element, attrs) {
                // dropdown show or hide
                scope.opened = false;

                scope.toggle = function () {
                    scope.opened = !scope.opened;
                };

                scope.onClickItem = function (item) {
                    scope.opened = false;
                    scope.data = item.value;
                };

                /*----------events-------------*/
                scope.$watch('data', function (newVal, oldVal) {
                    scope.change && scope.change({
                        value: scope.data
                    });
                });
                /*----------end events-------------*/
            }
        };
    });
}());
