(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgMeetingTimeLocation', ['entityServices', function (entityServices) {
        return {
            restrict: 'E',
            scope: {
                mdts:'=xgMdts'
            },
            replace: true,
            templateUrl: 'src/templates/xgMeetingTimeLocation/xgMeetingTimeLocation.tpl.html',
            link: function (scope, element, attrs) {
                scope.$watch('mdts', function(newVal, oldVal){

                    if(scope.mdts.mdt){
                        formatDateTime();
                    }
                });

                function formatDateTime() {
                    // get mdt's start time.
                    var mst = moment(scope.mdts.mdt.start).local(),
                        diff = 1000 * 60 * scope.mdts.mdt.duration,
                        met = new Date(mst + diff),
                        spacing = '  ',
                        from = moment(mst).format('HH:mm'),
                        to = moment(met).format('HH:mm');

                    //meeting start time, meeting end time, system time
                    var tempMeetingStartTime = '',
                        tempMeetingEndTime = '',
                        tempWeek = '';

                    // identity current language is chinese or not.
                    scope.isChinese = moment.locale().indexOf('cn') != -1;

                    // fileds for chinese locale.
                    scope.startDateTime = moment(mst).format('YYYY/MM/DD') + spacing + from + '-' + to + spacing + scope.mdts.mdt.location;

                    // fileds for other language like english locale.
                    tempMeetingStartTime = moment(mst).format('LLLL');
                    tempMeetingEndTime = moment(met).format('lll');
                    tempWeek = tempMeetingEndTime.substring(13, tempMeetingEndTime.length);

                    scope.startDateTimeEng = tempMeetingStartTime + '-' + tempWeek + spacing  + scope.mdts.mdt.location;
                }
            }
        };
    }]);
}());
