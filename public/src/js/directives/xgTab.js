(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgTab', [
        '$rootScope',
        '$translate',
        function ($rootScope, $translate) {
            return {
                restrict: 'E',
                scope: {
                    tabs: '=xgTabs',
                    form: '=xgForm',
                    tabName: '@xgTabName',
                    onTab: '&xgOnTab',
                    onRemove: '&xgOnRemove',
                    onAdd: '&xgOnAdd'
                },
                replace: true,
                templateUrl: 'src/templates/xgTab/xgTab.tpl.html',
                link: function (scope, element, attrs) {

                    // handler: on click tab.
                    scope.clickTab = function (tab, index) {
                        var originalIndex = indexOfActive();
                        activeTab(tab);

                        scope.onTab && scope.onTab({
                            item: {
                                tab: tab,
                                originalIndex: originalIndex,
                                activeIndex: index,
                                form: scope.form
                            }
                        });
                    };

                    // handler: add new tab.
                    var iNow = 1;
                    scope.addTab = function () {
                        var originalIndex = indexOfActive();

                        createNewTab(function (tab) {
                            scope.tabs.push(tab);
                            activeTab(tab);

                            scope.onAdd && scope.onAdd({
                                item: {
                                    tab: tab,
                                    originalIndex: originalIndex,
                                    activeIndex: scope.tabs.length - 1,
                                    form: scope.form
                                }
                            });
                        });

                        // set the first tab to support removed operation when tabs lenght >= 2
                        scope.tabs[0].removed = true;
                    };

                    // handler: on delete a existing tab
                    scope.deleteTab = function (tab, index) {
                        var originalIndex = indexOfActive();

                        scope.tabs.splice(index, 1);

                        // set active tab.
                        var activeIndex = index;
                        if (index >= scope.tabs.length - 1) {
                            activeIndex = scope.tabs.length - 1;
                        }

                        activeTab(scope.tabs[activeIndex]);

                        // always keep one tab there.
                        if (scope.tabs.length === 1) {
                            scope.tabs[0].removed = false;
                        }

                        scope.onRemove && scope.onRemove({
                            item: {
                                tab: tab,
                                originalIndex: originalIndex,
                                activeIndex: activeIndex,
                                form: scope.form
                            }
                        });
                    };

                    // init
                    init();

                    /*-------event--------*/

                    /*-------end--------*/


                    /*-------private--------*/
                    function init() {
                        // set default tab if tabs is empty.
                        if (scope.tabs == null || scope.tabs.length === 0) {
                            scope.tabs = [];

                            createNewTab(function (tab) {
                                tab.removed = false;
                                scope.tabs.push(tab);
                            });
                        } else if (scope.tabs.length === 1) {
                            scope.tabs[0].removed = false;
                        }
                    }

                    function createNewTab(cb) {
                        $translate(['TEXT_NEW_TAB']).then(function (texts) {
                            var tabName = scope.tabName || texts['TEXT_NEW_TAB'];
                            var tab = new Tab(tabName + (iNow++), true, true);

                            cb && cb(tab);
                        });
                    }

                    function Tab(name, active, removed) {
                        this.name = name;
                        this.active = active;
                        this.removed = removed;
                    }

                    function activeTab(tab) {
                        scope.tabs.forEach(function (t) {
                            t.active = false;
                        });

                        tab.active = true;
                    }

                    function indexOfActive() {
                        var index = 0;

                        scope.tabs.forEach(function (t, i) {
                            if (t.active) {
                                index = i;
                            }
                        });

                        return index;
                    }
                    /*-------end--------*/
                }
            };
        }]);
}());
