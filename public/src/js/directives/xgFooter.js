(function () {
    'use strict';

    //xgMessage
    angular.module('mdtDrectives').directive('xgFooter', ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            scope: {
                alignTo: '@xgAlignTo'
            },
            replace: true,
            templateUrl: 'src/templates/pageFooter/pageFooter.tpl.html',
            link: function (scope, element, attrs) {
                var toAlign = function () {
                    // get page height
                    var pHeight = document.documentElement.clientHeight;

                    // get speical DOM height.
                    var oAlign = document.getElementById(scope.alignTo);
                    var oHeight = oAlign.offsetHeight;

                    // fixed value of top bar.
                    var tHeight = 52;

                    // get current element height.
                    // fixed value here
                    var mHeight = 40;

                    // total
                    var totalHeight = oHeight + tHeight + mHeight;

                    // calc top value of current element 
                    var t = pHeight - mHeight;
                    if (totalHeight >= pHeight) {
                        t = oHeight + tHeight;
                    }

                    angular.element(element).css('width', (oAlign.offsetWidth));
                    angular.element(element).css('top', t);
                };

                $timeout(function () {
                    toAlign();
                }, 300);
                
                /*--------adjust postion when window resizing--------*/
                window.addEventListener("resize", function () {
                    toAlign();
                });
                /*-----------------end-------------------*/
            }
        };
    }]);
}());
