﻿(function () {
    'use strict';

    angular.module('mdtDrectives').directive('xgScrollBottom', ["$rootScope", function ($rootScope) {
        return {
            restrict: "EA", // attribute
            scope: {
                topic: '@',
                selector: '@xgSelector' // scroll target.
            },
            link: function (scope, element, attrs) {
                scope.topic = scope.topic ? scope.topic : 'xg.scroll.bottom';

                scope.$on(scope.topic, function (event, data) {
                    var ele = element;
                    if (scope.selector) {
                        ele = angular.element(scope.selector);
                    }

                    if (ele && ele.length != 0) {
                        ele.scrollTop(ele[0].scrollHeight);
                    }                    
                });
            }
        };
    }]);
}());