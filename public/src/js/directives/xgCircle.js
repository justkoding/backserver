(function () {
    'use strict';

    //xgCountdown
    angular.module('mdtDrectives').directive('xgCircle', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            scope: {
                timer: "=",
                iradius: '@radius',
                stroke: '@',
                color:'@'
            },
            replace: true,
            templateUrl: 'src/templates/xgCountdown/xgCountdown.tpl.html',
            link: function (scope, element, attrs) {
                scope.radius = parseInt(scope.iradius);
                scope.angle = 0;
                scope.done = false;

                // radius of backend circle.
                scope.cfill = '#e9e9e9';
                scope.cradius = scope.radius - parseInt(scope.stroke) / 2;

                initialize();

                scope.$on('tick', function (event, ticked) {
                    if (scope.done) {
                        scope.done = false;
                        scope.cfill = '#e9e9e9';
                    }
                    scope.angle += 2 * Math.PI * ticked;
                    initialize();                   
                });

                scope.$on('done', function () {
                    scope.done = true;
                    scope.cfill = scope.color;
                });

                function initialize() {
                    scope.diameter = scope.radius * 2;
                    scope.mid = ~~(scope.angle > Math.PI);
                    scope.x = Math.sin(scope.angle) * scope.radius;
                    scope.y = Math.cos(scope.angle) * -scope.radius;

                    var transform = 'translate(' + scope.radius + ',' + scope.radius + ')';
                    var d = 'M 0 0 v -' + scope.radius + ' A ' + scope.radius + ' ' + scope.radius + ' 1 ' + scope.mid + ' 1 ' + scope.x + ' ' + scope.y + ' z';
                    var fill = scope.color ? scope.color : 'red';

                    var svg = element[0];
                    var path = element.find('path')[0];
                   
                    svg.setAttribute('transform', transform);
                    path.setAttribute('transform', transform);
                    path.setAttribute('d', d);
                    path.setAttribute('fill', fill);
                }
            } 
        };
    }]);
}());
