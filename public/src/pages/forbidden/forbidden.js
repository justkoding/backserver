﻿(function () {
    'use strict';

    angular.module('app').controller('ForbiddenController', ['$scope',
        function ($scope) {

            /*---------end--------------*/
            $scope.$on('$destroy', function () {
                // clean up.
            });
        }]);
}());
