﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('Error401Ctrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        'strings',
        'commonService',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            strings,
            commonService) {

            $scope.back = commonService.history.back;

            $scope.navToLogin = function () {
                $state.go('login');
            };

            /*---------event----------*/
            $scope.$on('$destroy', function () {
                // clean up.
                $scope = null;
            });
            /*--------end-----------*/
        }]);
})();