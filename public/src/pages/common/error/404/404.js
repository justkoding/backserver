﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('Error404Ctrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$stateParams',
        'commonService',
        function ($scope,
            $rootScope,
            $cookieStore,
            $stateParams,
            commonService) {

            $scope.back = commonService.history.back;

            /*---------event----------*/
            $scope.$on('$destroy', function () {
                // clean up.
                $scope = null;
            });
            /*--------end-----------*/

    }]);
})();