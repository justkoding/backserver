﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('ModalInstanceCtrl', ['$scope',
        '$modalInstance',
        'data',
        function ($scope,
            $modalInstance,
            data) {
            $scope.data = JSON.parse(JSON.stringify(data));

            $scope.ok = function (form) {
                if (form.$valid) {
                    $modalInstance.close($scope.data);
                }
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }]);
})();