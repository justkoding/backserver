﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('DocxCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$stateParams',
        function ($scope,
            $rootScope,
            $cookieStore,
            $stateParams) {
        
        // search       
        var term = $stateParams.search ? $stateParams.search : '';
        $scope.filter = term;
    }]);
})();