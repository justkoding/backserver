﻿
(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('UserProfileCtrl', ['$scope',
        '$rootScope',
        '$state',
        '$stateParams',  
        'commonService',      
        'userServices',
        function ($scope,
            $rootScope,
            $state,
            $stateParams, 
            commonService,           
            userServices) {

            // get current user Id.
            var userId = $stateParams.id;

            // back
            $scope.back = commonService.history.back;

            // show loading icon
            commonService.progress.show();
            userServices.detail(userId);

            // tab
            $scope.iNow = 1;
            $scope.tab = function (iTab) {
                $scope.iNow = iTab;
            };

            /*--------event------*/
            $scope.$on('user.detail.done', function (event, data) {
                $scope.user = data;
                //set the user's avatar
                $scope.userAvatar = commonService.formatAvatar($scope.user.avatar,$scope.user.id);
                commonService.progress.hide();
            });

            $scope.$on('$destroy', function () {
                // clean up.
            });
            /*--------end------*/
        }]);
})();