﻿
(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('ListUserCtrl', ['$scope',
        '$rootScope',
        '$state',
        '$cookieStore',
        '$stateParams',
        'loadingServices',
        'localDataPaginationService',
        'commonService',
        'strings',
        'modalService',
        'userServices',      
        function ($scope,
            $rootScope,
            $state,
            $cookieStore,
            $stateParams,
            loadingServices,
            localDataPaginationService,
            commonService,
            strings,
            modalService,
            userServices) {

            var originalUsers = [];
            var asc = false;

            // show table if users array is not empty
            // else show default placeholder.
            $scope.hasData = false;

            $scope.users = [];

            // search           
            var term = $stateParams.search ? $stateParams.search : '';
            $scope.filter = $stateParams.search ? term : '';
            $scope.dept = -1;
            $scope.totalItems = 0;
            $scope.itemsPerPage = 10;
            $scope.currentPage = 1;
            $scope.dateorder = 1;

            var params = {
                filter: $scope.filter,
                dept: $scope.dept,
                size: $scope.itemsPerPage,
                page: $scope.currentPage,
                dateorder: $scope.dateorder
            };

            $scope.detail = function (user) {
                $state.go('page.user.detail', { id: user.id });
            };

            $scope.addAccount = function () {
                $state.go('page.user.add', null, {'reload': true});
            };

            $scope.joinRoles = function (roles) {
                return userServices.joinRoles(roles);
            };

            // xg-pagination
            $scope.pageChanged = function (item) {
                if (item.page != params.page) {
                    params.page = item.page;
                    params.size = item.size;
                    loadingServices.show();
                    userServices.list(params);
                }
            };

            $scope.pageSizeChanged = function (item) {
                if (item.size != params.size) {
                    params.page = item.page;
                    params.size = item.size;
                    loadingServices.show();
                    userServices.list(params);
                }
            };

            // load data for first page.
            loadingServices.show();
            userServices.list(params);

            // dept: xgfilter.   
            $scope.fstext = '';
            userServices.getDeptlist().then(function (data) {
                if ($scope) {
                    $scope.dbData = data;
                }
            });

            $scope.filterByDept = function (item) {
                $scope.dept = item.id;
                params.dept = $scope.dept;
                loadingServices.show();
                userServices.list(params);
            };

            // create time: xgSort.   
            $scope.sortByTime = function (asc) {
                var dateorder = asc ? 1 : 2;
                $scope.dateorder = dateorder;
                params.dateorder = dateorder;

                loadingServices.show();
                userServices.list(params);
            };

            /*--------event------*/
            $scope.$on('user.list.done', function (event, result) {
                if ($scope.users && $scope.users.length != 0) {
                    $scope.users.length = 0;
                }
                originalUsers = result.data;
                $scope.users = result.data;
                $scope.hasData = result.data && result.data.length != 0;
                $scope.totalItems = result.count;

                loadingServices.hide();
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope = null;
            });
            /*--------end------*/
        }]);
})();