﻿
(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('AddOrEidtUserCtrl', ['$scope',
        '$rootScope',
        '$state',
        '$translate',
        '$cookieStore',
        '$stateParams',
        'loadingServices',
        'strings',
        'modalService',
        'userServices',
        'passwordServices',
        'messageService',
        'rolesServices',
        'commonService',
        'entityServices',
        'healthrecordServices',
        'apiUrls',
        'hospitalsServices',
        function ($scope,
            $rootScope,
            $state,
            $translate,
            $cookieStore,
            $stateParams,
            loadingServices,
            strings,
            modalService,
            userServices,
            passwordServices,
            messageService,
            rolesServices,
            commonService,
            entityServices,
            healthrecordServices,
            apiUrls,
            hospitalsServices) {

            // request counts.
            var reqCount = 4;

            // set default settings.
            $scope.user = {
            };

            $scope.files = {
            };

            // to differentiate between the first upload or not
            $scope.isReupload = false;

            // to mark if the current action is edited or added.
            $scope.isEdit = true;
            $scope.isValidAccount = true;
            //$scope.depts = [];

            $scope.back = function () {
                var id = $scope.user.id;
                if (id) {
                    $state.go('page.user.detail', { id: $scope.user.id });
                } else {
                    $state.go('page.user', null, { 'reload': true });
                }
            };

            $scope.uploadImage = function () {
                loadingServices.show();
                var url = '/users/' + $scope.user.id + '/avatar';
                healthrecordServices.upload(url, $scope.files).then(function success(data) {
                    loadingServices.hide();

                    // display current user profile.           
                    var data = commonService.getCurrentUser();
                    if (data.account.userId == $scope.user.id) {
                        $rootScope.$broadcast('user.update.avatar', url);
                    }

                    //refresh the variable
                    var time = new Date();
                    $scope.userAvatar = url + '?timestamp=' + time.getTime();
                    $scope.files = {
                    };
                    $scope.isReupload = true;

                    $translate(['UPLOAD_AVATAR', 'MESSAGE_BODY_SUCCESS']).then(function (texts) {
                        messageService.success(texts['UPLOAD_AVATAR'], texts['MESSAGE_BODY_SUCCESS']);
                    });
                });
            }

            /*add/remove role*/
            $scope.removeRole = function (role) {
                // to do
            };

            $scope.addRole = function () {
                // to do
            };

            /*user type/ dept changed*/
            $scope.userTypeChanged = function () {
                // todo
            };

            $scope.deptChanged = function () {
                // todo
            };

            $scope.userTitleChanged = function () {
                // todo
            };

            /*account name change*/
            $scope.onChangeAccountName = function () {
                loadingServices.show();
                $scope.isValidAccount = true
                if ($scope.user.accountName) {
                    userServices.checkAccount($scope.user.accountName);
                }
            };

            /*add/remove assistants*/
            $scope.removeAssistant = function (ass) {
                // to do
            };

            $scope.addAssistants = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/users/addAssistant/addAssistant.html',
                   {
                       controller: 'AddAssistantCtrl',
                       data: {
                           user: $scope.user,
                           depts: $scope.depts,
                           userTypes: $scope.usertypes
                       },
                       closefn: addAssistantCallback
                   });
            };

            $scope.save = function (form) {
                loadingServices.show();

                // deep copy.
                var postData = JSON.parse(JSON.stringify($scope.user));

                // format post data.
                var ass = postData.assistants.map(function (assitant) {
                    return {
                        id: assitant.id,
                        name: assitant.name
                    };
                });

                postData.assistants = ass;

                // role
                if (postData.role && postData.role.id != -1) {
                    var tempRole = postData.role;
                    delete postData.roles;
                    delete postData.role;

                    postData.roles = [{
                        id: tempRole.id,
                        name: tempRole.name
                    }];
                }

                //TODO remove mock data
                delete postData.hospital;

                //userTitle
                if (postData.userTitle && postData.userTitle.id != -1) {
                    var tempTitle = postData.userTitle;
                    delete postData.userTitle;
                    postData.userTitle = {
                        id: tempTitle.id,
                        name: tempTitle.name
                    };
                }

                //dept
                if (postData.dept && postData.dept.id != -1) {
                    var tempDept = postData.dept;
                    delete postData.dept;
                    postData.dept = {
                        id: tempDept.id,
                        name: tempDept.name
                    };
                }

                //userType
                if (postData.userType && postData.userType.id != -1) {
                    var tempType = postData.userType;
                    delete postData.userType;
                    postData.userType = {
                        id: tempType.id,
                        name: tempType.name
                    };
                }


                // should update existing user if id is existing. else create a new account.
                if (postData.id) {
                    userServices.update(postData);
                } else {
                    userServices.add(postData);
                }
            };

            $scope.cancel = function () {
                $state.go('page.user', null, { 'reload': true });
            };

            $scope.delete = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/confirm/confirm.html',
                   {
                       controller: 'ConfirmCtrl',
                       data: {
                           title: 'USER_DELETE_CONFIRM_TITLE',
                           body: 'USER_DELETE_CONFIRM_BODY'
                       },
                       closefn: deleteUserCallback
                   });
            };

            $scope.resetPwd = function () {
                loadingServices.show();
                passwordServices.resetPwd($scope.user.id);
            };

            $scope.enableAccount = function (isEnable) {
                loadingServices.show();
                if (isEnable) {
                    userServices.enable($scope.user.id);
                } else {
                    userServices.disable($scope.user.id);
                }
            };

            // initialize
            init();

            /*----------private method-------------*/
            function init() {
                // show loading icon
                loadingServices.show();

                // get current user Id.
                var userId = $stateParams.id;

                // get user info by id.
                // will skip here for adding a new account.
                if (userId) {
                    reqCount++;
                    userServices.detail(userId);
                } else {
                    // set default settings.
                    $scope.user = {
                        accountStatus: 1,
                        avatar: null,
                        roles: [],
                        assistants: []
                    };

                    // get hosptial data.
                    hospitalsServices.list().then(function (results) {
                        $scope.hospitals = results || [];
                        $scope.user.hospital = $scope.hospitals.length != 0 ? $scope.hospitals[0] : null;
                    });
                }

                $scope.isEdit = userId != null;

                $scope.isDisabled = false;

                // get all available roles list.
                rolesServices.listValues();

                // get all user type
                userServices.getUserTypelist();

                // get all dept
                userServices.getDeptlist();

                // get all titles.
                userServices.getUserTitleList();
            }

            function addAssistantCallback(ass) {
                $scope.user.assistants = ass;
                $scope.user.assistants.forEach(function (item) {
                    item.userName = item.name;
                });
            }

            function deleteUserCallback() {
                loadingServices.show();
                userServices.remove($scope.user.id);
            }

            /*---------------------------------*/

            /*--------event------*/
            $scope.$on('user.detail.done', function (event, data) {

                var uTypeId = data.userType ? data.userType.id : -1,
                    oTypeId = $scope.user.userType ? $scope.user.userType.id : -1,
                    uDeptId = data.dept ? data.dept.id : -1,
                    oDeptId = $scope.user.dept ? $scope.user.dept.id : -1,
                    uTitleId = data.userTitle ? data.userTitle.id : -1,
                    oTitleId = $scope.user.userTitle ? $scope.user.userTitle.id : -1,
                    uRoleId = data.roles ? data.roles[0].id : -1,
                    oRoleId = $scope.user.role ? $scope.user.role.id : -1;

                var user = {};

                user = angular.extend(user, data, $scope.user);
                $scope.user = user;

                $scope.user.assistants.forEach(function (item) {
                    item.userName = item.name;
                });

                // mock hospital item.

                // get hosptial data.
                commonService.buildHosptialDataSource().then(function (results) {
                    $scope.hospitals = results || [];
                    $scope.user.hospital = $scope.hospitals[1];
                });

                //set the user's avatar                
                $scope.userAvatar = $scope.user.avatar ? '/users/' + $scope.user.id + '/avatar' : '/assets/src/img/icons/defaultuser.png';

                // set current usertype as selected item in usertype dropdown list.
                if (uTypeId != oTypeId) {
                    var index = commonService.getIndexOf(data.userType, $scope.usertypes);
                    index = index == -1 ? 0 : index;
                    $scope.user.userType = $scope.usertypes[index];
                }

                // set current usertype as selected item in usertype dropdown list.
                if (uDeptId != oDeptId) {
                    var index = commonService.getIndexOf(data.dept, $scope.depts);
                    index = index == -1 ? 0 : index;
                    $scope.user.dept = $scope.depts[index];
                }

                // set current userTitle as selected item in userTitle dropdown list.
                if (uTitleId != oTitleId) {
                    var index = commonService.getIndexOf(data.userTitle, $scope.usertitles);
                    index = index == -1 ? 0 : index;
                    $scope.user.userTitle = $scope.usertitles[index];
                }

                // set current user role as selected item in user role dropdown list.
                if (uRoleId != oRoleId) {
                    var index = commonService.getIndexOf(data.roles[0], $scope.aroles);
                    index = index == -1 ? 0 : index;
                    $scope.user.role = $scope.aroles[index];
                }

                if (--reqCount === 0) {
                    loadingServices.hide();
                }
            });

            $scope.$on('role.listValues.done', function (event, data) {
                $scope.aroles = data;

                if ($scope.user.role == null) {
                    $scope.user.role = $scope.aroles[0];
                } else {
                    var index = commonService.getIndexOf($scope.user.role, $scope.aroles);

                    index = index == -1 ? 0 : index;
                    $scope.user.role = $scope.aroles[index];
                }

                if (--reqCount === 0) {
                    loadingServices.hide();
                }
            });

            $scope.$on('usertype.list.done', function (event, data) {
                $scope.usertypes = data;
                if ($scope.user.userType == null) {
                    $scope.user.userType = $scope.usertypes[0];
                } else {
                    var index = commonService.getIndexOf($scope.user.userType, $scope.usertypes);

                    index = index == -1 ? 0 : index;
                    $scope.user.userType = $scope.usertypes[index];
                }

                if (--reqCount === 0) {
                    loadingServices.hide();
                }
            });

            $scope.$on('dept.list.done', function (event, data) {
                var arr = [];
                if (data.length != 0) {
                    commonService.resource.getValues('TEXT_DP_LIST_ALL').then(function (texts) {
                        arr.push(new entityServices.DpListItem(texts['TEXT_DP_LIST_ALL'], -1));

                        data.forEach(function (d) {
                            arr.push(new entityServices.DpListItem(d.name, d.id));
                        });

                        $scope.depts = arr;

                        if ($scope.user.dept == null) {
                            $scope.user.dept = $scope.depts[0];
                        } else {
                            var index = commonService.getIndexOf($scope.user.dept, $scope.depts);

                            index = index == -1 ? 0 : index;
                            $scope.user.dept = $scope.depts[index];
                        }

                        if (--reqCount === 0) {
                            loadingServices.hide();
                        }
                    });
                }
            });

            $scope.$on('title.list.done', function (event, data) {
                $scope.usertitles = data;
                if ($scope.user.userTitle == null) {
                    $scope.user.userTitle = $scope.usertitles[0];
                } else {
                    var index = commonService.getIndexOf($scope.user.userTitle, $scope.usertitles);

                    index = index == -1 ? 0 : index;
                    $scope.user.userTitle = $scope.usertitles[index];
                }

                if (--reqCount === 0) {
                    loadingServices.hide();
                }
            });

            $scope.$on('user.update.done', function (event, data) {
                loadingServices.hide();
                $state.go('page.user', null, { 'reload': true });
            });

            $scope.$on('user.add.done', function (event, data) {
                loadingServices.hide();
                $state.go('page.user', null, { 'reload': true });
            });

            $scope.$on('password.reset.done', function (event, data) {
                loadingServices.hide();
            });

            $scope.$on('account.enable.done', function (event, data) {
                loadingServices.hide();
                $scope.user.accountStatus = 1;
            });

            $scope.$on('account.disable.done', function (event, data) {
                loadingServices.hide();
                $scope.user.accountStatus = 0;
            });

            $scope.$on('user.remove.done', function (event, data) {
                loadingServices.hide();
                $state.go('page.user', null, { 'reload': true });
            });

            //$scope.isValidAccount = true
            $scope.$on('account.exist.done', function (event, data) {
                loadingServices.hide();
                if (data.result == 'OK') {
                    $scope.isValidAccount = true
                } else {
                    $scope.isValidAccount = false;
                }
            });

            $scope.$on('$destroy', function () {
                $scope = null;
                // clean up.
            });

            /*--------end------*/
        }]);
})();