﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('CommonSelectDoctorCtrl', ['$scope',
        '$timeout',
        '$translate',
        'messageService',
        'userServices',
        'commonService',
        '$modalInstance',
        'data',
        function ($scope,
            $timeout,
            $translate,
            messageService,
            userServices,
            commonService,
            $modalInstance,
            data) {
            var originalUsers = [];
            var subOriginalUsers = [];
            var params = {
                filter: '',
                dept: -1,
                size: -1,
                page: -1
            };
            var timer = null;
            var splicedUsers = null;

            commonService.progress.hide();
            // deep copy
            $scope.data = JSON.parse(JSON.stringify(data));
            $scope.isDisabledAttenance = $scope.data.isDisabledAttenance;

            $scope.tempAttendees = [];

            $scope.iNow = 1;

            $scope.tab = function (iTab) {
                $scope.iNow = iTab;

                $scope.fsText = '';
                $scope.filter = '';
                $scope.data.dept = $scope.data.depts[0];

                params.dept = $scope.data.dept.id;
                params.filter = $scope.fsText;

                userServices.list(params);
            };

            $scope.deptChanged = function () {
                var arr = [];

                //reset filter selected text.
                $scope.fsText = '';

                if ($scope.data.dept.id == -1) {
                    subOriginalUsers = originalUsers;
                    $scope.users = originalUsers;
                } else {
                    originalUsers.forEach(function (item) {
                        if (item.dept.id == $scope.data.dept.id) {
                            arr.push(item);
                        }
                    });

                    $scope.users = arr;
                    subOriginalUsers = arr;
                }
            };

            $scope.fsText = '';
            $scope.filterByTitle = function (title) {
                var arr = [];
                if (title.id == -1) {
                    $scope.users = subOriginalUsers;
                } else {
                    subOriginalUsers.forEach(function (ou) {
                        if (title.id == ou.userTitle.id) {
                            arr.push(ou);
                        }
                    });

                    $scope.users = arr;
                }
            };

            // search
            $scope.filter = '';
            $scope.search = function () {
                $timeout.cancel(timer);
                params.filter = $scope.filter;

                timer = $timeout(function () {
                    commonService.progress.show();
                    userServices.list(params);
                }, 300);
            };

            // table, checkbox handler
            $scope.addAttendee = function (attendee, $index) {
                var index = commonService.getIndexOf(attendee, $scope.data.attendees);

                if (attendee.checked) {
                    if (index != -1) {
                        $scope.data.attendees.splice(index, 1);
                    }
                } else {
                    if (index == -1) {
                        $scope.data.attendees.push(attendee);
                    }
                }

                $scope.users[$index].checked = !($scope.users[$index].checked);
            };

            $scope.removeAttendee = function (attendee, $index) {
                $scope.data.attendees.splice($index, 1);

                var index = commonService.getIndexOf(attendee, $scope.users);
                if (index != -1) {
                    $scope.users[index].checked = false;
                }
            };

            // modal.
            $scope.ok = function () {
                var attendees = [];

                if ($scope.data.attendees && $scope.data.attendees.length != 0) {
                    attendees = JSON.parse(JSON.stringify($scope.data.attendees));
                }

                if ($scope.isDisabledAttenance && $scope.tempAttendees && $scope.tempAttendees.length != 0) {
                    attendees = attendees.concat($scope.tempAttendees);
                }

                $modalInstance.close(attendees);
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
                console.log($scope.data);
            };

            /*--------events------*/
            $scope.$on('user.list.done', function (event, result) {               
                if ($scope.isDisabledAttenance) {
                    if (result && result.data && result.data.length != 0) {
                        for (var i = 0; i < result.data.length; i++) {
                            var index = commonService.getIndexOf(result.data[i], $scope.tempAttendees);
                            if (index != -1) {
                                result.data.splice(i, 1);
                                i = -1;
                            } else {
                                result.data[i].userName = result.data[i].name;
                            }
                        }
                    }
                } else {
                    if (result && result.data && result.data.length != 0) {
                        result.data.forEach(function (item, i) {
                            item.checked = commonService.getIndexOf(item, $scope.data.attendees) != -1;
                            item.userName = item.name;
                        });
                    }
                }

                originalUsers = result.data;
                subOriginalUsers = originalUsers;
                $scope.users = result.data;
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });
            /*--------end------*/

            /*--------private------*/
            function init() {
                commonService.progress.show();

                if ($scope.isDisabledAttenance) {
                    // backup
                    $scope.tempAttendees = JSON.parse(JSON.stringify($scope.data.attendees));

                    $scope.data.attendees = [];
                }

                // get depts list and available usertype.
                userServices.getDeptsAndUserTypesTitles().then(function (resp) {

                    // remove first item which is a clear item, no need in this case.
                    if (resp.depts && resp.depts.length != 0) {
                        resp.depts.splice(0, 1);
                    }
                    $scope.data.depts = resp.depts;
                    $scope.data.userTypes = resp.userTypes;
                    $scope.data.userTitles = resp.userTitles;

                    // dept dropdown list.
                    commonService.buildDpPlaceHolder('USER_ASSISTANT_DEPT_PLACEHOLDER', -1).then(function (item) {
                        // remove first one item which is a clear item.                        
                        $scope.data.depts.unshift(item);
                        $scope.data.dept = $scope.data.depts[0];
                    });

                    commonService.buildDpPlaceHolder('TEXT_DP_LIST_CLEAR', -1).then(function (item) {
                        $scope.data.userTypes.unshift(item);
                    });

                    userServices.list(params);
                });
            }
            /*--------end------*/

            // init
            init();
        }]);
})();