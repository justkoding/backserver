﻿
(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('DetailUserCtrl', ['$scope',
        '$rootScope',
        '$state',
        '$stateParams',
        'loadingServices',
        'strings',
        'modalService',
        'commonService',
        'userServices',
        function ($scope,
            $rootScope,
            $state,
            $stateParams,
            loadingServices,
            strings,
            modalService,
            commonService,
            userServices) {

            $scope.isDetail = true;

            // get current user Id.
            var userId = $stateParams.id;

            // show loading icon
            loadingServices.show();  
            userServices.detail(userId);

            $scope.edit = function () {
                $state.go('page.user.edit', { id: userId });
            };
            
            // back
            $scope.back = function () {
                $state.go('page.user', null, {'reload': true});
            };

            /*--------event------*/
            $scope.$on('user.detail.done', function (event, data) {
                $scope.user = data;

                $scope.user.assistants.forEach(function(item){
                    item.userName = item.name;
                });

                //set the user's avatar
                $scope.userAvatar = $scope.user.avatar ? '/users/' + $scope.user.id + '/avatar' : '/assets/src/img/icons/defaultuser.png';

                //TODO: mock data
                commonService.buildHosptialDataSource().then(function (results) {
                    $scope.hospitals = results || [];
                    $scope.user.hospital = $scope.hospitals[1];
                });

                loadingServices.hide();
            });

            $scope.$on('$destroy', function () {
                // clean up.
            });
            /*--------end------*/
        }]);
})();