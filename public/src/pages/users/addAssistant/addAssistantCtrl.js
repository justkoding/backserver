﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('AddAssistantCtrl', ['$scope',
        '$timeout',
        '$translate',
        'loadingServices',
        'messageService',
        'userServices',
        'commonService',
        '$modalInstance',
        'data',
        function ($scope,
            $timeout,
            $translate,
            loadingServices,
            messageService,
            userServices,
            commonService,
            $modalInstance,
            data) {
            var originalUsers = [];
            var subOriginalUsers = [];
            var params = {
                filter: '',
                dept: -1,
                size: -1,
                page: -1
            };
            var timer = null;

            loadingServices.hide();

            // deep copy
            $scope.data = JSON.parse(JSON.stringify(data));

            $scope.addByDept = true;

            $scope.tab = function (addByDept) {
                $scope.addByDept = addByDept;

                $scope.fsText = '';
                $scope.filter = '';
                $scope.data.dept = $scope.data.depts[0];

                params.dept = $scope.data.dept.id;
                params.filter = $scope.fsText;

                loadingServices.show();
                userServices.list(params);
            };

            $scope.deptChanged = function () {
                var arr = [];

                //reset filter selected text.
                $scope.fsText = '';

                if ($scope.data.dept.id == -1) {
                    subOriginalUsers = originalUsers;
                    $scope.users = originalUsers;
                } else {
                    originalUsers.forEach(function (item) {
                        if (item.dept.id == $scope.data.dept.id) {
                            arr.push(item);
                        }
                    });

                    $scope.users = arr;
                    subOriginalUsers = arr;
                }
            };

            $scope.fsText = '';
            $scope.filterByTitle = function (userType) {
                var arr = [];
                if (userType.id == -1) {
                    $scope.users = subOriginalUsers;
                } else {
                    subOriginalUsers.forEach(function (ou) {
                        if (userType.id == ou.userType.id) {
                            arr.push(ou);
                        }
                    });

                    $scope.users = arr;
                }
            };

            // search
            $scope.filter = '';
            $scope.search = function () {
                $timeout.cancel(timer);
                params.filter = $scope.filter;

                timer = $timeout(function () {
                    loadingServices.show();
                    userServices.list(params);
                }, 300);
            };

            // modal.
            $scope.ok = function () {
                var selectedAssistants = [];
                $scope.users.forEach(function (item) {
                    if (item.checked) {
                        selectedAssistants.push(item);
                    }
                    delete item.userName;
                });
                $modalInstance.close(selectedAssistants);
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
                console.log($scope.data);
            };

            loadingServices.show();
            userServices.list(params);

            // dept dropdown list.
            commonService.buildDpPlaceHolder('USER_ASSISTANT_DEPT_PLACEHOLDER', -1).then(function (item) {
                $scope.data.depts.unshift(item);
                $scope.data.dept = $scope.data.depts[0];
            });

            commonService.buildDpPlaceHolder('TEXT_DP_LIST_CLEAR', -1).then(function (item) {
                $scope.data.userTypes.unshift(item);
            });

            /*--------events------*/
            $scope.$on('user.list.done', function (event, result) {
                loadingServices.hide();
                var data = result.data.map(function (item) {
                    item.checked = commonService.getIndexOf(item, $scope.data.user.assistants) != -1;
                    item.userName = item.name;
                    return item;
                });

                originalUsers = data;
                subOriginalUsers = originalUsers;
                $scope.users = data;
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });
            /*--------end------*/
        }]);
})();