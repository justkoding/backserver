﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('PatientDiscussionCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        '$timeout',
        'loadingServices',
        'commonService',
        'modalService',
        'mdtServices',
        'healthrecordServices',
        'clientServices',
        'commentsServices',
        'favoritesServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            $timeout,
            loadingServices,
            commonService,
            modalService,
            mdtServices,
            healthrecordServices,
            clientServices,
            commentsServices,
            favoritesServices) {

            var mdtId = $scope.mdtid || $stateParams.id;
            var user = commonService.getCurrentUser();
            var commentCount = 20;

            var param = {
                doctorship: 0,
                mdtid: mdtId
            };

            /*----------base info---------------*/
            // to control whether the edit/trash button visibility.
            $scope.baseInfoFocus = false;

            // default selected patient.
            $scope.activePatientTab = {
                healthrecords: [],
                patient: null
            };
            $scope.iNow = 0;

            // view patient on clicking tab in heading.
            $scope.viewPatient = function (p, index) {
                $scope.iNow = index;
                $scope.activePatientTab = p;
                $scope.healthrecords = p.healthrecords;

                // set current tab to focus.
                $scope.patients.forEach(function (pt) {
                    pt.focus = false;
                });
                $scope.activePatientTab.focus = true;

                // get comments under this patient.
                getComments(p.patient.id, commentCount);
            };

            // add new patient.
            $scope.addPatient = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/meeting/addPatients/addPatients.html',
                 {
                     controller: 'AddPatientsCtrl',
                     data: {
                         patient: {
                             sex: 1 // set man as default.
                         },
                         existingPatients: getPatientsDataFromScope(),
                         userId: user.account.accountId
                     },
                     closefn: addPatientCallback
                 });
            };

            // will be used in step#2 when creating a new mdt.
            $rootScope.addPatient = $scope.addPatient;

            $scope.addPatientMaterial = function (data) {
                loadingServices.show();

                // get all healthrecords list.
                var id = $scope.activePatientTab.patient.id;
                healthrecordServices.list(id).then(function (hrs) {
                    loadingServices.hide();

                    // launched modal by passing healthrecords.
                    loadingServices.show();
                    modalService.showModal('/assets/src/pages/patients/material/addNewMaterialOrFromLibrary.html',
                      {
                          controller: 'AddPatientMaterialCtrl',
                          data: {
                              patientId: id,
                              healthrecords: hrs
                          },
                          closefn: addorEditMaterialCallback
                      });
                });
            };
            /*----------end base info---------------*/

            /*----------comment---------------*/
            // comments
            $scope.comments = [];

            // to control whether display 'load more' button.
            $scope.hasMoreComments = true;

            // load same coments count when refreshing.
            $scope.lastCommentsCount = commentCount;

            // to mark whether refreshing comments.
            $scope.refreshing = false;

            // post a new comment
            $scope.addComment = function () {
                modalService.showModal('/assets/src/pages/meeting/basic/comments/comments.html',
                     {
                         controller: 'AddCommentsCtrl',
                         data: {
                             mdtId: mdtId,
                             patientId: $scope.activePatientTab.patient.id
                         },
                         closefn: addOrReplyCallback
                     });
            };

            // review existing comment
            $scope.viewComment = function (id) {
            };

            // reply an old comment
            $scope.replyComment = function (originalComment) {
                modalService.showModal('/assets/src/pages/meeting/basic/comments/comments.html',
                    {
                        controller: 'AddCommentsCtrl',
                        data: {
                            mdtId: mdtId,
                            patientId: $scope.activePatientTab.patient.id,
                            quoteComment: originalComment
                        },
                        closefn: addOrReplyCallback
                    });
            };

            // retireve comments again.
            $scope.refreshComments = function () {
                $scope.refreshing = true;
                getComments($scope.activePatientTab.patient.id, $scope.lastCommentsCount);
            };

            // load all comments
            $scope.loadAllComments = function () {
                getComments($scope.activePatientTab.patient.id, 0);
            };
            /*----------end comment---------------*/

            /*----------launch pc client---------------*/
            // launch pc client to join meeting.
            $scope.meeting = '';
            $scope.startOnlineMeeting = function () {
                loadingServices.show();
                clientServices.meeting(user.token, parseInt(mdtId));
            };

            // launch pc client to view healthrecord.
            $scope.viewHealthrecordInView = function (hr) {
                $scope.viewHealthrecord({
                    healthrecordId: hr.id,
                    healthrecordType: hr.type
                });
            }

            // launch pc client to view healthrecord.
            $scope.viewHealthrecord = function (hr) {
                loadingServices.show();

                var patientId = parseInt($scope.activePatientTab.patient.id);
                clientServices.healthrecord({
                    mdtId: parseInt(mdtId),
                    ticket: user.token,
                    healthrecordId: hr.healthrecordId,
                    healthrecordType: hr.healthrecordType,
                    patientId: patientId
                });
            };

            // launch pc client to view snapshot.
            $scope.viewAttachment = function (comment) {
                // healthrecod
                if (comment.origin === 1) {
                    $scope.viewHealthrecord(comment);
                } else if (comment.origin === 2) { // snapshot
                    $scope.viewSnapshot(comment);
                }
            };

            $scope.viewSnapshot = function (comment) {
                var snapshot = comment.attach;

                loadingServices.show();

                var patientId = parseInt($scope.activePatientTab.patient.id);
                var snapshotId = parseInt(snapshot.id);
                clientServices.healthrecord({
                    ticket: user.token,
                    patientId: patientId,
                    mdtId: parseInt(mdtId),
                    healthrecordId: comment.healthrecordId,
                    healthrecordType: comment.healthrecordType,
                    snapshotId: snapshotId
                });
            };
            /*----------end pc client---------------*/

            // xg-edit-trash: edit and trash handler.
            // edit patient.
            $scope.editPatient = function () {
                loadingServices.show();
                var data = JSON.parse(JSON.stringify($scope.activePatientTab.patient));
                delete data.gender;

                modalService.showModal('/assets/src/pages/patients/addOrEdit/addOrEdit.html',
                   {
                       controller: 'AddorEditPatientCtrl',
                       data: {
                           patient: data,
                           isEdit: true,
                           fromMdt: true,
                           reason: $scope.activePatientTab.reason
                       },
                       closefn: addorEditPatientCallback
                   });
            };

            $scope.deletePatient = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/confirm/confirm.html',
                  {
                      controller: 'ConfirmCtrl',
                      data: {
                          title: 'PATIENT_DELETE_CONFIRM_TITLE',
                          body: 'PATIENT_DELETE_CONFIRM_BODY'
                      },
                      closefn: function () {
                          deletePatientCallback($scope.activePatientTab.patient);
                      }
                  });
            };

            // xgTrash
            $scope.deleteMaterial = function (hrId) {
                loadingServices.show();
                mdtServices.deleteHealthrecord({
                    mdtid: mdtId,
                    hrdId: hrId
                });
            };

            $scope.afterSave = function (item) {

                var option = {},
                    data = {};

                data.note = item.description;
                option.id = item.id;
                option.data = data;

                healthrecordServices.update(option);
            };

            // todo
            $scope.healthrecords = [];
            $scope.healthrecordInView = {};
            $scope.needView = false;
            $scope.isShow = false;

            // onmouse enter and leave healthrecord
            var healthrecordInView = document.getElementById('healthrecordInView');
            var timer = null;

            $scope.onEnterHealthrecord = function (event, hr) {
                $timeout.cancel(timer);
                $scope.isShow = true;
                $scope.needView = true;

                var element = angular.element(event.target);
                var offset = element.offset();
                var top = Math.floor(offset.top - element.height() / 2) + 'px';
                var left = Math.floor(offset.left - element.width() / 2) + 'px';

                if (healthrecordInView.style.top != top ||
                    healthrecordInView.style.left != left) {

                    $scope.healthrecordInView = hr;
                    var offset = angular.element(event.target).offset();
                    healthrecordInView.style.top = top;
                    healthrecordInView.style.left = left;
                }
            };
            $scope.onLeaveHealthrecord = function (event, hr) {
                timer = $timeout(function () {
                    if (!$scope.isShow) {
                        $scope.needView = false;
                    }
                }, 500);
            };

            $scope.onEnterViewHealthrecord = function () {
                $scope.isShow = true;
            }
            $scope.onLeaveViewHealthrecord = function () {
                $scope.isShow = false;
                $scope.needView = false;
            }

            /*-------favorite------*/
            $scope.addFavorite = function (favorite) {
                if (favorite.isAdd) {
                    loadingServices.show();
                    modalService.showModal('/assets/src/pages/patients/favorite/favorite.html',
                       {
                           controller: 'FavoriteCtrl',
                           data: {
                               patient: favorite.data
                           },
                           closefn: addToFavoriteCallback
                       });
                } else {
                    loadingServices.show();
                    modalService.showModal('/assets/src/pages/confirm/confirm.html',
                       {
                           controller: 'ConfirmCtrl',
                           data: {
                               title: 'COMMON_DELETE_CONFIRM_TITLE',
                               body: 'COMMON_REMOVE_FROM_FAVORITES_CONFIRM_BODY'
                           },
                           closefn: removeFavoriteCallback
                       });
                }
            };
            /*-------end------*/

            /*-------event------*/
            $scope.$on('mdt.patients.list.done', function (event, resp) {
                loadingServices.hide();
                // get healthrecords for each patient
                if (resp.data && resp.data.length != 0) {
                    var iCount = 0;
                    resp.data = resp.data.map(function (p) {
                        p.healthrecords = [];

                        mdtServices.healthrecordsList({
                            mdtid: mdtId,
                            patientId: p.patient.id
                        }).then(function (data) {
                            p.healthrecords = data;
                        });

                        return p;
                    });
                }

                $scope.totalItems = resp.count;
                $scope.patients = resp.data;

                // display the last one.
                if ($scope.iNow != 0 &&
                    $scope.patients &&
                    $scope.iNow >= $scope.patients.length) {

                    $scope.iNow = $scope.patients.length - 1;
                }

                if ($scope.patients && $scope.patients.length != 0) {
                    $scope.activePatientTab = $scope.patients[$scope.iNow];
                    $scope.activePatientTab.focus = true;
                    $scope.healthrecords = $scope.activePatientTab.healthrecords;
                    getComments($scope.activePatientTab.patient.id, commentCount);
                }
            });

            $scope.$on('hr.open.done', function (event, path) {
                $scope.hrPath = path;

                $timeout(function () {
                    document.getElementById('healthrecordlink').click();
                    loadingServices.hide();
                }, 0);
            });

            $scope.$on('hr.open.failed', function (event, path) {
                loadingServices.hide();
            });

            // delete patient from mdt
            $scope.$on('mdt.patient.delete.done', retrievePatients);
            $scope.$on('mdt.patient.addOrUpdate.done', retrievePatients);

            $scope.$on('mdt.healthrecords.add.done', retrieveHealthrecords);
            $scope.$on('mdt.healthrecord.delete.done', retrieveHealthrecords);

            // comments
            $scope.$on('comments.list.done', function (event, resp) {
                $scope.comments = resp.data;
                $scope.hasMoreComments = resp.count > resp.data.length;
                // refresh done.
                // add delay to optimaze UX.
                $timeout(function () {
                    if ($scope.refreshing) {
                        $scope.refreshing = false;
                    }
                }, 1000);
                console.log({ comments: $scope.comments });
            });

            $scope.$on('page.patient.discuss.focus', function () {
                if ($scope.activePatientTab) {
                    $scope.healthrecords = $scope.activePatientTab.healthrecords;
                }
            });

            // online meeting.
            $scope.$on('mdt.join.done', function (event, path) {
                $scope.meeting = path;

                $timeout(function () {
                    document.getElementById('meeting').click();
                    loadingServices.hide();
                }, 0);
            });
            $scope.$on('mdt.join.failed', function (event, path) {
                loadingServices.hide();
            });

            // view healthrecords
            $scope.$on('hr.open.done', function (event, path) {
                $scope.hrPath = path;

                $timeout(function () {
                    document.getElementById('healthrecordlink').click();
                    loadingServices.hide();
                }, 0);

                $scope.onLeaveViewHealthrecord();
            });
            $scope.$on('hr.open.failed', function (event, path) {
                loadingServices.hide();
                $scope.onLeaveViewHealthrecord();
            });

            // favorites
            $scope.$on('favorite.remove.done', retrievePatients);
            $scope.$on('favorite.add.done', retrievePatients);
            $scope.$on('favorite.remove.failed', commonService.progress.hide);
            $scope.$on('favorite.add.failed', commonService.progress.hide);

            $scope.$on('$destroy', function () {
                // clean up.
                $rootScope.addPatient = null;
            });
            /*---------end--------*/

            /*-------private------*/
            function init() {
                loadingServices.show();

                // get all patients in current mdt.
                mdtServices.patientsList(param);
            }

            function editPatientCallback(patient) {
                loadingServices.show();
                // update 'reason' filed for patient.
                mdtServices.addOrUpdatePatient({
                    reason: patient.reason
                }, {
                    mdtid: mdtId,
                    patientId: patient.id
                });
            }

            function deletePatientCallback(patient) {
                loadingServices.show();
                mdtServices.deletePatient({
                    mdtid: mdtId,
                    patientId: patient.id
                });
            }

            // retrieve all patients again from db to get the new patient we added.
            function addPatientCallback(data) {
                loadingServices.show();
                // update 'reason' filed for patient.
                mdtServices.addOrUpdatePatient({
                    reason: data.reason
                }, {
                    mdtid: mdtId,
                    patientId: data.id
                });
            }

            function addorEditPatientCallback(data) {
                $scope.activePatientTab.patient = data;
            }

            function addorEditMaterialCallback(data) {
                loadingServices.show();
                mdtServices.addHealthrecords({
                    ids: data.ids
                }, {
                    mdtid: mdtId
                });
            }

            // callback for closing/dismissing comment modal.
            function addOrReplyCallback(data) {
                getComments($scope.activePatientTab.patient.id, $scope.lastCommentsCount);
            }

            function retrievePatients() {
                // get all patient in current mdt.
                mdtServices.patientsList(param);
            }

            function retrieveHealthrecords() {
                var patient = $scope.activePatientTab.patient;
                mdtServices.healthrecordsList({
                    mdtid: mdtId,
                    patientId: patient.id
                }).then(function (data) {
                    loadingServices.hide();
                    $scope.healthrecords = data;

                    console.log({ 'retrieveHealthrecords': $scope.healthrecords });
                });
            }

            function getPatientsDataFromScope() {
                var data = [];

                if ($scope.patients) {
                    $scope.patients.forEach(function (p) {
                        data.push(p.patient);
                    });
                }

                return data;
            }

            // Retrieve comments by patient id.
            function getComments(patientId, count) {
                $scope.lastCommentsCount = count;
                commentsServices.list({
                    mdtId: mdtId,
                    patientId: patientId,
                    count: count
                });
            }

            function addToFavoriteCallback() {
                // retrievePatients();
            }

            function removeFavoriteCallback() {
                favoritesServices.remove($scope.activePatientTab.patient.id);
            }
            /*---------end--------*/

            // init
            init();
        }]);
})();