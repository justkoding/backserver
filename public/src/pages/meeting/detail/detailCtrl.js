﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('DetailMeetingCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        '$timeout',
        'commonService',
        'clientServices',
        'mdtServices',
        'modalService',
        'commentsServices',
        'announcementsServices',
        'strings',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            $timeout,
            commonService,
            clientServices,
            mdtServices,
            modalService,
            commentsServices,
            announcementsServices,
            strings) {

            $scope.permissions = strings.permissions;

            var mdtId = $stateParams.id;
            var param = {
                doctorship: 0,
                mdtid: mdtId
            };
            var loadingCount = 2;
                        
            $scope.user = commonService.getCurrentUser();

            /**
             * back to meeting list page
             */
            $scope.back = function () {
                commonService.history.back();
            };

            /*--------meeting----------*/
            /**
             * edit meeting basic info
             */
            $scope.edit = function (meeting) {
                if (meeting && meeting.id) {
                    $state.go('page.meeting.edit', {
                        id: meeting.id
                    });
                }
            };

            $scope.delete = function (meeting) {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/confirm/confirm.html',
                   {
                       controller: 'ConfirmCtrl',
                       data: {
                           title: 'COMMON_DELETE_CONFIRM_TITLE',
                           body: 'COMMON_DELETE_CONFIRM_BODY'
                       },
                       closefn: deleteMDTCallback
                   });
            };
            /*--------end----------*/

            /*--------patients----------*/
            $scope.onPatient = function (history) {
                // temp store in session, will be used in meeting patients.
                commonService.session.set('meetingHistories', $scope.mdt.histories);
                commonService.session.set('meetingAttendees', $scope.mdt.attendees);

                $state.go('page.meeting.patients', {
                    mid: mdtId, // meeting id.
                    hid: history.id, // patient id,
                    pid: history.patient.id
                });
            };
            /*--------end----------*/

            /*--------countdown----------*/
            /*--------countdown----------*/
            $scope.showCounddown = false;
            $scope.showMeetingStart = false;
            $scope.showExpired = false;

            $scope.countdownCompleted = function(){
                $scope.showCounddown = false;
                if($scope.mdt.status == 1){
                    $scope.showMeetingStart = true;
                }
                $scope.$digest();
            };
            /*----------end----------*/

            /*----------launch pc client---------------*/
            // launch pc client to join meeting.
            $scope.meeting = '';
            $scope.startOnlineMeeting = function () {
                commonService.progress.show();
                clientServices.meeting($scope.user.token, parseInt(mdtId));
            };
            /*----------end pc client---------------*/

            /*-----------announcement----------*/
            $scope.publishAnnouncement = function () {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/meeting/modals/announcement/announcement.html',
                     {
                         controller: 'AnnouncementCtrl',
                         data: {
                             mdtId: mdtId
                         },
                         closefn: addEditAnnouncementCallback
                     });
            };

            $scope.editAnnouncement = function (announcement) {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/meeting/modals/announcement/announcement.html',
                     {
                         controller: 'AnnouncementCtrl',
                         data: {
                             mdtId: mdtId,
                             announcement: announcement
                         },
                         closefn: addEditAnnouncementCallback
                     });
            };

            $scope.deleteAnnouncement = function (announcement) {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/confirm/confirm.html',
                   {
                       controller: 'ConfirmCtrl',
                       data: {
                           title: 'COMMON_DELETE_CONFIRM_TITLE',
                           body: 'COMMON_DELETE_CONFIRM_BODY'
                       },
                       closefn: function () {
                           deleteAnnouncementCallback(announcement);
                       }
                   });
            };
            /*-------end------*/

            /*-------event------*/
            // join/start mdt
            $scope.$on('mdt.join.done', function (event, path) {
                $scope.meeting = path;

                $timeout(function () {
                    document.getElementById('meeting').click();
                    commonService.progress.hide();
                }, 0);
            });
            $scope.$on('mdt.join.failed', commonService.progress.hide);

            $scope.$on('mdt.detail.done', function (event, result) {
                $scope.mdt = result;
                console.log($scope.mdt);
                // mdt  countdown
                //1-表示会议opened，2-表示会议closed
                if($scope.mdt.status == 1){
                    if(commonService.compare($scope.mdt.end, new Date() )> 0){
                        if(commonService.compare($scope.mdt.start, new Date() )> 0){
                            $scope.showCounddown = true;
                            $scope.showMeetingStart = false;
                            $scope.showExpired = false;
                        }else {
                            $scope.showCounddown = false;
                            $scope.showMeetingStart = true;
                            $scope.showExpired = false;
                        }
                    }else{
                        $scope.showCounddown = false;
                        $scope.showMeetingStart = false;
                        $scope.showExpired = true;
                    }
                }

                if (--loadingCount == 0) {
                    commonService.progress.hide();
                }
            });
            $scope.$on('mdt.detail.failed', function (event, result) {
                if (--loadingCount == 0) {
                    commonService.progress.hide();
                }
            });

            $scope.$on('mdt.remove.done', function (event, result) {
                commonService.progress.hide();
                $state.go('page.meeting');
            });
           
            // announcements
            $scope.$on('announcement.list.done', function (event, result) {
                $scope.announcements = result.data;
                if (--loadingCount == 0) {
                    commonService.progress.hide();
                }
            });

            $scope.$on('announcement.list.failed', function (event, result) {                
                if (--loadingCount == 0) {
                    commonService.progress.hide();
                }
            });

            $scope.$on('announcement.remove.done', function (event, result) {
                announcementsServices.list({ mdtId: mdtId });
            });
            /*---------end--------*/

            /*-------private------*/
            function init() {
                commonService.progress.show();

                // get mdt information.
                mdtServices.detail(mdtId);
               
                // get all announcements.
                $scope.announcements = [];
                announcementsServices.list({ mdtId: mdtId });
            }

            function deleteMDTCallback() {
                commonService.progress.show();
                mdtServices.remove(mdtId);
            }

            function getLatestComments(count) {
                commentsServices.list({
                    mdtId: mdtId,
                    count: count,
                    ordertype: 1 // 1->asc, 0-> dasc
                }).then(function (resp) {
                    $scope.comments = resp.data;
                });
            }

            function addEditAnnouncementCallback() {
                announcementsServices.list({ mdtId: mdtId });
            }

            function deleteAnnouncementCallback(announcement) {
                commonService.progress.hide();
                announcementsServices.remove(announcement.id);
            }
            /*---------end--------*/

            // init
            init();
        }]);
})();