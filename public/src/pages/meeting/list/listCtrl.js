﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('ListMeetingCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        '$location',
        '$timeout',
        'loadingServices',
        'commonService',
        'userServices',
        'mdtServices',
        'clientServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            $location,
            $timeout,
            loadingServices,
            commonService,
            userServices,
            mdtServices,
            clientServices) {

            // current login user.
            var user = commonService.getCurrentUser();

            // search
            var term = $stateParams.search ? $stateParams.search : '';
            var initTab = commonService.session.get('initMeetingTab') ? commonService.session.get('initMeetingTab') : null;
            commonService.session.reset('initMeetingTab');
            $scope.filter = term;
            $scope.initialStep = true;
            $scope.selections = {
                'ott' : {},
                'doctor' : {},
                'moderator' : {},
                'dept' : {}
            };
            $scope.showSelections = {
                'ott' : false,
                'doctor' : false,
                'dept' : false,
                'moderator' : false
            };
            if (user.account.roles.indexOf('role-doctor') != -1) {
                $scope.showSelections.ott = true;
                $scope.showSelections.dept = true;
                $scope.showSelections.doctor = true;
            }
            if (user.account.roles.indexOf('role-moderator') != -1) {
                $scope.showSelections.ott = true;
                $scope.showSelections.dept = true;
                $scope.showSelections.moderator = true;
            }
            if (user.account.roles.indexOf('role-for-admin-the-system') != -1) {
                $scope.showSelections.ott = true;
                $scope.showSelections.dept = true;
                $scope.showSelections.doctor = true;
                $scope.showSelections.moderator = true;
            }
            // Doctor list
            $scope.doctorList = [];
            // Moderator list
            $scope.moderatorList = [];
            var userParams = {
                'size' : 50
            };
            // Department list.
            $scope.deptList = [];

            // pagination
            $scope.paging = {
                totalItems : 0,
                itemsPerPage : 10,
                currentPage : 1,
                pageChanged : pageChangeHandle,
                pageSizeChanged : pageChangeHandle
            };

            var params = {
                filter : $scope.filter,
                status : $scope.status,

                // pagination
                size : $scope.paging.itemsPerPage,
                page : $scope.paging.currentPage,

                // date
                datepre : $scope.datepre,
                datepost : $scope.datepost,

                // sort
                creatorder : $scope.creatorder,
                cdateorder : $scope.cdateorder,
                mdateorder : $scope.mdateorder,
                edateorder : $scope.edateorder,

                doctor : $scope.selections.doctor.id,
                moderator : $scope.selections.moderator.id
            };

            userServices.getDeptValues().then(function(data){
                commonService.resource.getValues('MDT_LIST_SELECTION_DEPT').then(function(sources){
                    data.unshift({
                        "id" : "-1",
                        "name" : sources['MDT_LIST_SELECTION_DEPT']
                    });
                    $scope.deptList = data;
                    $scope.selections.dept = $scope.deptList[0];
                    userServices.detail(user.account.accountId);
                });
            });

            // timestamp
            $scope.datepre = '';
            $scope.datepost = '';
            $scope.selections.ott = null; // model for dropdown
            $scope.onChangedTimestamp = function(s){
                retrieveMdtWithTimestampchange(s.id);
            };

            // sort by moderator
            $scope.orderproperty = 0;
            $scope.sortByModerator = function(asc){
                $scope.ordertype = asc ? 1 : 0;
                $scope.orderproperty = 1;
                params.orderproperty = $scope.orderproperty;
                params.ordertype = $scope.ordertype;

                // retrieve mdt data again with new params.
                loadingServices.show();
                mdtServices.list(params);
            };

            // sort by creation time.
            $scope.sortByCreationTime = function(asc){
                $scope.ordertype = asc ? 1 : 0;
                $scope.orderproperty = 2;
                params.orderproperty = $scope.orderproperty;
                params.ordertype = $scope.ordertype;

                // retrieve mdt data again with new params.
                loadingServices.show();
                mdtServices.list(params);
            };

            // sort by meeting start date by default.
            $scope.sortByStartDate = function(asc){
                $scope.ordertype = asc ? 1 : 0;
                $scope.orderproperty = 0;
                params.orderproperty = $scope.orderproperty;
                params.ordertype = $scope.ordertype;

                // retrieve mdt data again with new params.
                loadingServices.show();
                mdtServices.list(params);
            };

            // filter meeting by selections
            $scope.onChangedCondition = function(type, val){
                if (type == 'doctor') {
                    params.doctor = val.id;
                }
                if (type == 'moderator') {
                    params.moderator = val.id;
                }
                // retrieve mdt data again with new params.
                loadingServices.show();
                mdtServices.list(params);
            }

            $scope.onChangedDept = function(val){
                params.dept = val.id;
                userParams['dept'] = params.dept;
                userServices.list(userParams);
            }

            // tabs
            $scope.iNow = 1;
            $scope.tab = function(iTab){
                $scope.iNow = iTab;

                // doctorship: 0-> all, 1-> owner, 2-> attendence
                params.status = --iTab;

                loadingServices.show();
                mdtServices.list(params);
            };

            // detail handler.
            $scope.detail = function(m){
                $state.go('page.meeting.detail', {
                    id : m.id
                });
            };

            // add mdt
            $scope.addMdt = function( ){
                $state.go('page.meeting.add', null, {
                    'reload' : true
                });
            };

            // launch pc client to join meeting.
            $scope.meeting = '';
            $scope.joinMeeting = function(mdt){
                loadingServices.show();
                clientServices.meeting(user.token, parseInt(mdt.id));
            };

            /*------events------*/
            $scope.$on('mdt.list.done', function(event, result){
                loadingServices.hide();
                $scope.mdts = result.data;
                $scope.userId = user.account.accountId;
                $scope.paging.totalItems = result.count;
            });

            $scope.$on('mdt.join.done', function(event, path){
                $scope.meeting = path;

                $timeout(function( ){
                    document.getElementById('meeting').click();
                    loadingServices.hide();
                }, 0);
            });

            $scope.$on('mdt.join.failed', function(event, path){
                loadingServices.hide();
            });

            $scope.$on('user.detail.done', function(event, result){
                $scope.selections.dept = result.dept;
                params.dept = $scope.selections.dept.id;
                userParams['dept'] = params.dept;
                userServices.list(userParams);
            });

            $scope.$on('user.list.done', function(event, result){
                var data = [];

                if (result && result.data && result.data.length != 0) {
                    result.data.forEach(function(item){
                        data.push({
                            'id' : item.id,
                            'name' : item.name
                        });
                    });
                }
                commonService.resource.getValues(['MDT_LIST_SELECTION_DOCTOR', 'MDT_LIST_SELECTION_MODERATOR'])
                        .then(function(sources){
                            data.unshift({
                                "id" : "0",
                                "name" : sources['MDT_LIST_SELECTION_DOCTOR']
                            });
                            if (user.account.roles.indexOf('role-doctor') != -1) {
                                $scope.doctorList = data;
                                $scope.selections.doctor = $scope.doctorList[0];
                                params['doctor'] = $scope.selections.doctor.id;
                                if ($scope.initialStep && user.account.roles.indexOf('role-doctor') != -1) {
                                    $scope.selections.doctor = {
                                        'id' : user.account.userId,
                                        'name' : user.account.userName
                                    };
                                    params['doctor'] = $scope.selections.doctor.id;
                                }
                            }
                            if (user.account.roles.indexOf('role-moderator') != -1) {
                                data.shift();
                                data.unshift({
                                    "id" : "0",
                                    "name" : sources['MDT_LIST_SELECTION_MODERATOR']
                                });
                                $scope.moderatorList = data;
                                $scope.selections.moderator = $scope.moderatorList[0];
                                params['moderator'] = $scope.selections.moderator.id;
                                if ($scope.initialStep && user.account.roles.indexOf('role-doctor') != -1) {
                                    $scope.selections.moderator = {
                                        'id' : user.account.userId,
                                        'name' : user.account.userName
                                    };
                                    params['moderator'] = $scope.selections.moderator.id;
                                }
                            }
                            mdtServices.BuildTimestampDpData(0).then(function(results){
                                $scope.oTimestamps = results;
                                $scope.selections.ott = $scope.oTimestamps[0];
                                retrieveMdtWithTimestampchange($scope.selections.ott.id);
                            });
                            //showPatientsList();
                        });
            });
            /*-------end-----*/

            /*------private------*/
            function pageChangeHandle(item){
                if (item.page != params.page || item.size != params.size) {

                    params.page = item.page;
                    params.size = item.size;

                    loadingServices.show();
                    mdtServices.list(params);
                }
            }

            function retrieveMdtWithTimestampchange(sId){
                var date = mdtServices.calcDateDuration(sId);

                // update scope
                $scope.datepre = date.datepre;
                $scope.datepost = date.datepost;

                // update params
                params.datepre = $scope.datepre;
                params.datepost = $scope.datepost;

                if ($scope.initialStep && initTab) {
                    $scope.initialStep = false;
                    $scope.tab(initTab);
                } else {
                    // retrieve mdt data again with new params.
                    loadingServices.show();
                    mdtServices.list(params);
                }
            }
            /*-------end-----*/
        }]);
})();