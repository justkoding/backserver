﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('AddTempAttendenceCtrl', ['$scope',
        '$timeout',
        '$translate',
        '$cookieStore',
        'messageService',
        'mdtServices',
        'userServices',
        'commonService',
        '$modalInstance',
        'data',
        function ($scope,
            $timeout,
            $translate,
            $cookieStore,
            messageService,
            mdtServices,
            userServices,
            commonService,
            $modalInstance,
            data) {
            commonService.progress.hide();
            var user = commonService.getCurrentUser().account;

            // deep copy
            $scope.data = JSON.parse(JSON.stringify(data));

            // tempAttendees.           
            $scope.tempAttendees = $scope.data.tempAttendees || [];
            $scope.attendence = {};

            // tab           
            var iNow = 0;
            $scope.tabs = [];
            $scope.onTab = function (item) {
                var oIndex = item.originalIndex,
                    aIndex = item.activeIndex;

                iNow = aIndex;

                $scope.tempAttendees[oIndex] = JSON.parse(JSON.stringify($scope.attendence));
                $scope.attendence = JSON.parse(JSON.stringify($scope.tempAttendees[aIndex]));
            };
            $scope.onRemoved = function (item) {
                var oIndex = item.originalIndex,
                    aIndex = item.activeIndex;

                iNow = aIndex;

                $scope.tempAttendees.splice(oIndex, 1);
                $scope.attendence = JSON.parse(JSON.stringify($scope.tempAttendees[aIndex]));
            };

            $scope.onAdd = function (item) {
                var oIndex = item.originalIndex,
                    aIndex = item.activeIndex,
                    form = item.form;

                iNow = aIndex;

                $scope.tempAttendees[oIndex] = JSON.parse(JSON.stringify($scope.attendence));
                $scope.attendence = {
                    name: '',
                    phone: null,
                    email: null
                };
                $scope.tempAttendees.push($scope.attendence);

                // reset form validate.
                form.name.$setUntouched();
                form.phone.$setUntouched();
                form.email.$setUntouched();
            };

            // modal.
            $scope.ok = function () {
                commonService.progress.show();
                var attendees = JSON.parse(JSON.stringify($scope.tempAttendees));

                // add creator for each attendence.
                attendees = attendees.map(function (a) {
                    a.creator = {
                        id: user.accountId,
                        name: user.accountName
                    };

                    a.name = a.userName;
                    delete a.userName;
                    return a;
                });

                mdtServices.addTempattendees(attendees).then(function (result) {
                    $modalInstance.close({
                        attendees: result.data
                    });

                    commonService.progress.hide();
                });
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            /*--------events------*/
            $scope.$watch('attendence.userName', function (newVal, oldVal) {
                if (newVal) {
                    $scope.tabs[iNow].name = newVal;
                }
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });
            /*--------end------*/

            /*--------private------*/
            function init() {
                if ($scope.tempAttendees && $scope.tempAttendees.length != 0) {
                    $scope.tempAttendees.map(function (v) {
                        v.userName = v.name;
                        return v;
                    });
                }
                setupTabsData();
            }

            function setupTabsData() {
                if ($scope.tempAttendees && $scope.tempAttendees.length != 0) {
                    $scope.tempAttendees.forEach(function (ta, i) {
                        var active = i === 0 ? true : false;

                        $scope.tabs.push({
                            name: ta.name,
                            active: active,
                            removed: true
                        });
                    });

                    $scope.attendence = JSON.parse(JSON.stringify($scope.tempAttendees[0]));
                } else {
                    $scope.tempAttendees.push($scope.attendence);
                }
            }

            /*--------end------*/

            // init
            init();
        }]);
})();