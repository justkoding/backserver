(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('AddPendingPatientsCtrl', ['$scope',
        '$rootScope',
        '$translate',
        'userServices',
        '$modalInstance',
        'commonService',
        'moderatorServices',
        'data',
        function ($scope,
                  $rootScope,
                  $translate,
                  userServices,
                  $modalInstance,
                  commonService,
                  moderatorServices,
                  data) {

            commonService.progress.hide();

            // deep copy
            var data = JSON.parse(JSON.stringify(data));

            // selected pending patients.
            $scope.pendingPatients = data.pendingPatients || [];
            $scope.isDisabledPatients = data.isDisabledPatients;
            $scope.tempPatients = [];

            /*----Add existing patients section-----*/
            var patientParam = {
                filter: '',
                doctor: 0,
                dept: 0,
                size: 200, // big number to get all data.
                page: 1
            };

            init();

            /*----------end------------*/
            /**
             * selected/unselected patient.
             */
            $scope.onselected = function (p, $index) {
                var index = commonService.getIndexOf(p, $scope.pendingPatients);

                if (p.checked) {
                    if (index != -1) {
                        $scope.pendingPatients.splice(index, 1);
                    }
                } else {
                    if (index == -1) {
                        $scope.pendingPatients.push(p);
                    }
                }

                $scope.patients[$index].checked = !($scope.patients[$index].checked);
            };

            /**
             * remove patient from pending patients list.
             */
            $scope.removePatient = function (p, $index) {
                $scope.pendingPatients.splice($index, 1);

                var index = commonService.getIndexOf(p, $scope.patients);
                if (index != -1) {
                    $scope.patients[index].checked = false;
                }
            };

            // modal.
            // handler for searching existing patients.
            $scope.save = function () {
                var pendingPatients = JSON.parse(JSON.stringify($scope.pendingPatients));

                // combine tempPatients
                if ($scope.isDisabledPatients && $scope.tempPatients && $scope.tempPatients.length != 0) {
                    pendingPatients = pendingPatients.concat($scope.tempPatients);
                }

                $modalInstance.close({
                    pendingPatients: pendingPatients
                });
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
            /*----------end------------*/

            /*----------events------------*/
            $scope.$on('moderator.list.done', function (event, results) {
                if (results &&
                    results.length != 0) {
                    // copy
                    results = JSON.parse(JSON.stringify(results));

                    if ($scope.isDisabledPatients) {
                        results.forEach(function (v, i) {
                            var index = commonService.getIndexOf(v, $scope.tempPatients);

                            if (index != -1) {
                                results.splice(i, 1);
                            }
                        });
                    } else {
                        if ($scope.pendingPatients && $scope.pendingPatients.length != 0) {
                            $scope.pendingPatients.forEach(function (v) {
                                results.map(function (p) {
                                    if (v.id === p.id) {
                                        p.checked = true;
                                    }

                                    return p;
                                });
                            });
                        }
                    }
                }
                $scope.patients = results;
            });

            /*------------end------------*/

            /*-----------private method-----------*/
            function init() {
                if ($scope.isDisabledPatients && $scope.pendingPatients && $scope.pendingPatients.length != 0) {
                    $scope.tempPatients = JSON.parse(JSON.stringify($scope.pendingPatients));
                    $scope.pendingPatients = [];
                }
                moderatorServices.list(patientParam);
            }
            /*--------------end-------------------*/
        }]);
})();