﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('AddPatientsCtrl', ['$scope',
        '$rootScope',
        '$translate',
        'userServices',
        '$modalInstance',
        '$timeout',
        'loadingServices',
        'commonService',
        'patientServices',
        'messageService',
        'data',
        function ($scope,
            $rootScope,
            $translate,
            userServices,
            $modalInstance,
            $timeout,
            loadingServices,
            commonService,
            patientServices,
            messageService,
            data) {

            loadingServices.hide();
            // deep copy
            var data = JSON.parse(JSON.stringify(data));

            // new variable to mark whether it is create a new patient or
            // search existing patient.
            $scope.isNewPatient = true;

            // array for existing patients in current mdt
            $scope.existingPatients = data.existingPatients || [];

            // tab
            $scope.iNow = 1;
            $scope.tab = function (iTab) {
                $scope.iNow = iTab;

                if (iTab === 1) {
                    $scope.isNewPatient = true;
                } else {
                    $scope.isNewPatient = false;
                }
            };

            // default value
            $scope.patient = data.patient || {};
            $scope.patient.reason = data.reason;
            $scope.isShowExtrPanel = false;
            $scope.patient.sex = $scope.patient.sex || 1; // 1 - man

            // datepicker
            //max date
            $scope.maxDate = new Date();
            var bt = $scope.patient.birthday ? moment($scope.patient.birthday).local() : new Date();
            $scope.patient.birthday = moment(bt).format('YYYY-MM-DD');
            $scope.opened = false;
            $scope.open = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.opened = !$scope.opened;
            };

            /*----Add existing patients section-----*/
            var patientParam = {
                page: 1,
                size: 50,
                dept: -1,
                filter: ''
            };
            $scope.existingPatient = {
                filter: '',
                reason: ''
            };
            $scope.iRadio = 1;
            $scope.onRadioClick = function (value) {
                $scope.iRadio = value;

                // by dept
                if (value === 1) {
                    patientParam.dept = $scope.existingPatient.selected.id;
                    patientParam.filter = '';
                } else {
                    patientParam.filter = $scope.existingPatient.filter;
                    patientParam.dept = -1;
                }

                loadingServices.show();
                patientServices.list(patientParam);
            };

            $scope.existingPatientDeptChanged = function () {
                patientParam.dept = $scope.existingPatient.selected.id;
                patientParam.filter = '';

                loadingServices.show();
                patientServices.list(patientParam);
            };

            var timer = null;
            $scope.onSearch = function () {
                $timeout.cancel(timer);
                timer = $timeout(function () {
                    patientParam.filter = $scope.existingPatient.filter;
                    patientParam.dept = -1;

                    loadingServices.show();
                    patientServices.list(patientParam);
                }, 300);
            };

            $scope.selectedPatientInfo = null;
            $scope.onSelectedPatient = function (index) {
                $scope.selectedPatientInfo = $scope.patientsInfo[index];

            };
            /*----------end------------*/

            // modal.      
            // handler for creating a new patient.
            $scope.ok = function () {
                // only support utc format in server.
                $scope.patient.birthday = moment($scope.patient.birthday).toISOString();

                var p = JSON.parse(JSON.stringify($scope.patient));

                // delete custom filed in post data to avoid parse error in server.
                delete p.reason;

                p.creator = {
                    id: data.userId
                };

                loadingServices.show();
                patientServices.add(p);
            };

            // handler for searching existing patients.
            $scope.save = function () {
                var data = {
                    reason: $scope.existingPatient.reason,
                    id: $scope.selectedPatientInfo.id,
                    patientInfo: $scope.selectedPatientInfo

                };

                $modalInstance.close(data);
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
                console.log($scope.patient);
            };

            // init
            loadingServices.show();
            userServices.getDeptValues().then(function (depts) {
                $scope.depts = depts;

                setDept();

                // set first one to selected for second tab.
                $scope.existingPatient.depts = depts;
                $scope.existingPatient.selected = depts[0];

                patientParam.dept = $scope.existingPatient.selected.id;
                patientServices.list(patientParam);
            });

            /*----------events------------*/
            $scope.$on('patient.add.done', function (event, results) {
                var data = {
                    reason: $scope.patient.reason,
                    id: results.ptid
                };

                loadingServices.hide();

                $modalInstance.close(data);
            });

            $scope.$on('patient.add.failed', function (event, results) {
                loadingServices.hide();
            });

            $scope.$on('patient.update.done', function (event, results) {
                loadingServices.hide();
                $modalInstance.close($scope.patient);
            });

            $scope.$on('patient.list.done', function (event, results) {
                loadingServices.hide();
                if ($scope.existingPatients.length != 0 && results.data) {
                    $scope.existingPatients.forEach(function (ep) {
                        results.data.forEach(function (p, i) {
                            if (ep.id === p.id) {
                                results.data.splice(i, 1);
                            }
                        });
                    });
                }

                $scope.patientsInfo = results.data;
            });

            /*------------end------------*/

            /*-----------private method-----------*/
            function setDept() {
                var index = commonService.getIndexOf($scope.patient.dept, $scope.depts);

                index = index == -1 ? 0 : index;
                $scope.patient.dept = $scope.depts[index];
            }
            /*--------------end-------------------*/
        }]);
})();