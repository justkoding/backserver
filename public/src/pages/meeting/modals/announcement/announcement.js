﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('AnnouncementCtrl', ['$scope',
        '$timeout',
        '$translate',
        '$cookieStore',
        'loadingServices',
        'messageService',       
        'commonService',
        '$modalInstance',
        'data',
        'announcementsServices',
        function ($scope,
            $timeout,
            $translate,
            $cookieStore,
            loadingServices,
            messageService,           
            commonService,
            $modalInstance,
            data,
            announcementsServices) {
            loadingServices.hide();
          
            // deep copy
            $scope.data = JSON.parse(JSON.stringify(data));                      

            // parse parma from data.
            var mdtId = $scope.data.mdtId;

            $scope.isEdit = $scope.data.announcement != null;
            $scope.announcement = $scope.isEdit ? $scope.data.announcement : {};
                       
            // modal.
            $scope.ok = function () {
                loadingServices.show();
                var data = JSON.parse(JSON.stringify($scope.announcement));

                // call service to add/edit announcement to server.
                if ($scope.isEdit) {
                    delete data.focus;
                    announcementsServices.update(data);
                } else {
                    data.mdtId = mdtId;
                    announcementsServices.add(data);
                }                
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');              
            };

            /*--------events------*/
            $scope.$on('announcement.add.done', function () {
                loadingServices.hide();
                $modalInstance.close();
            });

            $scope.$on('announcement.update.done', function () {
                loadingServices.hide();
                $modalInstance.close();
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });
            /*--------end------*/

            /*--------private------*/
            function init() {               
            } 
            /*--------end------*/

            // init
            init();
        }]);
})();