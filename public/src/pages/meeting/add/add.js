﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('AddMeetingCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        'commonService',
        'modalService',
        'mdtServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            commonService,
            modalService,
            mdtServices) {

            var endMeetingTimes = commonService.buildFullTimeDataSource();
            var meetingId = $stateParams.id || -1;
            var date = $stateParams.date;

            // mark whether it is editing existing meeting.
            $scope.isEdit = meetingId != -1;

            // back
            $scope.back = function () {
                commonService.history.back();                
            };            

            /*--------meeting-----------*/
            $scope.meeting = {
                dpStartDate: null,
                dpStartTime: null,
                dpEndTime: null,
                pendingPatients: commonService.session.get('pendingPatients') || [],
                attendings: getAttendings(), // in charge of doctors.
                attendees: getAttendees(),  // invited doctors
                tempAttendees: getTempAttendees()  // temparary invited doctors
            };

            // start time get changed.
            $scope.onChangedStartTime = function (time) {
                var index = $scope.startMeetingTimes.indexOf(time);

                // update second time select.
                var options = JSON.parse(JSON.stringify(endMeetingTimes));
                options.splice(0, index + 1);

                $scope.endMeetingTimes = options;
                $scope.meeting.dpEndTime = $scope.endMeetingTimes[1];
            };

            // start time get changed.
            $scope.onChangedEndTime = function () {
                // todo
            };

            /**
             * create new meeting.
             */
            $scope.save = function () {
                if ($scope.isEdit) {
                    mdtServices.update($scope.meeting);
                } else {
                    mdtServices.add($scope.meeting);
                }                
            };
            /*----------end---------*/

            /*----------datepicker---------*/
            $scope.startopen = function () {
                $scope.opened = !($scope.opened);
            };
            /*----------end---------*/

            /*----------add attendees/temprary attendees/pending patients---------*/
            /**
             * Invite doctor to current meeting(except attendings doctor).
             */
            $scope.addAttendees = function () {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/meeting/modals/addAttendence/addAttendence.html',
                  {
                      controller: 'AddAttendenceCtrl',
                      data: {
                          doctors: $scope.meeting.attendees.concat($scope.meeting.attendings)
                          //isDisabledAttenance: true
                      },
                      closefn: addAttendenceCallback
                  });
            };

            /**
             * Remove doctor from current meeting(except attendings doctor).
             */
            $scope.removeAttendee = function (index) {
                $scope.meeting.attendees.splice(index, 1);
            };

            /**
             * Invite external doctors to join current meeting.
             */
            $scope.addTempAttendees = function () {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/meeting/modals/addTempAttendence/addTempAttendence.html',
                  {
                      controller: 'AddTempAttendenceCtrl',
                      data: {
                          tempAttendees: $scope.meeting.tempAttendees
                      },
                      closefn: addTempAttendenceCallback
                  });
            };

            /**
             * Remove external doctors from current meeting.
             */
            $scope.removeTempAttendee = function ($index) {
                $scope.meeting.tempAttendees.splice($index, 1);
            };

            /**
             * Add more pending patients to current meeting.
             */
            $scope.addPendingPatients = function () {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/meeting/modals/addPendingPatients/addPendingPatients.html',
                  {
                      controller: 'AddPendingPatientsCtrl',
                      data: {
                          pendingPatients: $scope.meeting.pendingPatients
                          //isDisabledPatients: true
                      },
                      closefn: addPendingPatientsCallback
                  });
            };

            /**
             * Remove pending patients from current meeting.
             */
            $scope.removePendingPatient = function (pending, $index) {
                $scope.meeting.pendingPatients.splice($index, 1);
                updateChargeOfDoctors($scope.meeting.pendingPatients);
            };

            /*----------end---------*/

            /*--------datepicker-----------*/
            $scope.opened = false;
            $scope.startopen = function () {
                $scope.opened = !($scope.opened);
            };
            /*----------end---------*/

            init();

            /*--------private-----------*/
            function init() {
                // edit existing meeting
                if ($scope.isEdit) {
                    // get mdt information.
                    mdtServices.detail(meetingId);
                } else { // create new meeting.
                    $scope.startMeetingTimes = commonService.buildFullTimeDataSource();
                    $scope.meeting.dpStartTime = getDefaultMeetingStartTimeOption();
                    endMeetingTimes = commonService.buildFullTimeDataSource();

                    // do deeply copy
                    $scope.endMeetingTimes = JSON.parse(JSON.stringify(endMeetingTimes));
                    $scope.onChangedStartTime($scope.meeting.dpStartTime);

                    // date                    
                    $scope.meeting.dpStartDate = commonService.date.format(date ? new Date(parseInt(date)) : new Date(), 'YYYY/MM/DD');
                }
            }

            /**
             * Get attendings doctor list by pending patients
             */
            function getAttendings(patients) {
                var pendingPatients = patients || commonService.session.get('pendingPatients') || [];
                var attendings = [];

                if (pendingPatients && pendingPatients.length != 0) {
                    pendingPatients.forEach(function (v) {
                        // avoid add duplicate.
                        var index = commonService.getIndexOf(v.doctor, attendings);
                        if (index === -1) {
                            attendings.push(v.doctor);
                        }                        
                    });
                }
                return attendings;
            }

            /**
             * Get all attendees except attending doctors.
             */
            function getAttendees(doctors, patients) {
                var arr = [];
                if (doctors && doctors.length != 0) {
                    arr = JSON.parse(JSON.stringify(doctors));

                    patients.forEach(function (p) {
                        doctors.forEach(function (d, i) {
                            if (p.doctor.id === d.id) {
                                arr.splice(i, 1);
                            }
                        });
                    });
                }

                return arr;
            }

            /**
             * Get temparary attendees.
             */
            function getTempAttendees() {
                // todo
                return [];
            }

            /**
             * invoke after closed add attendees modal.
             */
            function addAttendenceCallback(ass) {
                // except in charge of doctors.
                if (ass && ass.length != 0) {
                    ass.forEach(function (v) {
                        v.userName = v.name;
                        v.name = v.accountName;
                        delete v.accountName;
                        $scope.meeting.attendees.push(v);
                    });
                }
            }

            /**
            * invoke after closed add temparary attendees modal.
            */
            function addTempAttendenceCallback(data) {
                $scope.meeting.tempAttendees = data.attendees;
            }

            /**
            * invoke after closed add pending patients modal.
            */
            function addPendingPatientsCallback(data) {
                $scope.meeting.pendingPatients = data.pendingPatients;
                updateChargeOfDoctors(data.pendingPatients);
            };

            /**
             * Get in charge of doctors by pending patients info.
             */
            function updateChargeOfDoctors(patients) {
                // reset
                $scope.meeting.attendings = [];

                // updated in charge of doctors.
                if (patients && patients.length != 0) {
                    patients.forEach(function (v) {
                        if ($scope.meeting.attendings.length === 0) {
                            $scope.meeting.attendings.push(v.doctor);
                        } else {
                            var index = commonService.getIndexOf(v.doctor, $scope.meeting.attendings);
                            if (index == -1) {
                                $scope.meeting.attendings.push(v.doctor);
                            }
                        }
                    });
                }

                // remove duplicate doctor in attendees and attendings. only keep one in attendings array.
                if ($scope.meeting.attendings.length != 0 && $scope.meeting.attendees.length != 0) {
                    $scope.meeting.attendings.forEach(function (v) {
                        var index = commonService.getIndexOf(v, $scope.meeting.attendees);
                        if (index != -1) {
                            $scope.meeting.attendees.splice(index, 1);
                        }
                    });
                }
            }

            /**
             * return the index of special time in options array.
             */
            function indexOfOptions(time, options) {
                var index = -1;

                if (time && options && options.length != 0) {
                    var newTime,
                        times = time.split(':');

                    if (times[0].length == 1) {
                        times[0] = '0' + times[0];
                    }

                    newTime = times.join(':');

                    for (var i = 0; i < options.length; i++) {
                        if (options[i].value === newTime) {
                            index = i;
                            break;
                        }
                    }
                }

                return index;
            }
            
            /**
             * get default meeting start time
             */
            function getDefaultMeetingStartTimeOption(){
            	var time;
                var now = new Date();
                var hour = now.getHours(),
                    min = now.getMinutes();
                
                if(min < 30){
                	time = (hour < 10 ? '0' + hour : hour) + ':' + '30';
                }else{
                	var i = hour + 1;
                	time = (i < 10 ? '0' + i : i) + ':' + '00';
                }
                return $scope.startMeetingTimes[indexOfOptions(time,$scope.startMeetingTimes)];
            }
            /*----------end---------*/

            /*--------events-----------*/
            $scope.$on('mdt.add.done', function () {
                // reset
                commonService.session.reset('pendingPatients');
                $scope.back();
                commonService.progress.hide();
            });
            $scope.$on('mdt.add.failed', commonService.progress.hide);

            $scope.$on('mdt.update.done', function () {
                // reset
                commonService.session.reset('pendingPatients');
                $scope.back();
                commonService.progress.hide();
            });
            $scope.$on('mdt.update.failed', commonService.progress.hide);

            $scope.$on('mdt.detail.done', function (event, result) {
                $scope.meeting = result;               

                // pending patients.
                $scope.meeting.pendingPatients = $scope.isEdit ? result.histories : commonService.session.get('pendingPatients') ;

                // in charge of doctors.
                $scope.meeting.attendings = getAttendings($scope.meeting.pendingPatients);

                // invited doctors
                $scope.meeting.attendees = getAttendees(result.attendees, $scope.meeting.pendingPatients);

                // init start
                $scope.meeting.dpStartDate = commonService.date.format(result.start, 'YYYY/MM/DD');

                // init start time
                $scope.startMeetingTimes = commonService.buildFullTimeDataSource();
                var startTime = commonService.date.format(result.start, 'HH:mm');
                var index = indexOfOptions(startTime, $scope.startMeetingTimes);
                if(index != -1){
                    $scope.meeting.dpStartTime = $scope.startMeetingTimes[index];
                } 

                // init end time  
                // update second time select.
                var options = JSON.parse(JSON.stringify(endMeetingTimes));
                if (index != -1) {
                    options.splice(0, index + 1);
                }
                $scope.endMeetingTimes = options;
                var endTime = commonService.date.format(result.end, 'HH:mm');
                var index = indexOfOptions(endTime, $scope.endMeetingTimes);
                if(index != -1){
                    $scope.meeting.dpEndTime = $scope.endMeetingTimes[index];
                }
               
                commonService.progress.hide();               
            });
            $scope.$on('mdt.detail.failed', commonService.progress.hide);

            $scope.$on('$destroy', function () {
                // clean up.
            });

            /*----------end---------*/
        }]);
})();