﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('MdtMaterialCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        '$timeout',
        'loadingServices',
        'commonService',
        'modalService',
        'mdtServices',
        'healthrecordServices',
        'clientServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            $timeout,
            loadingServices,
            commonService,
            modalService,
            mdtServices,
            healthrecordServices,
            clientServices) {

            var mdtId = $scope.mdtid || $stateParams.id;
            var user = commonService.getCurrentUser();

            var param = {
                doctorship: 0,
                mdtid: mdtId
            };

            // doctorship select
            $scope.doctorshipChanged = function () {
                loadingServices.show();

                param.doctorship = $scope.ds.id;

                // get all patients with the new query string.
                mdtServices.patientsList(param);
            };

            $scope.addPatient = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/meeting/addPatients/addPatients.html',
                 {
                     controller: 'AddPatientsCtrl',
                     data: {
                         patient: {
                             sex: 1 // set man as default.
                         },
                         existingPatients: getPatientsDataFromScope(),
                         userId: user.account.accountId
                     },
                     closefn: addPatientCallback
                 });
            };

            // will be used in step#2 when creating a new mdt.
            $rootScope.addPatient = $scope.addPatient;

            $scope.addPatientMaterial = function (data) {
                loadingServices.show();

                // get all healthrecords list.
                healthrecordServices.list(data.patient.id).then(function (hrs) {
                    loadingServices.hide();

                    // launched modal by passing healthrecords.
                    loadingServices.show();
                    modalService.showModal('/assets/src/pages/patients/material/addNewMaterialOrFromLibrary.html',
                      {
                          controller: 'AddPatientMaterialCtrl',
                          data: {
                              patientId: data.patient.id,
                              healthrecords: hrs
                          },
                          closefn: addorEditMaterialCallback
                      });
                });
            };

            $scope.view = function (hr, p) {
                loadingServices.show();

                var patientId = p.patient ? parseInt(p.patient.id) : -1;                
                clientServices.healthrecord({
                    ticket: user.token,
                    healthrecordId: hr.id,
                    healthrecordType: hr.type,
                    patientId: patientId
                });
            };

            // xg-edit-trash: edit and trash handler.
            $scope.edit = function (data) {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/patients/addOrEdit/addOrEdit.html',
                   {
                       controller: 'AddorEditPatientCtrl',
                       data: {
                           fromMdt: true,
                           patient: data.patient,
                           reason: data.reason,
                           isEdit: true
                       },
                       closefn: editPatientCallback
                   });
            };

            $scope.delete = function (data) {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/confirm/confirm.html',
                  {
                      controller: 'ConfirmCtrl',
                      data: {
                          title: 'PATIENT_DELETE_CONFIRM_TITLE',
                          body: 'PATIENT_DELETE_CONFIRM_BODY'
                      },
                      closefn: function () {
                          deletePatientCallback(data.patient);
                      }
                  });
            };

            // xgTrash
            $scope.deleteMaterial = function (hrId) {
                loadingServices.show();
                mdtServices.deleteHealthrecord({
                    mdtid: mdtId,
                    hrdId: hrId
                });
            };

            // xg-panination
            $scope.totalItems = 0;
            $scope.itemsPerPage = 5;
            $scope.currentPage = 1;

            param.size = $scope.itemsPerPage;
            param.page = $scope.currentPage;

            $scope.pageChanged = function (item) {
                if (item.page != param.page) {
                    param.page = item.page;
                    param.size = item.size;
                    loadingServices.show();
                    mdtServices.patientsList(param);
                }
            };

            $scope.pageSizeChanged = function (item) {
                if (item.size != param.size) {
                    param.page = item.page;
                    param.size = item.size;
                    loadingServices.show();
                    mdtServices.patientsList(param);
                }
            };

            $scope.afterSave = function(item){

                var option = {},
                    data = {};

                data.note = item.description;
                option.id =item.id;
                option.data = data;

                healthrecordServices.update(option);
            };

            // scroll.
            $scope.needScroll = false;

            /*-------event------*/
            $scope.$on('mdt.patients.list.done', function (event, resp) {
                loadingServices.hide();
                // get healthrecords for each patient
                if (resp.data && resp.data.length != 0) {
                    resp.data = resp.data.map(function (p) {
                        p.healthrecords = [];

                        mdtServices.healthrecordsList({
                            mdtid: mdtId,
                            patientId: p.patient.id
                        }).then(function (data) {
                            p.healthrecords = data;
                        });

                        return p;
                    });
                }

                $scope.totalItems = resp.count;
                $scope.patients = resp.data;
            });

            $scope.$on('hr.open.done', function (event, path) {
                $scope.hrPath = path;

                $timeout(function () {
                    document.getElementById('healthrecordlink').click();
                    loadingServices.hide();
                }, 0);
            });

            $scope.$on('hr.open.failed', function (event, path) {
                loadingServices.hide();
            });

            // delete patient from mdt
            $scope.$on('mdt.patient.delete.done', retrievePatients);
            $scope.$on('mdt.patient.addOrUpdate.done', retrievePatients);
            $scope.$on('mdt.healthrecords.add.done', retrievePatients);
            $scope.$on('mdt.healthrecord.delete.done', retrievePatients);

            $scope.$on('$destroy', function () {
                // clean up.
                $rootScope.addPatient = null;
            });

            $scope.$on('scroll.hit.bottom', function () {
                //alert('scroll.hit.bottom');
            });
            $scope.$on('scroll.hit.top', function () {
                //alert('scroll.hit.top');
            });

            // gotoTop diretive, will be called once scroll is done.
            $scope.$on('scroll.done', function () {
                $scope.needScroll = false;
            });

            // happen when try to scroll.
            $scope.$on('scroll.progress', function (event, data) {
                $scope.needScroll = data.scrollTop > 100;
            });
            /*---------end--------*/

            /*-------private------*/
            function init() {
                loadingServices.show();

                // get all patient in current mdt.
                mdtServices.patientsList(param);

                // setup doctorship dropdown list datasource.
                mdtServices.BuildDoctorShipDpData().then(function (data) {
                    $scope.doctorships = data;
                    // set first item to select by default.
                    $scope.ds = $scope.doctorships[0];
                });
            }

            function editPatientCallback(patient) {
                loadingServices.show();
                // update 'reason' filed for patient.
                mdtServices.addOrUpdatePatient({
                    reason: patient.reason
                }, {
                    mdtid: mdtId,
                    patientId: patient.id
                });
            }

            function deletePatientCallback(patient) {
                loadingServices.show();
                mdtServices.deletePatient({
                    mdtid: mdtId,
                    patientId: patient.id
                });
            }

            // retrieve all patients again from db to get the new patient we added.
            function addPatientCallback(data) {
                loadingServices.show();
                // update 'reason' filed for patient.
                mdtServices.addOrUpdatePatient({
                    reason: data.reason
                }, {
                    mdtid: mdtId,
                    patientId: data.id
                });
            }

            function addorEditMaterialCallback(data) {
                loadingServices.show();
                mdtServices.addHealthrecords({
                    ids: data.ids
                }, {
                    mdtid: mdtId
                });
            }

            function retrievePatients() {
                // get all patient in current mdt.
                mdtServices.patientsList(param);
            }

            function getPatientsDataFromScope() {
                var data = [];

                if ($scope.patients) {
                    $scope.patients.forEach(function (p) {
                        data.push(p.patient);
                    });
                }

                return data;
            }
            /*---------end--------*/

            // init
            init();
        }]);
})();