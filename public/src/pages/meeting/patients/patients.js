﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('MeetingPatientsCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        'commonService',
        'strings',
        'historiesServices',
        'healthrecordServices',
        'websocketServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            commonService,
            strings,
            historiesServices,
            healthrecordServices,
            websocketServices) {
            var requestNum = 2;
            var user = commonService.getCurrentUser();

            // get current history Id.
            var meetingId = $stateParams.mid ? parseInt($stateParams.mid) : -1;
            var historyId = $stateParams.hid ? parseInt($stateParams.hid) : -1;
            var patientId = $stateParams.pid ? parseInt($stateParams.pid) : -1;

            $scope.meetingId = meetingId;
            $scope.historyId = historyId;
            $scope.patientId = patientId;

            $scope.isModerator = commonService.roles.hasRole(strings.roles.moderator);
            $scope.isNavigator = commonService.roles.hasRole(strings.roles.navigator);

            // back
            $scope.back = function () {
                commonService.history.back();
            };

            /*-------patients tab------*/
            $scope.iNow = 0;
            $scope.onPatient = function (history, $index) {
                // reset request number.
                requestNum = 2;
                $scope.iNow = $index;
                $scope.historyId = history.id;

                // progress
                commonService.progress.show();

                // get selected history detail information.
                historiesServices.detail(history.id);

                // get grouped and combined healthrecords.
                getCombinedHealthrecords($scope.historyId);

                // patients tab changed in left side.
                $rootScope.$broadcast('patient.changed', {
                    history: $scope.selectedHistoryInformation,
                    historyId: $scope.historyId,
                    patient: history.patient
                });
            };
            /*-------end------*/

            /*-------event------*/
            $scope.$on('history.detail.done', function (event, result) {
                $scope.selectedHistoryInformation = result;

                if (--requestNum === 0) {
                    commonService.progress.hide();
                }
            });
            $scope.$on('history.detail.failed', function () {
                if (--requestNum === 0) {
                    commonService.progress.hide();
                }
            });

            $scope.$on('$destroy', function () {
                // reset session value.
                commonService.session.reset('meetingHistories');
            });
            /*---------end--------*/

            /*-------private------*/
            function init() {
                // progress
                commonService.progress.show();

                // get temp histories from session, to reduct request number.
                $scope.patientsHistories = commonService.session.get('meetingHistories') || [];

                // get selected history detail information.
                historiesServices.detail($scope.historyId);

                // get grouped and combined healthrecords.
                getCombinedHealthrecords($scope.historyId);

                // set active tab by history id.
                setActivePatientTab();

                // created new if websocket instance is not created.
                if (!$rootScope.ws) {
                    $rootScope.ws = websocketServices.createChannel(user.token);
                }
            }

            function getCombinedHealthrecords(historyId) {
                // get all healthrecord for current selected history.
                healthrecordServices.combine(historyId, true).then(function (groupedHealthrecords) {
                    $scope.groupedHealthrecords = groupedHealthrecords;
                    if (--requestNum === 0) {
                        commonService.progress.hide();
                    }
                }, function () {
                    if (--requestNum === 0) {
                        commonService.progress.hide();
                    }
                });
            }

            /**
             * Set selected patient tab as active.
             */
            function setActivePatientTab() {
                if ($scope.patientsHistories && $scope.patientsHistories.length != 0) {
                    if (historyId) {
                        for (var i = 0; i < $scope.patientsHistories.length; i++) {
                            if ($scope.patientsHistories[i].id == historyId) {
                                $scope.iNow = i;
                                break;
                            }
                        }
                    }
                }
            }
            /*---------end--------*/

            // init
            init();
        }]);
})();