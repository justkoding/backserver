﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('AddMeetingCtrl_old', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams) {
           
            // back
            $scope.back = function () {
                $state.go('page.meeting', null,  {'reload':true});
            };
            $scope.pageTitle = 'MDT_ADD_PAGE_TITLE';

            // tab: 1-> step 1, 2-> step 2
            $scope.iNow = 1;

            // mdt
            $scope.mdtid = null;

            /*--------events-----------*/
            $scope.$on('mdt.add.done.next', function (event, data) {
                // go to step#2 for adding material.
                $scope.iNow = 2;
                $scope.pageTitle = 'MDT_ADD_STEP2_MATERAIL_PAGE_TITLE';
                $scope.mdtid = data.id;
            });

            /*----------end---------*/
        }]);
})();