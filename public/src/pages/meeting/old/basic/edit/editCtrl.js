﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('MdtBasicEditCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        '$translate',
        'loadingServices',
        'commonService',
        'mdtServices',
        'modalService',
        'messageService',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            $translate,
            loadingServices,
            commonService,
            mdtServices,
            modalService,
            messageService) {

            // current login user.
            var user = commonService.getCurrentUser();

            //datapicker: min date
            $scope.minDate = new Date();

            // attendees
            $scope.removeAttendence = function (attendence) {
                // todo
            };

            $scope.addAttendence = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/meeting/basic/addAttendence/addAttendence.html',
                  {
                      controller: 'AddAttendenceCtrl',
                      data: {
                          attendees: $scope.mdt.attendees,
                          currentUserId: user.account.accountId
                      },
                      closefn: addAttendenceCallback
                  });
            };
            
            // temprary attendees
            $scope.removeTempAttendence = function (attendence) {
                // todo
            };

            $scope.addTempAttendence = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/meeting/basic/addTempAttendence/addTempAttendence.html',
                  {
                      controller: 'AddTempAttendenceCtrl',
                      data: {
                          tempAttendees: $scope.tempAttendeeItems
                      },
                      closefn: addTempAttendenceCallback
                  });
            };

            // btns
            $scope.cancel = function () {
                $rootScope.$broadcast('basic.edit.cancel', { cancel: true });
            };

            $scope.save = function () { 
                // resolve issue.
                if($scope.mdt.type == 1){
                    $scope.mdt.meetingStartDate = "";
                    $scope.mdt.duration = "";
                    $scope.mdt.location = "";
                }

                var mdt = JSON.parse(JSON.stringify($scope.mdt));

                mdtServices.update(mdt).then(function (data) {
                    //send a mail to tempAttendees
                    mdtServices.mailTempattendees(data.mdtid);
                });
            };

            //dateCompare
            $scope.lessThan = false;
            $scope.moreThan = false;

            // datepicker
            $scope.openedMeetingDate = false;
            $scope.openedDisDate = false;

            // xgTimePicker          
            $scope.mdt = {
                tfrom: '',
                tto: ''
            };

            $scope.onTimeChanged = function (t) {
                if (t.from) {
                    $scope.mdt.tfrom = t.time.value;
                } else {
                    $scope.mdt.tto = t.time.value;
                }
            };

            $scope.changeType = function(){
                if($scope.mdt.checked){
                    $scope.mdt.type = 2;
                }else{
                    $scope.mdt.type = 1;
                }

                console.log("mdt.checked is " + $scope.mdt.checked);
            };

            $scope.changeDiscussionDate = function(){
                var disDate = '',
                    startDate = '';

                if ($scope.mdt.checked) {
                    disDate = moment($scope.mdt.discussionDate).format('YYYYMMDD') + $scope.mdt.discussionTo.replace(':','');
                    startDate = moment($scope.mdt.meetingStartDate).format('YYYYMMDD') + $scope.mdt.tto.replace(':','');

                    if(disDate < startDate){
                        $scope.lessThan = true;
                    }else{
                        $scope.lessThan = false;
                    }

                    $scope.moreThan = false;
                }
            };


            $scope.changeStartMeetingDate = function(){
                var disDate = '',
                    startDate = '';

                if ($scope.mdt.checked) {
                    disDate = moment($scope.mdt.discussionDate).format('YYYYMMDD') + $scope.mdt.discussionTo.replace(':','');
                    startDate = moment($scope.mdt.meetingStartDate).format('YYYYMMDD') + $scope.mdt.tto.replace(':','');

                    if(disDate < startDate){
                        $scope.moreThan = true;
                    }else{
                        $scope.moreThan = false;
                    }

                    $scope.lessThan = false;
                }
            };

            /*-------event------*/
            $scope.$on('mdt.detail.done', function (event, result) {
                loadingServices.hide();
                $scope.mdt = result.data;
                $scope.tempAttendeeItems = $scope.mdt.tempAttendees;

                if ($scope.tempAttendeeItems && $scope.tempAttendeeItems.length != 0) {
                    $scope.tempAttendeeItems.forEach(function (item) {
                        item.userName = item.name;
                    });
                }
            });

            $scope.$on('mdt.update.done', function (event, data) {
                loadingServices.hide();  
                $rootScope.$broadcast('basic.edit.save', { save: true });
            });
            /*---------end--------*/

            /*-------private------*/
            function init() {
                loadingServices.show();
                var mdtId = $stateParams.id;
                mdtServices.detail(mdtId);
            }

            function addAttendenceCallback(ass) {
                $scope.mdt.attendees = ass;
            }

            function addTempAttendenceCallback(data) {
                $scope.mdt.tempAttendees = data.ids;
                $scope.tempAttendeeItems = data.attendees;
            }
            /*---------end--------*/

            // init
            init();
        }]);
})();