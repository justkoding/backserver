﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('MdtBasicAddCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        '$translate',
        'loadingServices',
        'messageService',
        'commonService',
        'mdtServices',
        'modalService',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            $translate,
            loadingServices,
            messageService,
            commonService,
            mdtServices,
            modalService) {

            // mark whether it is first loading.
            $scope.first = true;

            // current login user.
            var user = commonService.getCurrentUser().account,
                attendeesForSave = [];

            $scope.mdt = {
                discussionDate: moment(new Date()).format('YYYY-MM-DD'),
                discussionTo:'15:00',
                meetingStartDate: moment(new Date()).format('YYYY-MM-DD'),
                tfrom: '13:00',
                tto: '14:00',
                type: 1,
                attendees: [],
                tempAttendees: [],
                location: ''
            };

            //datapicker: min date
            $scope.minDate = new Date();

            // attendees
            $scope.removeAttendence = function (attendence) {
                // todo
                $scope.first = false;
            };

            $scope.addAttendence = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/meeting/basic/addAttendence/addAttendence.html',
                  {
                      controller: 'AddAttendenceCtrl',
                      data: {
                          attendees: $scope.mdt.attendees
                      },
                      closefn: addAttendenceCallback
                  });
            };

            // temprary attendees
            $scope.removeTempAttendence = function (attendence) {
                // todo
            };

            $scope.addTempAttendence = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/meeting/basic/addTempAttendence/addTempAttendence.html',
                  {
                      controller: 'AddTempAttendenceCtrl',
                      data: {
                          tempAttendees: $scope.tempAttendeeItems
                      },
                      closefn: addTempAttendenceCallback
                  });
            };

            // btns
            $scope.cancel = function () {
                $state.go('page.meeting', null,  {'reload':true});
            };

            $scope.save = function () {
                // resolve issue.
                var mdt = JSON.parse(JSON.stringify($scope.mdt));
                mdt.attendees = attendeesForSave;

                loadingServices.show();
                mdtServices.add(mdt).then(function () {
                    $state.go('page.meeting', null,  {'reload':true});
                });
            };

            $scope.saveAndContinual = function () {
                // resolve issue.

                var mdt = JSON.parse(JSON.stringify($scope.mdt));
                mdt.attendees = attendeesForSave;

                loadingServices.show();
                mdtServices.add(mdt).then(function (data) {
                    //send a mail to tempAttendees
                    mdtServices.mailTempattendees(data.mdtid);

                    // send broadcast with the new mdt id.
                    $rootScope.$broadcast('mdt.add.done.next', {
                        id: data.mdtid
                    });
                });
            };

            //dateCompare
            $scope.lessThan = false;
            $scope.moreThan = false;

            // datepicker
            $scope.openedDisDate = false;
            $scope.openDisDate = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.openedDisDate = !$scope.openedDisDate;
            };

            $scope.openedMeetingDate = false;
            $scope.openMeetingDate = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.openedMeetingDate = !$scope.openedMeetingDate;
            };

            // xgTimePicker 
            $scope.onTimeChanged = function (t) {
                if (t.from) {
                    $scope.mdt.tfrom = t.time.value;
                } else {
                    $scope.mdt.tto = t.time.value;
                }
            };

            $scope.changeType = function(){
                if($scope.mdt.checked){
                    $scope.mdt.type = 2;
                }else{
                    $scope.mdt.type = 1;
                }

                console.log("mdt.checked is " + $scope.mdt.checked);
            };

            $scope.changeDisDate = function(){
                var disDate = '',
                    startDate = '';

                if ($scope.mdt.checked) {
                    disDate = moment($scope.mdt.discussionDate).format('YYYYMMDD') + $scope.mdt.discussionTo.replace(':','');
                    startDate = moment($scope.mdt.meetingStartDate).format('YYYYMMDD') + $scope.mdt.tto.replace(':','');

                    if(disDate < startDate){
                        $scope.lessThan = true;
                    }else{
                        $scope.lessThan = false;
                    }

                    $scope.moreThan = false;
                }
            };


            $scope.changeMeetingDate = function(){
                var disDate = '',
                    startDate = '';

                if ($scope.mdt.checked) {
                    disDate = moment($scope.mdt.discussionDate).format('YYYYMMDD') + $scope.mdt.discussionTo.replace(':','');
                    startDate = moment($scope.mdt.meetingStartDate).format('YYYYMMDD') + $scope.mdt.tto.replace(':','');

                    if(disDate < startDate){
                        $scope.moreThan = true;
                    }else{
                        $scope.moreThan = false;
                    }

                    $scope.lessThan = false;
                }
            };

            /*-------event------*/
            $scope.$on('mdt.add.done', function (event, data) {
                loadingServices.hide();              
            });

            $scope.$on('mdt.add.failed', function (event, results) {
                loadingServices.hide();
            });
            /*---------end--------*/

            /*-------private------*/
            function init() {
            }

            function addAttendenceCallback(ass) {

                attendeesForSave = [];
                $scope.first = false;
                $scope.mdt.attendees = ass;

                //get attendees' ids
                if(ass){
                    ass.forEach(function(item){
                        var tempObj = {};
                            tempObj.id = item.id;
                        attendeesForSave.push(tempObj);
                    });
                }
            }

            function addTempAttendenceCallback(data) {
                $scope.mdt.tempAttendees = data.ids;
                $scope.tempAttendeeItems = data.attendees;
            }
            /*---------end--------*/

            // init
            init();
        }]);
})();