﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('AddCommentsCtrl', ['$scope',
        '$timeout',
        '$translate',
        '$cookieStore',
        'loadingServices',
        'messageService',       
        'commonService',
        '$modalInstance',
        'data',
        'commentsServices',
        function ($scope,
            $timeout,
            $translate,
            $cookieStore,
            loadingServices,
            messageService,           
            commonService,
            $modalInstance,
            data,
            commentsServices) {
            loadingServices.hide();
          
            // deep copy
            $scope.data = JSON.parse(JSON.stringify(data));                      

            // parse parma from data.
            var mdtId = $scope.data.mdtId;
            var patientId = $scope.data.patientId;
            $scope.quoteComment = $scope.data.quoteComment;
           
            // remove quote comment.
            $scope.removeQuote = function () {
                $scope.quoteComment = null;
            };

            // modal.
            $scope.ok = function () {
                loadingServices.show();

                // call service to add comment to server.
                commentsServices.add({
                    mdtId: mdtId,
                    patientId: patientId,
                    quoteId: $scope.quoteComment ? $scope.quoteComment.id : null,
                    content: $scope.content
                });
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');              
            };

            /*--------events------*/
            $scope.$on('comments.add.done', function () {
                loadingServices.hide();
                $modalInstance.close();
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });
            /*--------end------*/

            /*--------private------*/
            function init() {               
            } 
            /*--------end------*/

            // init
            init();
        }]);
})();