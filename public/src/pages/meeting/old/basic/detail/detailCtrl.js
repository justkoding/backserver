﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('MdtBasicDetailCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        'loadingServices',
        'commonService',
        'mdtServices',
        'CountdownTimer',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            loadingServices,
            commonService,
            mdtServices,
            CountdownTimer) {

            $scope.timer = new CountdownTimer(24 * 60 * 60);

            /*-------event------*/
            $scope.$on('mdt.detail.done', function (event, result) {
                loadingServices.hide();
                $scope.mdt = result.data;
            });
            /*---------end--------*/

            /*-------private------*/
            function init() {
                loadingServices.show();
                var mdtId = $stateParams.id;
                mdtServices.detail(mdtId);
            }
            /*---------end--------*/

            // init
            init();
        }]);
})();