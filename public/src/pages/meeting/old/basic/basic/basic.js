﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('MdtBasicCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        '$timeout',
        'loadingServices',
        'commonService',
        'clientServices',
        'mdtServices',
        'modalService',
        'commentsServices',
        'announcementsServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            $timeout,
            loadingServices,
            commonService,
            clientServices,
            mdtServices,
            modalService,
            commentsServices,
            announcementsServices) {

            var mdtId = $stateParams.id;
            var param = {
                doctorship: 0,
                mdtid: mdtId
            };
            var loadingCount = 3;

            $scope.user = commonService.getCurrentUser();

            $scope.edit = function () {
                $rootScope.$broadcast('basic.edit.start');
            };

            $scope.delete = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/confirm/confirm.html',
                   {
                       controller: 'ConfirmCtrl',
                       data: {
                           title: 'COMMON_DELETE_CONFIRM_TITLE',
                           body: 'COMMON_DELETE_CONFIRM_BODY'
                       },
                       closefn: deleteMDTCallback
                   });
            };

            // countdown
            $scope.mdtStartTotalTimespan = 0;
            $scope.mdtStartPassedTimespan = 0;
            $scope.showFirstCounddown = false;
            $scope.getStartedMeeting = function () {
                $scope.showFirstCounddown = false;
                $scope.$digest();
            };

            $scope.mdtEndTotalTimespan = 0;
            $scope.mdtEndPassedTimespan = 0;
            $scope.showSecondCounddown = false;
            $scope.endOfMdt = function () {
                $scope.showSecondCounddown = false;
                $scope.$digest();
            };

            /*----------launch pc client---------------*/
            // launch pc client to join meeting.
            $scope.meeting = '';
            $scope.startOnlineMeeting = function () {
                loadingServices.show();
                clientServices.meeting($scope.user.token, parseInt(mdtId));
            };
            /*----------end pc client---------------*/

            // announcement
            $scope.publishAnnouncement = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/meeting/basic/announcement/announcement.html',
                     {
                         controller: 'AnnouncementCtrl',
                         data: {
                             mdtId: mdtId
                         },
                         closefn: addEditAnnouncementCallback
                     });
            };

            $scope.editAnnouncement = function (announcement) {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/meeting/basic/announcement/announcement.html',
                     {
                         controller: 'AnnouncementCtrl',
                         data: {
                             mdtId: mdtId,
                             announcement: announcement
                         },
                         closefn: addEditAnnouncementCallback
                     });
            };

            $scope.deleteAnnouncement = function (announcement) {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/confirm/confirm.html',
                   {
                       controller: 'ConfirmCtrl',
                       data: {
                           title: 'COMMON_DELETE_CONFIRM_TITLE',
                           body: 'COMMON_DELETE_CONFIRM_BODY'
                       },
                       closefn: function () {
                           deleteAnnouncementCallback(announcement);
                       }
                   });
            };


            /*-------event------*/
            // join/start mdt
            $scope.$on('mdt.join.done', function (event, path) {
                $scope.meeting = path;

                $timeout(function () {
                    document.getElementById('meeting').click();
                    loadingServices.hide();
                }, 0);
            });
            $scope.$on('mdt.join.failed', loadingServices.hide);

            $scope.$on('mdt.detail.done', function (event, result) {
                $scope.mdt = result.data;

                // mdt start countdown
                var mdtStartTotalTimespan = commonService.getTimespan($scope.mdt.createTime, $scope.mdt.start);
                var mdtStartPassedTimespan = commonService.getTimespan($scope.mdt.createTime);
                $scope.showFirstCounddown = mdtStartPassedTimespan < mdtStartTotalTimespan;
                $scope.mdtStartTotalTimespan = mdtStartTotalTimespan > 0 ? mdtStartTotalTimespan : 0;
                $scope.mdtStartPassedTimespan = mdtStartPassedTimespan > 0 ? mdtStartPassedTimespan : 0;

                // mdt discussion end countdown.
                var mdtEndTotalTimespan = commonService.getTimespan($scope.mdt.createTime, $scope.mdt.end);
                var mdtEndPassedTimespan = commonService.getTimespan($scope.mdt.createTime);
                $scope.showSecondCounddown = mdtStartPassedTimespan < mdtStartTotalTimespan;
                $scope.mdtEndTotalTimespan = mdtEndTotalTimespan > 0 ? mdtEndTotalTimespan : 0;
                $scope.mdtEndPassedTimespan = mdtEndPassedTimespan > 0 ? mdtEndPassedTimespan : 0;

                if (--loadingCount == 0) {
                    loadingServices.hide();
                }
            });

            $scope.$on('mdt.patients.list.done', function (event, result) {
                $scope.patients = result.data;
                if (--loadingCount == 0) {
                    loadingServices.hide();
                }
            });

            $scope.$on('mdt.remove.done', function (event, result) {
                loadingServices.hide();
                $state.go('page.meeting');
            });

            // happens once click on base info tab.
            $scope.$on('page.patient.base.focus', function () {
                // get the latest 10 comments.
                getLatestComments(10);
            });

            // announcements
            $scope.$on('announcement.list.done', function (event, result) {
                $scope.announcements = result.data;
                loadingServices.hide();
            });

            $scope.$on('announcement.remove.done', function (event, result) {
                announcementsServices.list({ mdtId: mdtId });
            });
            /*---------end--------*/

            /*-------private------*/
            function init() {
                loadingServices.show();

                // get mdt information.
                mdtServices.detail(mdtId);

                // get all patients.
                mdtServices.patientsList(param);

                // get the latest 10 comments.
                $scope.comments = [];
                getLatestComments(10);

                // get all announcements.
                $scope.announcements = [];
                announcementsServices.list({ mdtId: mdtId });
            }

            function deleteMDTCallback() {
                loadingServices.show();
                mdtServices.remove(mdtId);
            }

            function getLatestComments(count) {
                commentsServices.list({
                    mdtId: mdtId,
                    count: count,
                    ordertype: 1 // 1->asc, 0-> dasc
                }).then(function (resp) {
                    $scope.comments = resp.data;
                });
            }

            function addEditAnnouncementCallback() {
                announcementsServices.list({ mdtId: mdtId });
            }

            function deleteAnnouncementCallback(announcement) {
                loadingServices.show();
                announcementsServices.remove(announcement.id);
            }
            /*---------end--------*/

            // init
            init();
        }]);
})();