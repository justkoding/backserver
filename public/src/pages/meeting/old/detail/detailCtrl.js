﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('DetailMeetingCtrl_old', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        'commonService',
        'mdtServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            commonService,
            mdtServices) {

            var mdtId = $stateParams.id;
            var defaultTab = $stateParams.tab;

            // back
            $scope.back = function () {
                $state.go('page.meeting', null, { 'reload': true });
            };

            // tab
            $scope.iNow = 1;
            $scope.tab = function (iTab) {
                $scope.iNow = iTab;

                if (iTab === 1) {
                    $rootScope.$broadcast('page.patient.base.focus');
                } else if (iTab === 2) {
                    $rootScope.$broadcast('page.resize', {});
                    $rootScope.$broadcast('page.patient.discuss.focus');
                }
            };

            // locked and unlock
            $scope.locked = true;
            $scope.doLocked = function (locked) {
                $scope.locked = locked;
            };

            /*-------event------*/
            $scope.$on('basic.edit.cancel', function () {
                $scope.locked = true;
            });

            $scope.$on('basic.edit.start', function () {
                $scope.locked = false;
            });

            $scope.$on('basic.edit.save', function () {
                $scope.locked = true;
            });
            /*---------end--------*/

            /*-------private------*/
            function init() {
                if (defaultTab) {
                    $scope.iNow = defaultTab;
                }
            }
            /*---------end--------*/

            // init
            init();
        }]);
})();