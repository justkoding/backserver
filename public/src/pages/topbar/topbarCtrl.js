﻿(function () {

    'use strict';

    var app = angular.module('app');

    app.controller('TopbarCtrl', ['$scope',
        '$rootScope',
        '$window',
        '$timeout',
         'modalService',
         'loadingServices',
        function ($scope,
            $rootScope,
            $window,
            $timeout,
            modalService,
            loadingServices) {

            // btns and collapsed
            $scope.btnFocus = false;
            $scope.collapsed = document.documentElement.clientWidth <= 845;
            $scope.isShowPanel = false;
            $scope.toggleCollapsed = function () {
                $scope.collapsed = !$scope.collapsed;
                $scope.isShowPanel = !$scope.collapsed;
                $scope.btnFocus = true;
            };

            // password.
            $scope.showLogoutPanel = function (e) {
                $scope.isShown = !$scope.isShown;

                if ($scope.isShowPanel) {
                    $scope.isShowPanel = false;
                    $scope.collapsed = true;
                }

                return false;
            };
            $scope.changePassword = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/password/changepwd.html',
                    {
                        controller: 'changePwdCtrl'
                    });
            };

            // system menu icon
            $scope.systemMenuIcon = '/assets/src/img/icons/sysmenu_normal.png';
            $scope.changeSystemMenuIcon = function (isFocus) {
                if (isFocus) {
                    $scope.systemMenuIcon = '/assets/src/img/icons/sysmenu_mouseover.png';
                } else {
                    $scope.systemMenuIcon = '/assets/src/img/icons/sysmenu_normal.png';
                }
            };

            // toggle btn icon.
            $scope.toggleBtnIcon = '/assets/src/img/icons/menuhideshow_normal.png';
            $scope.changeToggleBtnIcon = function (isFocus) {
                if (isFocus) {
                    $scope.toggleBtnIcon = '/assets/src/img/icons/menuhideshow_mouseover.png';
                } else {
                    $scope.toggleBtnIcon = '/assets/src/img/icons/menuhideshow_normal.png';
                }
            };

            /*---------search--------------*/
            // add this variable to $rootScope since we would share it in different pages.
            $rootScope.search = {
                opt: 2,
                onSearch: function (data) {
                    // this method will be called once user click on search icon or press enter.
                    // - $scope.search.opt -> option id.
                    // - data.text -> user input text in search control.
                    // - data.opt.name -> option name
                    // - data.opt.id -> option id, should same as $scope.search.opt
                    console.log('search.opt: ' + $scope.search.opt);
                    console.log('text: ' + data.text +
                        ' opt name: ' + data.opt.name +
                        ' opt.id: ' + data.opt.id);
                }
            };

            /*---------end--------------*/

            /*--------events------*/
            angular.element(document).bind('click', function (e) {
                hide(e);
            });

            $window.addEventListener('resize', function () {
                $scope.collapsed = document.documentElement.clientWidth <= 845;
                $scope.isShowPanel = $scope.collapsed;
                $scope.btnFocus = false;
                $scope.$digest();
            });

            $scope.$on('$destroy', function () {
                // clean up.
            });
            /*--------end------*/

            /*--------private methods------*/
            function hide(e) {
                if ($scope.isShown) {
                    // check whether the clicked element is outside of this wrap.
                    var wrap = $(e.target).closest('.topbar-more-action-wrap');
                    if (wrap.length === 0) {
                        $scope.isShown = false;
                        $scope.$digest();
                    }
                }
            }
            /*--------end------*/
        }]);
})();