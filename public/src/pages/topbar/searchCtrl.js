﻿(function () {

    'use strict';

    var app = angular.module('app');

    app.controller('SearchCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$timeout',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $timeout) {
            /*---------search--------------*/
            // add this variable to $rootScope since we would share it in different pages.
            $rootScope.search = {
                opt: 1,
                onSearch: function (data) {
                    // this method will be called once user click on search icon or press enter.
                    // - $scope.search.opt -> option id.
                    // - data.text -> user input text in search control.
                    // - data.opt.name -> option name
                    // - data.opt.id -> option id, should same as $scope.search.opt
                    //console.log('search.opt: ' + $scope.search.opt);
                    //console.log('text: ' + data.text +
                    //    ' opt name: ' + data.opt.name +
                    //    ' opt.id: ' + data.opt.id);

                    switchTo(data);
                }
            };

            /*---------end--------------*/

            /*---------private--------------*/
            function switchTo(data) {
                var id = data.opt.id;
                var path = '';

                //$cookieStore.put('s_' + id, data.text);

                switch (id) {
                    case 1: {
                        // MDT page
                        path = 'meeting';

                        break;
                    }
                    case 2: {
                        // MDT report page
                        path = 'report';

                        break;
                    }
                    case 3: {
                        // MDT docx page
                        path = 'docx';

                        break;
                    }
                    case 4: {
                        // patient page
                        path = 'patient';

                        break;
                    }
                    case 5: {
                        // user page
                        path = 'user';

                        break;
                    }
                    default: {
                        // log page
                        path = 'log';

                        break;
                    }
                }

                $timeout(function () {
                    $state.go('page.' + path, { search: data.text });
                    //$state.reload(true);
                });
            }

            /*---------end--------------*/
        }]);
})();