(function () {
    'use strict';

    angular.module('app').controller('NavigatorHomeCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$timeout',
        'commonService',
        'homeServices',
        'messageService',
        'templateService',
        'modalService',
        'loadingServices',
        'favoritesServices',
        'dashboardServices',
        'groupsServices',
        'mdtServices',
        'commentsServices',
        'clientServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $timeout,
            commonService,
            homeServices,
            messageService,
            templateService,
            modalService,
            loadingServices,
            favoritesServices,
            dashboardServices,
            groupsServices,
            mdtServices,
            commentsServices,
            clientServices) {
            // current login user.
            $scope.user = commonService.getCurrentUser();


            // init.
            function init() {
            }

            init();

            /*------events-------*/
            $scope.$on('$destroy', function () {
                // clean up.
            });
            /*------end-------*/
        }]);
}());
