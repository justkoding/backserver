(function () {
    'use strict';

    angular.module('app').controller('AdminHomeCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$timeout',
        'commonService',
        'homeServices',
        'messageService',
        'templateService',
        'modalService',
        'loadingServices',
        'favoritesServices',
        'dashboardServices',
        'groupsServices',
        'mdtServices',
        'commentsServices',
        'clientServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $timeout,
            commonService,
            homeServices,
            messageService,
            templateService,
            modalService,
            loadingServices,
            favoritesServices,
            dashboardServices,
            groupsServices,
            mdtServices,
            commentsServices,
            clientServices) {
            // current login user.
            var user = commonService.getCurrentUser();
            $scope.user = user;

            // record how many ajax requests would be sent in initializing
            // methods.
            var requestsNum = 4;

            // only retieve 20 records for my favorites, doctor groups, mdt discussion etc.
            // if want to view all items, you can go to related list page.
            var numbers = 20;            

            /*------count-down-----*/
            $scope.showCountdown = false;

            $scope.countdownCompleted = function () {
                $scope.showCountdown = false;
                $scope.$digest();
            };

            // launch pc client to join meeting.
            $scope.meeting = '';
            $scope.joinMeeting = function (mdt) {
                loadingServices.show();
                clientServices.meeting($scope.user.token, $scope.mdts.mdt.mdtid);
            };
            /*--------end--------*/

            /*---favorites----*/
            $scope.favorites = [];

            //mdts' info for dashboard
            $scope.mdts = [];
            $scope.space = ' ';

            // how long will mdt start.
            $scope.mdtTotalTimespan = 0;
            $scope.mdtPassedTimespan = 0;

            // redirect to favorites list page.
            $scope.gotoFavorites = function () {
                $state.go('page.favorite');
            };

            // redirect to patient detail page.
            $scope.patientDetail = function (favorite) {
                $state.go('page.patient.detail', {
                    id: favorite.patient.id
                });
            };

            /*------end------*/

            /*---doctor groups----*/
            $scope.doctorGroups = [];
            $scope.gotoDoctorGroups = function () {
                $state.go('page.group');
            };

            // go to doctor groups list page. and set
            // special doctor group with <id> is launched.
            $scope.viewDoctorGroup = function (dg) {
                $state.go('page.group', {
                    id: dg.id
                });
            };
            /*------end------*/

            /*---mdt discussion----*/
            $scope.allMdts = [];
            $scope.latestMdt = null;
            $scope.onDdlSelect = function (mdt) {
                if ($scope.latestMdt.mdtid != mdt.mdtid) {
                    $scope.latestMdt = mdt;
                    loadingServices.show();

                    // get comments for special mdt.
                    commentsServices.list({
                        mdtId: $scope.latestMdt.mdtid
                    });
                }
            };

            // go to mdt detail page, patient discussion tab.
            $scope.gotoMdt = function (mdt) {
                $state.go('page.meeting.detail', {
                    id: mdt.mdtid
                });
            };

            $scope.gotoMdtList = function () {
                $state.go('page.meeting');
            };
            /*------end------*/

            /*---mdt comments----*/
            $scope.comments = [];
            /*------end------*/

            /*----------launch pc client---------------*/
            // launch pc client to join meeting.
            $scope.meeting = '';
            $scope.startOnlineMeeting = function () {
                loadingServices.show();
                clientServices.meeting(user.token, mdtId);
            };

            $scope.viewAttachment = function (comment) {
                // healthrecod
                if (comment.origin === 1) {
                    $scope.viewHealthrecord(comment);
                } else if (comment.origin === 2) { // snapshot
                    $scope.viewSnapshot(comment);
                }
            };

            // launch pc client to view healthrecord.
            $scope.viewHealthrecord = function (hr) {
                loadingServices.show();

                var patientId = parseInt(hr.patientId);
                clientServices.healthrecord({
                    mdtId: parseInt($scope.latestMdt.mdtid),
                    ticket: user.token,
                    healthrecordId: hr.healthrecordId,
                    healthrecordType: hr.healthrecordType,
                    patientId: patientId
                });
            };

            // launch pc client to view snapshot.
            $scope.viewSnapshot = function (comment) {
                var snapshot = comment.attach;

                loadingServices.show();

                var patientId = parseInt(comment.patientId);
                var snapshotId = parseInt(snapshot.id);
                clientServices.healthrecord({
                    ticket: user.token,
                    patientId: patientId,
                    mdtId: parseInt($scope.latestMdt.mdtid),
                    healthrecordId: comment.healthrecordId,
                    healthrecordType: comment.healthrecordType,
                    snapshotId: snapshotId
                });
            };
            /*----------end pc client---------------*/

            // init.
            function init() {
                loadingServices.show();

                // get favorites list.                
                favoritesServices.list({
                    count: numbers
                });

                //get mdts for dashboard
                dashboardServices.list();

                // get groups list.                
                groupsServices.list({
                    count: 0 // 0-> all
                });

                // get all unclosed mdt.
                mdtServices.list({
                    orderproperty: 2, // 2-> created time
                    ordertype: 0  // 0-> dasc
                });
            }

            init();
            /*------events-------*/
            // favorites
            $scope.$on('favorite.list.done', function (event, result) {
                if (--requestsNum === 0) {
                    loadingServices.hide();
                }

                $scope.favorites = result.data;
                console.log(result.data);
            });

            // closet conference.
            $scope.$on('dashboard.list.done', function (event, result) {
                if (--requestsNum === 0) {
                    loadingServices.hide();
                }
                $scope.mdts = result;
                if (result && result.mdt && result.mdt.createTime) {
                    var mdtTotalTimespan = commonService.getTimespan(result.mdt.createTime, result.mdt.start);
                    var mdtPassedTimespan = commonService.getTimespan(result.mdt.createTime);

                    $scope.mdtTotalTimespan = mdtTotalTimespan > 0 ? mdtTotalTimespan : 0;
                    $scope.mdtPassedTimespan = mdtPassedTimespan > 0 ? mdtPassedTimespan : 0;

                    $scope.showCountdown = mdtPassedTimespan < mdtTotalTimespan;
                }
            });

            // doctor groups list.
            $scope.$on('group.list.done', function (event, result) {
                if (--requestsNum === 0) {
                    loadingServices.hide();
                }
                $scope.doctorGroups = result.data;
            });

            // all unclosed mdt list.
            $scope.$on('mdt.list.done', function (event, result) {
                if (--requestsNum === 0) {
                    loadingServices.hide();
                }
                $scope.allMdts = result.data;

                if ($scope.allMdts && $scope.allMdts.length != 0) {
                    $scope.latestMdt = $scope.allMdts[0];

                    // get comments for special mdt.
                    commentsServices.list({
                        mdtId: $scope.latestMdt.mdtid
                    });
                }

                console.log($scope.allMdts);
            });

            // comments list for special mdt.
            $scope.$on('comments.list.done', function (event, result) {
                loadingServices.hide();
                $scope.comments = result.data;
            });

            // healthrecord/snapshot 
            $scope.$on('hr.open.done', function (event, path) {
                $scope.hrPath = path;

                $timeout(function () {
                    document.getElementById('healthrecordlink').click();
                    loadingServices.hide();
                }, 0);
            });
            $scope.$on('hr.open.failed', loadingServices.hide);

            // join/start mdt
            $scope.$on('mdt.join.done', function (event, path) {
                $scope.meeting = path;

                $timeout(function () {
                    document.getElementById('meeting').click();
                    loadingServices.hide();
                }, 0);
            });
            $scope.$on('mdt.join.failed', loadingServices.hide);

            $scope.$on('$destroy', function () {
                // clean up.
            });
            /*------end-------*/
        }]);
}());
