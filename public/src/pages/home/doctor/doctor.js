(function () {
    'use strict';

    angular.module('app').controller('DoctorHomeCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$timeout',
        'commonService',
        'homeServices',
        'messageService',
        'templateService',
        'modalService',
        'loadingServices',
        'patientServices',
        'userServices',
        'dashboardServices',
        'mdtServices',
        'clientServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $timeout,
            commonService,
            homeServices,
            messageService,
            templateService,
            modalService,
            loadingServices,
            patientServices,
            userServices,
            dashboardServices,
            mdtServices,
            clientServices) {
            // current login user.
            var user = commonService.getCurrentUser();
            $scope.user = user;
            
            /*------count-down-----*/
            $scope.showCountdown = false;

            $scope.countdownCompleted = function () {
                $scope.showCountdown = false;
                $scope.$digest();
            };
            
            // launch pc client to join meeting.
            $scope.meeting = '';
            $scope.joinMeeting = function (mdt) {
                loadingServices.show();
                clientServices.meeting($scope.user.token, $scope.mdts.mdt.id);
            };
            /*--------end--------*/
          //mdts' info for dashboard
            $scope.mdts = {};

            // how long will mdt start.
            $scope.mdtTotalTimespan = 0;
            $scope.mdtPassedTimespan = 0;
            
            //patient list parameters
            var params = {
	    			size: 10,
	    			page: 1,
	    			doctor: $scope.user.account.accountId,
	    			dept: -1
	    	};
            
            // redirect to patient detail page.
            $scope.patientDetail = function (id) {
                $state.go('page.patient.detail', {
                    id: id
                });
            };
            $scope.patientList = function (tab) {
                commonService.session.set('initPatientTab',tab);
                $state.go('page.patient');
            };

            /*------end------*/

            // go to mdt detail page, patient discussion tab.
            $scope.gotoMeeting = function (m) {
                $state.go('page.meeting.detail', {
                    id: m.id
                });
            };
            
         // go to mdt detail page, patient discussion tab.
            $scope.gotoMeetingList = function (tab) {
                commonService.session.set('initMeetingTab',tab);
                $state.go('page.meeting');
            };

            /*------end------*/
            
            $scope.isMyPatient = function(patient){
            	return $scope.user.account.accountId == patient.doctor.id;
            };
            // init.
            function init() {
                loadingServices.show();
                
                // get mdts for dashboard
                dashboardServices.list();
                
                // Get mdt infomation for doctor home dashboard.
                mdtServices.doctorDashboardMdt();
                
                userServices.detail($scope.user.account.accountId);
            }

            init();
            /*------events-------*/

            // closet conference.
            $scope.$on('mdt.doctordashboard.done', function (event, result) {
                loadingServices.hide();
                $scope.mdts = result;
                $scope.mdts.mdt.start = new Date($scope.mdts.mdt.start);
                if (result && result.mdt && result.mdt.createTime) {
                    var mdtTotalTimespan = commonService.getTimespan(result.mdt.createTime, result.mdt.start);
                    var mdtPassedTimespan = commonService.getTimespan(result.mdt.createTime);

                    $scope.mdtTotalTimespan = mdtTotalTimespan > 0 ? mdtTotalTimespan : 0;
                    $scope.mdtPassedTimespan = mdtPassedTimespan > 0 ? mdtPassedTimespan : 0;

                    $scope.showCountdown = mdtPassedTimespan < mdtTotalTimespan;
                }
            });
            
            //user detail
            $scope.$on('user.detail.done', function (event, result) {
            	params.dept = result.dept.id;
        		patientServices.listPending(params);
        		delete params.dept;
        		patientServices.listDiscussion(params);
        		patientServices.listMeeting(params);
            });
            
            //Pending patient groups list.
            $scope.$on('patient.pending.done', function (event, result) {
            	commonService.progress.hide();
        		$scope.patientsInPending = result.data;
        		$scope.patientsInPendingCount = result.count;
            });
            
            //Discussion patient groups list.
            $scope.$on('patient.discussion.done', function (event, result) {
            	commonService.progress.hide();
        		$scope.patientsInDiscussion = result.data;
        		$scope.patientsInDiscussionCount = result.count;
            });
            
            //Meeting patient groups list.
            $scope.$on('patient.meeting.done', function (event, result) {
            	commonService.progress.hide();
        		$scope.patientsInMeeting = result.data;
        		$scope.patientsInMeetingCount = result.count;
            });
            
            // join/start mdt
            $scope.$on('mdt.join.done', function (event, path) {
                $scope.meeting = path;

                $timeout(function () {
                    document.getElementById('meeting').click();
                    loadingServices.hide();
                }, 0);
            });
            $scope.$on('mdt.join.failed', loadingServices.hide);

            $scope.$on('$destroy', function () {
                // clean up.
            });
            /*------end-------*/
        }]);
}());
