(function () {
    'use strict';

    angular.module('app').controller('ModerateHomeCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$timeout',
        '$stateParams',
        '$translate',
        'commonService',
        'userServices',
        'homeServices',
        'messageService',
        'templateService',
        'modalService',
        'loadingServices',
        'moderatorServices',
        'mdtServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $timeout,
            $stateParams,
            $translate,
            commonService,
            userServices,
            homeServices,
            messageService,
            templateService,
            modalService,
            loadingServices,
            moderatorServices,
            mdtServices) {

            // param: patients queue
            var options = {
                filter: '',
                status: 0,
                size: 20,
                page: 1
            }, param = {
                filter: '',
                status: 0,
                host: 0,
                datepre: '',
                datepost: '',
                orderproperty: 0, //order by start time
                ordertype: 1,// asc
                size: 0,
                page: 0
            };

            $scope.user = commonService.getCurrentUser();
            $scope.patient = {};

            //selected patients' count
            $scope.count = 0;

            //selected patients: add to a MDT
            $scope.selectedPatients = [];

            //meetingDate set: show flag on the calender
            $scope.meetingsDate = [];

            //pending patient list
            $scope.pendingPatients = [];

            //current mdt patients
            $scope.mdtExistedPatients = [];

            //all of mdt
            $scope.mdts = [];

            //current mdt
            $scope.mdt = null;

            $scope.hasInit = false;


            //current time
            $scope.today = function() {
                $scope.dt = new Date();
            };

            //datapicker: min date
            $scope.minDate = new Date();

            /*----------UI events begin------------------*/
            //add selected patients to a new Mdt
            $scope.addMdt = function () {
                commonService.session.set('pendingPatients', $scope.selectedPatients);
                $state.go('page.meeting.add', null, {'reload':true});
            };

            //add selected patients to an existed mdt
            $scope.addExistedMdt = function () {
                if($scope.selectedPatients.length > 0){
                    var queuesId = [],
                        options = {id :$scope.mdt.id};

                    $scope.selectedPatients.forEach(function(p,i){
                        if(p.checked){
                            //add to mdtService
                            queuesId.push({"id": $scope.selectedPatients[i].id});
                            if($scope.mdtExistedPatients){
                                $scope.mdtExistedPatients.push(p);
                            }else{
                                $scope.mdtExistedPatients = new Array(1);
                                $scope.mdtExistedPatients[0] = p;
                            }

                            for(var i = 0; i < $scope.pendingPatients.length; i++){
                                if($scope.pendingPatients[i].patient.id === p.patient.id){
                                    $scope.pendingPatients.splice(i, 1);
                                    break;
                                }
                            }
                        }
                    });

                    if(queuesId.length > 0){
                        mdtServices.addPatients(queuesId, options);
                    }
                }
            };

            //add attendance to current mdt
            $scope.addAttendance = function () {
                if(!$scope.overdue) {
                    loadingServices.show();
                    modalService.showModal('/assets/src/pages/meeting/modals/addAttendence/addAttendence.html',
                        {
                            controller: 'AddAttendenceCtrl',
                            data: {
                                doctors: $scope.mdt.attendees,
                                isEditAttenance: false
                            },
                            closefn: addAttendanceCallback
                        });
                }
             };

            // add a pending patient from pending patients' list
            $scope.addPatient = function () {
                if(!$scope.overdue) {
                    loadingServices.show();
                    modalService.showModal('/assets/src/pages/meeting/modals/addPendingPatients/addPendingPatients.html',
                        {
                            controller: 'AddPendingPatientsCtrl',
                            data: {
                                pendingPatients: $scope.mdtExistedPatients,
                                isDisabledPatients: false
                            },
                            closefn: addPatientCallback
                        });
                }
            };

            //select patients from pending patients' list
            $scope.onSelected = function (p, $index) {
                if(p.checked){
                    ++ $scope.count;
                    $scope.selectedPatients.push(p);
                }else{
                    -- $scope.count;
                    if($scope.selectedPatients){
                        for(var i = 0; i < $scope.selectedPatients.length; i++){
                            if($scope.selectedPatients[i].patient.id === p.patient.id){
                                $scope.selectedPatients.splice(i, 1);
                                break;
                            }
                        }
                    }
                }
            };

            //edit Mdt
            $scope.edit = function(){
                if(!$scope.overdue){
                    if($scope.selectedPatients){
                        commonService.session.set('pendingPatients', $scope.selectedPatients);
                    }

                    $state.go('page.meeting.edit', { id: $scope.mdt.id }, {'reload':true});
                }
            };

            /*----------UI events End------------------*/

            /*----------events begin------------------*/
            $scope.$on('moderator.list.done', function (event, result) {
                loadingServices.hide();
                $scope.pendingPatients = result;
            });

            $scope.$on('mdt.list.done', function (event, result) {
                loadingServices.hide();
                $scope.mdts = result.data;

                var mdtId = -1;
                if($scope.mdts){
                    $scope.mdts.forEach(function (p) {
                        $scope.meetingsDate.push(new Date(p.start));
                        //start == current day
                        if(compare(new Date(), new Date(p.start)) === 0){
                            mdtId = p.id;
                        }
                    });

                    //update the meetings date's icon
                    $rootScope.$broadcast('meetingsDate.update', $scope.meetingsDate);

                    //get the mdt of current day(if exist)
                    if(mdtId < 0){
                        // get the nearest meeting date
                        for(var i=0; i < $scope.mdts.length; i++){
                            if(compare($scope.mdts[i].start, new Date()) > 0){
                                mdtId = $scope.mdts[i].id;
                                break;
                            }
                        }
                    }

                    if(mdtId > 0){
                        mdtServices.detail(mdtId);
                    }
                }

            });

            $scope.$on('mdt.detail.done', function (event, result) {
                $scope.mdt = result;
                $scope.overdue = false;
                if($scope.mdt){
                    $scope.mdtExistedPatients = $scope.mdt.histories;
                    if(compare(new Date(),$scope.mdt.start) > 0){
                        $scope.overdue = true;
                    }

                    if($scope.hasInit){
                        $rootScope.$broadcast('moderator.init', $scope.mdt.start);
                    }
                }
            });

            $scope.$on('attendees.add.done', function (event, result) {
                // message
                $translate(['MODERATOR_ADD_ATTENDANCES_SUCCESS', 'MESSAGE_BODY_SUCCESS']).then(function (texts) {
                    messageService.success(texts['MODERATOR_ADD_ATTENDANCES_SUCCESS'], texts['MESSAGE_BODY_SUCCESS']);
                });
            });

            $scope.$on('patients.add.done', function (event, result) {
                $scope.selectedPatients = [];

                // message
                $translate(['MODERATOR_ADD_PATIENTS_SUCCESS', 'MESSAGE_BODY_SUCCESS']).then(function (texts) {
                    messageService.success(texts['MODERATOR_ADD_PATIENTS_SUCCESS'], texts['MESSAGE_BODY_SUCCESS']);
                });
            });


            $scope.$on('patients.delete.done', function (event, result) {

                console.log("patients.delete.done" + result);

            });

            $scope.$watch('dt', function (newVal, oldVal) {
               if(newVal){
                   if($scope.mdts){
                       var hasMdt = false;
                       for(var i =0; i< $scope.mdts.length; i++){
                           if(compare(newVal, new Date($scope.mdts[i].start)) === 0){
                               mdtServices.detail($scope.mdts[i].id);
                               hasMdt = true;
                               break;
                           }
                       }
                       if(!hasMdt){
                           commonService.session.set('pendingPatients', $scope.selectedPatients);
                           $state.go('page.meeting.add', {
                               date: newVal.getTime() //commonService.date.format(newVal, 'YYYY/MM/DD')
                           }, { 'reload': true });                          
                       }
                   }
               }
            });

            $scope.$on('$destroy', function () {
                // clean up.
            });

            $scope.$on('attendees.add.failed', function (event, result) {
                // message
                $translate(['MODERATOR_ADD_ATTENDANCES_FAILED', 'MESSAGE_BODY_SUCCESS']).then(function (texts) {
                    messageService.success(texts['MODERATOR_ADD_ATTENDANCES_FAILED'], texts['MESSAGE_BODY_SUCCESS']);
                });
            });

            $scope.$on('patients.add.failed', function (event, result) {
                $scope.selectedPatients = [];

                // message
                $translate(['MODERATOR_ADD_PATIENTS_FAILED', 'MESSAGE_BODY_SUCCESS']).then(function (texts) {
                    messageService.success(texts['MODERATOR_ADD_PATIENTS_FAILED'], texts['MESSAGE_BODY_SUCCESS']);
                });
            });

            $scope.$on('mdt.detail.failed', function (event, result) {
                loadingServices.hide();
            });

            /*----------events end----------------------*/


            // init.
            function init() {
                mdtServices.list(param);
                moderatorServices.list(options);
                $scope.hasInit = true;
            }

            init();

            /*-----------private method start-----------*/
            function setDept() {
                var index = commonService.getIndexOf($scope.patient.dept, $scope.depts);
                index = index == -1 ? 0 : index;
                $scope.patient.dept = $scope.depts[index];
            }

            function addAttendanceCallback(ass) {
                //add attendees to mdt
                var  hasAttendee = false,
                    options = {id :$scope.mdt.id},
                    accountsId = [];
                if (ass && $scope.mdt.attendees) {
                    for(var i =0; i< ass.length; i++){
                        hasAttendee = false;
                        for(var j = 0; j <$scope.mdt.attendees.length; j++ ){
                            if(ass[i].id === $scope.mdt.attendees[j].id){
                                hasAttendee = true;
                                break;
                            }
                        }
                        if(!hasAttendee){
                            accountsId.push(ass[i].id);
                            ass[i].avatar = commonService.formatAvatar(ass[i].avatar, ass[i].id);
                            ass[i].title = ass[i].userTitle.name;
                            ass[i].userName = ass[i].name;
                            $scope.mdt.attendees.push(ass[i]);
                        }
                    }
                }

                if(accountsId.length > 0){
                    mdtServices.addAttendees(accountsId, options);
                }
            }

            // retrieve all patients again from db to get the new patient we added.
            function addPatientCallback(data) {
                loadingServices.hide();
                // add patients to mdt
                var opt = {id :$scope.mdt.id},
                    delQueueId=[],
                    addQueueId = [];

                if (data) {
                    if($scope.mdtExistedPatients && $scope.mdtExistedPatients.length > 0){
                        $scope.mdtExistedPatients.forEach(function(p){
                            delQueueId.push({"id": p.id});
                        });

                        if(delQueueId.length > 0) {
                            mdtServices.deletePatients(delQueueId, opt).then(function(){
                                if(data.pendingPatients && data.pendingPatients.length > 0){
                                    data.pendingPatients.forEach(function(p){
                                        addQueueId.push({"id": p.id});
                                    });

                                    if(addQueueId.length > 0){
                                        mdtServices.addPatients(addQueueId, opt).then(function(){
                                            moderatorServices.list(options);
                                        });
                                    }

                                    $scope.mdtExistedPatients = data.pendingPatients;

                                    if($scope.mdtExistedPatients && $scope.mdtExistedPatients.length > 0) {
                                        $scope.mdtExistedPatients.forEach(function (p) {
                                            p.patient.avatar = '/assets/src/img/icons/patient.png';
                                        });
                                    }
                                }
                            });
                        }
                    }
                }
            }

            function compare(date1, date2) {
                return (new Date(date1.getFullYear(), date1.getMonth(), date1.getDate()) - new Date(date2.getFullYear(), date2.getMonth(), date2.getDate()));
            }

            /*--------------private method end----------*/
        }]);
}());
