﻿
(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('ListDoctorGroupsCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        '$timeout',
        'loadingServices',
        'commonService',
        'strings',
        'modalService',
        'groupsServices',
        'userServices',
        'clientServices',
        'websocketServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            $timeout,
            loadingServices,
            commonService,
            strings,
            modalService,
            groupsServices,
            userServices,
            clientServices,
            websocketServices) {

            /*--------variable---------*/
            var user = commonService.getCurrentUser();
            var sid = 0; //$stateParams.id;

            var messageOpts = {
                size: 1000, // set a big value to mock retrieve all.
                page: 1
            };
            // disable global overflow.
            $rootScope.overflowYHide = true;

            // current login user.
            $scope.user = user.account;

            // to mark which item was launched in left nav panel.
            $scope.iNow = 0;

            // to mark which item was launched in top nav.
            $scope.tNow = 0;

            // store all opened chart sessions.
            $scope.activeChartSessions = [];

            // back to dashboard.
            $scope.back = function () {
                $state.go('page.dashboard');
            };
            /*---------end---------*/

            /*--------nav tab--------*/
            $scope.navTab = function (doctorGroup, index) {
                $scope.iNow = index;

                // if empty, add directly.
                if ($scope.activeChartSessions.length == 0) {
                    $scope.activeChartSessions.push(doctorGroup);
                } else {
                    // check whether it is existing, avoid add duplicate.
                    var i = commonService.getIndexOf(doctorGroup, $scope.activeChartSessions);

                    // add if not added before.
                    if (i === -1) {
                        $scope.activeChartSessions.push(doctorGroup);
                        $scope.tNow = $scope.activeChartSessions.length - 1;
                    } else {
                        // set current tab to active in top tabs.
                        $scope.tNow = i;
                    }
                }

                // set this opened session as current active session.
                $scope.currentChartSession = doctorGroup;
                $scope.onlySingleSession = $scope.activeChartSessions.length === 1;

                // get members from this group.
                groupsServices.listMembers({
                    groupId: $scope.currentChartSession.id
                });

                // get messages for this group.
                groupsServices.listMessages($scope.currentChartSession.id, messageOpts);
            };

            /*---------end---------*/

            /*--------top tab--------*/
            $scope.onlySingleSession = $scope.activeChartSessions.length === 1;
            $scope.topTab = function (activeChartSession, index) {
                $scope.tNow = index;
                $scope.currentChartSession = $scope.activeChartSessions[$scope.tNow];

                // update active tab in left nav.
                var index = commonService.getIndexOf($scope.currentChartSession, $scope.doctorGroups);
                $scope.iNow = index != -1 ? index : 0;

                // get members from this group.
                groupsServices.listMembers({
                    groupId: $scope.currentChartSession.id
                });

                // get messages for this group.
                groupsServices.listMessages($scope.currentChartSession.id, messageOpts);
            };

            $scope.removeChartSession = function (activeChartSession, index) {
                $scope.activeChartSessions.splice(index, 1);
                $scope.onlySingleSession = $scope.activeChartSessions.length === 1;

                // set active tab to prevoius tab.
                if ($scope.currentChartSession.id == activeChartSession.id) {
                    $scope.tNow = index > 1 ? index - 1 : 0;
                    $scope.currentChartSession = $scope.activeChartSessions[$scope.tNow];
                } else {
                    var index = commonService.getIndexOf($scope.currentChartSession, $scope.activeChartSessions);
                    $scope.tNow = index != -1 ? index : 0;
                }

                // update active tab in left nav.
                var index = commonService.getIndexOf($scope.currentChartSession, $scope.doctorGroups);
                $scope.iNow = index != -1 ? index : 0;

                // get members from this group.
                groupsServices.listMembers({
                    groupId: $scope.currentChartSession.id
                });

                // get messages for this group.
                groupsServices.listMessages($scope.currentChartSession.id, messageOpts);
            };
            /*---------end---------*/

            /*--------members-------*/
            $scope.isQuitMyself = false;
            $scope.addMembers = function () {
                if ($scope.currentChartSession) {
                    loadingServices.show();
                    modalService.showModal('/assets/src/pages/groups/modal/addMembers/addMembers.html',
                       {
                           controller: 'AddMembersCtrl',
                           data: {
                               doctors: $scope.members,
                               groupId: $scope.currentChartSession.id,
                           },
                           closefn: addMembersCallback
                       });
                }
            };

            $scope.removeMember = function (memberId) {
                $scope.isQuitMyself = false;
                groupsServices.delMembers($scope.currentChartSession.id, memberId);
            };

            $scope.quitSession = function () {
                if ($scope.currentChartSession) {
                    modalService.showModal('/assets/src/pages/confirm/confirm.html',
                       {
                           controller: 'ConfirmCtrl',
                           data: {
                               title: 'COMMON_DELETE_CONFIRM_TITLE',
                               body: 'TEXT_CONFIRM_QUIT_SESSION'
                           },
                           closefn: quitSessionCallback
                       });
                }
            };
            /*---------end---------*/

            /*--------messages-------*/
            var chartInput = document.getElementById('chartInput');
            $scope.message = {
                content: ''
            };
            // attaches that was inserted by user.
            $scope.attaches = [];

            // send message by 'enter' key.
            $scope.sendMessage = function (content, event) {
                if (event.ctrlKey && event.keyCode === 13) { // enter
                    $scope.send(content);
                }
            };

            // send message.           
            $scope.send = function (content) {
                if (content || $scope.selectedPatient) {
                    loadingServices.show();

                    // format attache data. only post id and type.                   
                    var opt = {
                        content: content
                    };

                    if ($scope.attaches) {
                        var arr = formatAttachment($scope.attaches);
                        opt.attaches = arr;
                    }

                    if ($scope.selectedPatient) {
                        opt.patients = [{
                            id: $scope.selectedPatient.id
                        }];
                    }

                    groupsServices.sendMessage($scope.currentChartSession.id, opt);
                }
            };

            // remove special attachment from attaches array before sending to server.
            $scope.removeHealthrecordAttachment = function (attache, index) {
                $scope.attaches.healthrecords.splice(index, 1);
            };

            $scope.removeSnapshotAttachment = function (attache, index) {
                $scope.attaches.snapshots.splice(index, 1);
            };

            /**
             * insert healthrecord and snapshot to chat.
             */
            $scope.addAttachments = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/groups/modal/addAttachment/addAttachment.html',
                   {
                       controller: 'AddAttachmentCtrl',
                       data: {
                       },
                       closefn: addAttachmentsCallback
                   });
            };

            /**
             * insert patient to chat.
             */
            $scope.insertPatient = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/groups/modal/sharePatients/sharePatients.html',
                   {
                       controller: 'SharePatientsCtrl',
                       data: {
                       },
                       closefn: sharePatientsCallback
                   });
            };

            /*
             * clear patient.
             */
            $scope.clearPatient = function () {
                $scope.selectedPatient = null;
            };
            /*---------end---------*/

            /*--------group-------*/
            $scope.newGroup = null;
            $scope.addGroup = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/groups/modal/addGroup/addGroup.html',
                   {
                       controller: 'AddGroupCtrl',
                       data: {
                           userTypes: $scope.usertypes,
                           depts: $scope.depts,
                           user: $scope.user
                       },
                       closefn: addGroupCallback
                   });
            };
            /*---------end---------*/

            /*--------load more-------*/
            $scope.hasMoreChats = false;
            $scope.loadMoreChats = function () {

            };
            /*---------end---------*/

            /*----------launch pc client---------------*/
            $scope.viewAttachment = function (attachment) {
                if (attachment.attachtype == 1) {
                    $scope.viewHealthrecord(attachment);
                } else {
                    $scope.viewSnapshot(attachment);
                }
            };

            // launch pc client to view healthrecord.
            $scope.viewHealthrecord = function (hr) {
                loadingServices.show();

                var patientId = parseInt(hr.patientId);
                clientServices.healthrecord({
                    ticket: user.token,
                    healthrecordId: hr.attachid,
                    healthrecordType: hr.healthrecordType,
                    patientId: patientId
                });
            };

            // launch pc client to view snapshot.
            $scope.viewSnapshot = function (attachment) {
                loadingServices.show();

                var patientId = parseInt(attachment.patientId);
                var snapshotId = parseInt(attachment.attachid);
                clientServices.healthrecord({
                    ticket: user.token,
                    patientId: patientId,
                    healthrecordId: attachment.healthrecordId,
                    healthrecordType: attachment.healthrecordType,
                    snapshotId: snapshotId
                });
            };
            /*----------end pc client---------------*/

            /*----------patient--------------*/
            $scope.gotoPatientDetail = function (patient) {
                $state.go('page.patient.detail', {
                    id: patient.id,
                    fs: true
                });
            };
            /*----------end patient---------------*/

            // init.
            function init() {
                loadingServices.show();

                // get groups list.                
                groupsServices.list({
                    count: 0 // 0-> all
                });

                // get all user type
                userServices.getUserTypelist();

                // get all dept
                userServices.getDeptValues();

                // created new if websocket instance is not created.
                if (!$rootScope.ws) {
                    $rootScope.ws = websocketServices.createChannel(user.token);
                }
            }

            init();

            /*--------private--------*/
            function addMembersCallback(data) {
                // retrieve speical group to update UI with updated data.              
                // get members from this group.
                groupsServices.listMembers({
                    groupId: data.groupId
                });
            }

            // callback after closing confirm modal for quiting session.
            function quitSessionCallback() {
                var index = commonService.getIndexOf($scope.user, $scope.members, function (item, arr) {
                    var ind = -1;
                    if (arr && arr.length != 0) {
                        arr.forEach(function (v, i) {
                            if (item.userId === v.userId) {
                                ind = i;
                            }
                        });
                    }

                    return ind;
                });

                if (index != -1) {
                    var memberId = $scope.members[index].id;
                    $scope.isQuitMyself = true;
                    $scope.newGroup = null;
                    groupsServices.delMembers($scope.currentChartSession.id, memberId);
                }
            }

            // callback after closing add group modal.
            function addGroupCallback(newGroup) {
                $scope.newGroup = newGroup;
                $scope.activeChartSessions.push(newGroup);

                groupsServices.list({
                    count: 0 // 0-> all
                });
            }

            // callback after closing add attachments.
            function addAttachmentsCallback(attachements) {
                $scope.attaches = attachements;
            }

            function syncData(data) {
                // if empty, add directly.
                if ($scope.activeChartSessions.length == 0) {
                    $scope.activeChartSessions.push(data);
                } else {
                    // check whether it is existing.
                    var i = commonService.getIndexOf(data, $scope.activeChartSessions);

                    // update the values with the updated data.
                    if (i !== -1) {
                        $scope.activeChartSessions[i] = data;
                    } else {
                        $scope.activeChartSessions.push(data);
                    }
                }

                // updated current chart session UI.
                $scope.currentChartSession = data;

                // update latest message for each group.
                if (messageItems && messageItems.count != 0) {
                    $scope.doctorGroups[$scope.iNow].message = messageItems.data[messageItems.data.length - 1];
                }
            }

            function formatAttachment(attachments) {
                var arr = [];

                if (attachments) {
                    // format healthrecord.
                    if (attachments.healthrecords && attachments.healthrecords.length != 0) {
                        attachments.healthrecords.forEach(function (hr) {
                            arr.push({
                                attachid: hr.id,
                                attachtype: 1
                            });
                        });
                    }

                    // format snapshots.
                    if (attachments.snapshots && attachments.snapshots.length != 0) {
                        attachments.snapshots.forEach(function (sn) {
                            arr.push({
                                attachid: sn.id,
                                attachtype: 2
                            });
                        });
                    }
                }

                return arr;
            }

            function sharePatientsCallback(patient) {
                $scope.selectedPatient = patient;
            }
            /*---------end---------*/

            /*--------event------*/
            $scope.$on('xg.scroll.bottom', function () {
                //chartInput = chartInput ? chartInput : document.getElementById('chartInput');
                //chartInput.focus();
                //chartInput.scrollIntoView();
            });

            $scope.$on('group.list.done', function (event, result) {
                loadingServices.hide();
                $scope.doctorGroups = result.data;

                if ($scope.doctorGroups && $scope.doctorGroups.length != 0) {
                    // store currect active sessions.
                    var ind = 0;
                    if (sid && parseInt(sid)) {
                        var index = commonService.getIndexOf({
                            id: parseInt(sid)
                        }, $scope.doctorGroups);

                        ind = index != -1 ? index : 0;
                    }
                    $scope.currentChartSession = $scope.doctorGroups[ind];

                    // put current session in queue.
                    if ($scope.activeChartSessions && $scope.activeChartSessions.length != 0) {
                        // to avoid add duplicate item.
                        var index = commonService.getIndexOf($scope.currentChartSession, $scope.activeChartSessions);
                        if (index == -1) {
                            $scope.activeChartSessions.push($scope.currentChartSession);
                        }
                    } else {
                        $scope.activeChartSessions.push($scope.currentChartSession);
                    }

                    $scope.onlySingleSession = $scope.activeChartSessions.length === 1;

                    if ($scope.newGroup) {
                        var index = commonService.getIndexOf($scope.newGroup, $scope.activeChartSessions);
                        $scope.tNow = index;
                        $scope.currentChartSession = $scope.activeChartSessions[$scope.tNow];
                    } else {
                        // to resolve quit session issue. it need update currentChartSession object.
                        if ($scope.tNow != 0 && $scope.activeChartSessions && $scope.activeChartSessions.length > $scope.tNow) {
                            $scope.currentChartSession = $scope.activeChartSessions[$scope.tNow];
                        }
                    }

                    // update active tab in left nav.
                    var index = commonService.getIndexOf($scope.currentChartSession, $scope.doctorGroups);
                    $scope.iNow = index != -1 ? index : 0;

                    // get members from this group.
                    groupsServices.listMembers({
                        groupId: $scope.currentChartSession.id
                    });

                    // get messages for this group.
                    groupsServices.listMessages($scope.currentChartSession.id, messageOpts);
                }
            });

            $scope.$on('group.detail.done', function (event, data) {
                loadingServices.hide();
                syncData(data);

                //$rootScope.$broadcast('xg.scroll.bottom');
            });

            $scope.$on('group.members.remove.done', function (event, data) {
                loadingServices.hide();
                if ($scope.isQuitMyself) {
                    // update UI, removed current chart session from UI.
                    var index = commonService.getIndexOf($scope.currentChartSession, $scope.activeChartSessions);
                    if (index != -1) {
                        $scope.activeChartSessions.splice(index, 1);
                        $scope.tNow = index > 0 ? index - 1 : 0;
                    }
                    $scope.currentChartSession = null;
                    $scope.members = [];

                    groupsServices.list({
                        count: 0 // 0-> all
                    });
                } else {
                    // get members from this group.
                    groupsServices.listMembers({
                        groupId: $scope.currentChartSession.id
                    });

                    // get messages for this group.
                    groupsServices.listMessages($scope.currentChartSession.id, messageOpts);
                }
            });

            $scope.$on('group.message.add.done', function (event, data) {
                loadingServices.hide();
                $scope.message.content = '';
                $scope.attaches = [];
                $scope.selectedPatient = null;

                // get messages for this group.
                groupsServices.listMessages($scope.currentChartSession.id, messageOpts);
            });

            $scope.$on('group.members.list.done', function (event, data) {
                loadingServices.hide();
                $scope.members = data;
            });

            $scope.$on('group.messages.list.done', function (event, data) {
                loadingServices.hide();
                //$scope.messageItems = data;

                var session = $scope.activeChartSessions[$scope.tNow];
                session.messageItems = data;
                session.page = session.page || 1;

                $scope.currentChartSession = session;

                // update latest message context in left side.
                $scope.doctorGroups[$scope.iNow].message = session.messageItems.data[session.messageItems.data.length - 1];

                $rootScope.$broadcast('xg.scroll.bottom');
            });

            $scope.$on('usertype.list.done', function (event, data) {
                $scope.usertypes = data;
            });

            $scope.$on('dept.list.done', function (event, data) {
                $scope.depts = data;
            });

            // healthrecord/snapshot 
            $scope.$on('hr.open.done', function (event, path) {
                $scope.hrPath = path;

                $timeout(function () {
                    document.getElementById('healthrecordlink').click();
                    loadingServices.hide();
                }, 0);
            });
            $scope.$on('hr.open.failed', loadingServices.hide);

            // websocket, listening message received broadcast.
            $scope.$on(strings.websocket.topic.doctorSocial, function (event, result) {
                var message;
                if (result) {
                    message = groupsServices.formatMessage(result.message);

                    // find which doctor group does the message belong to.
                    var i = commonService.getIndexOf(result, $scope.doctorGroups);

                    if (i != -1) {
                        $scope.$apply(function () {
                            $scope.doctorGroups[i].message = message;

                            // if the current active session don't have message.
                            if(!$scope.doctorGroups[i].messageItems && $scope.currentChartSession.id===result.id){
                                // get messages for this group.
                                groupsServices.listMessages(result.id, messageOpts);
                            }else{
                                $scope.doctorGroups[i].messageItems.data.push(message);
                            }                            
                        });
                    }
                }
            });

            $scope.$on('$destroy', function () {
                $rootScope.overflowYHide = false;
                // clean up.
                $scope = null;
            });
            /*--------end------*/
        }]);
})();