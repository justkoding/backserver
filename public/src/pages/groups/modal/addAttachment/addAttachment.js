﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('AddAttachmentCtrl', ['$scope',
        '$timeout',
        '$translate',
        'loadingServices',
        'messageService',
        'patientServices',
        'groupsServices',
        'healthrecordServices',
        'snapshotsServices',
        'commonService',
        '$modalInstance',
        'data',
        function ($scope,
            $timeout,
            $translate,
            loadingServices,
            messageService,
            patientServices,
            groupsServices,
            healthrecordServices,
            snapshotsServices,
            commonService,
            $modalInstance,
            data) {

            var params = {
                page: 1,
                size: 15,
                dept: -1,
                filter: ''
            };

            loadingServices.hide();

            // deep copy
            $scope.data = JSON.parse(JSON.stringify(data));

            // to mark which patient was selected in left panel.
            $scope.iNow = 0;

            // store all attachments which are selected by user.
            $scope.selectedAttachment = {
                healthrecords: [],
                snapshots: []
            };

            // search
            var timer = null;
            $scope.filter = '';
            $scope.search = function () {
                $timeout.cancel(timer);
                params.filter = $scope.filter;

                timer = $timeout(function () {
                    loadingServices.show();
                    patientServices.list(params);
                }, 300);
            };

            // patient
            $scope.onPatient = function (patient, index) {
                $scope.iNow = index;

                // get healthrecords under current patient.
                healthrecordServices.list(patient.id);
            };

            // healthrecord:
            $scope.backToHealthrecords = function () {
                $scope.isSnapshotPage = false;
                // todo.
            };

            $scope.onHealthrecord = function (hr, index) {
                hr.checked = !hr.checked;

                // push/pop selected healthrecord from array.
                var index = commonService.getIndexOf(hr, $scope.selectedAttachment.healthrecords);
                if (hr.checked) {
                    if (index == -1) {
                        $scope.selectedAttachment.healthrecords.push(hr);
                    }
                } else {
                    if (index !== -1) {
                        $scope.selectedAttachment.healthrecords.splice(index, 1);
                    }
                }
            };

            $scope.removeHealthrecordAttachment = function (hr, index) {
                var i = commonService.getIndexOf(hr, $scope.healthrecords);
                if (i != -1) {
                    $scope.healthrecords[i].checked = false;
                }
                $scope.selectedAttachment.healthrecords.splice(index, 1);
            };

            // snapshot 
            $scope.isSnapshotPage = false;
            $scope.gotoSnapshot = function (hr) {
                $scope.isSnapshotPage = true;
                snapshotsServices.list({
                    healthrecordId: hr.id
                });
            };

            $scope.onSnapshot = function (sn, index) {
                sn.checked = !sn.checked;

                // push/pop selected snapshot from array.
                var index = commonService.getIndexOf(sn, $scope.selectedAttachment.snapshots);
                if (sn.checked) {
                    if (index == -1) {
                        $scope.selectedAttachment.snapshots.push(sn);
                    }
                } else {
                    if (index !== -1) {
                        $scope.selectedAttachment.snapshots.splice(index, 1);
                    }
                }
            };

            $scope.removeSnapshotAttachment = function (sn, index) {
                var i = commonService.getIndexOf(sn, $scope.snapshots);
                if (i != -1) {
                    $scope.snapshots[i].checked = false;
                }
                $scope.selectedAttachment.snapshots.splice(index, 1);
            };

            // modal.
            $scope.ok = function () {
                // deep copy
                var data = JSON.parse(JSON.stringify($scope.selectedAttachment));
                $modalInstance.close(data);
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            function init() {
                loadingServices.show();
                patientServices.list(params);
            }

            init();
            /*--------events------*/
            $scope.$on('patient.list.done', function (event, result) {
                $scope.patients = result.data;
                if ($scope.patients && $scope.patients.length != 0) {
                    healthrecordServices.list($scope.patients[0].id);
                }

                loadingServices.hide();
            });

            $scope.$on('healthrecord.list.done', function (event, result) {
                $scope.healthrecords = result;

                // check whether it is already selected.
                if ($scope.healthrecords &&
                    $scope.healthrecords.length != 0 &&
                    $scope.selectedAttachment.healthrecords.length != 0) {

                    $scope.healthrecords.map(function (hr) {
                        var index = commonService.getIndexOf(hr, $scope.selectedAttachment.healthrecords);

                        if (index != -1) {
                            hr.checked = true;
                        }
                        return hr;
                    });
                }
            });

            $scope.$on('snapshot.list.done', function (event, result) {
                $scope.snapshots = result.data;

                // check whether it is already selected.
                if ($scope.snapshots &&
                    $scope.snapshots.length != 0 &&
                    $scope.selectedAttachment.snapshots.length != 0) {

                    $scope.snapshots.map(function (sn) {
                        var index = commonService.getIndexOf(sn, $scope.selectedAttachment.snapshots);

                        if (index != -1) {
                            sn.checked = true;
                        }
                        return sn;
                    });
                }
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });
            /*--------end------*/
        }]);
})();