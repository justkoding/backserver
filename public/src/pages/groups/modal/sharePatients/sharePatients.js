(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('SharePatientsCtrl', ['$scope',
        '$rootScope',
        '$timeout',
        '$translate',
        'userServices',
        '$modalInstance',
        'commonService',
        'patientServices',
        'data',
        function ($scope,
                  $rootScope,
                  $timeout,
                  $translate,
                  userServices,
                  $modalInstance,
                  commonService,
                  patientServices,
                  data) {

            commonService.progress.hide();

            // deep copy
            var data = JSON.parse(JSON.stringify(data));

            /*----------modal------------*/
            $scope.ok = function () {
                if ($scope.patientInfo) {
                    var info = JSON.parse(JSON.stringify($scope.patientInfo.patient));
                    $modalInstance.close(info);
                }                
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
            /*----------end------------*/

            /*----------search------------*/
            var timer = null;
            $scope.search = '';
            $scope.searchChanged = function () {
                $timeout.cancel(timer);

                timer = $timeout(getPatients, 500);
            };
            /*----------end------------*/

            /*----------patients------------*/
            $scope.patients = [];
            $scope.patientInfo = null;

            /**
             * toggle selected patient
             */
            $scope.toggleSelected = function (patient, $index) {
                var checked = patient.checked;

                // reset checked status to false.
                $scope.patients.forEach(function (p) {
                    p.checked = false;
                });

                // if has checked, do unchecked operation.
                if (checked) {
                    patient.checked = false;
                    // clear this info to refresh UI.
                    $scope.patientInfo = null;
                } else {
                    patient.checked = true;

                    // get this patient summary.
                    getPatientInfo(patient.id);
                }
            };
            /*----------end------------*/

            /*----------events------------*/
            $scope.$on('$destroy', function () {
                // clear
            });

            /*------------end------------*/

            /*-----------private method-----------*/
            function getPatients() {
                patientServices.relation({
                    filter: $scope.search
                }, true).then(function (result) {
                    if (result) {
                        $scope.patients = result;

                        if (result && result.length != 0) {
                            $scope.patients[0].checked = true;

                            // get this patient summary.
                            getPatientInfo($scope.patients[0].id);
                        } else {
                            $scope.getPatientInfo = null;
                        }
                    }
                });
            }

            function getPatientInfo(pId) {
                patientServices.summary(pId, true).then(function (result) {
                    $scope.patientInfo = result;
                    console.log($scope.patientInfo);
                });
            }

            function init() {
                getPatients();
            }
            init();
            /*--------------end-------------------*/
        }]);
})();