﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('AddGroupCtrl', ['$scope',
        '$timeout',
        '$translate',
        'loadingServices',
        'messageService',
        'userServices',
        'groupsServices',
        'commonService',
        'hospitalsServices',
        '$modalInstance',
        'data',
        'strings',
        function ($scope,
            $timeout,
            $translate,
            loadingServices,
            messageService,
            userServices,
            groupsServices,
            commonService,
            hospitalsServices,
            $modalInstance,
            data,
            strings) {
            commonService.progress.hide();
            var requestNum = 2;

            // set default params for retrieving doctors
            var options = {
                // to filter by doctor title. only show the users whose title is 'doctor'
                // 2-> 主任医师,3-> 副主任医师,4-> 主治医师,5-> 医师
                title: strings.doctorTitles,
                size: 1000, // set big value for mocking get all data.
                page: 1
            };

            /*-----tab-----*/
            // tab
            $scope.iNow = 1;
            $scope.tab = function (iTab) {
                var opt;

                $scope.iNow = iTab;

                // department
                if (iTab === 1) {
                    opt = angular.extend({}, options, {
                        hospital: $scope.currentHospital.id,
                        dept: $scope.currentDept.id
                    });
                } else if (iTab === 2) { // search doctor name changed
                    opt = angular.extend({}, options, {
                        hospital: $scope.currentHospital.id,
                        filter: $scope.search
                    });
                }

                userServices.list(opt, true).then(function (result) {
                    $scope.doctors = result ? result.data : [];
                    filterExistedUser();
                    restoreSelected();
                });
            };
            /*-----end-----*/

            /*-----selected doctors-----*/
            // array of doctors who have existed in group
            $scope.data = JSON.parse(JSON.stringify(data));
            if($scope.data.user){
                $scope.data.user.id = $scope.data.user.userId;
            }
            // array of selected doctors
            $scope.selectedDoctors = [];

            // remove doctor from selected doctors list.
            $scope.unselected = function (doctor, $index) {
                var index = commonService.getIndexOf(doctor, $scope.doctors);
                $scope.selectedDoctors.splice($index, 1);
                $scope.doctors[index].checked = false;
            };

            // add doctor to selected list.
            $scope.selected = function (doctor, $index) {
                var index = commonService.getIndexOf(doctor, $scope.selectedDoctors);

                if (doctor.checked) {
                    if (index != -1) {
                        $scope.selectedDoctors.splice(index, 1);
                    }
                } else {
                    if (index == -1) {
                        $scope.selectedDoctors.push(doctor);
                    }
                }

                $scope.doctors[$index].checked = !($scope.doctors[$index].checked);
            };
            /*-----end-----*/

            /*-----condition changes------*/
            // department select changed
            $scope.deptChanged = function (dept) {
                var opt = angular.extend({}, options, {
                    hospital: $scope.currentHospital.id,
                    dept: dept.id
                });
                userServices.list(opt, true).then(function (result) {
                    $scope.doctors = result ? result.data : [];
                    filterExistedUser();
                    restoreSelected();
                });
            };

            // hospital select changed
            $scope.hospitalChanged = function (hospital) {
                var opt;
                $scope.currentHospital = hospital;

                // department changed
                if ($scope.iNow === 1) {
                    opt = angular.extend({}, options, {
                        hospital: hospital.id,
                        dept: $scope.currentDept.id
                    });
                } else { // search doctor name changed
                    opt = angular.extend({}, options, {
                        hospital: hospital.id,
                        filter: $scope.search
                    });
                }

                userServices.list(opt, true).then(function (result) {
                    $scope.doctors = result ? result.data : [];
                    filterExistedUser();
                    restoreSelected();
                });
            };

            // search text changed
            var timer = null;
            $scope.search = '';
            $scope.searchChanged = function (text) {
                $timeout.cancel(timer);

                // delay search
                timer = $timeout(function () {
                    var opt = angular.extend({}, options, {
                        hospital: $scope.currentHospital.id,
                        filter: text
                    });
                    userServices.list(opt, true).then(function (result) {
                        $scope.doctors = result ? result.data : [];
                        filterExistedUser();
                        restoreSelected();
                    });
                }, 300);
            };

            /*-----end-----*/

            /*-----event-----*/
            // add memebers to group
            $scope.$on('group.add.done', function (event, result) {
                $modalInstance.close(result.data);
            });

            $scope.$on('group.add.failed', function (event, data) {
                // todo
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });

            //departments
            $scope.$on('dept.list.done', function (event, data) {
                $scope.depts = data || [];
                if (data.length != 0) {
                    $scope.currentDept = data[0];
                }

                // retrieve doctors.
                if (--requestNum == 0) {
                    if ($scope.currentDept && $scope.currentHospital) {
                        var opt = angular.extend({}, options, {
                            hospital: $scope.currentHospital.id,
                            dept: $scope.currentDept.id
                        });
                        userServices.list(opt, true).then(function (result) {
                            $scope.doctors = result ? result.data : [];
                            filterExistedUser();
                            restoreSelected();
                        });
                    }
                }
            });


            //hospitals
            $scope.$on('hospitals.list.done', function (event, data) {
                $scope.hospitals = data || [];
                if (data.length != 0) {
                    $scope.currentHospital = data[0];
                }

                // retrieve doctors.
                if (--requestNum == 0) {
                    if ($scope.currentDept && $scope.currentHospital) {
                        var opt = angular.extend({}, options, {
                            hospital: $scope.currentHospital.id,
                            dept: $scope.currentDept.id
                        });
                        userServices.list(opt, true).then(function (result) {
                            $scope.doctors = result ? result.data : [];
                            filterExistedUser();
                            restoreSelected();
                        });
                    }
                }
            });

            // modal.
            $scope.ok = function () {
                var doctorsId = [];

                if ($scope.selectedDoctors && $scope.selectedDoctors.length != 0) {

                    $scope.selectedDoctors.forEach(function (d){
                        doctorsId.push(d.id);
                    });
                }

                //add the originalUser
                doctorsId.push($scope.data.user.userId);

                // call api to add members to group.
                groupsServices.add(doctorsId);

            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            /*-----end-----*/

            init();

            function init() {
                // get all hospitals
                hospitalsServices.list();

                // get all departments
                userServices.getDeptlist();
            }

            // if user has selected doctor and then try to refresh doctor list.
            // the selected doctor will be marked as selected if it is existing in new doctors list.
            function restoreSelected() {
                if ($scope.selectedDoctors.length != 0 &&
                    $scope.doctors.length != 0) {
                    // single selected.
                    var s = $scope.selectedDoctors[0];
                    $scope.doctors.forEach(function (v) {
                        if (v.id == s.id) {
                            v.checked = true;
                        }
                    });
                }
            }

            //filter users whom  has existed in group
            function filterExistedUser(){
                if($scope.data.user){
                    var index = -1;
                    index = commonService.getIndexOf($scope.data.user, $scope.doctors);
                    if(index != -1){
                        $scope.doctors.splice(index, 1);
                    }
                }
            }
        }]);
})();