﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('ListOfflineCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$stateParams',
        function ($scope,
            $rootScope,
            $cookieStore,
            $stateParams) {
       
        // search        
        var term = $stateParams.search ? $stateParams.search : '';
        $scope.filter = term;
    }]);
})();