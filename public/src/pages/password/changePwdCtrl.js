﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('changePwdCtrl', ['$scope',
        '$translate',
        'loadingServices',
        'messageService',
        'passwordServices',
        '$modalInstance',
        'data',
        function ($scope,
            $translate,
            loadingServices,
            messageService,
            passwordServices,
            $modalInstance,
            data) {
            loadingServices.hide();
            $scope.data = {
                oldPassword: '',
                newPassword: '',
                confirmPassword: ''
            };

            $scope.notMatchOldNewPwd = false;
            $scope.changePwdFailed = false;

            // new password shouldn't be same as orignal pwd.
            $scope.equalOrignalPwd = false;

            $scope.ok = function (form) {
                if (form.$valid) {
                    loadingServices.show();
                    passwordServices.changePwd($scope.data).then(null, function () {                        
                        loadingServices.hide();
                        $scope.changePwdFailed = true;

                        // reset form
                        form.oldPassword.$setUntouched();
                        form.newPassword.$setUntouched();
                        form.confirmPassword.$setUntouched();
                    });
                }
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            /*--------events------*/
            $scope.$on('password.change.done', function (event, data) {
                loadingServices.hide();
                $scope.changePwdFailed = false;

                $modalInstance.close($scope.data);
            });

            $scope.$watch('data.newPassword', function (newVal, oldVal) {
                // check whether new password equals old password.
                if (newVal && 
                    $scope.data.oldPassword &&
                    newVal === $scope.data.oldPassword) {
                    $scope.equalOrignalPwd = true;
                    return;
                }
                $scope.equalOrignalPwd = false;

                // check whether confirm password equals new password.
                if (newVal &&
                    $scope.data.confirmPassword &&
                    newVal !== $scope.data.confirmPassword) {
                    $scope.notMatchOldNewPwd = true;
                } else {
                    $scope.notMatchOldNewPwd = false;
                }

                $scope.changePwdFailed = false;
            });

            $scope.$watch('data.confirmPassword', function (newVal, oldVal) {
                if (newVal &&
                    $scope.data.newPassword &&
                    newVal !== $scope.data.newPassword) {
                    $scope.notMatchOldNewPwd = true;
                } else {
                    $scope.notMatchOldNewPwd = false;
                }

                $scope.changePwdFailed = false;
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });
            /*--------end------*/
        }]);
})();