﻿(function () {
    'use strict';

    angular.module('app').controller('ErrorController', ['$scope', '$rootScope',
        function ($scope, $rootScope) {           
            /*---------event--------------*/
            $scope.$on('$destroy', function () {
                // clean up.
            });
            /*---------end--------------*/
        }]);
}());
