﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('RightbarCtrl', ['$scope',
        '$state',
        '$cookieStore',
        'navServices',
        'commonService',
        'strings',
        'userServices',
        function ($scope,
            $state,
            $cookieStore,
            navServices,
            commonService,
            strings,
            userServices) {

            // display current user profile.           
            var data = commonService.getCurrentUser();
            $scope.permissions = strings.permissions;

            if (!data) {
                $state.go('login');
                return;
            }

            // user types.
            $scope.usertypes = strings.usertypes;

            // user's profile
            data.account = data.account ? data.account : {};
            $scope.userAvatar = data.account.avatar ? '/users/' + data.account.userId + '/avatar' : '/assets/src/img/icons/defaultuser.png';
            $scope.user = data.account;

            // navigation
            $scope.index = navServices.getCurrentActiveNavItem();
            //$scope.navItems = navServices.getNavItems();

            $scope.navTo = function (state, ind) {
                $scope.index = ind;
                $state.go(state, null, { 'reload': true });
            };

            $scope.gotoUserProfile = function () {
                var user = commonService.getCurrentUser().account;
                $state.go('page.profile', {
                    id: user.accountId
                });
            };

            // event listener.
            $scope.$on('$locationChangeSuccess', function (event, newUrl, oldUrl) {
                $scope.index = navServices.getCurrentActiveNavItem();
            });

            $scope.$on('user.update.avatar', function (event, url) {
                var time = new Date();
                $scope.userAvatar = url + '?timestamp=' + time.getTime();
                data.account.avatar = '/' + data.account.userId;
                $cookieStore.put('user', data);
            });
        }]);
})();