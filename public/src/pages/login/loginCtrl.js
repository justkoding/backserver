﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('LoginCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        'strings',
        'commonService',
        'loginServices',
        'navServices',
        'websocketServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            strings,
            commonService,
            loginServices,
            navServices,
            websocketServices) {

            $scope.user = {
                account: '',
                password: '',
                avatar: strings.defaultAvatar,
                checked: false
            };

            $scope.validFailed = false;
            $rootScope.loginPage = true;

            // login
            $scope.ok = function (form) {
                commonService.progress.show();
                $scope.validFailed = false;
                loginServices.login($scope.user);
            };

            $scope.signIn = function (form, event) {
                // 13->enter
                if (form.$valid && event.keyCode === 13) {
                    $scope.ok(form);
                }
            };

            $scope.forgetPwd = function () {
                $state.go('login.forgetpassword');
            };

            $scope.pwdChanged = function () {
                $scope.validFailed = false;
            };

            $scope.accountChanged = function (account) {
                $scope.validFailed = false;
            };

            // auto complete.
            var historyAccounts = $cookieStore.get('accounts');
            $scope.historyAccounts = historyAccounts || [];
            $scope.selectedUser = function (item) {
                if (item) {
                    $scope.user.account = item.title;
                    $scope.user.avatar = item.image;

                    // restore password from history.
                    var index = indexOfHistoryAccounts({ accountName: item.title });
                    if (index !== -1) {
                        $scope.user.password = $scope.historyAccounts[index].password;
                    }
                } else {
                    $scope.user.avatar = defaultAvatar;
                }
            };

            $scope.nameChange = function (name) {
                $scope.user.account = name;
            };

            init();

            function init() {
                commonService.session.clear();

                commonService.resource.getValues(['AUTO_COMPLETE_TEXT_SEARCHING', 'AUTO_COMPLETE_TEXT_NO_RESULTS']).then(function (sources) {
                    var texts = {
                        searching: sources['AUTO_COMPLETE_TEXT_SEARCHING'],
                        noResults: sources['AUTO_COMPLETE_TEXT_NO_RESULTS']
                    };

                    $scope.resource = texts;
                });
            }
            /*--------events-----------*/
            $scope.$on('user.login.done', function (event, data) {
                commonService.progress.hide();

                // create global websocket.
                $rootScope.ws = websocketServices.createChannel(data.token, {
                    onopen: function () {
                        console.log('websocket is connected!!!!');
                    },
                    onmessage: function (result) {
                        console.log('websocket received message: ' + result);
                    },
                    onclose: function () {
                        console.log('websocket is closed');
                        $rootScope.ws = null;
                    }
                });

                if ($scope.historyAccounts.length === 0) {
                    $scope.historyAccounts.push({
                        accountName: data.account.accountName,
                        avatar: data.account.avatar ? '/users/' + data.account.userId + '/avatar' : strings.defaultAvatar,
                        desc: data.account.userTitle ? (data.account.userName + ' ' + data.account.userTitle) : data.account.userName,
                        password: $scope.user.checked ? $scope.user.password : null
                    });

                    // update cookie
                    $cookieStore.put('accounts', $scope.historyAccounts);
                } else {
                    // check whether current account is existing in history accounts array.
                    // add it if not else skip.
                    var index = indexOfHistoryAccounts(data.account);

                    // if don't find.
                    if (index !== -1) {
                        $scope.historyAccounts.splice(index, 1);
                    }

                    $scope.historyAccounts.unshift({
                        accountName: data.account.accountName,
                        avatar: data.account.avatar ? '/users/' + data.account.userId + '/avatar' : strings.defaultAvatar,
                        desc: data.account.userName + ' ' + data.account.usertype,
                        password: $scope.user.checked ? $scope.user.password : null
                    });

                    // update cookie
                    $cookieStore.put('accounts', $scope.historyAccounts);
                }
                var dest = $stateParams.dest;
                var state = dest ? dest.name : navServices.getHomeRoute();
                var parmas = dest ? dest.params : {};
                $state.go(state, parmas);
            });

            $scope.$on('user.login.failed', function (event, data) {
                $scope.validFailed = true;
                $scope.token = null;

                commonService.progress.hide();
            });

            /*-------private-------*/
            function indexOfHistoryAccounts(account) {
                var index = -1;

                if ($scope.historyAccounts && $scope.historyAccounts.length != 0 && account) {
                    $scope.historyAccounts.forEach(function (ha, i) {
                        if (ha.accountName === account.accountName) {
                            index = i;
                        }
                    });
                }

                return index;
            }
            /*---------end----------*/

            /*---------events----------*/
            $scope.$on('$destroy', function () {
                // clean up.
                $scope = null;

                $rootScope.loginPage = false;
            });
            /*--------end-----------*/
        }]);
})();