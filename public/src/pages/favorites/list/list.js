﻿
(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('ListFavoriteCtrl', ['$scope',
        '$rootScope',
        '$state',
        'loadingServices',
        'commonService',
        'strings',
        'modalService',
        'favoritesServices',
        function ($scope,
            $rootScope,
            $state,
            loadingServices,
            commonService,
            strings,
            modalService,
            favoritesServices) {

            /**
             * Go to patient detail page.
             */
            $scope.detail = function (favorite) {
                $state.go('page.patient.detail', {
                    id: favorite.patient.id
                });
            };

            // init.
            function init() {
                loadingServices.show();

                // get favorites list.                
                favoritesServices.list({
                    count: 0 // 0-> all
                });
            }

            init();

            /*--------event------*/
            $scope.$on('favorite.list.done', function (event, result) {
                loadingServices.hide();
                $scope.favorites = result.data;
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope = null;
            });
            /*--------end------*/
        }]);
})();