﻿(function () {
    'use strict';

    angular.module('app').controller('ForgetPasswordController', ['$scope',
        '$state',
        function ($scope,
            $state) {

            $scope.returnToLogin = function () {
                $state.go('login');
            };

            /*---------event--------------*/
            $scope.$on('$destroy', function () {
                // clean up.
            });
            /*---------end--------------*/
        }]);
}());
