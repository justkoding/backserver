﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('FooterCtrl', ['$scope',
       'versionServices',
        function ($scope,
           versionServices) {

            init();

            function init() {
                if (!$scope.version) {
                    // get web app version.
                    versionServices.detail().then(function (result) {
                        $scope.version = result ? result.version : '0.0.0.0';
                    });
                }
            }
        }]);
})();