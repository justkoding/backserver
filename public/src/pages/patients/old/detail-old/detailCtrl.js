﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('DetailPatientCtrl_old', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$translate',
        '$state',
        '$stateParams',
        '$timeout',
        'loadingServices',
        'messageService',
        'patientServices',
        'commonService',
        'modalService',
        'Upload',
        'healthrecordServices',
        'mdtServices',
        'clientServices',
        'favoritesServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $translate,
            $state,
            $stateParams,
            $timeout,
            loadingServices,
            messageService,
            patientServices,
            commonService,
            modalService,
            Upload,
            healthrecordServices,
            mdtServices,
            clientServices,
            favoritesServices) {

            // current login user.
            var user = commonService.getCurrentUser();

            // request count
            var reqCount = 3;

            // get current patient Id.
            var patientId = $stateParams.id ? parseInt($stateParams.id) : -1;

            $scope.isShow = false;
            $scope.showModal = false;
            $scope.buttonClicked = "";
            $scope.toggleModal = function (btnClicked) {
                $scope.buttonClicked = btnClicked;
                $scope.showModal = !$scope.showModal;
            };

            // back
            $scope.back = function () {
                $state.go('page.patient', null, { 'reload': true });
            };

            // edit patient
            $scope.edit = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/patients/addOrEdit/addOrEdit.html',
                   {
                       controller: 'AddorEditPatientCtrl',
                       data: {
                           patient: $scope.patient,
                           isEdit: true
                       },
                       closefn: addorEditPatientCallback
                   });
            };

            // delete patient
            $scope.delete = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/confirm/confirm.html',
                  {
                      controller: 'ConfirmCtrl',
                      data: {
                          title: 'PATIENT_DELETE_CONFIRM_TITLE',
                          body: 'PATIENT_DELETE_CONFIRM_BODY'
                      },
                      closefn: deletePatientCallback
                  });
            };

            // favorite.
            $scope.addFavorite = function (favorite) {
                if (favorite.isAdd) {
                    loadingServices.show();
                    modalService.showModal('/assets/src/pages/patients/favorite/favorite.html',
                       {
                           controller: 'FavoriteCtrl',
                           data: {
                               patient: favorite.data
                           },
                           closefn: addToFavoriteCallback
                       });
                } else {
                    loadingServices.show();
                    modalService.showModal('/assets/src/pages/confirm/confirm.html',
                       {
                           controller: 'ConfirmCtrl',
                           data: {
                               title: 'COMMON_DELETE_CONFIRM_TITLE',
                               body: 'COMMON_REMOVE_FROM_FAVORITES_CONFIRM_BODY'
                           },
                           closefn: removeFavoriteCallback
                       });
                }
            };

            /*--------patient material------*/
            $scope.addPatientMaterial = function (mr) {
                var url = '/assets/src/pages/patients/material/addNewMaterial.html';

                if (mr && mr.mdt.data.mdtid) {
                    url = '/assets/src/pages/patients/material/addNewMaterialOrFromLibrary.html';
                }

                loadingServices.show();
                modalService.showModal(url,
                   {
                       controller: 'AddPatientMaterialCtrl',
                       data: {
                           patientId: $scope.patient.id,
                           healthrecords: $scope.healthrecords
                       },
                       closefn: function (data) {
                           if (mr) {
                               addorEditMaterialCallback({
                                   ids: data.ids,
                                   mdtid: mr.mdt.data.mdtid
                               });
                           } else {
                               retrieveHealthrecords();
                           }
                       }
                   });
            };

            // xgTrash
            $scope.deleteMaterial = function (hrId) {
                loadingServices.show();
                healthrecordServices.del(hrId);
            };

            $scope.deleteMaterialFromMdt = function (hrId, mdtId) {
                loadingServices.show();
                mdtServices.deleteHealthrecord({
                    mdtid: mdtId,
                    hrdId: hrId
                });
            };

            $scope.gotoMdt = function (mr) {
                $state.go('page.meeting.detail', { id: mr.mdt.data.mdtid });
            };

            $scope.view = function (hr, mr) {
                var mdtid = mr ? mr.mdt.data.mdtid : 0;
                loadingServices.show();
                clientServices.healthrecord({
                    ticket: user.token,
                    mdtId: parseInt(mdtid),
                    healthrecordId: hr.id,
                    healthrecordType: hr.type,
                    patientId: patientId
                });
            };
            $scope.startScanner = function () {
                loadingServices.show();
                clientServices.scanner(user.token, patientId);
            };

            $scope.afterSave = function (item) {

                var option = {},
                    data = {};

                data.note = item.description;
                option.id = item.id;
                option.data = data;

                healthrecordServices.update(option);
            };


            /*--------end------*/

            /*--------event------*/
            $scope.$on('patient.detail.done', function (event, result) {
                $scope.patient = result.data;

                if (--reqCount === 0) {
                    loadingServices.hide();
                }
            });

            $scope.$on('patient.remove.done', function (event, data) {
                loadingServices.hide();
                $state.go('page.patient', null, { 'reload': true });
            });

            $scope.$on('healthrecord.list.done', function (event, data) {
                $scope.healthrecords = data;

                if (--reqCount === 0) {
                    loadingServices.hide();
                }
            });

            $scope.$on('hr.open.done', function (event, path) {
                $scope.mockPath = path;

                $timeout(function () {
                    document.getElementById('mocklink').click();
                    loadingServices.hide();
                }, 0);
            });

            $scope.$on('hr.open.failed', function (event, path) {
                loadingServices.hide();
            });

            $scope.$on('scanner.open.done', function (event, path) {
                $scope.mockPath = path;

                $timeout(function () {
                    document.getElementById('mocklink').click();
                    loadingServices.hide();
                }, 0);
            });

            $scope.$on('scanner.open.failed', function (event, path) {
                loadingServices.hide();
            });

            $scope.$on('patient.mdts.failed', function (event, path) {
                loadingServices.hide();
            });

            $scope.$on('mdt.healthrecords.add.done', retrieveHealthrecords);
            $scope.$on('mdt.healthrecord.delete.done', retrieveHealthrecords);
            $scope.$on('healthrecord.delete.done', delHealthrecordSuccess);
            $scope.$on('healthrecord.delete.failed', delHealthrecordFailed);
            $scope.$on('mdt.healthrecord.delete.done', delHealthrecordSuccess);
            $scope.$on('mdt.healthrecord.delete.failed', delHealthrecordFailed);

            // favorites
            $scope.$on('favorite.remove.done', function () {
                patientServices.detail(patientId);
            });
           
            $scope.$on('favorite.add.done', function () {
                patientServices.detail(patientId);
            });

            $scope.$on('favorite.remove.failed', commonService.progress.hide);
            $scope.$on('favorite.add.failed', commonService.progress.hide);

            $scope.$on('$destroy', function () {
                // clean up.
            });

            $scope.$watch('hr.description', function (newVal, oldVal) {

            });
            /*--------end------*/

            /*--------private------*/
            function init() {
                loadingServices.show();

                // get patient detail by id.
                patientServices.detail(patientId);

                // get all healthrecords list.
                healthrecordServices.list(patientId);

                // get all mdts for current patient
                patientServices.patientMdtHealthrecords(patientId).then(function (mRecords) {

                    if (mRecords && mRecords.length != 0) {
                        mRecords.forEach(function (f) {
                            if (f.mdt.data.date) {
                                f.mdt.data.date = getDateTime(f.mdt.data.date, "00.00");
                            }
                        });
                    }

                    $scope.mRecords = mRecords;
                    if (--reqCount === 0) {
                        loadingServices.hide();
                    }
                });
            }

            var getDateTime = function (date, time) {
                var year = date.substring(0, 4);
                var month = date.substring(5, 7);
                var day = date.substring(8, 10);
                return new Date(year, month, day, 0, 0, 0, 0).toJSON();
            };

            function addorEditPatientCallback(data) {
                $scope.patient = data;
            }

            function deletePatientCallback() {
                loadingServices.show();
                patientServices.remove($scope.patient.id);
            }

            function addorEditMaterialCallback(data) {
                loadingServices.show();
                mdtServices.addHealthrecords({
                    ids: data.ids
                }, {
                    mdtid: data.mdtid
                });
            }

            function retrieveHealthrecords() {
                reqCount = 2;

                loadingServices.show();
                // refresh them after updated.
                healthrecordServices.list(patientId);

                // get all mdts for current patient
                patientServices.patientMdtHealthrecords(patientId).then(function (mRecords) {
                    $scope.mRecords = mRecords;

                    if (--reqCount === 0) {
                        loadingServices.hide();
                    }
                });
            }

            function delHealthrecordSuccess() {
                retrieveHealthrecords();
            }

            function delHealthrecordFailed() {
                loadingServices.hide();
            }

            function addToFavoriteCallback() {
            }

            function removeFavoriteCallback() {
                favoritesServices.remove(patientId);
            }
            /*--------end------*/

            // init
            init();
        }]);
})();