﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('ConclusionSummaryPatientCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$stateParams',
        'historiesServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $stateParams,
            historiesServices) {
       
            var historyId = null;

            /*--------diagnostic summary------*/           
            $scope.editSummary = function () {
                // todo
            };
            /*--------end------*/

            /*--------event------*/
            // watch $scope.selectedHistoryInformation
            $scope.$watch('selectedHistoryInformation', function (newVal, oldVal) {
                if (newVal) {                
                    // get diagnosis summary.
                    if (newVal.id) {
                        historyId = newVal.id;
                        historiesServices.summary(newVal.id).then(function (data) {
                            $scope.summaries = data;
                        });
                    }                    
                }
            });
           
            $scope.$on('patient.edit.finished', function (event, data) {
                if (historyId) {
                    historiesServices.summary(historyId).then(function (data) {
                        $scope.summaries = data;
                    });
                }
            });

            $scope.$on('$destroy', function () {
                $scope.summaries = null;
            });
            /*--------end------*/
    }]);
})();