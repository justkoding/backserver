﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('DiagnosisCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        '$timeout',
        'commonService',
        'modalService',
        'clientServices',
        'historiesServices',
        'suggestionsServices',
        'mdtServices',
        'strings',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            $timeout,
            commonService,
            modalService,
            clientServices,
            historiesServices,
            suggestionsServices,
            mdtServices,
            strings) {

            // mock. only history id = 3 has values by now.
            //$scope.historyId = $scope.selectedHistoryInformation.id;
            var user = commonService.getCurrentUser();
            var rawSuggestions = [];
            $scope.now = new Date();
            $scope.enlarge = false;            

            /*--------tab------*/
            $scope.slide = function (ga, iTab) {
                ga.iNow = iTab;
            }
            /*--------end------*/

            /*--------magnify content------*/
            $scope.enlargeContent = function (content) {
                if (!content) {
                    return;
                }

                $scope.enlarge  = true;
                modalService.showModal('/assets/src/pages/patients/modal/enlarge/enlarge.html',
                    {
                        controller: 'EnlargeCtrl',
                        backdrop: true,
                        windowClass: 'width-scale-50',
                        data: {
                            enlargeContent: content
                        },
                        closefn: function () {
                        }
                    });
            };
            /*--------tab------*/

            /*--------join meeting------*/
            $scope.joinMeeting = function (diagnosis) {
                commonService.progress.show();
                clientServices.meeting(user.token, parseInt(diagnosis.mdt.id));
            };

            $scope.meetingDetail = function (diagnosis) {
                $state.go('page.meeting.detail', {
                    id: diagnosis.mdt.id
                });
            };
            /*--------end------*/

            /*-------invite doctor---------*/
            $scope.inviteDoctors = function (diagnosis) {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/meeting/modals/addAttendence/addAttendence.html',
                  {
                      controller: 'AddAttendenceCtrl',
                      data: {
                          attendees: diagnosis.mdt.attendees,
                          isDisabledAttenance: true
                      },
                      closefn: function (doctors) {
                          var ids = [];
                          if (doctors && doctors.length != 0) {
                              doctors.forEach(function (v) {
                                  ids.push(v.id);
                              });
                          }
                          // save selected doctors to db.
                          mdtServices.addAttendees(ids, {
                              id: diagnosis.mdt.id,
                              skipBroadcast: true
                          }).then(function () {
                              mdtServices.getAttendees({
                                  id: diagnosis.mdt.id
                              });
                          }, commonService.progress.hide);
                      }
                  });
            };
            /*--------end---------*/

            /*--------private------*/
            init();
            function init() {
                if ($scope.historyId) {
                    historiesServices.mdtsList($scope.historyId);
                }
            }

            function retrieveSuggestions(mdtId) {
                var options = {
                    doctor: 0,  // doctor id, 0-> all
                    mdt: mdtId,  // mdt id, 0-> all
                    history: $scope.historyId
                };

                suggestionsServices.list(options, true).then(function (suggestions) {
                    // store suggestions to local variable.
                    rawSuggestions = suggestions;
                    combineSuggestion(suggestions, mdtId);
                });
            }

            function combineSuggestion(suggestions, mdtId) {
                if ($scope.diagnosises && $scope.diagnosises.length != 0) {
                    $scope.diagnosises.map(function (d) {
                        if (d.mdt && d.mdt.id == mdtId) {
                            if (d.mdt.groupedAttendees && d.mdt.groupedAttendees.length != 0) {
                                d.mdt.groupedAttendees.forEach(function (ga) {
                                    ga.iNow = 0;
                                    if (ga && ga.attendees && ga.attendees.length != 0) {
                                        ga.suggestions = [];
                                        ga.attendees.forEach(function (att) {
                                            var index = -1;
                                            for (var i = 0; i < suggestions.length; i++) {
                                                // trim space.
                                                if (suggestions[i].content) {
                                                    suggestions[i].content = suggestions[i].content.trim();
                                                }
                                                
                                                if (att.id === suggestions[i].doctor.id) {
                                                    index = i;
                                                    break;
                                                }
                                            }

                                            if (index != -1) {
                                                ga.suggestions.push(suggestions[index]);
                                            }
                                        });
                                    }
                                });
                            }
                        }

                        return d;
                    });
                }
            }
            /*--------end------*/

            /*--------events------*/
            $scope.$on('history.mdtslist.done', function (event, resp) {
                $scope.diagnosises = resp;
                // to mark the active meeting for patient detail.
                var meeting = null;

                if (resp && resp.length != 0) {
                    for(var i=0; i<resp.length; i++){
                        var v = resp[i];

                        // status: 1-> open, 2-> closed
                        // maybe has multiply active meetings at same time.
                        if (v.mdt.status === 1 && !meeting) {
                            meeting = v.mdt;
                        }
                        retrieveSuggestions(v.mdt.id);
                    }
                }

                // find active meeting when parent page is patient detail.
                if ($scope.fromPatient && meeting) {
                    // send broadcast to notify brother page(suggestion page)
                    // to retrieve suggestions for this meeting.                    
                    $rootScope.$broadcast('find.active.meeting', {
                        meeting: meeting
                    });

                    // update meeting attendees.
                    commonService.session.set('meetingAttendees', meeting.attendees);
                }
            });

            $scope.$on('history.mdtslist.failed', commonService.progress.hide);

            $scope.$on('attendees.list.done', function (event, results) {
                // update meeting attendees.
                commonService.session.set('meetingAttendees', results);

                // call init again to load new data.
                init();
            });

            $scope.$on('attendees.list.failed', commonService.progress.hide);

            //$scope.$on('attendees.add.failed', commonService.progress.hide);

            // watch $scope.selectedHistoryInformation
            $scope.$watch('selectedHistoryInformation', function (newVal, oldVal) {
                if (newVal) {
                    $scope.historyId = newVal.id;
                    historiesServices.mdtsList($scope.historyId);
                }
            });

            $scope.$on('history.changed', function (event, data) {
                if ($scope.historyId != data.history.id || data.force) {
                    $scope.historyId = data.history.id;
                    historiesServices.mdtsList($scope.historyId);
                }
            });

            $scope.$on('suggestion.changed', function (event, data) {
                if (data && data.meetingId) {
                    retrieveSuggestions(data.meetingId);
                }
            });

            // join/start mdt
            $scope.$on('mdt.join.done', function (event, path) {
                $scope.meetingPath = path;

                $timeout(function () {
                    document.getElementById('diagnosisMeetinglink').click();
                    commonService.progress.hide();
                }, 0);
            });

            $scope.$on('mdt.join.failed', commonService.progress.hide);

            // websocket, listening message received broadcast.
            $scope.$on(strings.websocket.topic.discussion, function (event, result) {
                var message;
                if (result) {
                    message = suggestionsServices.formatSuggestion(result.message);

                    if (result.mdtId) {
                        $scope.$apply(function () {
                            if (rawSuggestions.length === 0) {
                                rawSuggestions.push(message);
                            } else {
                                rawSuggestions = rawSuggestions.map(function (v, i) {
                                    if (v.id === message.id || v.doctor.id === message.doctor.id) {
                                        return message;
                                    }

                                    return v;
                                });
                            }
                            
                            combineSuggestion(rawSuggestions, result.mdtId);
                        });
                    }                    
                }
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });
            /*--------end------*/
        }]);
})();