﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('SuggestionPatientCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$stateParams',
        '$timeout',
        'strings',
        'commonService',
        'suggestionsServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $stateParams,
            $timeout,
            strings,
            commonService,
            suggestionsServices) {

            var user = commonService.getCurrentUser();
            var isEditSuggestion = false;

            // predefined permission list.
            $scope.permissions = strings.permissions;

            /*--------attending doctors------*/
            $scope.addSuggestion = function () {
                if ($scope.attendingDoctor.suggestion &&
                    $scope.attendingDoctor.suggestion.content) {
                    if (isEditSuggestion) {
                        var id = $scope.attendingDoctor.suggestion.id;
                        if (id) {
                            suggestionsServices.update(id, {
                                content: $scope.attendingDoctor.suggestion.content
                            });
                        }
                    } else {
                        // create a new one
                        // posted data structure:
                        // doctorid -> account_id, default is creator
                        // mdtid -> meeting id
                        // historyid -> history id
                        // content -> suggestion
                        suggestionsServices.add({
                            doctorid: $scope.attendingDoctor.id,
                            historyid: $scope.historyId, // defined on parent scope in patients.js file. 
                            mdtid: $scope.meetingId,
                            content: $scope.attendingDoctor.suggestion.content
                        });
                    }

                    isEditSuggestion = false;
                }
            };

            // send message by 'enter' key.
            //$scope.onAddFromKey = function (event) {
            //    if (event.ctrlKey && event.keyCode === 13) { // enter
            //        $scope.addSuggestion();
            //    }
            //};
            var originalSuggestion = {};
            var suggestionInput;
            $scope.editSuggestion = function () {
                if ($scope.selectedHistoryInformation.status != 5) {
                    $scope.attendingDoctor.editable = true;
                    isEditSuggestion = true;
                    originalSuggestion[$scope.attendingDoctor.id] = $scope.attendingDoctor.suggestion.content;
                    $timeout(function () {
                        suggestionInput = suggestionInput || document.getElementById('suggestionInput');
                        suggestionInput && suggestionInput.focus();
                    }, 0);                    
                }
            };

            $scope.cancelSuggestion = function () {
                if ($scope.selectedHistoryInformation.status != 5 && $scope.attendingDoctor.editable) {
                    $scope.attendingDoctor.editable = false;
                    isEditSuggestion = false;
                    $scope.attendingDoctor.suggestion.content = originalSuggestion[$scope.attendingDoctor.id];
                }
            };

            $scope.iAttendanceNow = 0;
            $scope.onAttendance = function (attendance, $index) {
                $scope.iAttendanceNow = $index;
                $scope.attendingDoctor = attendance;
            };
            /*--------end------*/

            /*--------private------*/
            function init() {
                if ($scope.fromPatient) {
                    $scope.historyId = $scope.selectedHistory ? $scope.selectedHistory.id : -1;
                } else {
                    $scope.historyId = $scope.historyId != -1 ? $scope.historyId : -1;
                }

                $scope.meetingId = $scope.meetingId ? $scope.meetingId : -1;

                // get all attendance.
                $scope.meetingAttendees = commonService.session.get('meetingAttendees') || [];

                if ($scope.historyId != -1 && $scope.meetingId != -1) {
                    retrieveSuggestion($scope.historyId, $scope.meetingId);
                }
            }

            function getDoctorId() {
                var doctor = -1;

                if (commonService.roles.hasRole('role-moderator') ||
                    commonService.roles.hasRole('role-admin')) {
                    doctor = 0; // all doctors
                } else if (commonService.roles.hasRole('role-doctor')) {
                    doctor = commonService.getCurrentUser().account.userId;
                }

                return doctor;
            }

            function retrieveSuggestion(historyId, meetingId) {
                var doctor = getDoctorId();
                var meetingId = meetingId ? meetingId : $scope.meetingId;
                var historyId = historyId ? historyId : $scope.historyId;

                if (doctor != -1) {
                    // get all suggestions for attendance.
                    suggestionsServices.list({
                        doctor: doctor, // all
                        mdt: meetingId, // defined on parent scope in patients.js file.
                        history: historyId // defined on parent scope in patients.js file.
                    });
                }

                // set first attandance as default when patient tab is changed.
                if ($scope.meetingAttendees && $scope.meetingAttendees.length != 0) {
                    $scope.iAttendanceNow = 0;
                    $scope.attendingDoctor = $scope.meetingAttendees[0];
                }
            }

            init();
            /*--------end------*/

            /*--------event------*/
            $scope.$on('suggestion.add.done', function (event, data) {
                $scope.attendingDoctor.editable = false;
                commonService.progress.hide();

                // send this broadcast to notify diagnosis page to refresh suggestion data.
                $rootScope.$broadcast('suggestion.changed', {
                    meetingId: $scope.meetingId
                });
            });

            $scope.$on('suggestion.add.failed', function (event, error) {
                commonService.progress.hide();
            });

            $scope.$on('suggestion.update.done', function (event, data) {
                $scope.attendingDoctor.editable = false;
                commonService.progress.hide();

                // send this broadcast to notify diagnosis page to refresh suggestion data.
                $rootScope.$broadcast('suggestion.changed', {
                    meetingId: $scope.meetingId
                });
            });

            $scope.$on('suggestion.update.failed', function (event, error) {
                commonService.progress.hide();
            });

            $scope.$on('suggestions.list.done', function (event, data) {
                $scope.suggestions = data;

                var isDoctor = commonService.roles.hasRole('role-doctor');

                if ($scope.meetingAttendees && $scope.meetingAttendees.length != 0) {
                    $scope.meetingAttendees.map(function (v, j) {
                        var index = -1;
                        if (data && data.length != 0) {
                            for (var i = 0; i < data.length; i++) {
                                if (v.id === data[i].doctor.id) {
                                    index = i;
                                    break;
                                }
                            }
                        }
                        v.suggestion = index != -1 ? data[i] : null;
                        v.editable = v.suggestion == null; // be able to edit

                        if (isDoctor) {
                            if (v.id === getDoctorId()) {
                                $scope.attendingDoctor = v;
                            }
                        }

                        return v;
                    });

                    if (!isDoctor) {
                        $scope.attendingDoctor = $scope.meetingAttendees[0];
                    }
                }

                commonService.progress.hide();
            });

            $scope.$on('suggestions.list.failed', function (event, error) {
                commonService.progress.hide();
            });

            $scope.$on('patient.changed', function (event, data) {
                $scope.historyId = data.historyId;
                //$scope.historyStatus = data.history ? data.history.status :  5;
                retrieveSuggestion(data.historyId);
            });

            $scope.$on('history.changed', function (event, data) {
                if (data.history.id && data.history.id != -1) {
                    $scope.historyId = data.history.id;
                }

                if (!(data.ignoreSuggestion)) {
                    retrieveSuggestion(data.history.id);
                }
            });

            // listening the event, which was senting from diagnosis page.
            $scope.$on('find.active.meeting', function (event, data) {
                if ($scope.fromPatient) {
                    $scope.meetingId = data.meeting.id;
                    retrieveSuggestion($scope.historyId, data.meetingId);
                }
            })

            //$scope.$watch('selectedHistoryInformation', function (newVal, oldVal) {
            //    //--diagnostic status
            //    // 1->pending
            //    // 2->discussing
            //    // 3->queuing
            //    // 4->meeting
            //    // 5->finished    
            //    $scope.historyStatus = newVal ? newVal.status : 5;
            //});

            $scope.$on('$destroy', function () {
                commonService.session.reset('meetingAttendees');
            });
            /*--------end------*/
        }
    ]);
})();
