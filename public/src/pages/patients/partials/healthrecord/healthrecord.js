﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('HealthrecordCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$stateParams',
        '$document',
        '$timeout',
        'commonService',
        'modalService',
        'historiesServices',
        'healthrecordServices',
        'clientServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $stateParams,
            $document,
            $timeout,
            commonService,
            modalService,
            historiesServices,
            healthrecordServices,
            clientServices) {
            $scope.focus = false;

            var user = commonService.getCurrentUser();

            /*--------methods------*/
            // upload new healthrecord to specific patient.
            $scope.addNewHealthrecord = function (classification) {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/patients/modal/material/addNewMaterial.html',
                   {
                       controller: 'AddPatientMaterialCtrl',
                       data: {
                           historyId: $scope.selectedHistoryInformation.id,
                           classification: classification
                       },
                       closefn: addNewHealthrecordCallback
                   });
            };

            $scope.deleteHealthrecord = function (record) {
                commonService.confirm.del({
                    id: record.id
                }, function (opt) {
                    healthrecordServices.del(opt.id);
                });
            };

            // select
            $scope.onChangedHistory = function (history) {
                //commonService.progress.show();
                //getCombinedHealthrecords(history.id);

                // get selected history detail information.
                historiesServices.detail(history.id);

                history.index = $scope.patientHistories.indexOf(history);
                $rootScope.$broadcast('history.changed', {
                    history: history,
                    histories: $scope.histories,
                    patientId: $scope.patientId
                });
            };
            /*--------end------*/
            
            /*-------initialize------*/
            function init() {
                historiesServices.list($scope.patientId);
            }

            init();
            /*--------end------*/

            /*--------popover------*/
            $scope.iNow = -1;
            $scope.popoverOpt = {
                show: false,
                left: 0,
                top: 0
            };
            $scope.popover = function (item, $index, $event) {
                $scope.popoverOpt.show = false;
                $scope.iNow = $index;

                $scope.healthrecordType = item;

                $timeout(function () {
                    var ele = angular.element($event.currentTarget);
                    var size = 140;
                    var popoverWidth = size; //= angular.element('.xg-healthrecord-popover').outerWidth();
                    var offset = ele.offset();

                    // only show 7 item each row.
                    if (item.healthrecords) {
                        if (item.healthrecords.length < 7) {
                            // calc the width.
                            // and 'plus icon' width.
                            popoverWidth = size * (item.healthrecords.length + 1);
                        } else {
                            popoverWidth = size * 7;
                        }                       
                    }

                    $scope.popoverOpt.left = (offset.left + (ele.outerWidth() / 2) - popoverWidth / 2) + 'px';
                    $scope.popoverOpt.top = (ele.outerHeight() + offset.top) + 'px';

                    $scope.popoverOpt.show = true;
                }, 50);

                $event.stopPropagation();
            };
            /*--------end------*/

            /*--------pc client------*/
            // launch pc client to view healthrecord/snapshot.
            $scope.viewAttachment = function (attachment) {
                // healthrecod
                // type: 1-> image, 3->dicom
                if (attachment.type === 1 || attachment.type === 3) {
                    $scope.viewHealthrecord(attachment);
                } else if (attachment.type === 2) { // snapshot
                    $scope.viewSnapshot(attachment);
                }
            };

            // launch pc client to view healthrecord.
            $scope.viewHealthrecord = function (hr) {
                commonService.progress.show();

                var patientId = parseInt($scope.patientId);
                var historyId = parseInt($scope.history.id);
                clientServices.healthrecord({
                    ticket: user.token,
                    healthrecordId: hr.id,
                    healthrecordType: hr.type,
                    patientId: patientId,
                    historyId: historyId
                });
            };

            // launch pc client to view snapshot.
            $scope.viewSnapshot = function (attachment) {
                commonService.progress.show();

                var patientId = parseInt($scope.patientId);
                var historyId = parseInt($scope.history.id);
                var snapshotId = parseInt(attachment.attachid);
                clientServices.healthrecord({
                    ticket: user.token,
                    patientId: patientId,
                    healthrecordId: hr.id,
                    healthrecordType: hr.type,
                    snapshotId: snapshotId,
                    historyId: historyId
                });
            };

            // launch pc client for scanner.
            $scope.launchScanner = function (classification) {
                commonService.progress.show();

                var patientId = parseInt($scope.patientId);
                var historyId = parseInt($scope.history.id);
                var classificationId = parseInt(classification.id);

                clientServices.scanner({
                    ticket: user.token,               
                    patientId: patientId,
                    historyId: historyId,
                    classification: classificationId
                });
            };
            /*--------end------*/

            /*--------private------*/
            function getCombinedHealthrecords(historyId) {
                // get all healthrecord for current selected history.
                healthrecordServices.combine(historyId, true).then(function (groupedHealthrecords) {
                    $scope.groupedHealthrecords = groupedHealthrecords;
                    commonService.progress.hide();
                }, commonService.progress.hide);
            }

            function addNewHealthrecordCallback() {
                if ($scope.history) {
                    getCombinedHealthrecords($scope.history.id);
                }               
            }         
            /*--------end------*/

            /*--------event------*/
            $document.on('click', function (event) {
                var popover = angular.element(event.target).closest('#xgHealthrecordPopover');
                if (popover && popover.length === 0 && $scope.popoverOpt.show) {
                    $scope.popoverOpt.show = false;
                    $scope.iNow = -1;
                    $scope.$digest();
                }
            });

            // hide popover once scrolling.
            $scope.$on('scroll.progress', function (event) {
                if ($scope.popoverOpt.show) {
                    $scope.popoverOpt.show = false;
                    $scope.iNow = -1;
                    $scope.$digest();
                }
            });

            // histories list
            $scope.$on('history.list.done', function (event, result) {
                $scope.patientHistories = result;
                $scope.history = result[0] || {};
                $scope.history.index = 0;
            });

            // get histories list failed
            $scope.$on('history.list.failed', commonService.progress.hide);

            // history detail
            $scope.$on('history.detail.done', function (event, result) {
                commonService.progress.hide();
                $scope.selectedHistoryInformation = result;
            });

            //get history information failed
            $scope.$on('history.detail.failed', commonService.progress.hide);

            // watch $scope.groupedHealthrecords
            $scope.$watch('groupedHealthrecords', function (newVal, oldVal) {
                if (newVal) {
                    $scope.patientGroupedHealthrecords = newVal;
                }
            });

            // history tab changed.
            $scope.$on('history.changed', function (event, data) {
                var historyId = data.history.id;
                var patientId = data.patientId;

                if ($scope.patientHistories && $scope.patientHistories.length != 0) {
                    $scope.history = $scope.patientHistories[data.history.index];
                }                

                if (patientId != $scope.patientId) {
                    $scope.history = data.history;
                    $scope.patientId = data.patientId;

                    historiesServices.list($scope.patientId);
                }

                getCombinedHealthrecords(historyId);
            });

            // patient select changed in meeting patient detail.
            $scope.$on('patient.changed', function (event, data) {
                $scope.patientId = data.patient.id;
                historiesServices.list($scope.patientId);
            });

            // delete healthrecord completed.
            $scope.$on('healthrecord.delete.done', function (event, result) {
                getCombinedHealthrecords($scope.history.id);
            });

            // delete healthrecord failed
            $scope.$on('healthrecord.delete.failed', commonService.progress.hide);

            // client launcher for viewing healthrecord.
            $scope.$on('hr.open.done', function (event, path) {
                $scope.hrPath = path;

                $timeout(function () {
                    document.getElementById('healthrecordlink').click();
                    commonService.progress.hide();
                }, 0);
            });
            $scope.$on('hr.open.failed', commonService.progress.hide);

            // client launcher for scanner.
            $scope.$on('scanner.open.done', function (event, path) {
                $scope.scanerPath = path;

                $timeout(function () {
                    document.getElementById('scanerlink').click();
                    commonService.progress.hide();
                }, 0);
            });
            $scope.$on('scanner.open.failed', commonService.progress.hide);

            $scope.$on('$destroy', function () {
                // clean up.
            });          
            /*--------end------*/
        }]);
})();