﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('BaseInfoPatientCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$stateParams',
        'modalService',
        'commonService',
        'patientServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $stateParams,
            modalService,
            commonService,
            patientServices) {

            /*--------patient------*/
            // selectedHistoryInformation was defined in parent.
            $scope.historyInfo = $scope.selectedHistoryInformation || {};

            // edit patient.
            $scope.editPatient = function () {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/patients/addOrEdit/addOrEdit.html',
                  {
                      controller: 'AddorEditPatientCtrl',
                      data: {
                          patient: $scope.patientInfo,
                          //userId: user.accountId,
                          isEdit: true
                      },
                      closefn: function (patient) {
                          $scope.patientInfo = patient;

                          $rootScope.$broadcast('patient.edit.finished', {
                              patient: patient
                          });
                      }
                  });
            };

            // share a patient to doctor group.
            $scope.sharePatient = function (patient) {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/patients/modal/share/share/share.html',
                  {
                      controller: 'SharePatientCtrl',
                      data: {
                          patient: $scope.patientInfo
                      },
                      closefn: function (patient) {
                          // todo
                      }
                  });
            };
            /*--------end------*/

            function init() {
                if ($scope.patientId) {
                    patientServices.detail($scope.patientId);
                }
            }
            init();
            /*--------event------*/
            //$scope.$watch('selectedHistoryInformation', function (newVal, oldVal) {
            //    if (newVal && newVal != oldVal) {
            //        $scope.patientInfo = $scope.selectedHistoryInformation.patient;
            //    }
            //});

            $scope.$on('patient.detail.done', function (event, result) {
                if (result) {
                    $scope.patientInfo = result.data;
                }
            });

            $scope.$on('patient.detail.failed', commonService.progress.hide);

            // meeting patients.
            $scope.$on('patient.changed', function (event, data) {
                $scope.patientInfo = data.patient;
            });

            $scope.$on('$destroy', function () {
                // clean up.
            });
            /*--------end------*/
        }]);
})();