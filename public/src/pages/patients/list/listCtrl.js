﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('ListPatientsCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        'commonService',
        'strings',
        'modalService',
        'patientServices',
        'userServices',
        'favoritesServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $state,
            $stateParams,
            commonService,
            strings,
            modalService,
            patientServices,
            userServices,
            favoritesServices) {

            $scope.permissions = strings.permissions;
            var term = $stateParams.search ? $stateParams.search : '';

            var initTab = commonService.session.get('initPatientTab') ? commonService.session
                    .get('initPatientTab') : 'All';
            commonService.session.reset('initMeetingTab');
            // current login user.
            var user = commonService.getCurrentUser().account;

            // resolve bug#582, using 'title' to replace
            // 'type' for filtering.
            var doctorParams = {
                'size' : 50,
                'title' : strings.doctorTitles
            };

            var status = {
                'All' : -1,
                'Onging' : 0,
                'InPending' : 1,
                'InDiscussion' : 2,
                'InQueue' : 3,
                'InMeeting' : 4,
                'Finished' : 5
            };
            // pagination
            $scope.paging = {
                totalItems : 0,
                itemsPerPage : 10,
                currentPage : 1,
                pageChanged : pageChangeHandle,
                pageSizeChanged : pageChangeHandle
            };
            // search
            $scope.filter = term;
            var params = {
                filter : $scope.filter,
                size : $scope.paging.itemsPerPage,
                page : $scope.paging.currentPage,
                doctor : -1,
                status : -1,
                dept : -1
            };

            function init( ){
                $scope.initialStep = true;
                $scope.filters = {
                    'dept' : {},
                    'doctor' : {},
                    'status' : {}
                };
                // Department list.
                $scope.deptList = [];
                // Doctor list.
                $scope.doctorList = [];
                // Status list.
                $scope.statusList = [];
                commonService.resource.getValues(
                        ['PATIENT_LIST_FILTER_STATUS', 'PATIENT_LIST_STATUS_ONGOING',
                                'PATIENT_LIST_STATUS_FINISH']).then(function(sources){
                    $scope.statusList.push({
                        'id' : -1,
                        'name' : sources['PATIENT_LIST_FILTER_STATUS']
                    });
                    $scope.statusList.push({
                        'id' : 0,
                        'name' : sources['PATIENT_LIST_STATUS_ONGOING']
                    });
                    $scope.statusList.push({
                        'id' : 5,
                        'name' : sources['PATIENT_LIST_STATUS_FINISH']
                    });
                    $scope.filters.status = $scope.statusList[0];
                });
                // $scope.focusTab = { 'All': true };
                $scope.showFilters = {
                    'deptFilter' : true,
                    'doctorFilter' : true,
                    'statusFilter' : true,
                    'myFocus' : false
                };
                if (user.roles.indexOf('role-doctor') != -1) {
                    $scope.myFocus = {
                        'value' : true
                    };
                } else {
                    $scope.myFocus = {
                        'value' : false
                    };
                }
                userServices.getDeptValues().then(
                        function(data){
                            commonService.resource.getValues('PATIENT_LIST_FILTER_DEPT').then(
                                    function(sources){
                                        data.unshift({
                                            "id" : "-1",
                                            "name" : sources['PATIENT_LIST_FILTER_DEPT']
                                        });
                                        $scope.deptList = data;
                                        $scope.filters.dept = $scope.deptList[0];
                                        params.dept = $scope.filters.dept.id;
                                        userServices.detail(user.accountId);
                                    });
                        });
            }

            init();

            $scope.onChangedCondition = function(type, val){
                commonService.progress.show();
                if (type == 'tab') {
                    $scope.focusTab = {};
                    $scope.focusTab[val] = true;
                    params['status'] = status[val];

                    if (val == 'InPending' || val == 'InDiscussion' || val == 'InMeeting') {
                        $scope.showFilters['deptFilter'] = false;
                        $scope.showFilters['doctorFilter'] = false;
                        $scope.showFilters['statusFilter'] = false;
                        $scope.showFilters['myFocus'] = true;
                        params['dept'] = $scope.deptList[0].id;
                        if ($scope.myFocus.value) {
                            params['doctor'] = user.userId;
                        } else {
                            params['doctor'] = -1;
                        }
                        if ((val == 'InDiscussion' || val == 'InMeeting') && $scope.myFocus.value) {
                            showMyFocus();
                        }
                    } else if (val == 'InQueue') {
                        $scope.showFilters['deptFilter'] = true;
                        $scope.showFilters['doctorFilter'] = true;
                        $scope.showFilters['statusFilter'] = false;
                        $scope.showFilters['myFocus'] = false;
                        params.dept = $scope.filters.dept.id;
                        params['doctor'] = $scope.filters.doctor.id;
                    } else if (val == 'All') {
                        $scope.showFilters['deptFilter'] = true;
                        $scope.showFilters['doctorFilter'] = true;
                        $scope.showFilters['statusFilter'] = true;
                        $scope.showFilters['myFocus'] = false;
                        params.dept = $scope.filters.dept.id;
                        params.status = $scope.filters.status.id
                        params['doctor'] = $scope.filters.doctor.id;
                    }
                }

                if (type == 'status') {
                    params['status'] = val.id;
                }

                if (type == 'doctor') {
                    params['doctor'] = val.id;
                }

                if (type == 'myFocus') {
                    if ($scope.myFocus.value) {
                        params['doctor'] = user.userId;
                        if ($scope.focusTab.InPending || $scope.focusTab.InDiscussion
                                || $scope.focusTab.InMeeting) {
                            showMyFocus();
                        }
                    } else {
                        params['doctor'] = -1;
                    }
                }

                if (type == 'dept') {
                    $scope.filters.dept = val;
                    params.dept = val.id;
                    doctorParams['dept'] = params.dept;
                    userServices.list(doctorParams);
                }

                if (type != 'dept'
                        && !(($scope.focusTab.InDiscussion || $scope.focusTab.InMeeting) && $scope.myFocus.value)) {
                    console.log(params);
                    showPatientsList();
                }
            };

            // detail
            $scope.detail = function(history){
                $state.go('page.patient.detail', {
                    id : history.patient.id
                });
            };

            // add new patient
            $scope.addPatient = function( ){
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/patients/addOrEdit/addOrEdit.html', {
                    controller : 'AddorEditPatientCtrl',
                    data : {
                        patient : {
                            sex : 1
                        // set man as default.
                        },
                        userId : user.accountId,
                        isEdit : false
                    },
                    closefn : addorEditPatientCallback
                });
            };

            /*-------events------*/
            //Pending patient groups list.
            $scope.$on('patient.meeting.done', function(event, result){
                commonService.progress.hide();
                $scope.patients = result.data;
                $scope.paging.totalItems = result.count;
            });

            //Discussion patient groups list.
            $scope.$on('patient.discussion.done', function(event, result){
                commonService.progress.hide();
                $scope.patients = result.data;
                $scope.paging.totalItems = result.count;
            });

            $scope.$on('patient.list.done', function(event, result){
                commonService.progress.hide();
                $scope.patients = result.data;
                $scope.paging.totalItems = result.count;
            });

            $scope.$on('user.detail.done', function(event, result){
                $scope.filters.dept = result.dept;
                params.dept = $scope.filters.dept.id;
                doctorParams['dept'] = params.dept;
                userServices.list(doctorParams);
            });

            $scope.$on('user.list.done', function(event, result){
                commonService.progress.hide();
                var data = [];

                if (result && result.data && result.data.length != 0) {
                    result.data.forEach(function(item){
                        data.push({
                            'id' : item.id,
                            'name' : item.name
                        });
                    });
                }
                commonService.resource.getValues('PATIENT_LIST_FILTER_DOCTOR').then(
                        function(sources){
                            data.unshift({
                                "id" : "-1",
                                "name" : sources['PATIENT_LIST_FILTER_DOCTOR']
                            });
                            $scope.doctorList = data;
                            $scope.filters.doctor = $scope.doctorList[0];
                            params['doctor'] = $scope.filters.doctor.id;
                            if ($scope.initialStep && user.roles.indexOf('role-doctor') != -1) {
                                $scope.filters.doctor = {
                                    'id' : user.userId,
                                    'name' : user.userName
                                };
                                params['doctor'] = $scope.filters.doctor.id;
                            }
                            $scope.initialStep = false;
                            //                    showPatientsList();
                            $scope.onChangedCondition('tab', initTab);
                        });
            });
            /*-------end------*/

            /*-------private------*/
            function showMyFocus( ){
                // deep copy
                var p = JSON.parse(JSON.stringify(params));
                delete p.dept;
                delete p.filter;
                delete p.status;
                if ($scope.focusTab.InDiscussion) {
                    patientServices.listDiscussion(p);
                }
                if ($scope.focusTab.InMeeting) {
                    patientServices.listMeeting(p);
                }
            }

            function showPatientsList( ){
                commonService.progress.show();
                patientServices.list(params);
            }

            function pageChangeHandle(item){
                if (item.page != params.page || item.size != params.size) {

                    params.page = item.page;
                    params.size = item.size;

                    if (($scope.focusTab.InDiscussion || $scope.focusTab.InMeeting)
                            && $scope.myFocus.value) {
                        showMyFocus();
                    } else {
                        showPatientsList();
                    }
                }
            }

            // retrieve all patients again from db to get the new patient we added.
            function addorEditPatientCallback(data){
                $scope.filters.doctor = $scope.doctorList[0];
                params.doctor = $scope.filters.doctor.id;
                $scope.filters.dept = $scope.deptList[0];
                params.dept = $scope.filters.dept.id;
                $scope.filters.status = $scope.statusList[0];
                params.status = $scope.filters.status.id;
                $scope.onChangedCondition('tab', 'All');
            }

            /*-------end------*/
        }]);
})();