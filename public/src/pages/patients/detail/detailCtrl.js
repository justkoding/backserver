﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('DetailPatientCtrl', ['$scope',
        '$rootScope',
        '$state',
        '$stateParams',
        'commonService',
        'strings',
        'modalService',
        'historiesServices',
        'healthrecordServices',
        'mdtServices',
        'websocketServices',
        function ($scope,
            $rootScope,
            $state,
            $stateParams,
            commonService,
            strings,
            modalService,
            historiesServices,
            healthrecordServices,
            mdtServices,
            websocketServices) {
            // predefined permissions.
            $scope.permissions = strings.permissions;

            // current login user.
            var user = commonService.getCurrentUser();

            // get current patient Id.
            var patientId = $stateParams.id ? parseInt($stateParams.id) : -1;           
            var userRoles = commonService.getRoleInfoOfCurrentUser();
            var userPermissions = commonService.getPermissionOfCurrentUser();
            //mock
            //var patientId = 1;
            $scope.patientId = patientId;
            $scope.fromShare = $stateParams.fs; // from share

            // to mark whether show 'request offline', 'request meeting' and 'decision' buttons
            // those buttons only can be seen by patient owner and moderator.
            $scope.isModerator = commonService.roles.hasRole(strings.roles.moderator);
            $scope.isNavigator = commonService.roles.hasRole(strings.roles.navigator);
            $scope.isPatientOwner = false;
            $scope.isBasicInfoEditable = false;
            $scope.isAttendance = false;

            // flag. which will be used on suggestion sub-page.
            $scope.fromPatient = true;

            /*--------history tab------*/
            var requestNum = 2;
            $scope.tab = function (history, $index) {
                if ($scope.selectedHistory != history) {
                    commonService.progress.show();

                    history.index = $index;
                    $scope.selectedHistory = history;
                   
                    // get selected history detail information.
                    historiesServices.detail($scope.selectedHistory.id);

                    // get combined healthrecords.
                    getCombinedHealthrecords();

                    $rootScope.$broadcast('history.changed', {
                        history: history,
                        histories: $scope.histories,
                        patientId: patientId
                    });
                }
            };

            /*--------end------*/
            // back
            $scope.back = function () {
                commonService.history.back();
            };

            /*--------diagnostic------*/
            $scope.requestOffline = function () {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/patients/modal/reason/offlinereason.html',
                   {
                       controller: 'ReasonForMeetingOrOfflineCtrl',
                       data: {
                           isMeeting: false,
                           historyId: $scope.selectedHistory.id,
                           status: $scope.selectedHistory.status
                       },
                       closefn: requestOfflineCallback
                   });
            };

            $scope.requestMeeting = function () {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/patients/modal/reason/meetingreason.html',
                   {
                       controller: 'ReasonForMeetingOrOfflineCtrl',
                       data: {
                           isMeeting: true,
                           historyId: $scope.selectedHistory.id,
                           status: $scope.selectedHistory.status
                       },
                       closefn: requestMeetingCallback
                   });
            };

            $scope.makeDesion = function () {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/patients/modal/conclusion/conclusion.html',
                   {
                       controller: 'ConclusionCtrl',
                       data: {
                           historyId: $scope.selectedHistoryInformation.id,
                           decision: $scope.selectedHistoryInformation.decision
                       },
                       closefn: addorEditConclusionCallback
                   });
            };

            $scope.transform = function () {
                commonService.progress.show();
                modalService.showModal('/assets/src/pages/patients/modal/transform/transform.html',
                   {
                       controller: 'TransformCtrl',
                       data: {
                           historyId: $scope.selectedHistoryInformation.id,
                           doctorId: $scope.selectedHistoryInformation.doctor.id
                       },
                       closefn: transformCallback
                   });
            };

            /*--------end------*/

            /*--------event------*/
            $scope.$on('history.list.done', function (event, result) {
                if (result && result.length != 0) {
                    $scope.histories = result;
                    $scope.selectedHistory = result[0] || {};
                    $scope.selectedHistory.index = 0;

                    // get selected history detail information.
                    historiesServices.detail($scope.selectedHistory.id);

                    // healthrecords
                    getCombinedHealthrecords();

                    $rootScope.$broadcast('history.changed', {
                        history: $scope.selectedHistory,
                        histories: $scope.histories,
                        patientId: patientId
                    });
                } else {
                    commonService.progress.hide();
                }
            });
            $scope.$on('history.list.failed', commonService.progress.hide);

            $scope.$on('history.detail.done', function (event, result) {
                if (--requestNum === 0) {
                    commonService.progress.hide();
                }

                // if $scope.isShowBtns is false, means the current user is not moderator.
                // so we will check whether the current user is the patient owner. if yes, he also
                // can access those button.
                if (result) {
                    $scope.isPatientOwner = user.account.userId === result.doctor.userId;

                    // to check whether the current user is a attendence.
                    //$scope.isAttendance = commonService.meeting.hasAttandence(user.account.userId);
                    
                    if(($scope.isPatientOwner || (userRoles.indexOf("role-navigator") != -1)) 
                    		&& (userPermissions.indexOf($scope.permissions.patient.update) != -1)){
                    	$scope.isBasicInfoEditable = true;
                    }
                }

                $scope.selectedHistoryInformation = result;
            });
            $scope.$on('history.detail.failed', commonService.progress.hide);

            // to find the opened meeting.
            $scope.$on('find.active.meeting', function (event, data) {
                $scope.meetingId = data.meeting.id;

                mdtServices.getAttendees({
                    id: data.meeting.id,
                    skipBroadcast: true
                }).then(function (results) {
                    commonService.session.set('meetingAttendees', results);
                    $scope.isAttendance = commonService.meeting.hasAttandence(user.account.userId);
                });
            });

            $scope.$on('$destroy', function () {
                commonService.session.reset('meetingAttendees');
            });
            /*--------end------*/

            /*--------private------*/
            function init() {
                commonService.progress.show();
                if (patientId != -1) {
                    historiesServices.list(patientId);
                }

                // created new if websocket instance is not created.
                if (!$rootScope.ws) {
                    $rootScope.ws = websocketServices.createChannel(user.token);
                }
            }

            function getCombinedHealthrecords() {
                // get all healthrecord for current selected history.
                healthrecordServices.combine($scope.selectedHistory.id, true).then(function (groupedHealthrecords) {
                    $scope.groupedHealthrecords = groupedHealthrecords;

                    if (--requestNum === 0) {
                        commonService.progress.hide();
                    }
                }, commonService.progress.hide);
            }

            // revoke when conclusion modal was closed.
            function addorEditConclusionCallback() {
                if (patientId != -1) {
                    historiesServices.list(patientId);
                }

                if ($scope.selectedHistory.id != -1) {
                    // get selected history detail information.
                    historiesServices.detail($scope.selectedHistory.id);
                }
            }

            // revoke when request offline modal was closed.
            function requestOfflineCallback(data) {
                if ($scope.selectedHistory.id != -1) {
                    // get selected history detail information.
                    historiesServices.detail($scope.selectedHistory.id);
                }

                $rootScope.$broadcast('history.changed', {
                    history: $scope.selectedHistory,
                    histories: $scope.histories,
                    patientId: patientId,
                    force: true,
                    ignoreSuggestion: true
                });
            }

            // revoke when request meeting modal was closed.
            function requestMeetingCallback(data) {
                if ($scope.selectedHistory.id != -1) {
                    // get selected history detail information.
                    historiesServices.detail($scope.selectedHistory.id);
                }

                $rootScope.$broadcast('history.changed', {
                    history: $scope.selectedHistory,
                    histories: $scope.histories,
                    patientId: patientId,
                    force: true,
                    ignoreSuggestion: true
                });
            }

            // revoke when request meeting modal was closed.
            function transformCallback(data) {
                $state.go($state.current, {
                    id: patientId
                }, {
                    reload: true
                });
            }

            // init
            init();
        }]);
})();