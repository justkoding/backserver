﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('FavoriteCtrl', ['$scope',
        '$timeout',
        '$translate',
        '$cookieStore',
        'loadingServices',
        'messageService',
        'commonService',
        '$modalInstance',
        'data',
        'favoritesServices',
        function ($scope,
            $timeout,
            $translate,
            $cookieStore,
            loadingServices,
            messageService,
            commonService,
            $modalInstance,
            data,
            favoritesServices) {
            loadingServices.hide();

            // deep copy
            $scope.data = JSON.parse(JSON.stringify(data));

            // parse parma from data.
            var patient = $scope.data.patient;

            // define a new variable to scope for favorite object.
            $scope.favorite = {};

            // modal.
            $scope.ok = function () {
                loadingServices.show();
                favoritesServices.add(patient.id, $scope.favorite);
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            /*--------events------*/
            $scope.$on('favorite.add.done', function () {
                loadingServices.hide();
                $modalInstance.close();
            });

            $scope.$on('favorite.add.failed', function () {
                loadingServices.hide();               
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });
            /*--------end------*/

            /*--------private------*/
            function init() {
            }
            /*--------end------*/

            // init
            init();
        }]);
})();