﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('AddSocialCtrl', ['$scope',
        '$rootScope',
        '$timeout',
        '$translate',
        'messageService',
        'userServices',
        'commonService',
        'hospitalsServices',
        'strings',
        function ($scope,
            $rootScope,
            $timeout,
            $translate,
            messageService,
            userServices,
            commonService,
            hospitalsServices,
            strings) {

            commonService.progress.hide();

            var requestNum = 2;

            // set default params for retrieving doctors
            var options = {
                // to filter by doctor title. only show the users whose title is 'doctor'
                // 2-> 主任医师,3-> 副主任医师,4-> 主治医师,5-> 医师
                title: strings.doctorTitles,
                size: 1000, // set big value for mocking get all data.
                page: 1
            };

            /*-----tab-----*/
            // tab
            $scope.iNow = 1;
            $scope.tab = function (iTab) {
                var opt;

                $scope.iNow = iTab;

                // department
                if (iTab === 1) {
                    opt = angular.extend({}, options, {
                        hospital: $scope.currentHospital.id,
                        dept: $scope.currentDept.id
                    });
                } else if (iTab === 2) { // search doctor name changed
                    opt = angular.extend({}, options, {
                        hospital: $scope.currentHospital.id,
                        filter: $scope.search
                    });
                }

                userServices.list(opt, true).then(function (result) {
                    $scope.doctors = result ? result.data : [];
                    restoreSelected();
                });
            };
            /*-----end-----*/

            /*-----selected doctors-----*/
            // array of doctors who have existed in meeting
            //$scope.data = JSON.parse(JSON.stringify(data));

            // array of selected doctors
            $scope.selectedDoctors = [];

            // remove doctor from selected doctors list.
            $scope.unselected = function (doctor, $index) {
                var index = commonService.getIndexOf(doctor, $scope.doctors);
                $scope.selectedDoctors.splice($index, 1);
                $scope.doctors[index].checked = false;
                notifyDoctorsChanged();
            };

            // add doctor to selected list.
            $scope.selected = function (doctor, $index) {
                var index = commonService.getIndexOf(doctor, $scope.selectedDoctors);

                if (doctor.checked) {
                    if (index != -1) {
                        $scope.selectedDoctors.splice(index, 1);
                    }
                } else {
                    if (index == -1) {
                        $scope.selectedDoctors.push(doctor);
                    }
                }

                $scope.doctors[$index].checked = !($scope.doctors[$index].checked);
                notifyDoctorsChanged();
            };
            /*-----end-----*/

            /*-----condition changes------*/
            // department select changed
            $scope.deptChanged = function (dept) {
                var opt = angular.extend({}, options, {
                    hospital: $scope.currentHospital.id,
                    dept: dept.id
                });
                userServices.list(opt, true).then(function (result) {
                    $scope.doctors = result ? result.data : [];
                    restoreSelected();
                });
            };

            // hospital select changed
            $scope.hospitalChanged = function (hospital) {
                var opt;
                $scope.currentHospital = hospital;

                // department changed
                if ($scope.iNow === 1) {
                    opt = angular.extend({}, options, {
                        hospital: hospital.id,
                        dept: $scope.currentDept.id
                    });
                } else { // search doctor name changed
                    opt = angular.extend({}, options, {
                        hospital: hospital.id,
                        filter: $scope.search
                    });
                }

                userServices.list(opt, true).then(function (result) {
                    $scope.doctors = result ? result.data : [];
                    restoreSelected();
                });
            };

            // search text changed
            var timer = null;
            $scope.search = '';
            $scope.searchChanged = function (text) {
                $timeout.cancel(timer);

                // delay search
                timer = $timeout(function () {
                    var opt = angular.extend({}, options, {
                        hospital: $scope.currentHospital.id,
                        filter: text
                    });
                    userServices.list(opt, true).then(function (result) {
                        $scope.doctors = result ? result.data : [];
                        restoreSelected();
                    });
                }, 300);
            };

            /*-----end-----*/

            /*-----event-----*/
            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });

            //departments
            $scope.$on('dept.list.done', function (event, data) {
                $scope.depts = data || [];
                if (data.length != 0) {
                    $scope.currentDept = data[0];
                }

                // retrieve doctors.
                if (--requestNum == 0) {
                    if ($scope.currentDept && $scope.currentHospital) {
                        var opt = angular.extend({}, options, {
                            hospital: $scope.currentHospital.id,
                            dept: $scope.currentDept.id
                        });
                        userServices.list(opt, true).then(function (result) {
                            $scope.doctors = result ? result.data : [];
                            restoreSelected();
                        });
                    }
                }
            });


            //hospitals
            $scope.$on('hospitals.list.done', function (event, data) {
                $scope.hospitals = data || [];
                if (data.length != 0) {
                    $scope.currentHospital = data[0];
                }

                // retrieve doctors.
                if (--requestNum == 0) {
                    if ($scope.currentDept && $scope.currentHospital) {
                        var opt = angular.extend({}, options, {
                            hospital: $scope.currentHospital.id,
                            dept: $scope.currentDept.id
                        });
                        userServices.list(opt, true).then(function (result) {
                            $scope.doctors = result ? result.data : [];
                            restoreSelected();
                        });
                    }
                }
            });

            /*-----end-----*/
            init();

            function init() {
                // get all hospitals
                hospitalsServices.list();

                // get all departments
                userServices.getDeptlist();
            }

            /**
             * send broadcast about the selected doctors got changed.
             */
            function notifyDoctorsChanged() {
                var doctors = JSON.parse(JSON.stringify($scope.selectedDoctors));
                $rootScope.$broadcast('share.doctor.selected', {
                    doctors: doctors
                });
            }
 
            /**
             * if user has selected doctor and then try to refresh doctor list.
             * the selected doctor will be marked as selected if it is existing in new doctors list.
             */
            function restoreSelected() {
                if ($scope.selectedDoctors.length != 0 &&
                    $scope.doctors.length != 0) {
                    // single selected.
                    var s = $scope.selectedDoctors[0];
                    $scope.doctors.forEach(function (v) {
                        if (v.id == s.id) {
                            v.checked = true;
                        }
                    });
                }
            }
        }]);
})();