﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('SocialDetailCtrl', ['$scope',
        function ($scope) {
            /*--------events------*/
            $scope.$on('share.social.members', function (event, data) {
                $scope.members = data.members;
            });

            $scope.$on('$destroy', function () {
                // clean up.               
            });
            /*--------end------*/

            /*--------private------*/
            function init() {
            }
            /*--------end------*/

            // init
            init();
        }]);
})();