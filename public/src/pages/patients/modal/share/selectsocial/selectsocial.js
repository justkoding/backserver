﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('SelectSocialCtrl', ['$scope',
        '$rootScope',
        'commonService',
        'groupsServices',
        function ($scope,
            $rootScope,
            commonService,
            groupsServices) {

            var sessionKey = 'socials';
            var selectedSocial;

            // selected a social
            $scope.onSelectItem = function (social) {
                // check whether selected same social.
                if (selectedSocial == social) {
                    selectedSocial.active = !(selectedSocial.active);
                } else {
                    // unselected original one.
                    if (selectedSocial) {
                        selectedSocial.active = false;
                    }

                    social.active = true;
                    selectedSocial = social;
                }
                // send broadcast, it is in listening in share.js file.
                $rootScope.$broadcast('social.selected', {
                    social: social.active ? social : null
                });
            };

            // show all memebers of special social.
            $scope.showAllMembers = function (social) {
                $rootScope.$broadcast('share.tab.changed', {
                    iStage: 2,
                    social: social
                });
            }

            /*--------private------*/
            function init() {
                var socials = commonService.session.get(sessionKey);
                $scope.socials = socials || [];

                if ($scope.socials.length === 0) {
                    // get current doctor's socials 
                    groupsServices.list({
                        size: -1, // no pagination.
                        page: 1
                    }, true).then(function (result) {
                        if (result && result.data && result.data.forEach) {
                            var groupNum = result.data.length;

                            // get members for each social.
                            result.data.forEach(function (v) {
                                groupsServices.listMembers({
                                    groupId: v.id
                                }, true).then(function (members) {
                                    v.members = members;
                                    if (--groupNum === 0) {
                                        commonService.session.set(sessionKey, result.data);
                                        $scope.socials = result.data;
                                    }
                                }, function (err) {
                                    v.members = [];
                                    if (--groupNum === 0) {
                                        commonService.session.set(sessionKey, result.data);
                                        $scope.socials = result.data;
                                    }
                                });
                            });
                        }
                    });
                }
            }
            /*--------end------*/

            /*--------events------*/
            $scope.$on('$destroy', function () {
                // clean up.
                commonService.session.reset(sessionKey);
            });
            /*--------end------*/

            // init
            init();
        }]);
})();