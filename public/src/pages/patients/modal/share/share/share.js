﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('SharePatientCtrl', ['$scope',
        '$rootScope',
        '$timeout',
        '$translate',
        '$cookieStore',
        'messageService',
        'commonService',
        '$modalInstance',
        'historiesServices',
        'groupsServices',
        'data',
        function ($scope,
            $rootScope,
            $timeout,
            $translate,
            $cookieStore,
            messageService,
            commonService,
            $modalInstance,
            historiesServices,
            groupsServices,
            data) {
            commonService.progress.hide();
            // to mark whether has ajax error occurs.
            $scope.hasError = false;

            // show the first section as default.
            $scope.iStage = 1;

            // deep copy
            $scope.data = JSON.parse(JSON.stringify(data));

            /**
             * Total have 3 pages.
             * - 1 -> social list
             * - 2 -> social members
             * - 3 -> select members to create new social
             */
            $scope.switchTo = function (stage) {
                $scope.iStage = stage || 1;
            };

            /**
             * share the patient to special social and closed the modal.
             */
            $scope.ok = function () {
                // make sure the doctor social was selected.
                if ($scope.social) {
                    sharePatient($scope.social.id, $scope.data.patient.id);

                    // clear to avoid duplicat share by mistake.
                    $scope.social = null;
                }
            };

            /**
             * closed the modal
             */
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            /**
             * create doctor social first and then share the patient to the social
             */
            $scope.createAndShare = function () {
                if ($scope.doctors && $scope.doctors.length != 0) {
                    var arr = [];
                    $scope.doctors.forEach(function (v) {
                        arr.push(v.id);
                    });
                    groupsServices.add(arr, true).then(function (result) {
                        if (result && result.data) {
                            sharePatient(result.data.id, $scope.data.patient.id);
                        }
                    }, function (err) {
                        $scope.errorText = err.text;
                        $scope.hasError = true;
                    });
                    // reset array to avoid duplicated add.
                    $scope.doctors = [];
                }                
            };

            /*--------events------*/
            /**
             * Total have 3 pages.
             * - 1 -> social list
             * - 2 -> social members
             * - 3 -> select members to create new social
             */
            $scope.$on('share.tab.changed', function (event, data) {
                $scope.iStage = data.iStage || 1;

                // show all memebers of special social.
                if (data.iStage === 2) {
                    $scope.tempSocial = data.social;
                    $rootScope.$broadcast('share.social.members', {
                        members: data.social.members
                    });
                }
            });

            /**
             * in social list panel, once social was selected, the event
             * will be fired.
             */
            $scope.$on('social.selected', function (event, data) {
                $scope.hasError = false;
                $scope.social = data.social;
            });

            /**
             * in select doctor page, once doctor was selected, the event will be
             * fired.
             */
            $scope.$on('share.doctor.selected', function (event, data) {
                $scope.hasError = false;
                $scope.doctors = data.doctors;
                console.log($scope.doctors);
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });
            /*--------end------*/

            /*--------private------*/
            function init() {
            }

            /**
             * share speical patient to a doctor social.
             */
            function sharePatient(socialId, patientId) {
                // share a patient to special doctor social
                groupsServices.sendMessage(socialId, {
                    patients: [
                        {
                            id: patientId
                        }
                    ]
                }, true).then(function (resp) {
                    commonService.resource.getValues(['SHARE_SUCCESS', 'TEXT_INFO']).then(function (texts) {
                        messageService.success(texts['TEXT_INFO'], texts['SHARE_SUCCESS']);
                    });
                    $modalInstance.close();
                }, function (err) {
                    $scope.errorText = err.text;
                    $scope.hasError = true;
                });
            }
            /*--------end------*/

            // init
            init();
        }]);
})();