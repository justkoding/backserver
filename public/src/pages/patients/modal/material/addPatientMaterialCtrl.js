﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('AddPatientMaterialCtrl', ['$scope',
        '$rootScope',
        '$translate',
        'loadingServices',
        'userServices',
        '$modalInstance',
        '$timeout',
        'commonService',
        'patientServices',
        'messageService',
        'data',
        'healthrecordServices',
        'apiUrls',
        function ($scope,
            $rootScope,
            $translate,
            loadingServices,
            userServices,
            $modalInstance,
            $timeout,
            commonService,
            patientServices,
            messageService,
            data,
            healthrecordServices,
            apiUrls) {

            // hide loading bar.
            loadingServices.hide();

            // deep copy
            var data = JSON.parse(JSON.stringify(data));
            var historyId = data.historyId;
            var classification = data.classification;

            // mount to $scope.
            $scope.classification = classification;

            // array for healthrecord id, which selected from library or uploading.
            $scope.hrIds = [];

            // from library.
            $scope.healthrecords = data.healthrecords || [];
            $scope.onSelectedMaterial = function (hr) {
                hr.checked = !hr.checked;

                if (hr.checked) {
                    $scope.hrIds.push(hr.id);
                } else {
                    var index = -1;

                    $scope.hrIds.forEach(function (sf, i) {
                        if (sf == hr.id) {
                            index = i;
                        }
                    });

                    if (index != -1) {
                        $scope.hrIds.splice(index, 1);
                    }
                }
            };

            // modal.           
            $scope.ok = function () {
                $modalInstance.close({
                    files: $scope.files,
                    ids: $scope.hrIds
                });
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            /*----------tab---------*/
            $scope.files = [];
            $scope.iNow = 1;
            $scope.rate = 0.1;
            $scope.hasFiles = $scope.files.length != 0;

            $scope.tab = function (iTab) {
                $scope.iNow = iTab;
            };

            $scope.deleteFile = function (file) {
                var index = commonService.indexOfFiles(file, $scope.files);

                if (index != -1) {
                    $scope.files.splice(index, 1);
                    $scope.hasFiles = $scope.files.length != 0;
                }
            };

            $scope.toSize = commonService.toFileSize;
            /*----------end---------*/

            /*----------upload---------*/
            $scope.uploaded = false;
            $scope.uploading = false;

            var oFiles = [],
                iFileNow = 0;

            $scope.upload = function () {
                $scope.uploading = true;

                if ($scope.files && $scope.files.length != 0) {
                    iFileNow = 1;

                    var file = $scope.files[0];
                    if (!file.uploaded) {
                        uploadFile(historyId,
                            file,
                            {
                                current: iFileNow,
                                count: $scope.files.length,
                                desc: file.desc,
                                classification: classification.id
                            });
                    } else {
                        $rootScope.$broadcast('file.upload.done', {
                            current: iFileNow,
                            count: $scope.files.length
                        });
                    }
                }
            };

            function uploadFile(historyId, file, opt) {
                healthrecordServices.upload(historyId, file, opt).then(function success(data) {
                    if (file.current === opt.count) {
                        $scope.uploading = false;
                        $scope.uploaded = true;
                    }

                    file.uploaded = true;
                    if ($scope.hrIds.indexOf(data.results.healthRecordId) == -1) {
                        $scope.hrIds.push(data.results.healthRecordId);
                    }

                    $rootScope.$broadcast('file.upload.done', {
                        current: opt.current,
                        count: opt.count
                    });
                }, function error(err) {
                    //++iFileNow;
                }, function progress(p) {
                    file.rate = p.value;
                });
            }
            /*----------end---------*/


            /*----------events------------*/
            $scope.$watch('files', function (newVal, oldVal) {
                // merge newval and oldval.
                if (newVal != null && !commonService.equalArray(newVal, oldVal)) {
                    $scope.files = commonService.mergeFiles(newVal, oldVal);

                    // set file comments.
                    //updateFileComments();

                    $scope.uploaded = false;
                    $scope.uploading = false;
                }

                $scope.hasFiles = $scope.files != null && $scope.files.length != 0;
            });

            $scope.$on('file.upload.done', function (event, data) {
                if (data.current < data.count) {
                    iFileNow = data.current + 1;

                    var file = $scope.files[data.current];
                    // avoid duplicate upload
                    if (!file.uploaded) {                       
                        uploadFile(historyId,
                            file,
                            {
                                current: iFileNow,
                                count: $scope.files.length,
                                desc: file.desc,
                                classification: classification.id
                            });

                    } 
                } else {
                    $scope.uploading = false;
                    $scope.uploaded = true;
                }
            });

            /*------------end------------*/

            /*-----------private method-----------*/
            function updateFileComments() {
                var getName = function (s) {
                    var index = s.indexOf('.');

                    if (index != -1) {
                        return s.substring(0, index);
                    }

                    return s;
                };
                if ($scope.files && $scope.files.length != 0) {
                    $scope.files.forEach(function (f) {
                        if (!f.desc) {
                            f.desc = getName(f.name);
                        }
                    });
                }
            }
            /*--------------end-------------------*/
        }]);
})();