﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('TransformCtrl', ['$scope',
        '$timeout',
        '$translate',
        '$cookieStore',
        'messageService',
        'commonService',
        '$modalInstance',
        'historiesServices',
        'data',
        function ($scope,
            $timeout,
            $translate,
            $cookieStore,
            messageService,
            commonService,
            $modalInstance,
            historiesServices,
            data) {
            commonService.progress.hide();

            // transform object.
            $scope.transform = {
                doctor: null,
                description: ''
            };

            // to mark ajax error.
            $scope.hasError = false;

            // deep copy
            $scope.data = JSON.parse(JSON.stringify(data));

            // control whether show selected doctor panel.
            $scope.isSelectedDoctor = false;

            // parse parma from data.
            var historyId = $scope.data.historyId;

            $scope.content = $scope.data.decision || null;
            $scope.isEdit = $scope.data.decision != null;

            // modal.
            $scope.ok = function () {
                historiesServices.transform(historyId, {
                    description: $scope.transform.description,
                    doctor: {
                        id: $scope.transform.doctor.id
                    }
                }, true).then(function () {
                    $modalInstance.close();
                }, function (err) {
                    $scope.errorText = err.errorText;
                    $scope.hasError = true;
                });
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            // show selected doctor panel.
            $scope.selectDoctor = function () {
                $scope.isSelectedDoctor = true;
                $scope.hasError = false;
            };

            // description
            $scope.contentChanged = function () {
                $scope.hasError = false;
            };

            /*--------events------*/
            $scope.$on('make.decision.done', function () {
                $modalInstance.close();
            });

            // monitor this event, which will be send when user click 'back'
            // and 'ok' button in select doctor section.
            $scope.$on('back.transform', function (event, result) {
                // hide the selected doctor panel.
                $scope.isSelectedDoctor = false;

                if (!(result.cancel)) {
                    $scope.transform.doctor = result.doctor;
                }
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });
            /*--------end------*/

            /*--------private------*/
            function init() {
            }
            /*--------end------*/

            // init
            init();
        }]);
})();