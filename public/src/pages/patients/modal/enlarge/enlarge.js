(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('EnlargeCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$state',
        '$stateParams',
        '$timeout',
        'commonService',
        'data',
        '$modalInstance',
        function ($scope,
                  $rootScope,
                  $cookieStore,
                  $state,
                  $stateParams,
                  $timeout,
                  commonService,
                  data,
                  $modalInstance) {

            $scope.data = JSON.parse(JSON.stringify(data));
            $scope.suggestion = $scope.data.enlargeContent;

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

        }]);
})();