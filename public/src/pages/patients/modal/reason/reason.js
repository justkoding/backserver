﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ReasonForMeetingOrOfflineCtrl', ['$scope',
        '$timeout',
        '$translate',
        '$cookieStore',
        'loadingServices',
        'historiesServices',
        'commonService',
        '$modalInstance',
        'data',
        function ($scope,
            $timeout,
            $translate,
            $cookieStore,
            loadingServices,
            historiesServices,
            commonService,
            $modalInstance,
            data) {

            loadingServices.hide();

            $scope.hasError = false;
            $scope.errorText = 'UNKNOWN_FAILED';

            // deep copy
            $scope.data = JSON.parse(JSON.stringify(data));
            $scope.historyId = $scope.data.historyId;

            /**
             * status
             * 1-> pending
             * 2-> discussion
             * 3->meeting
             * 4->completed
             */ 
            $scope.historyStatus = $scope.data.status;

            // parse parma from data.
            // to mark whether it is a request meeting modal or not.
            $scope.reason = {              
                content: '' // reason
            };           

            // modal.
            $scope.ok = function () {
                var data = JSON.parse(JSON.stringify($scope.reason));

                if ($scope.data.isMeeting) {
                    historiesServices.requestMeeting($scope.historyId, data);
                } else {
                    historiesServices.requestOffline($scope.historyId, data);
                } 
                //$modalInstance.close(data);
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            // textarea
            $scope.onChange = function () {
                if ($scope.hasError) {
                    $scope.hasError = false;
                }
            };

            /*--------events------*/
            $scope.$on('request.offline.done', function (event, resp) {
                $modalInstance.close(resp);
            });
            $scope.$on('request.offline.failed', function (event, resp) {
                $scope.errorText = resp.errorText;
                $scope.hasError = true;
            });

            $scope.$on('request.meeting.done', function (event, resp) {
                $modalInstance.close(resp);
            });
            $scope.$on('request.meeting.failed', function (event, resp) {
                $scope.errorText = resp.errorText;
                $scope.hasError = true;
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });
            /*--------end------*/

            /*--------private------*/
            function init() {
            }
            /*--------end------*/

            // init
            init();
        }]);
})();