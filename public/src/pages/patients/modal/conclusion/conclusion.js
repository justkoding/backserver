﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ConclusionCtrl', ['$scope',
        '$timeout',
        '$translate',
        '$cookieStore',       
        'messageService',       
        'commonService',
        '$modalInstance',
        'historiesServices',
        'data',      
        function ($scope,
            $timeout,
            $translate,
            $cookieStore,          
            messageService,           
            commonService,
            $modalInstance,
            historiesServices,
            data) {
            commonService.progress.hide();
          
            // deep copy
            $scope.data = JSON.parse(JSON.stringify(data));                      

            // parse parma from data.
            var historyId = $scope.data.historyId;

            $scope.content = $scope.data.decision || null;
            $scope.isEdit = $scope.data.decision != null;
                    
            // modal.
            $scope.ok = function () {                
                var data = JSON.parse(JSON.stringify($scope.content));

                // call service to add/edit announcement to server.
                if ($scope.isEdit) {
                    // todo
                    alert('todo');
                    //delete data.focus;
                    //announcementsServices.update(data);
                } else {
                    historiesServices.makeDecision(historyId, {
                        content: data
                    });
                }                
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');              
            };

            /*--------events------*/
            $scope.$on('make.decision.done', function () {
                $modalInstance.close();
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope.data = null;
            });
            /*--------end------*/

            /*--------private------*/
            function init() {               
            } 
            /*--------end------*/

            // init
            init();
        }]);
})();