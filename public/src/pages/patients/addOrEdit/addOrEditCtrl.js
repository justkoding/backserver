﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('AddorEditPatientCtrl', ['$scope',
        '$rootScope',
        '$translate',
        'userServices',
        '$modalInstance',
        '$timeout',
        'strings',
        'commonService',       
        'patientServices',
        'messageService',
        'historiesServices',
        'data',
        function ($scope,
            $rootScope,
            $translate,
            userServices,
            $modalInstance,
            $timeout,
            strings,
            commonService,         
            patientServices,
            messageService,
            historiesServices,
            data) {

            // hide loading bar
            commonService.progress.hide();
            
            // deep copy
            var data = JSON.parse(JSON.stringify(data));
            // default value
            $scope.patient = data.patient || {};
            if($scope.patient.id){
            	historiesServices.list($scope.patient.id).then(function(result){
            		if (result && result.length > 0){
            			historiesServices.detail(result[0].id).then(function(resp){
            				if(resp){
            					$scope.patient.status = resp.status;
            					$scope.patient.doctor = $scope.doctorList[0];
            					if(resp.status != 5){
            						$scope.patient.historyid = resp.id;
            						$scope.patient.bed = resp.bed;
            						$scope.patient.reason = resp.description;
            						if(resp.doctor){
            							$scope.patient.doctor = {'id':resp.doctor.id, 'name':resp.doctor.name};
            						}
            					}
            				}
            			},function (err) {
            				commonService.resource.getValues(['MESSAGE_PATIENT_UPDATE_DONE_TITLE','MESSAGE_BODY_FAILED']).then(function (sources) {
                            	messageService.error(sources['MESSAGE_PATIENT_UPDATE_DONE_TITLE'],sources['MESSAGE_BODY_FAILED']);
                            	$modalInstance.dismiss('cancel');
                            });
                        });
            		}else{
            			commonService.resource.getValues(['MESSAGE_PATIENT_UPDATE_DONE_TITLE','MESSAGE_BODY_FAILED']).then(function (sources) {
            				messageService.error(sources['MESSAGE_PATIENT_UPDATE_DONE_TITLE'],sources['MESSAGE_BODY_FAILED']);
                        	$modalInstance.dismiss('cancel');
                        });
            		}
            	},function (err) {
            		commonService.resource.getValues(['MESSAGE_PATIENT_UPDATE_DONE_TITLE','MESSAGE_BODY_FAILED']).then(function (sources) {
            			messageService.error(sources['MESSAGE_PATIENT_UPDATE_DONE_TITLE'],sources['MESSAGE_BODY_FAILED']);
                    	$modalInstance.dismiss('cancel');
                    });
                });
            }
            var params = {
                'size': 50,
                title: strings.doctorTitles
            };
            
            var updateRequestList = {};
            
            var originPatient = JSON.parse(JSON.stringify($scope.patient));
            
            var currentDate = new Date();
            
            function init(){
                
                $scope.isEdit = data.isEdit;
                $scope.fromMdt = data.fromMdt;
                
                
                $scope.doctorList = [];
                
                $scope.isShowExtrPanel = false;
                
                $scope.patient.sex = $scope.patient.sex || 1; // 1 - man
                
                $scope.opened = false;
                $scope.existingPatient = {};
                $scope.patientExist = false;
                
                userServices.list(params,true).then(function(result){
                	 var data = [];

                     if (result && result.data && result.data.length != 0) {
                         result.data.forEach(function (item) {
                             data.push({'id':item.id,'name':item.name});
                         });
                     }
                     commonService.resource.getValues('PATIENT_DETAIL_SELECT_DOCTOR').then(function (results) {
                         commonService.progress.hide();
                         data.unshift({'id':-1,'name':results['PATIENT_DETAIL_SELECT_DOCTOR']});
                         $scope.doctorList = data;

                         var index = -1;
                         // support edit patient.
                         if ($scope.patient.id) {
                             index = commonService.getIndexOf($scope.patient, $scope.doctorList);
                         }
                         // set first one as default if didn't find the patient.
                         index = index != -1 ? index : 0;
                         $scope.patient.doctor = $scope.doctorList[index];
                     }); 
                });
            }
            
            init();
            
            $scope.open = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.opened = !$scope.opened;
            };

            // modal.
            $scope.ok = function () {

                var p = JSON.parse(JSON.stringify($scope.patient));

                // delete custom filed in post data to avoid parse error in server.
                delete p.focus;
                delete p.opened;
                delete p.bed;
                delete p.reason;
                delete p.doctor;
                delete p.gender;

                //commonService.progress.show();
                if ($scope.isEdit) {
                    updateRequestList = ajaxReq();
                    if(updateRequestList.patientHistory && !$scope.patient.historyid){
                        addHistory();
                    }
                    if(updateRequestList.patientHistory && $scope.patient.historyid){
                        updateHistory();
                    }
                    if(updateRequestList.patientBasic){
                    	delete p.historyid;
                    	patientServices.update(p);
                    }
                    if(!updateRequestList.patientHistory && !updateRequestList.patientBasic){
                    	$modalInstance.close();
                    }
                } else {
                    patientServices.add(p);
                }
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
            
            /*------check patient-----*/
            $scope.idnumChanged = function () {
                if($scope.addOrEditPatient['idnum'].$valid){
                	/*var birthday;
                	if($scope.patient.idnum.length == 18){
                		birthday = $scope.patient.idnum.substr(6,8);
                	}else if($scope.patient.idnum.length == 15){
                		birthday = $scope.patient.idnum.substr(6,6);
                		birthday = '19' + birthday;
                	}
                	birthday = birthday.split('');
                	birthday.splice(6,0,'-');
                	birthday.splice(4,0,'-');
                	birthday = birthday.toString().replace(/,/g,'');
                	birthday = new Date(birthday);
                	$scope.patient.age = Math.round((currentDate - birthday)/(24*3600*1000*365));*/
                	
                    commonService.progress.show();
                    patientServices.checkPatient($scope.patient.idnum);
                }
            };

            $scope.loadPatient = function (p) {
                $scope.patient = JSON.parse(JSON.stringify(p));
                $scope.isEdit = true;
            };
            /*-----------------------end-----------------------*/


            /*----------events------------*/
            /*--------events------*/
            /*$scope.$on('user.list.done', function (event, result) {
                var data = [];

                if (result && result.data && result.data.length != 0) {
                    result.data.forEach(function (item) {
                        data.push({'id':item.id,'name':item.name});
                    });
                }
                commonService.resource.getValues('PATIENT_DETAIL_SELECT_DOCTOR').then(function (results) {
                    commonService.progress.hide();
                    data.unshift({'id':-1,'name':results['PATIENT_DETAIL_SELECT_DOCTOR']});
                    $scope.doctorList = data;
                    $scope.patient.doctor = $scope.doctorList[0];
                }); 
            });*/
            
            
            $scope.$on('patient.add.done', function (event, results) {
                if(results.result == 'OK'){
                	$scope.patient.id = results.ptid;
                    addHistory();
                }else{
                	commonService.progress.hide();
                	$modalInstance.close($scope.patient);
                }
            });

            $scope.$on('patient.update.done', function (event, results) {
                commonService.progress.hide();
                $modalInstance.close($scope.patient);
            });

            $scope.$on('patient.exist.done', function (event, results) {
                commonService.progress.hide();
                $scope.existingPatient = results.data;
                $scope.patientExist = true;
                originPatient = JSON.parse(JSON.stringify($scope.existingPatient));
                historiesServices.list($scope.existingPatient.id).then(function(result){
            		if (result && result.length > 0){
            			historiesServices.detail(result[0].id).then(function(resp){
            				if(resp){
            					$scope.existingPatient.status = resp.status;
            					$scope.existingPatient.doctor = $scope.doctorList[0];
            					if(resp.status != 5){
            						$scope.existingPatient.historyid = resp.id;
            						$scope.existingPatient.bed = resp.bed;
            						$scope.existingPatient.reason = resp.description;
            						if(resp.doctor){
            							$scope.existingPatient.doctor = {'id':resp.doctor.id, 'name':resp.doctor.name};
            						}
            					}
            					originPatient = JSON.parse(JSON.stringify($scope.existingPatient));
            				}
            			},function (err) {
                            defer.reject(err);
                        });
            		}
            	},function (err) {
                    defer.reject(err);
                });
            });
            
            $scope.$on('history.add.done', function (event, results) {
                commonService.progress.hide();
                if(results.result == 'ERROR_STATUS'){
                    messageService.error('Add Patient', 'Current treatment information of patient already exists');
                }
                $modalInstance.close($scope.patient);
            });
            
            $scope.$on('patient.add.failed', function (event, results) {
                commonService.progress.hide();
                commonService.resource.getValues(['MESSAGE_PATIENT_ADD_DONE_TITLE','MESSAGE_BODY_FAILED']).then(function (sources) {
                	messageService.error(sources['MESSAGE_PATIENT_ADD_DONE_TITLE'],sources['MESSAGE_BODY_FAILED']);
                });
//                $modalInstance.close($scope.patient);
            });
            
            $scope.$on('history.update.done', function (event, results) {
                commonService.progress.hide();
                $modalInstance.close($scope.patient);
            });
            
            $scope.$on('history.update.failed', function (event, results) {
                commonService.progress.hide();
//                $modalInstance.close($scope.patient);
            });

            $scope.$on('patient.update.failed', function (event, results) {
                commonService.progress.hide();
//                $modalInstance.close($scope.patient);
            });

            $scope.$on('patient.exist.failed', function (event, results) {
                commonService.progress.hide();
                $scope.existingPatient = {};
                $scope.patientExist = false;
            });

            /*------------end------------*/

            /*-----------private method-----------*/
            function addHistory(){
                var history = {'bed':$scope.patient.bed, 'description':$scope.patient.reason};
                if('' != $scope.patient.doctor && 
                        null != $scope.patient.doctor && 
                        typeof $scope.patient.doctor != 'undefined'){
                    history['doctor'] = {'id':$scope.patient.doctor.id};
                    historiesServices.add($scope.patient.id,history);
                }
            }
            
            function updateHistory(){
                var id = $scope.patient && $scope.patient.historyid ? $scope.patient.historyid : null;
                if(id){
                    var history = {'id':$scope.patient.historyid, 'bed':$scope.patient.bed, 'description':$scope.patient.reason};
                    if('' != $scope.patient.doctor && 
                            null != $scope.patient.doctor && 
                            typeof $scope.patient.doctor != 'undefined'){
                        history['doctor'] = {'id':$scope.patient.doctor.id};
                        historiesServices.update(history);
                    }
                }
            }
            //compare the current value with initial value and ajax request flg. 
            function ajaxReq(){
                
                var ajaxList = {'patientBasic':false,'patientHistory':false};
                
                if($scope.addOrEditPatient['name'].$dirty
                        || originPatient.sex != $scope.patient.sex
                        || $scope.addOrEditPatient['mobile'].$dirty){
                    ajaxList.patientBasic = true;
                }
                if($scope.addOrEditPatient['doctor'].$dirty
                        || $scope.addOrEditPatient['reason'].$dirty
                        || $scope.addOrEditPatient['bed'].$dirty){
                    ajaxList.patientHistory = true;
                }
                
                return ajaxList;
            }
            /*--------------end-------------------*/
        }]);
})();