﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('AnonymousCtrl', ['$scope',
        '$rootScope',
        '$cookieStore',
        '$timeout',
        '$stateParams',
        '$window',
        'loadingServices',
        'mdtServices',
        'clientServices',
        function ($scope,
            $rootScope,
            $cookieStore,
            $timeout,
            $stateParams,
            $window,
            loadingServices,
            mdtServices,
            clientServices) {

            $scope.validFailed = false;
            $scope.meeting = '';
            var id = '',
                originalName = '',
                timer = null,
                mdtid = '',
                options = {
                };

            // user change the nickname
            $scope.changeName = function (form) {
                $timeout.cancel(timer);

                if (form.$valid) {
                    timer = $timeout(function () {
                        loadingServices.show();

                        // update 'nickname' filed for anonymous user.
                        mdtServices.updateTempAttendee({
                            id: id
                        }, {
                            name: $scope.tempAttendee.name
                        });
                    }, 600);
                }
            };

            // launch pc client to join meeting.
            $scope.joinMeeting = function () {
                //todo
                loadingServices.show();
                clientServices.meeting(options.token, mdtid);
            };

            //close the page
            $scope.closeThePage = function () {
                $window.open('', '_self');
                $window.close();
            };

            init();
            /*----------events------------*/
            $scope.$on('mdt.anonymous.access.done', function (event, data) {
                $scope.tempData = data.data;

                /*
                 status = 1;//待开始
                 status = 2;//进行中
                 status = 3;//已完成
                 status = 4;//已取消
                 */
                if ($scope.tempData.status == 3) {
                    $scope.failedReason = 'meetingOver';
                } else if ($scope.tempData.status == 4) {
                    $scope.failedReason = 'meetingCancel';
                } else {
                    $scope.tempAttendee = $scope.tempData.tempAttendee;
                    id = $scope.tempAttendee.id;
                    originalName = $scope.tempAttendee.name;
                    mdtid = $scope.tempData.mdtid;
                }

                loadingServices.hide();
            });

            $scope.$on('mdt.anonymous.access.failed', function (event, results) {
                // failed to get the mdt information
                $scope.tempData = null;
                $scope.failedReason = 'contentGettingFailed';
                loadingServices.hide();
            });

            $scope.$on('mdt.join.done', function (event, path) {
                $scope.meeting = path;

                $timeout(function () {
                    document.getElementById('meeting').click();
                    loadingServices.hide();
                }, 0);
            });

            $scope.$on('tempAttendees.update.done', function (event, results) {
                loadingServices.hide();
            });
            /*--------end-----------*/

            /*----------private method-------------*/
            function init() {

                // show loading icon
                loadingServices.show();

                //token is null
                options.token = $stateParams.token;

                if (options.token && options.token.length == 32) {
                    mdtServices.getTempattendees(options);
                } else {
                    //illegal path
                    $scope.failedReason = 'illegalPath';
                    loadingServices.hide();
                }
            }
            /*--------end-----------*/
        }]);
})();