﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ConfirmCtrl', ['$scope',
        '$translate',        
        '$modalInstance',
        'loadingServices',
        'data',
        function ($scope,
            $translate,            
            $modalInstance,
            loadingServices,
            data) {

            loadingServices.hide();

            $scope.content = {
                title: '',
                body: ''
            };
          
            $scope.ok = function () {
                $modalInstance.close();
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            $translate([data.title, data.body]).then(function (texts) {
                $scope.content.title = texts[data.title];
                $scope.content.body = texts[data.body];
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope = null;
            });
            /*--------end------*/
        }]);
})();