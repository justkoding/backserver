﻿(function () {
    'use strict';

    angular.module('app').controller('PageController', ['$scope', '$rootScope',
        function ($scope, $rootScope) {
            // scroll.
            $rootScope.needScroll = false;

            /*---------event--------------*/
            $scope.$on('scroll.hit.bottom', function () {
                //alert('scroll.hit.bottom');
            });
            $scope.$on('scroll.hit.top', function () {
                //alert('scroll.hit.top');
            });

            // gotoTop diretive, will be called once scroll is done.
            $scope.$on('scroll.done', function () {
                $rootScope.needScroll = false;
            });

            // happen when try to scroll.
            $scope.$on('scroll.progress', function (event, data) {
                var b = data.scrollTop >= 100;

                if (b && !$rootScope.needScroll) {
                    $rootScope.needScroll = b;
                    $rootScope.scrollTop = data.scrollTop;
                    $scope.$digest();
                } else if (!b && $rootScope.needScroll) {
                    $rootScope.needScroll = b;
                    $rootScope.scrollTop = data.scrollTop;
                    $scope.$digest();
                }
            });

            $scope.$on('$destroy', function () {
                // clean up.
            });
            /*---------end--------------*/
        }]);
}());
