﻿
(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('DetailRoleCtrl', ['$scope',
        '$rootScope',
        '$state',
        '$stateParams',
        'loadingServices',
        'strings',
        'modalService',
        'rolesServices',
        'systemMiscServices',
        'commonService',
        function ($scope,
            $rootScope,
            $state,
            $stateParams,
            loadingServices,
            strings,
            modalService,
            rolesServices,
            systemMiscServices,
            commonService) {

            // get current user Id.
            var roleId = $stateParams.id;

            $scope.isDetail = true;

            $scope.back = function () {
                $state.go('page.role', null, {'reload': true});
            };

            $scope.edit = function () {
                $state.go('page.role.edit', { id: roleId });
            };

            loadingServices.show();
            rolesServices.detail(roleId);

            /*--------event------*/
            $scope.$on('role.detail.done', function (event, data) {
                $scope.role = data;

                // get permission detail by id array.
                rolesServices.permissions(data.permissions)
            });

            $scope.$on('permission.filter.list.done', function (event, data) {
                loadingServices.hide();
                $scope.permissions = data;
            });

            $scope.$on('$destroy', function () {
                // clean up.
            });
            /*--------end------*/
        }]);
})();