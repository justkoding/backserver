﻿
(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('ListRoleCtrl', ['$scope',
        '$rootScope',
        '$state',
        'loadingServices',
        'commonService',
        'strings',
        'modalService',
        'rolesServices',
        'ajaxMessageService',
        'messageService',
        function ($scope,
            $rootScope,
            $state,
            loadingServices,
            commonService,
            strings,
            modalService,
            rolesServices,
            ajaxMessageService,
            messageService) {

            // show table if users array is not empty
            // else show default placeholder.
            $scope.hasData = false;
            $scope.detail = function (role) {
                $state.go('page.role.detail', { id: role.id });
            };

            $scope.addRole = function () {
                $state.go('page.role.add', null, { 'reload': true });
            };

            loadingServices.show();
            rolesServices.list();

            /*--------event------*/
            $scope.$on('role.list.done', function (event, result) {
                loadingServices.hide();
                $scope.roles = result;
                $scope.hasData = result && result.length != 0;
            });

            $scope.$on('$destroy', function () {
                // clean up.
                $scope = null;
            });
            /*--------end------*/
        }]);
})();