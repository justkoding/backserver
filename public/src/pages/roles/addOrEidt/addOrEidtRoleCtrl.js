﻿
(function () {

    'use strict';

    var app = angular.module('app');
    app.controller('AddOrEidtRoleCtrl', ['$scope',
        '$rootScope',
        '$state',
        '$stateParams',
        '$translate',
        'loadingServices',
        'strings',
        'modalService',
        'rolesServices',
        'systemMiscServices',
        'commonService',
        'messageService',
        function ($scope,
            $rootScope,
            $state,
            $stateParams,
            $translate,
            loadingServices,
            strings,
            modalService,
            rolesServices,
            systemMiscServices,
            commonService,
            messageService) {

            $scope.role = {};

            $scope.back = function () {
                if ($scope.role.id) {
                    $state.go('page.role.detail', { id: $scope.role.id });
                } else {
                    $state.go('page.role', null, {'reload': true});
                }
            };

            $scope.cancel = function () {
                $state.go('page.role', null, {'reload': true});
            };

            $scope.save = function (form) {
                var role = JSON.parse(JSON.stringify($scope.role));

                loadingServices.show();

                role.permissions = rolesServices.getCheckedPermissionIds($scope.permissions);

                // update existing role if id is not null.
                if (role.id) {
                    rolesServices.update(role);
                } else {
                    rolesServices.add(role);
                }
            };

            $scope.delete = function () {
                loadingServices.show();
                modalService.showModal('/assets/src/pages/confirm/confirm.html',
                   {
                       controller: 'ConfirmCtrl',
                       data: {
                           title: 'USER_ROLE_DELETE_CONFIRM_TITLE',
                           body: 'USER_ROLE_DELETE_CONFIRM_BODY'
                       },
                       closefn: deleteRoleCallback
                   });
            };

            $scope.togglePermission = function (p) {
                p.checked = !p.checked;
            };

            init();

            /*--------private method------*/
            function init() {
               
                $scope.role = {};

                // get current user Id.
                var roleId = $stateParams.id;

                loadingServices.show();
                if (roleId) {
                    rolesServices.detail(roleId);
                } else {
                    rolesServices.permissions([])
                }
            }

            function deleteRoleCallback() {
                loadingServices.show();
                rolesServices.remove($scope.role.id);
            }
            /*--------end------*/

            /*--------event------*/
            $scope.$on('role.detail.done', function (event, data) {
                $scope.role = data;

                // get permission detail by id array.
                rolesServices.permissions(data.permissions)
            });

            $scope.$on('role.update.done', function (event, data) {
                loadingServices.hide();               
                $state.go('page.role', null, {'reload': true});
            });

            $scope.$on('role.add.done', function (event, data) {
                loadingServices.hide();               
                $state.go('page.role', null, {'reload': true});
            });

            $scope.$on('role.remove.done', function (event, data) {
                loadingServices.hide(); 
                $state.go('page.role', null, {'reload': true});
            });

            $scope.$on('role.remove.failed', function (event, data) {
                loadingServices.hide();                
                $state.go('page.role', null, { 'reload': true });
            });

            $scope.$on('permission.filter.list.done', function (event, data) {
                loadingServices.hide();
                $scope.permissions = data;
            });

            $scope.$on('$destroy', function () {
                // clean up.
            });
            /*--------end------*/
        }]);
})();