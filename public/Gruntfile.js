module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            mdt: {
                files: {
                    'build/debug/css/lib.css': [
                        'node_modules/bootstrap/dist/css/bootstrap.css',
                        'node_modules/bootstrap/dist/css/bootstrap-theme.css',
                        'node_modules/angularjs-toaster/toaster.css',
                        'node_modules/angularjs-toaster/toaster.css',
                        'node_modules/angular-loading-bar/build/loading-bar.css',
                        'node_modules/font-awesome/css/font-awesome.css'                       
                    ],
                    'build/debug/css/site.css': [
                        'src/css/*.css',
                        'src/pages/**/*.css'
                    ],
                    'build/debug/js/lib.js': [
                        'node_modules/jquery/dist/jquery.js',
                        'node_modules/angular/angular.js',
                        'node_modules/angular-ui-router/build/angular-ui-router.js',
                        'node_modules/angular-animate/angular-animate.js',
                        'node_modules/angular-sanitize/angular-sanitize.js',                       
                        'node_modules/angularjs-toaster/toaster.js',
                        'node_modules/ngstorage/ngStorage.js',
                        'node_modules/angular-moment/node_modules/moment/min/moment-with-locales.js',
                        'node_modules/angular-moment/angular-moment.js',
                        'node_modules/angular-bootstrap/ui-bootstrap-tpls.js',                       
                        'node_modules/angular-cookies/angular-cookies.js',
                        'node_modules/ng-file-upload/dist/ng-file-upload-all.js',
                        'node_modules/valdr/valdr.js',
                        'node_modules/valdr/valdr-message.js'
                    ],
                    'build/debug/js/translate.js': [
                        'node_modules/angular-translate/dist/angular-translate.js',
                        'node_modules/angular-translate/dist/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
                        'node_modules/angular-translate/dist/angular-translate-storage-local/angular-translate-storage-local.js',
                        'node_modules/angular-translate/dist/angular-translate-storage-cookie/angular-translate-storage-cookie.js'
                    ],
                    'build/debug/js/<%= pkg.name %>.js': [
                        'src/js/libs/**/*.js',
                        'src/js/setup/**/*.js',
                        'src/js/decorator/**/*.js',
                        'src/js/templates/**/*.js',
                        'src/js/constants/**/*.js',
                        'src/js/repositories/**/*.js',
                        'src/js/services/**/*.js',
                        'src/js/filters/**/*.js',
                        'src/js/directives/**/*.js',
                        'src/pages/**/*.js',
                        'src/js/app.js'
                    ]
                }
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            mdt: {
                files: {
                    'build/release/js/translate.min.js': ['build/debug/js/translate.js'],
                    'build/release/js/lib.min.js': ['build/debug/js/lib.js'],
                    'build/release/js/<%= pkg.name %>.min.js': ['build/debug/js/<%= pkg.name %>.js']
                }
            }
        },
        copy: {
            mdt: {
                files: [
                    {
                        expand: true,
                        src: ['node_modules/bootstrap/fonts/*', 'node_modules/font-awesome/fonts/*'],
                        dest: 'build/debug/fonts',
                        flatten: true,
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        src: ['node_modules/bootstrap/fonts/*', 'node_modules/font-awesome/fonts/*'],
                        dest: 'build/release/fonts',
                        flatten: true,
                        filter: 'isFile'
                    }
                ]
            }
        },
        cssmin: {
            mdt: {
                files: [
                    {
                        src: ['build/debug/css/site.css'],
                        dest: 'build/release/css/site.min.css'
                    },
                    {
                        src: ['build/debug/css/lib.css'],
                        dest: 'build/release/css/lib.min.css'
                    }
                ]
            }
        },
        ngtemplates: {
            mdt: {
                options: { 
                    module: 'mdtTemplates'
                },
                src: 'src/templates/**/*.html',
                dest: 'src/js/templates/templates.js'
            }
        },

        watch: {
            templates: {
                files: ['src/templates/**/*.html'],
                tasks: ['ngtemplates', 'concat', 'uglify']
            },
            scripts: {
                files: ['src/**/*.js'],
                tasks: ['concat']
            },
            css: {
                files: ['src/**/*.css'],
                tasks: ['concat', 'cssmin']
            }
        }
    });

    // 加载包含 "uglify", "concat", 'copy', 'cssmin' 任务的插件。
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // 默认被执行的任务列表。
    grunt.registerTask('default', ['concat']);
    grunt.registerTask('cw', ['concat', 'watch']);
    grunt.registerTask('cu', ['concat', 'uglify']);
    grunt.registerTask('fonts', ['copy']);
    grunt.registerTask('template', ['ngtemplates']);
    grunt.registerTask('css', ['cssmin']);
    grunt.registerTask('release', ['concat', 'uglify', 'copy', 'cssmin']);
};
