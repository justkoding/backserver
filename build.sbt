name := "BackServer"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaJpa.exclude("org.hibernate.javax.persistence", "hibernate-jpa-2.0-api"),
  cache,
  "org.hibernate" % "hibernate-entitymanager" % "4.3.8.Final",
  "mysql" % "mysql-connector-java" % "5.1.34",
  "com.typesafe.akka" % "akka-remote_2.10" % "2.2.3",
  "com.typesafe" %% "play-plugins-mailer" % "2.2.0",
  "be.objectify" %% "deadbolt-java" % "2.2.1-RC2",
  "org.apache.pdfbox" % "pdfbox" % "1.8.9",
  "org.bouncycastle" % "bcprov-jdk15" % "1.44",
  "org.bouncycastle" % "bcmail-jdk15" % "1.44",
  "net.coobird" % "thumbnailator" % "0.4.8",
  "org.dcm4che" % "dcm4che-core" % "3.3.5",
  "org.dcm4che" % "dcm4che-imageio" % "3.3.5",
  "commons-io" % "commons-io" % "1.3.2"
)


play.Project.playJavaSettings


Keys.fork in (Test) := false

resolvers += "DCM4CHE Repository" at "http://www.dcm4che.org/maven2"

resolvers += Resolver.url("Objectify Play Repository", url("http://deadbolt.ws/releases/"))(Resolver.ivyStylePatterns)
